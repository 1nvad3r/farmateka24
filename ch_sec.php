<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	if(CModule::IncludeModule("iblock")):
		$uf_arresult = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => 4), false, $uf_name);
		while ($arSect = $uf_arresult->GetNext())
		{
			$arTransParams = array(
				"max_len" => 100,
				"change_case" => 'L', // 'L' - toLower, 'U' - toUpper, false - do not change
				"replace_space" => '-',
				"replace_other" => '-',
				"delete_repeat_replace" => true
			);
			$transName = CUtil::translit($arSect["NAME"], "ru", $arTransParams);

			$bs = new CIBlockSection;
			$arFields = Array(
				"CODE" => $transName,
			);
			$bs->Update($arSect["ID"], $arFields);

			d($transName);

		}
	endif;