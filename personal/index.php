<?
	define("NEED_AUTH", true);
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Персональный раздел");
	$APPLICATION->AddChainItem("Профиль", "");
	global $USER;
	global $userFilter;

	$userFilter = ["PROPERTY_USER" => $USER->GetId()];

	if ($_REQUEST['update_profile'] === 'Y') {
		updateProfile();
	}
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"profile_special",
	[
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "content",
		"NEWS_COUNT" => "",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PROPERTY_CODE" => ["CATALOG_LINK"],
	],
	false
); ?>

	<section class="cabinet js_cabinet">

		<div class="cabinet__menu cabinet-menu">
			<h1 class="cabinet-menu__title">
				Профиль
			</h1>

			<ul class="cabinet-menu__list  js_cabinet_menu_list">
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link active" href="/personal/" title="">
						Профиль
					</a>
				</li>
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/favorites/" title="">
						Избранное
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/order/">
						Мои заказы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/reviews/">
						Мои отзывы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link " href="/personal/cart/">
						Корзина
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link cabinet-menu__link--exit js_btn_popup" data-btn="leave"
					   href="/?logout=yes&<?= bitrix_sessid_get() ?>">
						Выход
					</a>
				</li>
			</ul>

			<button class="cabinet-menu__btn js_cabinet_menu_open_btn">
			</button>
		</div>

		<div class="container">
			<? if ($USER->IsAuthorized()): ?>
				<?
				$arUser = CUser::GetByID($USER->GetId())->GetNext();
				?>
				<div class="cabinet__inner">
					<ul class="cabinet__tabs-btns-list">
						<li class="cabinet__tabs-btns-item">
							<button class="cabinet__tabs-btn <? if ($_REQUEST['address']): ?><? elseif ($_REQUEST['pharmacy']): ?><? else: ?>active<? endif; ?> js_cabinet_tab_btn"
									data-num="1">
								Личные данные
							</button>
						</li>

						<li class="cabinet__tabs-btns-item">
							<button class="cabinet__tabs-btn <? if ($_REQUEST['pharmacy'] === 'ok'): ?>active<? endif; ?> js_cabinet_tab_btn"
									data-num="2">
								Сохраненная аптека
							</button>
						</li>

						<li class="cabinet__tabs-btns-item">
							<button class="cabinet__tabs-btn <? if ($_REQUEST['address'] === 'ok'): ?>active<? endif; ?> js_cabinet_tab_btn"
									data-num="3">
								Сохраненный адрес
							</button>
						</li>

						<li class="cabinet__tabs-btns-item">
							<button class="cabinet__tabs-btn js_cabinet_tab_btn" data-num="4">
								Персональная скидка
							</button>
						</li>
					</ul>

					<ul class="cabinet__content-list">
						<li class="cabinet__content-item personal-data js_cabinet_tab <? if ($_REQUEST['address']): ?><? elseif ($_REQUEST['pharmacy']): ?><? else: ?>active<? endif; ?>"
							data-num="1">
							<form class="personal-data__form js_personal_data_form" action="">
                                <span class="personal-data__intro">
                                    Не забудьте нажать на кнопку “Сохранить изменения” внизу страницы
                                </span>

								<div class="personal-data__top-label-wrapper">
									<label class="personal-data__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="personal-data__clients-title required">
                                            Имя
                                        </span>

										<input class="personal-data__client-input js_personal_data_name_input"
											   type="text" name="user_name" value="<?= $arUser['NAME'] ?>">
									</label>

									<label class="personal-data__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="personal-data__clients-title">
                                            Фамилия
                                        </span>

										<input class="personal-data__client-input js_checkout_client_name_input"
											   type="text" name="user_last_name" value="<?= $arUser['LAST_NAME'] ?>">
									</label>

									<label class="personal-data__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="personal-data__clients-title">
                                            Отчество
                                        </span>

										<input class="personal-data__client-input js_checkout_client_name_input"
											   type="text" name="user_second_name"
											   value="<?= $arUser['SECOND_NAME'] ?>">
									</label>

									<label class="personal-data__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="personal-data__clients-title">
                                            Дата рождения
                                        </span>

										<!-- <input class="personal-data__client-input js_checkout_client_name_input"
											type="date"> -->
										<?
											$arBirthday = explode('.', $arUser['PERSONAL_BIRTHDAY']);
										?>
										<div class="personal-data__birthday-wrapper">
											<div
													class="personal-data__birthday-select-day my-select my-select--wrappered">
												<select class="js_my_select" name="birthday_day" id="selectDay">
													<option value="">День</option>
													<?
														for ($i = 1; $i <= 31; $i++) {
															$selected = false;
															if ($arBirthday[0] == $i) {
																$selected = 'selected';
															}
															echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
														}
													?>
												</select>
											</div>

											<div
													class="personal-data__birthday-select-month my-select my-select--wrappered">
												<select class="js_my_select" name="birthday_month" id="selectMonth">
													<option value="">Месяц</option>
													<option value="01"
															<? if ($arBirthday[1] == '01'): ?>selected<? endif; ?>>
														январь
													</option>
													<option value="02"
															<? if ($arBirthday[1] == '02'): ?>selected<? endif; ?>>
														февраль
													</option>
													<option value="03"
															<? if ($arBirthday[1] == '03'): ?>selected<? endif; ?>>март
													</option>
													<option value="04"
															<? if ($arBirthday[1] == '04'): ?>selected<? endif; ?>>
														апрель
													</option>
													<option value="05"
															<? if ($arBirthday[1] == '05'): ?>selected<? endif; ?>>май
													</option>
													<option value="06"
															<? if ($arBirthday[1] == '06'): ?>selected<? endif; ?>>июнь
													</option>
													<option value="07"
															<? if ($arBirthday[1] == '07'): ?>selected<? endif; ?>>июль
													</option>
													<option value="08"
															<? if ($arBirthday[1] == '08'): ?>selected<? endif; ?>>
														август
													</option>
													<option value="09"
															<? if ($arBirthday[1] == '09'): ?>selected<? endif; ?>>
														сентябрь
													</option>
													<option value="10"
															<? if ($arBirthday[1] == '10'): ?>selected<? endif; ?>>
														октябрь
													</option>
													<option value="11"
															<? if ($arBirthday[1] == '11'): ?>selected<? endif; ?>>
														ноябрь
													</option>
													<option value="12"
															<? if ($arBirthday[1] == '12'): ?>selected<? endif; ?>>
														декабрь
													</option>
												</select>
											</div>

											<div
													class="personal-data__birthday-select-year my-select my-select--wrappered">
												<select class="js_my_select" name="birthday_year" id="selectYear">
													<option value="">Год</option>
													<?
														for ($i = 1900; $i <= date('Y'); $i++) {
															$arYears[] = $i;
														}
														$index = count($arYears);

														while ($index) {
															$item = $arYears[--$index];
															$selected = false;
															if ($arBirthday[2] == $item) {
																$selected = 'selected';
															}
															echo '<option value="' . $item . '" ' . $selected . '>' . $item . '</option>';
														}
													?>
												</select>
											</div>
										</div>
									</label>

								</div>

								<div class="personal-data__client-label personal-data__client-label--gender" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="personal-data__clients-title required">
                                        Пол
                                    </span>
									<?
										$male_check = false;
										$female_check = false;
										if ($arUser['PERSONAL_GENDER'] === 'M') {
											$male_check = 'Y';
										} elseif ($arUser['PERSONAL_GENDER'] === 'F') {
											$female_check = 'Y';
										}

									?>
									<div class="personal-data__gender-inputs">
										<label class="check personal-data__radio-check">
											<input class="check__input" type="radio"
												   name="user_gender" <? if ($male_check) echo 'checked'; ?> value="M">
											<span class="check__box"></span>
											Мужской
										</label>

										<label class="check personal-data__radio-check">
											<input class="check__input" type="radio"
												   name="user_gender" <? if ($female_check) echo 'checked'; ?>
												   value="F">
											<span class="check__box"></span>
											Женский
										</label>
									</div>
								</div>


								<div class="personal-data__email-and-password-wrapper">
                                    <span class="personal-data__intro">
                                        Если Вы хотите использовать Email для входа в личный кабинет, то заполните поля
                                        ниже
                                    </span>

									<div
											class="personal-data__top-label-wrapper personal-data__top-label-wrapper--email">

										<label class="personal-data__client-label" for="">
											<!-- если поле обязательно добавть класс required - добавится звездочка -->
											<span class="personal-data__clients-title ">
                                                Email
                                            </span>

											<input class="personal-data__client-input js_personal_data_email_input"
												   type="text" name="user_email" value="<?= $arUser['EMAIL'] ?>">
										</label>

										<!-- чтобы показать добавь класс active -->
										<span class="personal-data__email-warning">
                                            Данный Email использовался для регистрации, поэтому его нельзя изменить.
                                        </span>

										<label class="personal-data__client-label" for="">
											<!-- если поле обязательно добавть класс required - добавится звездочка -->
											<span class="personal-data__clients-title ">
                                                Пароль
                                            </span>

											<input class="personal-data__client-input js_personal_data_password_input"
												   type="password" name="user_password">
										</label>
									</div>

									<!-- чтобы показать добавь класс active -->
									<button class="personal-data__change-password-btn js_btn_popup"
											data-btn="update-password" type="button">
										Изменить пароль
									</button>

									<!-- чтобы показать добавь класс active -->
									<label class="check personal-data__check personal-data__check--terms active">
										<input class="check__input" type="checkbox" name="user_get_news"
											   value="1" <? if ($arUser['UF_GET_NEWS'] == 1) echo 'checked'; ?>>
										<span class="check__box"></span>
										Да, я соглашаюсь получать новости и информацию об акциях
									</label>
								</div>

								<span class="personal-data__intro">
                                    Если Вы хотите использовать номер телефона для входа в личный кабинет, то заполните
                                    поля ниже
                                </span>

								<div class="personal-data__top-label-wrapper personal-data__top-label-wrapper--email">

									<label class="personal-data__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="personal-data__clients-title">
                                            Номер телефона
                                        </span>
										<input class="personal-data__client-input js_personal_data_phone_input"
											   type="text" name="user_personal_phone"
											   value="<?= formatPhone('+'.$arUser['PERSONAL_PHONE']) ?>">
									</label>

									<!-- чтобы показать добавь класс active -->
									<span class="personal-data__email-warning active">
                                        Данный номер телефона использовался для регистрации, поэтому его нельзя
                                        изменить.
                                    </span>
								</div>

								<span class="personal-data__intro">
                                    Предпочитаемые способы связи
                                </span>

								<div class="personal-data__contact-ways-wrapper">
									<label class="check personal-data__check personal-data__contact-ways-check">
										<input class="check__input" type="checkbox" name="contact_way_call"
											   value="11" <? if (in_array("11", $arUser['UF_CONTACT_WAYS'])) echo 'checked'; ?>>
										<span class="check__box"></span>
										Звонок на телефон
									</label>

									<label class="check personal-data__check personal-data__contact-ways-check">
										<input class="check__input" type="checkbox" name="contact_way_sms"
											   value="12" <? if (in_array("12", $arUser['UF_CONTACT_WAYS'])) echo 'checked'; ?>>
										<span class="check__box"></span>
										СМС сообщение
									</label>

									<label class="check personal-data__check personal-data__contact-ways-check">
										<input class="check__input" type="checkbox" name="contact_way_email"
											   value="13" <? if (in_array("13", $arUser['UF_CONTACT_WAYS'])) echo 'checked'; ?>>
										<span class="check__box"></span>
										Письмо на Email
									</label>
								</div>
								<input type="hidden" name="update_profile" value="Y"/>
								<input type="hidden" name="user_id" value="<?= $arUser["ID"] ?>"/>

								<button class="personal-data__save-btn js_personal_data_submit_btn"
										type="submit">Сохранить изменения
								</button>
							</form>
						</li>

						<!-- покажется при добавлении класса active - это сделает js при переключении табов -->
						<li class="cabinet__content-item js_cabinet_tab <? if ($_REQUEST['pharmacy'] === 'ok'): ?>active<? endif; ?>"
							data-num="2">
							<?
								global $savedPharmacy;
								$savedPharmacy = ['PROPERTY_USER' => $arUser['ID']];
							?>
							<? $APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"saved_pharmacy",
								[
									"IBLOCK_ID" => "15",
									"IBLOCK_TYPE" => "users",
									"NEWS_COUNT" => "99",
									"SET_TITLE" => "N",
									"SORT_BY1" => "ACTIVE_FROM",
									"SORT_ORDER1" => "DESC",
									"SORT_BY2" => "SORT",
									"SORT_ORDER2" => "ASC",
									"FILTER_NAME" => "savedPharmacy",
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "36000000",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"SET_STATUS_404" => "N",
									"SHOW_404" => "N",
									"MESSAGE_404" => "",
									"PROPERTY_CODE" => ["USER"],
								],
								false
							); ?>

						</li>

						<!-- покажется при добавлении класса active - это сделает js при переключении табов -->
						<li class="cabinet__content-item js_cabinet_tab <? if ($_REQUEST['address'] === 'ok'): ?>active<? endif; ?>"
							data-num="3">
							<? $APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"saved_address",
								[
									"IBLOCK_ID" => "16",
									"IBLOCK_TYPE" => "users",
									"NEWS_COUNT" => "",
									"SET_TITLE" => "N",
									"SORT_BY1" => "ACTIVE_FROM",
									"SORT_ORDER1" => "DESC",
									"SORT_BY2" => "SORT",
									"SORT_ORDER2" => "ASC",
									"FILTER_NAME" => "userFilter",
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "36000000",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"SET_STATUS_404" => "N",
									"SHOW_404" => "N",
									"MESSAGE_404" => "",
									"PROPERTY_CODE" => ["USER"],
								],
								false
							); ?>


						</li>

						<!-- покажется при добавлении класса active - это сделает js при переключении табов -->
						<li class="cabinet__content-item js_cabinet_tab " data-num="4">
							<!-- покажется при добавлении класса active -->
							<div class="cabinet__content-item-inner active">
								<div class="cabinet__item-title-wrapper">
									<h3 class="cabinet__item-title">
										Ваша персональная скидка <?= $arUser['UF_PERSONAL_DISCOUNT'] ?>%
									</h3>
								</div>

								<div class="cabinet__sale-scale sale-scale">
									<div class="sale-scale__main-range js_sale_scale_main_range">
										<?
											$max_discount = 30;
											$width = $arUser['UF_PERSONAL_DISCOUNT'] / $max_discount * 100;
										?>
										<!-- в ширину заполенной части надо указать процент текущей скидки от максимально возможной -->
										<div class="sale-scale__progress-range js_sale_scale_progress_range"
											 style="width: <?= $width ?>%">
										</div>
									</div>
									<div class="sale-scale__numbers">
										<span class="sale-scale__number">0%</span>
										<span class="sale-scale__number">10%</span>
										<span class="sale-scale__number">20%</span>
										<span class="sale-scale__number">30%</span>
									</div>
								</div>
							</div>

						</li>
					</ul>
				</div>
			<? endif ?>
		</div>
	</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>