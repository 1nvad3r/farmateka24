<?
	define("NEED_AUTH", true);
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Мои отзывы");
	$APPLICATION->AddChainItem("Мои отзывы", "");

	global $USER;
	global $reviewFilter;

	$arUser = CUser::GetByID($USER->GetId())->GetNext();
	$reviewFilter = ['PROPERTY_USER_ID' => $USER->GetId()];
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"profile_special",
	[
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "content",
		"NEWS_COUNT" => "",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PROPERTY_CODE" => ["CATALOG_LINK"],
	],
	false
); ?>

	<section class="my-review js_review">

		<div class="my-review__menu cabinet-menu">
			<h1 class="cabinet-menu__title">
				Отзывы
			</h1>

			<ul class="cabinet-menu__list  js_cabinet_menu_list">
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/">
						Профиль
					</a>
				</li>
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/favorites/">
						Избранное
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/order/">
						Мои заказы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link active" href="/personal/reviews/">
						Мои отзывы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/cart/">
						Корзина
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link cabinet-menu__link--exit  js_btn_popup"
					   data-btn="leave" href="/?logout=yes&<?= bitrix_sessid_get() ?>">
						Выход
					</a>
				</li>
			</ul>

			<button class="cabinet-menu__btn js_cabinet_menu_open_btn">
			</button>
		</div>

		<div class="container">
			<? $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"reviews",
				[
					"IBLOCK_ID" => "14",
					"IBLOCK_TYPE" => "content",
					"NEWS_COUNT" => "",
					"SET_TITLE" => "N",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "reviewFilter",
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"PROPERTY_CODE" => ["RATING", ""],
					"FIELD_CODE" => ["CREATED_BY"],
					"PRODUCT_CATALOG" => '4',
				],
				false
			); ?>
		</div>

	</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>