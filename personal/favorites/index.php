<?
	define("NEED_AUTH", true);
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Персональный раздел");
	$APPLICATION->AddChainItem("Избранное", "");

	global $USER;
	global $favFilter;

	$arUser = CUser::GetByID($USER->GetId())->GetNext();
	$favFilter = ['ID' => $arUser['UF_FAVORITE_PRODUCTS']];
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"profile_special",
	[
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "content",
		"NEWS_COUNT" => "",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PROPERTY_CODE" => ["CATALOG_LINK"],
	],
	false
); ?>
	<section class="favorites js_favorites">

		<div class="favorites__menu cabinet-menu">
			<h1 class="cabinet-menu__title">
				Избранное
			</h1>

			<ul class="cabinet-menu__list  js_cabinet_menu_list">
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/">
						Профиль
					</a>
				</li>
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link active" href="/personal/favorites/">
						Избранное
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/order/">
						Мои заказы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/reviews/">
						Мои отзывы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link " href="/personal/cart/">
						Корзина
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link cabinet-menu__link--exit js_btn_popup" data-btn="leave"
					   href="/?logout=yes&<?= bitrix_sessid_get() ?>">
						Выход
					</a>
				</li>
			</ul>

			<button class="cabinet-menu__btn js_cabinet_menu_open_btn">
			</button>
		</div>

		<div class="container">
			<? if (count($arUser['UF_FAVORITE_PRODUCTS']) > 0): ?>
				<div class="favorites__top active js_favorites_top">
                    <span class="favorites__sub-title">
                        (<?= count($arUser['UF_FAVORITE_PRODUCTS']); ?> товара)
                    </span>
				</div>
				<? $APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					"favorites",
					[
						"SHOW_TABS" => "Y",
						"TITTLE" => "Избранноое",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
						"ELEMENT_SORT_FIELD2" => "id",
						"ELEMENT_SORT_ORDER" => "asc",
						"ELEMENT_SORT_ORDER2" => "asc",
						"FILTER_NAME" => 'favFilter',
						"IBLOCK_ID" => "4",
						"IBLOCK_TYPE" => "catalog",
						"PRICE_CODE" => ["BASE"],
						"DISPLAY_SHOW_MORE" => "Y",
						"SHOW_MORE_LINK" => "",
					]
				); ?>
			<? else: ?>
				<div class="favorites__empty-row active js_favorites_empty_row">
					<h2 class="favorites__empty-title">
						Список избранного пока пуст.
					</h2>

					<div class="favorites__empty-text-wrapper">
						<p class="favorites__empty-text">
							Чтобы добавить товар в корзину нажмите на кнопку
							<svg class="favorites__empty-icon" width="33" height="28">
								<use xlink:href="images/sprite.svg#card-heart"></use>
							</svg>
						</p>

						<p class="favorites__empty-text">
							Чтобы сохранить избранный товар авторизуйтесь.
						</p>
					</div>

					<a class="favorites__empty-btn" href="/catalog/">
						Перейти в каталог
					</a>
				</div>
			<? endif; ?>
		</div>

	</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>