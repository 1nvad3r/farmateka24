<?
	define("HIDE_SIDEBAR", true);
	global $WITHOUT_CONTAINER;
	$WITHOUT_CONTAINER = "Y";
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

	use \Bitrix\Main\Loader;

	global $USER;
	Loader::includeModule('catalog');
	Loader::includeModule('sale');
	$APPLICATION->SetTitle("Корзина");
	$APPLICATION->AddChainItem($APPLICATION->GetTitle());
?>


	<section class="cart js_cart">

		<div class="cart__menu cabinet-menu">
			<h1 class="cabinet-menu__title">
				<? $APPLICATION->ShowTitle(false); ?>
			</h1>

			<? if ($USER->IsAuthorized()): ?>
				<? $APPLICATION->IncludeComponent("bitrix:menu", "cabinet_menu", [
						"ROOT_MENU_TYPE" => "cabinet",
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "cabinet",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_TIME" => "360000",
						"MENU_CACHE_USE_GROUPS" => "Y",
					]
				); ?>
			<? endif; ?>
		</div>

		<? $APPLICATION->IncludeComponent(
			"bitrix:sale.basket.basket",
			"",
			[
				"ACTION_VARIABLE" => "action",
				"AUTO_CALCULATION" => "Y",
				"TEMPLATE_THEME" => "blue",
				"COLUMNS_LIST" => ["NAME", "DISCOUNT", "WEIGHT", "DELETE", "DELAY", "TYPE", "PRICE", "QUANTITY"],
				"LABEL_PROP" => ["BRAND"],
				"COMPONENT_TEMPLATE" => ".default",
				"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
				"GIFTS_CONVERT_CURRENCY" => "Y",
				"GIFTS_HIDE_BLOCK_TITLE" => "N",
				"GIFTS_HIDE_NOT_AVAILABLE" => "N",
				"GIFTS_MESS_BTN_BUY" => "Выбрать",
				"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
				"GIFTS_PAGE_ELEMENT_COUNT" => "4",
				"SHOW_RESTORE" => "N",
				"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
				"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
				"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
				"GIFTS_SHOW_IMAGE" => "Y",
				"GIFTS_SHOW_NAME" => "Y",
				"GIFTS_SHOW_OLD_PRICE" => "Y",
				"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
				"GIFTS_PLACE" => "BOTTOM",
				"HIDE_COUPON" => "N",
				"OFFERS_PROPS" => ["SIZES_SHOES", "SIZES_CLOTHES"],
				"PATH_TO_ORDER" => "/personal/cart/payment/",
				"PRICE_VAT_SHOW_VALUE" => "N",
				"QUANTITY_FLOAT" => "N",
				"USE_GIFTS" => "Y",
				"USE_PREPAYMENT" => "N",
				"SET_TITLE" => "N",
			]
		); ?>

	</section>
<?php if ($_REQUEST['clear'] == 'y') {

	$obBasket = \Bitrix\Sale\Basket::getList(
		[
			'select' => [
				'FUSER_ID',
			],
			'filter' => [
				'ORDER_ID' => 'NULL',
			],
		]
	);
	while ($bItem = $obBasket->Fetch()) {
		CSaleBasket::DeleteAll(
			$bItem['FUSER_ID'],
			false
		);
	}
	LocalRedirect('/personal/cart/');
}
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>