<?
	define("NEED_AUTH", true);
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Заказы");
	$APPLICATION->AddChainItem("Мои заказы", "");
	global $USER;
?>
	<? $APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"profile_special",
		[
			"IBLOCK_ID" => "7",
			"IBLOCK_TYPE" => "content",
			"NEWS_COUNT" => "",
			"SET_TITLE" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "arrFilter",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"SET_STATUS_404" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => "",
			"PROPERTY_CODE" => ["CATALOG_LINK"],
		],
		false
	); ?>
	<?php if(!$_REQUEST['ID']): ?>
	<section class="my-orders js_my-orders">

		<div class="my-orders__menu cabinet-menu">
			<h1 class="cabinet-menu__title">
				Мои заказы
			</h1>

			<ul class="cabinet-menu__list  js_cabinet_menu_list">
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/">
						Профиль
					</a>
				</li>
				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link " href="/personal/favorites/">
						Избранное
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link active" href="/personal/order/">
						Мои заказы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link" href="/personal/reviews/">
						Мои отзывы
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link " href="/personal/cart/">
						Корзина
					</a>
				</li>

				<li class="cabinet-menu__item">
					<a class="cabinet-menu__link cabinet-menu__link--exit js_btn_popup" data-btn="leave"
					   href="/?logout=yes&<?= bitrix_sessid_get() ?>">
						Выход
					</a>
				</li>
			</ul>

			<button class="cabinet-menu__btn js_cabinet_menu_open_btn">
			</button>
		</div>
	<? endif; ?>
		<? $APPLICATION->IncludeComponent(
			"bitrix:sale.personal.order",
			"list",
			[
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"ALLOW_INNER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "3600",
				"CACHE_TYPE" => "A",
				"CUSTOM_SELECT_PROPS" => [""],
				"DETAIL_HIDE_USER_INFO" => ["0"],
				"DISALLOW_CANCEL" => "N",
				"HISTORIC_STATUSES" => ["F"],
				"NAV_TEMPLATE" => "orders",
				"ONLY_INNER_FULL" => "N",
				"ORDERS_PER_PAGE" => "10",
				"ORDER_DEFAULT_SORT" => "STATUS",
				"PATH_TO_BASKET" => "/personal/cart/",
				"PATH_TO_CATALOG" => "/catalog/",
				"PATH_TO_PAYMENT" => "/personal/order/payment/",
				"PROP_1" => [""],
				"REFRESH_PRICES" => "N",
				"RESTRICT_CHANGE_PAYSYSTEM" => ["0"],
				"SAVE_IN_SESSION" => "Y",
				"SEF_MODE" => "N",
				"SET_TITLE" => "Y",
				"STATUS_COLOR_F" => "gray",
				"STATUS_COLOR_N" => "green",
				"STATUS_COLOR_P" => "yellow",
				"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
			]
		); ?>
	<? if(!$_REQUEST['ID']): ?>
	</section>
	<? endif; ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>