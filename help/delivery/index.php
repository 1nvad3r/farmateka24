<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");
$APPLICATION->AddChainItem("Доставка", "");?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "mini_baners",
    array(
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "content",
        "NEWS_COUNT" => "12",
        "SET_TITLE" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => ""
    ),
    false
);?>

    <section class="delivery ">
        <div class="container">
            <h1 class="delivery__title">
                Доставка товаров
            </h1>

            <div class="js_delivery_inner">
                <ul class="delivery__switch-list">
                    <li class="delivery__switch-item">
                        <button class="delivery__btn active js_delivery_tab_btn" type="button">Доставка по
                            Москве</button>
                    </li>

                    <li class="delivery__switch-item">
                        <button class="delivery__btn js_delivery_tab_btn" type="button">Доставка по МО</button>
                    </li>
                </ul>

                <ul class="delivery__tabs-list">
                    <li class="delivery__tab active js_delivery_item">
                        <ul class="delivery__company-list">
                            <li class="delivery__company-item-title">
                                <div class="delivery__company-name-title">
                                    Курьерская компания
                                </div>

                                <div class="delivery__company-time-title">
                                    Сроки доставки
                                </div>

                                <div class="delivery__company-price-title">
                                    Стоимость доставки
                                </div>

                                <div class="delivery__company-payment-title">
                                    Способы оплаты
                                </div>
                            </li>

                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    Яндекс Доставка
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            от 220 ₽
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>
                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    СДЭК
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            от 360 ₽
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>

                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    Курьерская доставка
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            от 0 ₽
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>

                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    Самовывоз
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн, оплата наличными
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            Бесплатно
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li class="delivery__tab js_delivery_item">
                        <ul class="delivery__company-list">
                            <li class="delivery__company-item-title">
                                <div class="delivery__company-name-title">
                                    Курьерская компания
                                </div>

                                <div class="delivery__company-time-title">
                                    Сроки доставки
                                </div>

                                <div class="delivery__company-price-title">
                                    Стоимость доставки
                                </div>

                                <div class="delivery__company-payment-title">
                                    Способы оплаты
                                </div>
                            </li>

                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    Яндекс Доставка
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            от 1220 ₽
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>
                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    СДЭК
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            от 1360 ₽
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>

                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    Курьерская доставка
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            от 0 ₽
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>

                            <li class="delivery__company-item">
                                <h3 class="delivery__company-name">
                                    Самовывоз
                                </h3>

                                <span class="delivery__company-time">
                                        При наличии товара доставка возможна в день заказа
                                    </span>

                                <span class="delivery__company-payment">
                                        Оплата онлайн, оплата наличными
                                    </span>

                                <div class="delivery__company-price-wrapper">
                                        <span class="delivery__company-price">
                                            Бесплатно
                                        </span>

                                    <a class="delivery__company-price-link" href="#">
                                        Рассчитать стоимость доставки
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="swiper delivery__swiper js_delivery_swiper">
                <div class="swiper-wrapper delivery__swiper-wrapper">
                    <div class="swiper-slide delivery__slide">
                        <img class="delivery__slide-img" src="<?=SITE_TEMPLATE_PATH.'/images/icons/delivery-icon-1.png'?>" alt="">

                        <span class="delivery__slide-text">Круглосуточная доставка заказов по Москве</span>
                    </div>

                    <div class="swiper-slide delivery__slide">
                        <img class="delivery__slide-img" src="<?=SITE_TEMPLATE_PATH.'/images/icons/delivery-icon-2.png'?>" alt="">

                        <span class="delivery__slide-text">Сотрудничаем с тремя курьерскими службами</span>
                    </div>

                    <div class="swiper-slide delivery__slide">
                        <img class="delivery__slide-img" src="<?=SITE_TEMPLATE_PATH.'/images/icons/delivery-icon-3.png'?>" alt="">

                        <span class="delivery__slide-text">Возможность самовывоза в удобной Вам аптеке</span>
                    </div>

                    <div class="swiper-slide delivery__slide">
                        <img class="delivery__slide-img" src="<?=SITE_TEMPLATE_PATH.'/images/icons/delivery-icon-4.png'?>" alt="">

                        <span class="delivery__slide-text">Бесплатная доставка при заказе от 2000 рублей</span>
                    </div>
                </div>

                <div class="swiper-scrollbar"></div>
            </div>


            <h2 class="delivery__title">
                Условия доставки
            </h2>

            <div class="js_delivery_inner">
                <ul class="delivery__switch-list">
                    <li class="delivery__switch-item">
                        <button class="delivery__btn active js_delivery_tab_btn" type="button">Доставка по
                            Москве</button>
                    </li>

                    <li class="delivery__switch-item">
                        <button class="delivery__btn js_delivery_tab_btn" type="button">Доставка по МО</button>
                    </li>
                </ul>



                <ul class="delivery__tabs-list">
                    <li class="delivery__tab active js_delivery_item">
                        <ul class="delivery__info-list">
                            <li class="delivery__info-item">
                                Доставка осуществляется на следующий день, если товар есть в аптеке или на ближайшем
                                складе.
                            </li>

                            <li class="delivery__info-item">
                                Вы можете забрать заказ самостоятельно, оформив самовывоз в удобной Вам аптеке. Если
                                в данной аптеке нет всех заказанных товаров, Вам будут предложены ближайшие аптеки,
                                где все товары есть в наличии.
                            </li>

                            <li class="delivery__info-item">
                                Доставка рецептурных препаратов возможно только до аптеки. Для получения
                                рецептурного препарата необходимо предъявить выписанный рецепт.
                            </li>

                            <li class="delivery__info-item">
                                Если товара нет в наличии, но он есть на складе, то сроки доставки могут быть
                                увелечены.
                            </li>

                            <li class="delivery__info-item">
                                Осуществить возврат заказа можно только в момент доставки при соблюдения правил и
                                порядка обмена/возврата товара.
                            </li>

                            <li class="delivery__info-item">
                                Доставка осуществляется на следующий день, если товар есть в аптеке или на ближайшем
                                складе.
                            </li>
                        </ul>
                    </li>

                    <li class="delivery__tab active js_delivery_item">
                        <ul class="delivery__info-list">
                            <li class="delivery__info-item">
                                111Доставка осуществляется на следующий день, если товар есть в аптеке или на
                                ближайшем
                                складе.
                            </li>

                            <li class="delivery__info-item">
                                Вы можете забрать заказ самостоятельно, оформив самовывоз в удобной Вам аптеке. Если
                                в данной аптеке нет всех заказанных товаров, Вам будут предложены ближайшие аптеки,
                                где все товары есть в наличии.
                            </li>

                            <li class="delivery__info-item">
                                Доставка рецептурных препаратов возможно только до аптеки. Для получения
                                рецептурного препарата необходимо предъявить выписанный рецепт.
                            </li>

                            <li class="delivery__info-item">
                                Если товара нет в наличии, но он есть на складе, то сроки доставки могут быть
                                увелечены.
                            </li>

                            <li class="delivery__info-item">
                                Осуществить возврат заказа можно только в момент доставки при соблюдения правил и
                                порядка обмена/возврата товара.
                            </li>

                            <li class="delivery__info-item">
                                Доставка осуществляется на следующий день, если товар есть в аптеке или на ближайшем
                                складе.
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <h2 class="delivery__title">
                Порядок обмена/возврата
            </h2>

            <p class="delivery__warning-card">
                Товар может быть возвращен только в момент доставки!
            </p>

            <p class="delivery__warning-text">
                Согласно Постановлению Правительства РФ от 31.12.2020 №2463 не подлежат обмену и возврату следующие товары надлежащего качества:
            </p>

            <ul class="delivery__warning-list">
                <li class="delivery__warning-itme">
                    Товары для профилактики и лечения заболеваний в домашних условиях (предметы санитарии и гигиены из металла, резины, текстиля и других материалов, медицинские изделия, средства гигиены и полости рта, линзы очковые, предметы по уходу за детьми), лекарственные препараты;
                </li>

                <li class="delivery__warning-itme">
                    Предметы личной гигиены (зубные щетки и другие аналогичные товары); Парфюмерно-косметические товары.
                </li>
            </ul>

        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>