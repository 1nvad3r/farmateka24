<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата товаров");?>
<?$APPLICATION->AddChainItem("Оплата товаров", "");?>
<?$APPLICATION->SetPageProperty("section_class", "js_payment_inner")?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "mini_baners",
    array(
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "content",
        "NEWS_COUNT" => "12",
        "SET_TITLE" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => ""
    ),
    false
);?>
<div class="policy-main__top">
    <h1><?$APPLICATION->ShowTitle(false);?></h1>
</div>

<ul class="payment__switch-list">
    <li class="payment__switch-item">
        <button class="payment__btn active js_payment_tab_btn" type="button">Оплата наличными</button>
    </li>

    <li class="payment__switch-item">
        <button class="payment__btn js_payment_tab_btn" type="button">Оплата картой онлайн</button>
    </li>
</ul>

<ul class="payment__tabs-list">
    <li class="payment__tab active js_payment_item">
        <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include/help_payment_tab1.php"
            )
        );?>
    </li>

    <li class="payment__tab js_payment_item">
        <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include/help_payment_tab2.php"
            )
        );?>
    </li>
</ul>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
