<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Частые вопросы");
$APPLICATION->AddChainItem("Частые вопросы", "");?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "mini_baners",
    array(
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "content",
        "NEWS_COUNT" => "12",
        "SET_TITLE" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => ""
    ),
    false
);?>
    <div class="policy-main__top">
        <h1><?$APPLICATION->ShowTitle(false);?></h1>
    </div>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "faq",
    array(
        "IBLOCK_ID" => "10",
        "IBLOCK_TYPE" => "content",
        "NEWS_COUNT" => "20",
        "SET_TITLE" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => ""
    ),
    false
);?>

    <div class="description__extra description">
        <h3 class="description__extra-title">
            Не нашли ответа на свой вопрос?
        </h3>

        <h4 class="description__extra-subtitle">
            Напишите или позвоните нам!
        </h4>

        <a class="footer__contact footer__contact-phone" href="tel:<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include/phone_in_head.php",
                "EDIT_TEMPLATE" => "standard.php"
            ), false, array("HIDE_ICONS"=>"Y")
        );?>">
            <svg width="19" height="20">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#footer-phone'?>"></use>
            </svg>
            <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/include/phone_in_head.php"
                )
            );?>
        </a>

        <a class="footer__contact footer__contact-email" href="mailto:<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include/email.php",
                "EDIT_TEMPLATE" => "standard.php"
            ), false, array("HIDE_ICONS"=>"Y")
        );?>">
            <svg width="20" height="19">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#footer-email'?>"></use>
            </svg>
            <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/include/email.php",
                    "EDIT_TEMPLATE" => "standard.php"
                ), false, array("HIDE_ICONS"=>"Y")
            );?>
        </a>

        <a class="description__extra-contact-form-link" href="/feedback/">
            Форма обратной связи
        </a>
    </div>

    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>