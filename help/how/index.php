<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как сделать заказ");?><?$APPLICATION->AddChainItem("Как сделать заказ", "");?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"mini_baners",
	Array(
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "content",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "12",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
<div class="policy-main__top">
	<h1><?$APPLICATION->ShowTitle(false);?></h1>
</div>
<div class="how__step">
	<h2 class="how__step-title">
	Шаг 1 </h2>
	<p class="how__text">
		 Выберите необходимые товары через форму поиска или в каталоге.
	</p>
    <img width="963" alt="step-1.jpg" src="/upload/medialibrary/ddf/avutyzkqc3nytsmjm9ikilcp780hrpi0.jpg" height="355" title="step-1.jpg"><br>
</div>
<div class="how__step">
	<h2 class="how__step-title">
	Шаг 2 </h2>
	<p class="how__text">
		 Положите товары в корзину
	</p>
    <img width="955" alt="step-2.jpg" src="/upload/medialibrary/ea5/rc1kvw5ihfctihl7ufhhw21ptqrlqu1a.jpg" height="347" title="step-2.jpg"><br>
</div>
<div class="how__step">
	<h2 class="how__step-title">
	Шаг 3 </h2>
	<p class="how__text">
		 Перейдите на страницу «Корзина», проверьте свой выбор, выберите способ доставки и нажмите кнопку “Перейти к оформлению заказа”.
	</p>
    <img width="955" alt="step-3.jpg" src="/upload/medialibrary/21e/w7688t79rb4uhinub9ie6ppeb96g5dqh.jpg" height="347" title="step-3.jpg"><br>
</div>
<div class="how__step">
	<h2 class="how__step-title">
	Шаг 4 </h2>
	<p class="how__text">
		 Заполните поля формы, выберете удобный способ оплаты. Далее нажмите кнопку “Оформить заказ”
	</p>
    <img width="955" alt="step-4.jpg" src="/upload/medialibrary/8d1/56q7i689uqoc2neboopk40rs3mneqacw.jpg" height="347" title="step-4.jpg"><br>
</div>
<div class="how__step">
	<h2 class="how__step-title">
	Шаг 5 </h2>
	<p class="how__text">
		 Статус заказа можно отслеживать в Личном кабинете.
	</p>
    <img width="955" alt="step-5.jpg" src="/upload/medialibrary/bfb/uf5jyk22e6gloem3y5nom6cgz6jj3pq9.jpg" height="347" title="step-5.jpg"><br>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>