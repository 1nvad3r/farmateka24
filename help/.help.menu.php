<?/*
$aMenuLinks = Array(
    Array(
        "Оплата товаров",
        "payment",
        Array(),
        Array(),
        ""
    ),

    Array(
        "Доставка товаров",
        "delivery",
        Array(),
        Array(),
        ""
    ),

    Array(
        "Как сделать заказ",
        "how",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Частые вопросы",
        "faq",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Обратная связь",
        "/feedback/",
        Array(),
        Array(),
        ""
    ),
);
*/?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	$aMenuLinks = Array(
		Array(
			"О нас",
			"/about/about-us/",
			Array(),
			Array(),
			""
		),
		Array(
			"Вакансии",
			"/help/vacancies/",
			Array(),
			Array(),
			""
		),
		Array(
			"Контакты",
			"/contacts/",
			Array(),
			Array(),
			""
		),
		Array(
			"Оплата и доставка",
			"/help/payment/",
			Array(),
			Array(),
			""
		),
		Array(
			"Политика конфиденциальности",
			"/about/privacy-policy/",
			Array(),
			Array(),
			""
		),
		Array(
			"Пользовательское соглашение",
			"/about/user-rules/",
			Array(),
			Array(),
			""
		)
	);
?>
