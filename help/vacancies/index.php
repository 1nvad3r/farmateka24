<?
	GLOBAL $WITHOUT_CONTAINER, $HIDEHEAD_BREADCRUMB;
	$WITHOUT_CONTAINER = "Y";
	$HIDEHEAD_BREADCRUMB = "Y";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");?>
<?$APPLICATION->AddChainItem("Вакансии", "");?>
<?$APPLICATION->SetPageProperty("page_class", "job")?>
<?$APPLICATION->SetPageProperty("section_class", "job js_job_inner")?>

	<section class="breadcrumbs breadcrumbs--white-and-moved-to-bottom">
		<div class="container breadcrumbs__container">
			<a href="/">Главная</a>

			<a href="/help/">Помощь</a>

			<a href="/help/vacancies/">Вакансии</a>

		</div>
	</section>

	<section class="job js_job_inner">
		<div class="job__hero">
			<div class="container">
				<h2 class="job__title">
					Вакансии
				</h2>

				<p class="job__subtitle">
					Стань частью крутой комнады Фарматека!
				</p>

				<img class="job__hero-mobile-img" src="<?= SITE_TEMPLATE_PATH ?>/images/special/job-hero-img.jpg" alt="">

				<!-- фильтр над картинкой на десктопе -->
				<div class="job__hero-mobile-img-filter" style="background-color: #000;
                    opacity: 0.35"></div>

				<a class="job__send-cv-btn" type="button" href="#vacancies">
					Открытые вакансии
				</a>
			</div>
		</div>

		<div class="container">
			<div class="swiper delivery__swiper js_delivery_swiper">
				<div class="swiper-wrapper delivery__swiper-wrapper">
					<div class="swiper-slide delivery__slide">
						<img class="delivery__slide-img" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/job-icon-1.png" alt="">

						<span class="delivery__slide-text">Первые на рынке</span>
					</div>

					<div class="swiper-slide delivery__slide">
						<img class="delivery__slide-img" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/job-icon-2.png" alt="">

						<span class="delivery__slide-text">Бонусная система для сотрудников</span>
					</div>

					<div class="swiper-slide delivery__slide">
						<img class="delivery__slide-img" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/job-icon-3.png" alt="">

						<span class="delivery__slide-text">Расширенный социальный пакет</span>
					</div>

					<div class="swiper-slide delivery__slide">
						<img class="delivery__slide-img" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/job-icon-4.png" alt="">

						<span class="delivery__slide-text">Крутой коллектив</span>
					</div>
				</div>

				<div class="swiper-scrollbar"></div>
			</div>

			<h2 class="job__title" id="vacancies">
				Открытые вакансии
			</h2>

			<ul class="job__links-list">
				<li class="job__link-item">
					<button class="job__link  active js_job_tab_btn" type="button" data-num="1">
						Москва
					</button>
				</li>

				<li class="job__link-item">
					<button class="job__link  js_job_tab_btn" type="button" data-num="2">
						Новые Ватутники
					</button>
				</li>

				<li class="job__link-item">
					<button class="job__link js_job_tab_btn" type="button" data-num="3">
						ТЦ Вегас
					</button>
				</li>

				<li class="job__link-item">
					<button class="job__link  js_job_tab_btn" type="button" data-num="4">
						Московский
					</button>
				</li>
			</ul>

			<ul class="job__spoilers-list active js_job_item">
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							1Сборщик заказов / комплектовщик
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Водитель-курьер на личном автомобиле
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>

				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Фрамацевт-провизор
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
			</ul>

			<ul class="job__spoilers-list js_job_item">
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							2Сборщик заказов / комплектовщик
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Водитель-курьер на личном автомобиле
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>

				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Фрамацевт-провизор
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
			</ul>

			<ul class="job__spoilers-list js_job_item">
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							3Сборщик заказов / комплектовщик
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Водитель-курьер на личном автомобиле
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>

				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Фрамацевт-провизор
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
			</ul>

			<ul class="job__spoilers-list js_job_item">
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							4Сборщик заказов / комплектовщик
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Водитель-курьер на личном автомобиле
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>

				<li class="job__spoiler-item js_job_spoiler_item" data-target="1">
					<div class="job__spoiler-top">
						<h3 class="job__spoiler-title">
							Фрамацевт-провизор
						</h3>
					</div>

					<div class="job__spoiler-bottom">
						<!-- <p class="job__spoiler-text">
							Комбинированный препарат для лечения
							инфекционно-воспалительных заболеваний (ОРВИ, грипп), сопровождающихся повышенной
							температурой, ознобом, головной болью, болями в суставах и мышцах, заложенностью носа и
							болями в горле и пазухах носа, и применяемый для взрослых и детей с 15-х лет.
						</p> -->

						<h4 class="job__spoiler-bottom-title">Задачи</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Нам важно</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>

						<h4 class="job__spoiler-bottom-title">Что мы предлагаем</h4>

						<ul class="job__spoiler-ul">
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
							<li class="job__spoiler-ul-li">Получение заказов на складе и доставка согласно маршруту
							</li>
						</ul>


						<!--
						<ol class="job__spoiler-ol">
							<li class="job__spoiler-ol-li">нумерованный список</li>
							<li class="job__spoiler-ol-li">нумерованный список</li>
						</ol>

						<a class="job__spoiler-link" href="#">выделенная цветом гиперссыла</a>

						<span class="job__spoiler-accent-text">выделенный текст</span>

						<img class="job__spoiler-img" src="https://www.meme-arsenal.com/memes/0358bb9ee96905503341696292e11583.jpg" alt="фото товара">

						<div class="job__spoiler-video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/_rMZt292mJc?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>

						<a class="job__spoiler-document" href="#" target="_blank">Документ</a> -->

						<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
							Отправить резюме
						</button>
					</div>
				</li>
			</ul>

			<div class=" job__extra">
				<h3 class=" job__extra-title">
					Не нашли подходящую вакансию?
				</h3>

				<h4 class=" job__extra-subtitle">
					Отправьте нам свое резюме! Возможно, в скором времени нам потребуются сотрдуники с Вашими
					навыками.
				</h4>

				<button class="job__send-cv-btn js_btn_popup" data-btn="send-cv" type="button">
					Отправить резюме
				</button>
			</div>

		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
