<?
GLOBAL $WITHOUT_CONTAINER;
$WITHOUT_CONTAINER = "Y";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Помощь");?>
<?$APPLICATION->AddChainItem("Помощь", "");?>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "mini_baners",
    array(
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "content",
        "NEWS_COUNT" => "12",
        "SET_TITLE" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => ""
    ),
    false
);?>

<section class="about">
    <?$APPLICATION->IncludeComponent("bitrix:menu","page_menu",Array(
            "ROOT_MENU_TYPE" => "help",
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "help",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_TIME" => "360000",
            "MENU_CACHE_USE_GROUPS" => "Y",
        )
    );?>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
