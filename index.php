<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Фарматека");
	$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
	<!-- ГЛАВНЫЙ СЛАЙДЕР -->
<?php
	$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"main_slider",
		[
			"IBLOCK_ID" => "6",
			"IBLOCK_TYPE" => "content",
			"NEWS_COUNT" => "10",
			"SET_TITLE" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"SET_BROWSER_TITLE" => "N",
			"SET_STATUS_404" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => "",
			"PROPERTY_CODE" => ["CATALOG_LINK", "BUTTON_TEXT"],
		],
		false
	);
?>
<?php
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"",
		[
			"SHOW_TABS" => "Y",
			"TITTLE" => "Популярные товары",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "Y",
			"ELEMENT_SORT_FIELD" => "PROPERTY_POPULAR",
			"ELEMENT_SORT_FIELD2" => "SORT",
			"ELEMENT_SORT_ORDER" => "DESC",
			"ELEMENT_SORT_ORDER2" => "DESC",
			"FILTER_NAME" => "",
			"IBLOCK_ID" => "4",
			"IBLOCK_TYPE" => "catalog",
			"PRICE_CODE" => ["BASE"],
			"CONVERT_CURRENCY" => "Y",
			"CURRENCY_ID" => "RUB",
		]
	);
?>

<?php
	$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"special",
		[
			"IBLOCK_ID" => "7",
			"IBLOCK_TYPE" => "content",
			"NEWS_COUNT" => "3",
			"SET_TITLE" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "arrFilter",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"SET_STATUS_404" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => "",
			"PROPERTY_CODE" => ['CATALOG_LINK']
		],
		false
	);
?>
<?php
	global $novFilter;
	$novFilter = ["PROPERTY_SECTION" => "42", "PROPERTY_ON_MAIN" => "18"];

	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"",
		[
			"SHOW_TABS" => "Y",
			"TITTLE" => "Новинки",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
			"ELEMENT_SORT_FIELD2" => "id",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_ORDER2" => "asc",
			"FILTER_NAME" => "novFilter",
			"IBLOCK_ID" => "4",
			"IBLOCK_TYPE" => "catalog",
			"PRICE_CODE" => ["BASE"],
			"DISPLAY_SHOW_MORE" => "Y",
			"SHOW_MORE_LINK" => "/catalog/list/news/",
		]
	);
?>

<?php
	$APPLICATION->IncludeComponent(
		"bitrix:news.detail",
		"index_banner",
		[
			"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "8",
			"ELEMENT_ID" => "326",
			"ELEMENT_CODE" => "",
			"SET_TITLE" => "N",
			"USE_PERMISSIONS" => "Y",
			"GROUP_PERMISSIONS" => ["2"],
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600000",
			"CACHE_GROUPS" => "Y",
			"PROPERTY_CODE" => ["BTN_LINK", "BTN_TEXT"],
		]
	);
?>

<?php
	$arViewed = [];
	$basketUserId = (int)CSaleBasket::GetBasketUserID(false);
	if ($basketUserId > 0) {
		$viewedIterator = \Bitrix\Catalog\CatalogViewedProductTable::getList([
			'select' => ['PRODUCT_ID', 'ELEMENT_ID'],
			'filter' => ['=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID],
			'order' => ['DATE_VISIT' => 'DESC'],
			'limit' => 10,
		]);

		while ($arFields = $viewedIterator->fetch()) {
			$arViewed[] = $arFields['ELEMENT_ID'];
		}
	}
?>

<?php
	if (count($arViewed) > 0):
		global $viewedFilter;
		$viewedFilter = ["ID" => $arViewed];

		$APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"",
			[
				"SHOW_TABS" => "N",
				"TITTLE" => "Недавно просмотренные",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER" => "asc",
				"ELEMENT_SORT_ORDER2" => "asc",
				"FILTER_NAME" => "viewedFilter",
				"IBLOCK_ID" => "4",
				"IBLOCK_TYPE" => "catalog",
				"PRICE_CODE" => ["BASE"],
			]
		);
	endif; ?>

<?php $APPLICATION->IncludeComponent(
	"bitrix:catalog.store.list",
	"on_main",
	[
		"PHONE" => "Y",
		"SCHEDULE" => "Y",
		"EMAIL" => "Y",
		"PATH_TO_ELEMENT" => "store/#store_id#",
		"MAP_TYPE" => "0",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"SHOW_ALL_LINK" => "",
		"SET_TITLE" => "N",
	]
); ?>

<?php $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"useful_articles",
	[
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "content",
		"NEWS_COUNT" => "3",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
	],
	false
); ?>

<?php
	$APPLICATION->IncludeComponent(
		"bitrix:sender.subscribe",
		"",
		[
			"COMPONENT_TEMPLATE" => ".default",
			"USE_PERSONALIZATION" => "Y",
			"CONFIRMATION" => "Y",
			"SHOW_HIDDEN" => "Y",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "Y",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "360000",
			"HIDE_MAILINGS" => "Y",
			"SET_TITLE" => "N",
		]
	);
?>

	<!-- ПОП-АП 'БЛАГОДАРНОСТЬ ЗА ПОДПИСКУ' -->
	<div class="popup popup--change-password  js_popup js_thenks" data-popup="thenks">
		<div class="popup__wrap">
			<button class="popup__exit js_popup_exit" type="button">
				<svg width="24" height="24">
					<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
				</svg>
			</button>
			<form class="popup__form js_popup_form js_popup_report_form">
				<div class="popup__title">Благодарим, что подписались на нашу рассылку</div>

			</form>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>