<?
GLOBAL $WITHOUT_CONTAINER;
$WITHOUT_CONTAINER = "Y";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты"); ?>
<?$APPLICATION->AddChainItem("Контакты", "");?>

    <section class="contacts">
        <div class="container">
            <h1 class="contacts__title">
                <?$APPLICATION->ShowTitle(false);?>
            </h1>

            <ul class="contacts__list">
                <li class="contacts__item">
                        <span class="contacts__item-title">
                            Справочная служба
                        </span>

                    <a class="footer__contact footer__contact-phone contacts__phone" href="tel:<?=tplvar('phone');?>">
                        <svg width="20" height="20">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#contacts-phone'?>"></use>
                        </svg>
                        <?=tplvar('phone');?>
                    </a>
                </li>

                <li class="contacts__item">
                        <span class="contacts__item-title">
                            Адрес для электронных обращений
                        </span>

                    <a class="footer__contact footer__contact-email contacts__email"
                       href="mailto:<?=tplvar('email');?>">
                        <svg width="20" height="20">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#contacts-email'?>"></use>
                        </svg>
                        <?=tplvar('email');?>
                    </a>
                </li>

                <li class="contacts__item">
                        <span class="contacts__item-title">
                            Фарматека в соиальных сетях
                        </span>

                    <ul class="footer__social">
                        <li>
                            <a href="<?=tplvar('twitter');?>" target="_blank">
                                <svg width="40" height="40">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#footer-twitter'?>"></use>
                                </svg>
                            </a>
                        </li>

                        <li>
                            <a href="<?=tplvar('youtube');?>" target="_blank">
                                <svg width="40" height="40">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#footer-youtube'?>"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </section>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.store.list",
    "contacts",
    Array(
        "PHONE" => "Y",
        "SCHEDULE" => "Y",
        "PATH_TO_ELEMENT" => "store/#store_id#",
        "MAP_TYPE" => "0",
        "SET_TITLE" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "SET_TITLE" => "N"
    )
);?>

<?$APPLICATION->IncludeComponent(
    "bitrix:main.feedback",
    "contacts",
    Array(
        "USE_CAPTCHA" => "N",
        "OK_TEXT" => "Ваше сообщение успешно отправлено, наш менеджер свяжется с вами в ближайшее время",
        "REQUIRED_FIELDS" => array(),
        "EVENT_MESSAGE_ID" => array(7),
        "AJAX_MODE" => "Y",  // режим AJAX
        "AJAX_OPTION_SHADOW" => "N", // затемнять область
        "AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента.
        "AJAX_OPTION_STYLE" => "Y", // подключать стили
        "AJAX_OPTION_HISTORY" => "N",
    )
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>