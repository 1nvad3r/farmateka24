<?
	GLOBAL $WITHOUT_CONTAINER, $WITHOUT_VIEWED;
	$WITHOUT_CONTAINER = "Y";
	$WITHOUT_VIEWED = "Y";
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?php $APPLICATION->SetTitle("Обратная связь");?>
<?php $APPLICATION->AddChainItem("Обратная связь", "");?>
<?php $APPLICATION->SetPageProperty("section_class", "about-bonus"); ?>
<?php $APPLICATION->SetPageProperty("page_class", "about__wrapper"); ?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback",
	"contacts",
	Array(
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => "Ваше сообщение успешно отправлено, наш менеджер свяжется с вами в ближайшее время",
		"EMAIL_TO" => "tipikin.work@yandex.ru",
		"REQUIRED_FIELDS" => array(),
		"EVENT_MESSAGE_ID" => array(7),
		"AJAX_MODE" => "Y",  // режим AJAX
		"AJAX_OPTION_SHADOW" => "N", // затемнять область
		"AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента.
		"AJAX_OPTION_STYLE" => "Y", // подключать стили
		"AJAX_OPTION_HISTORY" => "N",
	)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>