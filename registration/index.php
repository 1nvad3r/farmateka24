<?
$showFooter = false;
if ($_REQUEST['ajax_mode'] == 'Y') {
    require $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
    $APPLICATION->IncludeComponent(
        "bitrix:system.auth.form",
        "errors",
        Array(
            "REGISTER_URL" => "",
            "FORGOT_PASSWORD_URL" => "",
            "PROFILE_URL" => "/personal/profile/",
            "SHOW_ERRORS" => "Y"
        )
    );
   $APPLICATION->IncludeComponent(
        "bitrix:main.register",
        "popup",
        Array(
            "SHOW_FIELDS" => array("EMAIL","NAME", "LAST_NAME", "SECOND_NAME", "PERSONAL_GENDER", "PERSONAL_BIRTHDAY"),
            "REQUIRED_FIELDS" => array("EMAIL","NAME", "PERSONAL_GENDER"),
            "AUTH" => "Y",
            "USE_BACKURL" => "Y",
            "SUCCESS_PAGE" => "",
            "SET_TITLE" => "N",
            "USER_PROPERTY" => array(),
            "USER_PROPERTY_NAME" => "",
            "AJAX_MODE" => "Y",
        )
    );
    die;
} elseif (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Регистрация");
    $APPLICATION->IncludeComponent(
        "bitrix:main.register",
        "",
        Array(
            "SHOW_FIELDS" => array("EMAIL","NAME"),
            "REQUIRED_FIELDS" => array("EMAIL","NAME"),
            "AUTH" => "Y",
            "USE_BACKURL" => "Y",
            "SUCCESS_PAGE" => "",
            "SET_TITLE" => "N",
            "USER_PROPERTY" => array(),
            "USER_PROPERTY_NAME" => "",
            "USE_CAPTCHA" => "N",
            "AJAX_MODE" => "Y",
        )
    );
    $showFooter = true;
}
