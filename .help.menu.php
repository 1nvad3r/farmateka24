<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$aMenuLinks = Array(
    Array(
        "О нас",
        "/about/about-us/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Вакансии",
        "/help/vacancies/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Контакты",
        "/contacts/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Оплата и доставка",
        "/help/payment/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Политика конфиденциальности",
        "/about/privacy-policy/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Пользовательское соглашение",
        "/about/user-rules/",
        Array(),
        Array(),
        ""
    )
);
?>