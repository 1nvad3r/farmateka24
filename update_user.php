<?
	include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	global $USER;
	$ID = $USER->GetID();

	$user = new CUser;
	$arFields = Array(
		"NAME"             => $_REQUEST['REGISTER']['NAME'],
		"SECOND_NAME"             => $_REQUEST['REGISTER']['SECOND_NAME'],
		"LAST_NAME"          => $_REQUEST['REGISTER']['LAST_NAME'],
		"PERSONAL_BIRTHDAY"  => $_REQUEST['REGISTER']['PERSONAL_BIRTHDAY'],
		"PERSONAL_GENDER"  => $_REQUEST['REGISTER']['PERSONAL_GENDER'],
	);



	$user->Update($ID, $arFields);
	$USER->Authorize($ID);

	if ($user->LAST_ERROR)
		echo $user->LAST_ERROR;
	else
		return 'ok';

