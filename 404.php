<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
GLOBAL $WITHOUT_VIEWED;
$WITHOUT_VIEWED="Y";
$APPLICATION->SetPageProperty("section_class", "p404");
$APPLICATION->SetTitle("404");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>


            <img class="p404__img" src="<?=SITE_TEMPLATE_PATH.'/images/icons/p404.png'?>" alt="">
            <h1 class="p404__title">Страница не найдена</h1>
            <a class="p404__link" href="/">Вернуться на главную</a>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>