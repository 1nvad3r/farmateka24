<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	$APPLICATION->IncludeComponent(
		"bitrix:sale.basket.basket.line",
		"ajax",
		[
			"COMPONENT_TEMPLATE" => ".default",
			"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
			"SHOW_NUM_PRODUCTS" => "Y",
			"SHOW_TOTAL_PRICE" => "Y",
			"SHOW_EMPTY_VALUES" => "Y",
			"SHOW_PERSONAL_LINK" => "N",
			"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
			"SHOW_AUTHOR" => "N",
			"PATH_TO_REGISTER" => SITE_DIR . "auth/",
			"PATH_TO_PROFILE" => SITE_DIR . "personal/",
			"SHOW_PRODUCTS" => "Y",
			"POSITION_FIXED" => "N",
			"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
			"PATH_TO_AUTHORIZE" => "",
			"SHOW_REGISTRATION" => "Y",
			"HIDE_ON_BASKET_PAGES" => "Y",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
		]
	);