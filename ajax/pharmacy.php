<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("iblock");
	header('Content-type: application/json');

	function addPharmacy($id)
	{
		global $USER;
		$arUser = CUser::GetByID($USER->GetID())->GetNext();

		$arSelect = ["*", "PROPERTY_*"];
		$arFilter = ["IBLOCK_ID" => 15, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_STORE' => $id];
		$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
		}

		if (!in_array($id, $arFields['USER'])) {
			CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], ['USER' => $USER->GetID()]);
			return true;
		}
	}



	if ($_REQUEST["action"] === "add") {
		$result = addPharmacy($_REQUEST['id']);
	}

	echo json_encode($result);
	die();

