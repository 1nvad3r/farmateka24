<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("iblock");
	header('Content-type: application/json');

	function addAddress($city, $street, $house, $litera, $flat, $front, $floor, $code, $comment, $user)
	{
		$el = new CIBlockElement;
		$PROP = [];
		$PROP[67] = $city;
		$PROP[68] = $street;
		$PROP[69] = $house;
		$PROP[70] = $litera;
		$PROP[71] = $flat;
		$PROP[72] = $front;
		$PROP[73] = $floor;
		$PROP[74] = $code;
		$PROP[75] = $comment;
		$PROP[76] = $user;

		$name = $city.', '.$street.', '.$house;

		$arLoadProductArray = [
			'MODIFIED_BY' => $user,
			'CREATED_BY' => $user,
			'IBLOCK_ID' => 16,
			'PROPERTY_VALUES' => $PROP,
			'NAME' => $name,
			'ACTIVE' => 'Y',
		];

		if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
			echo 'New ID: ' . $PRODUCT_ID;
		} else {
			echo 'Error: ' . $el->LAST_ERROR;
		}
	}

	function deleteAddress($id)
	{
		$el = new CIBlockElement;

		$res = $el->Delete($id);
		return $res;
	}

	$result = true;
	if ($_REQUEST["action"] == "add") {
		$result = addAddress(
			$_REQUEST["city"],
			$_REQUEST["street"],
			$_REQUEST["house"],
			$_REQUEST["litera"],
			$_REQUEST['flat'],
			$_REQUEST['frontDoor'],
			$_REQUEST['floor'],
			$_REQUEST['code'],
			$_REQUEST['comment'],
			$_REQUEST['user']
		);
	}

	if ($_REQUEST["action"] == "delete") {
		$result = deleteAddress($_REQUEST['id']);
	}

	echo json_encode($result);
	die();

