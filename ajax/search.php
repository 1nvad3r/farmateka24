<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("iblock");
	if ($_REQUEST['search']) {
		echo '<ul class="header__search-start-list">';
		$arSelect = ["*", "PROPERTY_*"];

		$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y",
			[
				"LOGIC" => "OR",
				'NAME' => '%' . $_REQUEST['search'] . '%',
				'CODE' => '%' . $_REQUEST['search'] . '%'
			]
		];
		$res = CIBlockElement::GetList(["NAME" => "DESC"], $arFilter, false, ["nPageSize" => 5], $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			echo '<li class="header__search-start-item">
					<a class="header__search-start-link" href="' . $arFields['DETAIL_PAGE_URL'] . '" >' . $arFields['NAME'] . '</a></li>';
		}

		$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_INTER_NAME' => '%' . $_REQUEST['search'] . '%'];
		$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 5], $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			echo '<li class="header__search-start-item">
					<a class="header__search-start-link" href="' . $arFields['DETAIL_PAGE_URL'] . '" >' . $arFields['NAME'] . '</a></li>';
		}

		$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_VENDOR_NAME' => '%' . $_REQUEST['search'] . '%'];
		$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 5], $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			echo '<li class="header__search-start-item">
					<a class="header__search-start-link" href="' . $arFields['DETAIL_PAGE_URL'] . '" >' . $arFields['NAME'] . '</a></li>';
		}




		$SectList = CIBlockSection::GetList(["NAME" => "ASC"], ["IBLOCK_ID" => 4, "ACTIVE" => "Y", 'NAME' => '%' . $_REQUEST['search'] . '%'], false, ["ID", "IBLOCK_ID", "IBLOCK_TYPE_ID", "IBLOCK_SECTION_ID", "CODE", "SECTION_ID", "NAME", "SECTION_PAGE_URL"]);
		while ($SectListGet = $SectList->GetNext()) {
			echo '<li class="header__search-start-item">
					<a class="header__search-start-link" href="' . $SectListGet['SECTION_PAGE_URL'] . '" >' . $SectListGet['NAME'] . '</a></li>';
		}

		if (count($arFields) > 0) {
			echo '<li class="header__search-start-item"><a class="header__search-show-all" href="/catalog/filter/?q=' . $_REQUEST['search'] . '">
			Показать все
		</a></li>';
		}else{
			$idsFailed = '';
			$arFilter = ["IBLOCK_ID" => 18, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_FAILED_REQ' => '%' . $_REQUEST['search'] . '%'];
			$res = CIBlockElement::GetList(["NAME" => "ASC"], $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {

				$arFields = $ob->GetFields();
				$idsFailed = $arFields['NAME'];
				$arrFfff[] = $idsFailed;
			}
			if($arrFfff[0]){
				$_REQUEST['search'] = $arrFfff[0];
				$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", [
					'LOGIC' => 'OR',
					'NAME' => '%' . $arrFfff[0] . '%',
					'CODE' => '%' . $arrFfff[0] . '%',
				]
				];
				$res = CIBlockElement::GetList(["NAME" => "ASC"], $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields = $ob->GetFields();
					$idsff[] = $arFields['ID'];
				}

				$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y",
					[
						'ID' => $idsff
					]
				];
				$res = CIBlockElement::GetList(["NAME" => "DESC"], $arFilter, false, ["nPageSize" => 5], $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields = $ob->GetFields();
					echo '<li class="header__search-start-item">
					<a class="header__search-start-link" href="' . $arFields['DETAIL_PAGE_URL'] . '" >' . $arFields['NAME'] . '</a></li>';
				}
				if (count($idsff) > 0){
					echo '<li class="header__search-start-item"><a class="header__search-show-all" href="/catalog/filter/?q=' . $_REQUEST['search'] . '">
						Показать все
					</a></li>';
				}
			}

		}

		echo '</ul>';
	}