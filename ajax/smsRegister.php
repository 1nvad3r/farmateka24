<?
	include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	global $USER;

	$phone = \Bitrix\Main\UserPhoneAuthTable::normalizePhoneNumber($_REQUEST['phone']);

	d(trim($phone, '+'));

	$user = new CUser;
	$arFields = [
		"EMAIL" => $_REQUEST['email'],
		"LOGIN" => $_REQUEST['login'],
		"ACTIVE" => "Y",
		"GROUP_ID" => [6],
		"PASSWORD" => $_REQUEST['pass'],
		"CONFIRM_PASSWORD" => $_REQUEST['ch_pass'],
		"PHONE_NUMBER" => '+' . $phone,
		"PERSONAL_MOBILE" => '+' . $phone,
		"PERSONAL_PHONE" => trim($phone, '+'),
	];

	$ID = $user->Add($arFields);
	if (intval($ID) > 0)
		echo "Пользователь успешно добавлен.";
	else
		echo $user->LAST_ERROR;

	[$code, $phoneNumber] = CUser::GeneratePhoneCode($ID);

	$sms = new \Bitrix\Main\Sms\Event(
		"SMS_USER_CONFIRM_NUMBER",
		[
			"USER_PHONE" => trim($phoneNumber, '+'),
			"CODE" => $code,
		]
	);
	//$smsResult = $sms->send(true);

	if (!$smsResult->isSuccess()) {
		$arResult["ERRORS"] = array_merge($arResult["ERRORS"], $smsResult->getErrorMessages());
	}

	$USER->Authorize($ID);