<?
	include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	header('Content-type: application/json');
	global $USER;
	use Bitrix\Main\Mail\Event;

	\Bitrix\Main\Mail\Event::sendImmediate([

		"EVENT_NAME" => "USER_PASS_REQUEST",

		"LID" => "s1",

		"C_FIELDS" => [

			"EMAIL" => $_REQUEST['REGISTER']['USER_LOGIN'],

//			"USER_ID" => $new_user_id,

		],

	]);
