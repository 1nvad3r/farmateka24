<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("iblock");

	$SectList = CIBlockSection::GetList(["SORT" => "ASC"], ["IBLOCK_ID" => 4, "ACTIVE" => "Y"], false, ["*", "UF_*"]);

	while ($SectListGet = $SectList->GetNext()) {
		$arParams = ["replace_space" => "-", "replace_other" => "-"];
		$SectListGet['CODE'] = \Cutil::translit($SectListGet['NAME'], "ru", $arParams);

		$bs = new CIBlockSection;

		$arFields = [
			"IBLOCK_ID" => 4,
			"NAME" => $SectListGet['NAME'],
			"IBLOCK_SECTION_ID" => $SectListGet['IBLOCK_SECTION_ID'],
			"CODE" => $SectListGet["CODE"],
		];

		$bs->Update($SectListGet['ID'], $arFields);

		$arResult[] = $SectListGet;
	}