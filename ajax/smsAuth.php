<?php
	include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	global $USER;
	if ($_REQUEST['phone']) {
		$phone = '+' . preg_replace('![^0-9]+!', '', $_REQUEST['phone']);

		$filter = [
			"PERSONAL_MOBILE" => $phone,
		];
		$rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter);

		$arUser = $rsUsers->Fetch();

		if ($arUser) {

			[$code, $phoneNumber] = CUser::GeneratePhoneCode($arUser['ID']);

			$sms = new \Bitrix\Main\Sms\Event(
				"SMS_USER_CONFIRM_NUMBER",
				[
					"USER_PHONE" => $phone,
					"CODE" => $code,
				]
			);

			$smsResult = $sms->send(true);

			if (!$smsResult->isSuccess()) {
				$arResult["ERRORS"] = array_merge($smsResult->getErrorMessages());
			}

			$USER->Authorize($arUser['ID']);
		} else {
			echo 'notFound';
		}
	}