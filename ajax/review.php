<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("iblock");
	header('Content-type: application/json');

	function deleteReview($id)
	{
		$el = new CIBlockElement;

		$res = $el->Delete($id);
		return $res;

	}

	function updateReview($id, $name, $text, $rating, $product)
	{
		$el = new CIBlockElement;

		$PROP = [];
		$PROP[57] = $product;
		$PROP[59] = $name;    // Имя автора
		$PROP[58] = $rating;    // Оценка

		$arLoadProductArray = [
			"PROPERTY_VALUES" => $PROP,
			"DETAIL_TEXT" => $text,
		];

		$res = $el->Update($id, $arLoadProductArray);

		return $res;
	}

	function addReview($product, $rating, $user_name, $text, $user_id)
	{
		$el = new CIBlockElement;

		$PROP = [];
		$PROP[57] = $product;
		$PROP[58] = intval($rating);
		$PROP[59] = $user_name;
		$PROP[61] = $user_id;

		$arLoadProductArray = [
			"IBLOCK_ID" => 14,
			"PROPERTY_VALUES" => $PROP,
			"NAME" => "Отзыв к товару " . $product,
			"ACTIVE" => "N",            // активен
			"DETAIL_TEXT" => $text,
		];

		if ($res = $el->Add($arLoadProductArray))
			return $rating;
		else
			return $el->LAST_ERROR;
	}

	$result = true;
	if ($_REQUEST["action"] == "update") {
		$result = updateReview($_REQUEST["id"], $_REQUEST["name"], $_REQUEST["text"], $_REQUEST["rating"], $_REQUEST['product']);
	}
	if ($_REQUEST["action"] == "delete") {
		$result = deleteReview($_REQUEST['id']);
	}
	if ($_REQUEST['addReview'] == 'Y') {
		$result = addReview($_REQUEST['product'], $_REQUEST['stars'], $_REQUEST['user_name'], $_REQUEST['text'], $_REQUEST['user_id']);
	}
	echo json_encode($result);
	die();
?>