<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-type: application/json');


function addFavorite($id){
    global $USER_FIELD_MANAGER;
    global $USER;
    $userID = $USER->GetID();
    $rsUser = \CUser::GetByID($userID);
    $arUser = $rsUser->Fetch();
    $arProducts = $arUser["UF_FAVORITE_PRODUCTS"];

    if(array_search($id, (array)$arProducts)===false){
        $arProducts[] = $id;
        $result = $USER_FIELD_MANAGER->Update('USER', $USER->GetID(), array('UF_FAVORITE_PRODUCTS' => $arProducts));
    }else{
        $result = true;
    }
    return $result;
}

function deleteFavorite($id){
    global $USER_FIELD_MANAGER;
    global $USER;
    $userID = $USER->GetID();
    $rsUser = \CUser::GetByID($userID);
    $arUser = $rsUser->Fetch();
    $arProducts = $arUser["UF_FAVORITE_PRODUCTS"];

    $index = array_search($id, (array)$arProducts);
    if($index!==false){
        unset($arProducts[$index]);
        $result = $USER_FIELD_MANAGER->Update('USER', $USER->GetID(), array('UF_FAVORITE_PRODUCTS' => $arProducts));
    }else{
        $result = true;
    }
    return $result;
}
$result = true;
if($_REQUEST["action"] == "add"){
    $result = addFavorite($_REQUEST["id"]);
}
if($_REQUEST["action"] == "delete"){
    $result = deleteFavorite($_REQUEST["id"]);
}

echo json_encode($result);
die();
?>