<?
	include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	header('Content-type: application/json');
	global $USER;

//	$arResult = $USER->Register($_REQUEST['REGISTER']['LOGIN'], "", "",  $_REQUEST['REGISTER']['PASSWORD'],  $_REQUEST['REGISTER']['PASSWORD'], $_REQUEST['REGISTER']['LOGIN']);
	//ShowMessage($arResult); // выводим результат в виде сообщения
//	echo $USER->GetID(); // ID нового пользователя
//	echo $ID;
	$user = new CUser;
	$arFields = [
		"LOGIN" => $_REQUEST['REGISTER']['LOGIN'],
		"EMAIL" => $_REQUEST['REGISTER']['LOGIN'],
		"ACTIVE" => "Y",
		"PASSWORD" => $_REQUEST['REGISTER']['PASSWORD'],
		"CONFIRM_PASSWORD" => $_REQUEST['REGISTER']['PASSWORD'],
		"GROUP_ID" => [3, 4, 6],
	];

	$new_user_id = $user->Add($arFields);
	if ($new_user_id)
		\Bitrix\Main\Mail\Event::sendImmediate([

			"EVENT_NAME" => "NEW_USER_CONFIRM",

			"LID" => "s1",

			"C_FIELDS" => [

				"EMAIL" => $_REQUEST['REGISTER']['LOGIN'],

				"USER_ID" => $new_user_id,

			],

		]);
	$USER->Authorize($new_user_id);
	echo $new_user_id;