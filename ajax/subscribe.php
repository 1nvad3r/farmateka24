<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	header('Content-type: application/json');

	$APPLICATION->IncludeComponent(
		"bitrix:sender.subscribe",
		"",
		[
			"COMPONENT_TEMPLATE" => ".default",
			"USE_PERSONALIZATION" => "Y",
			"CONFIRMATION" => "Y",
			"SHOW_HIDDEN" => "Y",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "Y",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "360000",
			"HIDE_MAILINGS" => "Y",
			"SET_TITLE" => "N",
		]
	);
?>