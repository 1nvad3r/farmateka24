<?php
	include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	$session = \Bitrix\Main\Application::getInstance()->getSession();
	global $USER;

	if ($_POST['action'] == "send") {

		$phone = \Bitrix\Main\UserPhoneAuthTable::normalizePhoneNumber($_POST['phone']);
		$code = rand(1111, 9999);
		$session->set('phoneCode', $code);
		$sms = new \Bitrix\Main\Sms\Event(
			'SMS_USER_CONFIRM_NUMBER', // SMS_USER_RESTORE_PASSWORD - для восстановления
			[
				'USER_PHONE' => $phone,
				'CODE' => $code,
			]
		);
		$sms->setSite('s1');
		$sms->setLanguage('ru');
		$sms->send(true);

		header("Content-type: application/json; charset=utf-8");
		echo json_encode($code);

	}

	if ($_POST['action'] == "confirm") {

		$result = [];
		if ($session['phoneCode'] == $_POST["code"]) {

			/*$phone = \Bitrix\Main\UserPhoneAuthTable::normalizePhoneNumber($_POST['phone']);
			*/
			$result["success"] = true;
		} else {
			$result["error"] = "uncorrect";
		}

		header("Content-type: application/json; charset=utf-8");
		echo json_encode($result);

	}
