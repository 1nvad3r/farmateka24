<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Поиск");
?>
	<section class="category-main">
		<div class="container category-main__container">
			<div class="category-main__top-left-wrapper">
				<? $APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter",
					"main",
					[
						"COMPONENT_TEMPLATE" => ".default",
						"IBLOCK_TYPE" => 'catalog',
						"IBLOCK_ID" => '4',
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"FILTER_NAME" => "arrFilter",
						"FILTER_PRICE_CODE" => [
							0 => "BASE",
						],
						"FILTER_PROPERTY_CODE" => [
							0 => "BRAND_NAME",
							1 => "VENDOR_NAME",
							3 => "TKR_NAME",
						],
						"HIDE_NOT_AVAILABLE" => "N",
						"DISPLAY_ELEMENT_COUNT" => "Y",
						"SEF_MODE" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "Y",
						"SAVE_IN_SESSION" => "N",
						"INSTANT_RELOAD" => "Y",
						"PRICE_CODE" => [
							0 => "BASE",
						],
						"SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
						"CURRENCY_ID" => "RUB",
					],
					false
				); ?>
			</div>
			<div class="category-main__main-wrapper">
				<div class="category-main__filters-column js_filters_list">
					<div class="category-main__title-wrapper">
						<h1 class="category-main__title">
							Поиск по запросу “<?= $_REQUEST['q'] ?>”</h1>
						<span class="category-main__subtitle">
                                ( <? $APPLICATION->ShowViewContent("countSearch"); ?> товаров)
                            </span>
					</div>
				</div>
				<div
					class="category-main__product-column category-main__product-column--move-up js_category_main_product-column ">
					<div class="category-main__product-column-top">
						<?
							$arSort = [
								"shows" => [
									"asc" => "По популярности",
								],
								"CATALOG_PRICE_1" => [
									"asc" => "По возрастанию цены",
									"desc" => "По убыванию цены",
								],
							];
							$sort = $_REQUEST["by"];
							if (strlen($sort) > 0) {
								if (!array_key_exists($sort, $arSort)) {
									$sort = key($arSort);
								}
								$_SESSION["SORT"] = $sort;
							} elseif (strlen($_SESSION["SORT"]) > 0) {
								$sort = $_SESSION["SORT"];
							} else {
								$sort = key($arSort);
							}

							$order = $_REQUEST["order"];
							if (strlen($order) > 0) {
								if (!array_key_exists($order, $arSort[$sort])) {
									$order = key($arSort[$sort]);
								}
								$_SESSION["ORDER"] = $order;
							} elseif (strlen($_SESSION["ORDER"]) > 0) {
								$order = $_SESSION["ORDER"];
							} else {
								$order = key($arSort[$sort]);
							}

						?>
						<div class="category-main__product-column-select my-select my-select">
							<select class="js_my_select js_my_select_sort_products_mode" name="select" id="select">
								<? foreach ($arSort as $sortKey => $arOrder): ?>
									<? foreach ($arOrder as $orderKey => $sortName): ?>
										<option value="<?= $sortKey . "/" . $orderKey ?>"
												<? if ($sortKey == $sort && $orderKey == $order): ?>selected<? endif ?>><?= $sortName ?></option>
									<? endforeach ?>
								<? endforeach ?>
								<!--<option value="Акции">Акции</option>
								<option value="Спецпредложения">Спецпредложения</option>
								<option value="Новинки">Новинки</option>-->
							</select>
						</div>

						<button class="category-main__products-by-rows-btn js_category_list_by_row_btn"
								type="submit"></button>
						<button class="category-main__products-by-table-btn js_category_list_by_table_btn"
								type="button"></button>
					</div>
			<?php
				$searchQuery = '';
				global $searchFilter;
				if (isset($_REQUEST['q']) && is_string($_REQUEST['q']))
					$searchQuery = trim($_REQUEST['q']);
				if ($searchQuery !== '') {
					$searchFilter = [
						'*SEARCHABLE_CONTENT' => $searchQuery,
					];
				}

				$arElements = $APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					"search",
					[
						"SHOW_TABS" => "N",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
						"ELEMENT_SORT_FIELD2" => "id",
						"ELEMENT_SORT_ORDER" => "asc",
						"ELEMENT_SORT_ORDER2" => "asc",
						"FILTER_NAME" => "searchFilter",
						"IBLOCK_ID" => "4",
						"IBLOCK_TYPE" => "catalog",
						"PRICE_CODE" => ["BASE"],
						"DISPLAY_SHOW_MORE" => "N",
						"PAGE_ELEMENT_COUNT" => '20',
						"PAGER_TEMPLATE" => "catalog",
					]
				); ?>
					<?
					//d($arElements);
					?>
				</div>
		</div>
		</div>
	</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>