<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подписка");
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"profile_special",
	[
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "content",
		"NEWS_COUNT" => "",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PROPERTY_CODE" => ["CATALOG_LINK"],
	],
	false
); ?>
	<section class="cabinet">
		<?if($_REQUEST['sender_subscription']):?>
		<h1 style="margin-bottom:6rem; min-height: 100px">Поздравляем! Вы успешно подписались на рассылку.</h1>
		<?endif?>
	</section>
<?php
	$arViewed = [];
	$basketUserId = (int)CSaleBasket::GetBasketUserID(false);

	if ($basketUserId > 0) {
		$viewedIterator = \Bitrix\Catalog\CatalogViewedProductTable::getList([
			'select' => ['PRODUCT_ID', 'ELEMENT_ID'],
			'filter' => ['=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID],
			'order' => ['DATE_VISIT' => 'DESC'],
			'limit' => 10,
		]);

		while ($arFields = $viewedIterator->fetch()) {
			if ($arFields['ELEMENT_ID'] != $GLOBALS['elementRemove'])
				$arViewed[] = $arFields['ELEMENT_ID'];
		}
	}

	if (count((array)$arViewed) > 0):
		global $viewedFilter;
		$viewedFilter = ["ID" => $arViewed];

		$APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"",
			[
				"SHOW_TABS" => "N",
				"TITTLE" => "Недавно просмотренные",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER" => "asc",
				"ELEMENT_SORT_ORDER2" => "asc",
				"FILTER_NAME" => "viewedFilter",
				"IBLOCK_ID" => "4",
				"IBLOCK_TYPE" => "catalog",
				"PRICE_CODE" => ["BASE"],
			]
		);
	endif;
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>