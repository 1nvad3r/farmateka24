<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?php $APPLICATION->SetTitle("Лицензии");?>
<?$APPLICATION->AddChainItem("Лицензии", "");?>
<?php $APPLICATION->SetPageProperty("section_class", "about-licenses"); ?>
	<div class="about-licenses__wrap">
		<h1 class="about-licenses__title title-mini">Лицензии</h1>
		<div class="about-licenses__inner">
			<div class="about-licenses__block">
				<h2 class="about-licenses__subtitle">Лицензия на разрешение дистанционной торговли</h2>
				<div class="about-licenses__items">
					<div class="about-licenses__item">
						<a class="about-licenses__item-img" data-fslightbox="licenses1" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-1.jpg">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-1.jpg" alt="Лицензия">
						</a>
					</div>
				</div>
			</div>
			<div class="about-licenses__block">
				<h2 class="about-licenses__subtitle">Лицензия на осуществление фармацевтической деятельности
				</h2>
				<div class="about-licenses__items">
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-2.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-2.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-3.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-3.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-4.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-4.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-5.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-5.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-6.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-6.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-7.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-7.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-8.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-8.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-9.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-9.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-10.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-10.jpg" alt="Лицензия">
						</div>
					</a>
					<a class="about-licenses__item" data-fslightbox="licenses2" href="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-11.jpg">
						<div class="about-licenses__item-img">
							<img src="<?= SITE_TEMPLATE_PATH ?>/images/about/about-licenses-item-img-11.jpg" alt="Лицензия">
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>