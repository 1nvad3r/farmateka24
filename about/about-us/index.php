<?
GLOBAL $WITHOUT_CONTAINER, $HIDEHEAD_BREADCRUMB;
$WITHOUT_CONTAINER = "Y";
$HIDEHEAD_BREADCRUMB = "Y";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О нас");?>
<?$APPLICATION->AddChainItem("О нас", "");?>

    <section class="about-us">
        <div class="about-us__wrap">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
                    "START_FROM" => "0",
                    "SITE_ID" => "s1"
                )
            );?>

            <div class="container">
                <div class="about-us__inner">
                    <div class="about-us__top">
                        <div class="about-us__top-logo">
                            <img src="<?=SITE_TEMPLATE_PATH.'/images/logo.svg'?>" alt="Лого">
                        </div>
                        <p class="about-us__top-descr">больше, чем аптека</p>
                    </div>
                    <div class="about-us__block">
                        <h1 class="about-us__title title-mini">О нас</h1>
                        <div class="about-us__text">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/include/about_us.php"
                                )
                            );?>
                        </div>
                    </div>
                    <div class="about-us__row">
                        <div class="about-us-benefit">
                            <div class="about-us-benefit__img">
                                <img src="<?=SITE_TEMPLATE_PATH.'/images/about/about-us-benefit-img-1.svg'?>">
                            </div>
                            <div class="about-us-benefit__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_block1.php"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="about-us-benefit">
                            <div class="about-us-benefit__img">
                                <img src="<?=SITE_TEMPLATE_PATH.'/images/about/about-us-benefit-img-2.svg'?>">
                            </div>
                            <div class="about-us-benefit__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_block2.php"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="about-us-benefit">
                            <div class="about-us-benefit__img">
                                <img src="<?=SITE_TEMPLATE_PATH.'/images/about/about-us-benefit-img-3.svg'?>">
                            </div>
                            <div class="about-us-benefit__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_block3.php"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="about-us-benefit">
                            <div class="about-us-benefit__img">
                                <img src="<?=SITE_TEMPLATE_PATH.'/images/about/about-us-benefit-img-4.svg'?>">
                            </div>
                            <div class="about-us-benefit__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_block4.php"
                                    )
                                );?>
                            </div>
                        </div>
                    </div>
                    <div class="about-us__block">
                        <h2 class="about-us__title title-mini">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/include/about_us_title-mini.php"
                                )
                            );?>
                        </h2>
                        <div class="about-us__text">
                            <p><?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_text.php"
                                    )
                                );?></p>
                        </div>
                    </div>
                    <div class="about-us__block">
                        <h2 class="about-us__title title-mini">Лицензии</h2>
                        <a href="/about/licenses/" class="about-us__btn btn btn--green">Посмотреть лицензии компании</a>
                    </div>
                    <div class="about-us__row">
                        <div class="about-us-completed">
                            <div class="about-us-completed__val">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info1_val.php"
                                    )
                                );?>
                            </div>
                            <div class="about-us-completed__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info1_desc.php"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="about-us-completed">
                            <div class="about-us-completed__val">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info2_val.php"
                                    )
                                );?>
                            </div>
                            <div class="about-us-completed__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info2_desc.php"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="about-us-completed">
                            <div class="about-us-completed__val">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info3_val.php"
                                    )
                                );?>
                            </div>
                            <div class="about-us-completed__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info3_desc.php"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="about-us-completed">
                            <div class="about-us-completed__val">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info4_val.php"
                                    )
                                );?>
                            </div>
                            <div class="about-us-completed__desc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/about_us_info4_desc.php"
                                    )
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>