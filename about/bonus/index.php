<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
	<?php $APPLICATION->SetTitle("Бонусная система");?>
<?$APPLICATION->AddChainItem("Бонусная система", "");?>
	<?php $APPLICATION->SetPageProperty("section_class", "about-bonus"); ?>
	<div class="about-bonus__inner">
		<div class="about-bonus__item js_btn_popup" data-btn="bonus">
			<div class="about-bonus__item-img">
				<img src="<?= SITE_TEMPLATE_PATH?>/images/about/about-bonus-item-img-1.jpg" alt="Бонусная программа">
			</div>
		</div>
		<div class="about-bonus__item js_btn_popup" data-btn="bonus">
			<div class="about-bonus__item-img">
				<img src="<?= SITE_TEMPLATE_PATH?>/images/about/about-bonus-item-img-2.jpg" alt="Бонусная программа">
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>