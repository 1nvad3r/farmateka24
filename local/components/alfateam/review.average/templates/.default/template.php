<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
?>
<? if ($arParams["SHOW_COUNT"] == "Y"): ?>
	<a href="#reviews" class="product__stars-link">
		<span class="product__stars-text"><?= $arResult["COUNT"] ?> <? echo endingWord($arResult["COUNT"], array('отзыв', 'отзыва', 'отзывов')); ?></span>
<? endif; ?>
		<div class="<?= $arParams["CUSTOM_CLASS"] ?> reviews__rating rating" data-total-value="<?= $arResult["REVIEW"] ?>">
			<div class="rating__item js_rating_item" data-item-value="5"></div>
			<div class="rating__item js_rating_item" data-item-value="4"></div>
			<div class="rating__item js_rating_item" data-item-value="3"></div>
			<div class="rating__item js_rating_item" data-item-value="2"></div>
			<div class="rating__item js_rating_item" data-item-value="1"></div>
		</div>
<? if ($arParams["SHOW_COUNT"] == "Y"): ?>
	</a>
<? endif; ?>