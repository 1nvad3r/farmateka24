<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
  if(CModule::IncludeModule("iblock")){
    $arFilter = array(
      "IBLOCK_ID" => $arParams["IBLOCK_ID"],
      "PROPERTY_PRODUCT_ID" => $arParams["ELEMENT_ID"],
    );
    $arOrder = Array();
    $arSelectFields = Array("ID","NAME","PROPERTY_RATING");

    $rsReview = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelectFields);
	$rating = $count = 0;
    while($arReview = $rsReview->getNext())
    {
      $rating += $arReview["PROPERTY_RATING_VALUE"];
		$count += 1;
    }
    $arResult['COUNT'] = $count;
    if($count != 0){
        $arResult['REVIEW'] = round($rating / $count);
    }else{
        $arResult['REVIEW'] = 0;
    }
  $this->IncludeComponentTemplate();
}
?>