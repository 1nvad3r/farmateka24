<?if($_REQUEST["AJAX_CALL"]=="Y"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	if(CModule::IncludeModule("iblock")){
		$rsUser = CUser::GetByID($USER->GetID());
		$arUser = $rsUser->Fetch();
		$elem = new CIBlockElement;
		$PROP = array();
		$PROP["RATING"] = $_REQUEST["rating"];
		$PROP["PRODUCT_ID"] = $_REQUEST["ELEMENT_ID"];
		if ($arUser["NAME"] && $arUser["LAST_NAME"])
			$name = $arUser["NAME"] . " " . $arUser["LAST_NAME"];
		else
			$name = $arUser["LOGIN"];
		$PROP["USER_NAME"] = $name;
		$PROP["USER_PHOTO"] = $arUser["USER_PHOTO"];
		$PROP["USER_ID"] = $arUser["ID"];

		$arMas = Array(
		  "IBLOCK_SECTION_ID" => false,       
		  "IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
		  "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(),"FULL"),
		  "PROPERTY_VALUES"=> $PROP,
		  "NAME"           => "Отзыв к товару(".$_REQUEST["ELEMENT_ID"].")",
		  "ACTIVE"         => "Y",
		  "DETAIL_TEXT"    => $_REQUEST["text"],

		  );
    if($PRODUCT_ID = $elem->Add($arMas)){
    $arFilter = array(
      "IBLOCK_ID" => $_REQUEST["IBLOCK_ID"],
      "PROPERTY_PRODUCT_ID" => $_REQUEST["ELEMENT_ID"],
    );

    $arOrder = Array();
    $arSelectFields = Array("ID","NAME","PROPERTY_RATING");

    $rsReview = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelectFields);
    $ReviewCount = 0;
    $ReviewCurrent = 0;
    while ($arReview = $rsReview->getNext())
    {
        $ReviewCount += 1;
        $ReviewCurrent += $arReview["PROPERTY_RATING_VALUE"];
    }
    $review = $ReviewCurrent/$ReviewCount;
    CIBlockElement::SetPropertyValues($_REQUEST["ELEMENT_ID"], $_REQUEST["RATING_IBLOCK_ID"], $review, 'RATING'); 
    CIBlockElement::SetPropertyValues($_REQUEST["ELEMENT_ID"], $_REQUEST["RATING_IBLOCK_ID"], $ReviewCount, 'REVIEW_COUNT');
    }
	}
    header('Content-type: application/json');
    echo json_encode($arMas);
}else{
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arFilter = Array(
		"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
		"ID"=>$arParams["ELEMENT_ID"]
	);
	$res = CIBlockElement::GetList(false, $arFilter, false, false, array("NAME", "PROPERTY_BRAND", "DETAIL_PICTURE", "PREVIEW_PICTURE"));
	while($ar_fields = $res->GetNext())
	{
		$arResult["ELEMENT"] = $ar_fields;
	}

	$this->IncludeComponentTemplate();
}
?>
