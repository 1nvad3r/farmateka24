<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!-- ПОП-АП 'ОСТАВИТЬ ОТЗЫВ О ТОВАРЕ' -->
<div class="popup popup--write-a-review js_popup js_write_a_review" data-popup="write-a-review">
    <div class="popup__wrap">
        <button class="popup__exit js_popup_exit" type="button">
            <svg width="24" height="24">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
            </svg>
        </button>
		<form class="popup__form js_popup_form js_popup_report_form" >
            <div class="popup__title">Оставить отзыв о товаре</div>

            <li class="cart__product-item">
                <a class="cart__img-link" href="#">
                    <img class="cart__img" src="<?=!empty($arResult["ELEMENT"]["PREVIEW_PICTURE"])?CFile::GetPath($arResult["ELEMENT"]["PREVIEW_PICTURE"]):SITE_TEMPLATE_PATH.'/images/notimages.png';?>">
                </a>

                <div class="cart__name-wrapper">
                    <a class="cart__product-name" href="#">
                        <?=$arResult["ELEMENT"]["NAME"]?>
                    </a>

                    <div class="cart__product-brend-wrapper">
                            <span class="cart__product-brend-intro">
                                Бренд:
                            </span>

                        <a class="cart__product-brend" href="#">
                            <?=$arResult["ELEMENT"]["PROPERTY_BRAND_VALUE"]?>
                        </a>
                    </div>
                </div>
            </li>
            <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["PRODUCT_IBLOCK_ID"]?>">
            <input type="hidden" name="element" value="<?=$arParams["ELEMENT_ID"]?>" class="js_rating_product">
            <input type="hidden" name="user_id" value="<?=$USER->GetId()?>" class="js_rating_user_id">
			<input type="hidden" name="user_name" value="<?=$USER->GetFirstName()?>" class="js_rating_user_name">
			<input type="hidden" name="addReview" value="Y" class="js_rating_add_review" >

            <div class="product__rating rating js_validation_rating js_active_rating" data-total-value="">
                <input id="review-rating" type="hidden" name="rating" value="">
                <div class="rating__item js_rating_item" data-item-value="5"></div>
                <div class="rating__item js_rating_item" data-item-value="4"></div>
                <div class="rating__item js_rating_item" data-item-value="3"></div>
                <div class="rating__item js_rating_item" data-item-value="2"></div>
                <div class="rating__item js_rating_item" data-item-value="1"></div>
            </div>

            <label class="popup__review-label" for="">
                    <span class="popup__review-label-text">
                        Ваш отзыв
                    </span>

                <textarea class="popup__review-textarea js_write_a_review_input" name="comment" id="" cols="30"
                          rows="10"></textarea>
            </label>

            <button class="popup__btn btn btn--tr
                js_write_a_review_add">Опубликовать</button>
        </form>
    </div>
</div>

<div class="popup popup--change-password  js_popup js_write_a_review_thanks" data-popup="write-a-review-thanks">
    <div class="popup__wrap">
        <button class="popup__exit js_popup_exit" type="button">
            <svg width="24" height="24">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
            </svg>
        </button>
        <form class="popup__form js_popup_form js_popup_report_form">
            <div class="popup__title">Спасибо за ваш отзыв</div>
            <div class="popup__title">Вы помогаете нам стать лучше!</div>


        </form>
    </div>
</div>