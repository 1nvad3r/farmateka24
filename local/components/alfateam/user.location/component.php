<?

use Bitrix\Main\Loader;

if(empty($arParams["DEFAULT_LOCATION"]))
    $arParams["DEFAULT_LOCATION"] = "Москва";

Loader::includeModule('sale');

$res = \Bitrix\Sale\Location\DefaultSiteTable::getList(array(
    'filter' => array('SITE_ID' => SITE_ID, 'LOCATION.NAME.LANGUAGE_ID' => LANGUAGE_ID),
    'order' => array('SORT' => 'asc'),
    'select' => array('*', 'NAME' => 'LOCATION.NAME.NAME', 'ID' => 'LOCATION.ID',)
));
while($item = $res->Fetch())
{
    $arResult["DEFAULTS"][$item["ID"]] = $item;
}

$res = \Bitrix\Sale\Location\LocationTable::getList(array(
    'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID, 'TYPE.CODE' => "CITY"),
    'select' => array('*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
));
while($item = $res->fetch())
{
    if(array_key_exists($item["ID"], $arResult["DEFAULTS"])){
        $item["LOC_DEFAULT"] = "Y";
    }
   $arResult["LIST"][] = $item;
}

global $USER;
if ($USER->IsAuthorized()){
    $userID = $USER->GetID();
    $rsUsers = CUser::GetList(array('sort' => 'asc'), "asc", array("ID"=>$userID), array("SELECT" => array("UF_LOCATION"))); // выбираем пользователей
    if($arUser = $rsUsers->Fetch()){
        $arResult["CURENT"] = $arUser["UF_LOCATION"];
        $arResult["SHOW"] = false;
    }
}else{
    if(!empty($_COOKIE["LOCATION"])){
        $arResult["CURENT"] = $_COOKIE["LOCATION"];
        $arResult["SHOW"] = false;
    }else{
        $arResult["CURENT"] = $arParams["DEFAULT_LOCATION"];
        $arResult["SHOW"] = true;
    }
}


$this->IncludeComponentTemplate();

?>


