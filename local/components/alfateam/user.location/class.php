<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Engine\Contract\Controllerable;

class UserLocation extends \CBitrixComponent implements Controllerable
{

    public function configureActions()
    {
        return [
            'saveLocation' => [
                'prefilters' => [],
            ],
        ];
    }


    public function saveLocationAction($location)
    {
        global $USER_FIELD_MANAGER;
        global $USER;
        global $APPLICATION;
        $userID = $USER->GetID();

        if($userID > 0) {
            $USER_FIELD_MANAGER->Update('USER', $USER->GetID(), array(
                'UF_LOCATION' => $location,
            ));
        }else{
            $APPLICATION->set_cookie("LOCATION", $location);
        }
        return $location;
    }
}

?>