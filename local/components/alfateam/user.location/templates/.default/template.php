<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script type="text/javascript">
	var url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/iplocate/address?ip=";
	var token = "7e22540a4f38a0fdbd4a7df64afc2b394fe45b26";
	var query = "<?= $_SERVER['HTTP_X_FORWARDED_FOR']?>";//"188.162.172.82";

	var options = {
		method: "GET",
		mode: "cors",
		headers: {
			"Content-Type": "application/json",
			"Accept": "application/json",
			"Authorization": "Token " + token
		}
	}
	var dadata;

	fetch(url + query, options)
		.then(response => response.text())
		.then(result => {
			dadata = JSON.parse(result);
			var city = dadata.location.value.replace(/^.{2}/, '');
			$('.js_popup_accept_city_span').text(city);
		})
		.catch(error => console.log("error", error));
</script>
<?php
if($arResult["CURENT"]){
	$city = $arResult["CURENT"];
}else{
	$city = $arParams["DEFAULT_LOCATION"];
}
?>
<div class="header__city">
	<div class="header__city-current js_popup_accept_city_open_btn js_popup_change_city_target_span">
		<?= $city ?>
	</div>
	<!-- ПОП-АП 'Подтвердить город' -->
	<div class="popup-accept-city <?= $arResult["SHOW"] ? "active" : "" ?> js_popup_accept_city">
		<h2 class="popup-accept-city__title">Ваш город <span
					class="popup-accept-city__title-include js_popup_accept_city_span"><?= $arResult["CURENT"] ?></span>
			?</h2>

		<ul class="popup-accept-city__list">
			<li class="popup-accept-city__item">
				<button
						class="popup-accept-city_btn popup-accept-city_btn--ok js_popup_accept_city_ok_btn"
						type="button">
					Да, верно
				</button>
			</li>
			<li class="popup-accept-city__item">
				<button
						class="popup-accept-city_btn popup-accept-city_btn--change js_popup_accept_city_change_btn"
						type="button">
					Изменить город
				</button>
			</li>
		</ul>

		<button class="popup-accept-city__btn-close  js_popup_accept_city_close_btn" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
			</svg>
		</button>
	</div>
</div>

<div class="popup-change-city js_popup_change_city">
	<div class="popup-change-city__wrap">
		<button class="popup-change-city__exit js_popup_change_city_close_btn" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
			</svg>
		</button>
		<form class="popup-change-city__form js_popup_form js_popup_order_form">
			<div class="popup-change-city__title">Укажите свой город</div>

			<div class="popup-change-city__input-wrapper">
				<input class="popup-change-city__input js_popup_change_city_input" id="address" name="address"
					   type="text" placeholder="Поиск города"/>

				<button class="popup-change-city__search-btn" type="button"></button>
			</div>

			<ul class="popup-change-city__cities-list">
				<li class="popup-change-city__city-item accent js_popup_change_city_city_item">
					<?= $arResult["CURENT"] ?>
				</li>
				<? foreach ($arResult["LIST"] as $item): ?>
					<? if ($item["NAME_RU"] == $arResult["CURENT"]) continue; ?>
					<li class="popup-change-city__city-item <?= ($item["LOC_DEFAULT"] == "Y") ? "default_item" : "" ?> js_popup_change_city_city_item" <?= ($item["LOC_DEFAULT"] != "Y") ? "style='display:none;'" : "" ?>>
						<?= $item["NAME_RU"] ?>
					</li>
				<? endforeach; ?>
			</ul>

			<button
					class="popup-change-city__btn popup-change-city__btn--ok btn btn--tr js_popup_change_city_city_btn_ok"
					type="button">Выбрать
			</button>
			<button
					class="popup-change-city__btn popup-change-city__btn--deny btn btn--tr js_popup_change_city_city_btn_deny"
					type="button">Отменить
			</button>
		</form>
	</div>
</div>
