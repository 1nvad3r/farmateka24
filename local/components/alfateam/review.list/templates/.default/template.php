<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="reviews js_reviews <?=($arResult["NavPageNomer"]>1)?"active":""?>" id="reviews">
    <div class="container">
        <div class="reviews__top">
            <div class="reviews__title-wrapper">
                <h3 class="reviews__title">
                    Отзывы
                </h3>

                <span class="reviews__subtitle">
                            <?=$arResult["COUNT"]?> отзывов
                        </span>
            </div>
            <?
            $APPLICATION->IncludeComponent("alfateam:review.average", "", array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ELEMENT_ID" => $arParams["ELEMENT_ID"],
                "PRODUCT_IBLOCK_ID" => $arParams["PRODUCT_IBLOCK_ID"]
            ));
            ?>

            <?if($arResult["COUNT"]>1):?>
            <div class="reviews__select-wrapper">
                        <span class="reviews__select-description">
                            Сортировать по
                        </span>

                <div class="reviews__select my-select">
                    <select class="js_my_select" name="select" id="select">
                        <option value="По популярности">Сначала новые</option>
                        <option value="Сначала дешевле">Сначала старые</option>
                        <option value="Сначала дороже">Сначала положительные</option>
                        <option value="Сначала дороже">Сначала отрицательные</option>
                    </select>
                </div>
            </div>

            <button class="reviews__btn" type="btn"></button>
            <?endif;?>

        </div>

        <ul class="reviews__list">
            <?foreach ($arResult["ITEMS"] as $item):?>
                <li class="reviews__item">
                            <span class="reviews__name">
                                <?=$item["USER_NAME"]?>
                            </span>

                    <date class="reviews__date">
                        <?=$DB->FormatDate($item["DATE"], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY");?>
                    </date>

                    <!--<h4 class="reviews__item-title">
                        Отличный препарат
                    </h4>-->

                    <p class="reviews__text">
                        <?=$item["TEXT"];?>
                    </p>
                </li>
            <?endforeach;?>

        </ul>
        <?if($arResult["COUNT"]>1):?>
        <?=$arResult["NAV_STRING"]?>

        <?endif;?>
        <div class="reviews__btns">

            <?if($arResult["COUNT"]>1):?>
            <button class="reviews__add-btn reviews__add-btn--more js_show_more_reviews <?=($arResult["NavPageNomer"]>1)?"active":""?>" type="button">
                <span class="reviews__link-more">Смотреть все отзывы</span>
                <span class="reviews__link-less">Скрыть отзывы</span>
            </button>
            <?endif;?>

                <button class="reviews__add-btn js_btn_popup" data-btn="<?=$USER->IsAuthorized()?"write-a-review":"log-in"?>" type="button">
                    Написать отзыв
                </button>
            <?if($USER->IsAuthorized()):?>
                <?
                $APPLICATION->IncludeComponent("alfateam:review.add", "", array(
                    "IBLOCK_ID" => $arParams["PRODUCT_IBLOCK_ID"],
                    "ELEMENT_ID" => $arParams["ELEMENT_ID"],
                    "PRODUCT_IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "RATING_IBLOCK_ID" => $arParams["PRODUCT_IBLOCK_ID"]
                ));?>
            <?endif;?>
        </div>
    </div>
</section>