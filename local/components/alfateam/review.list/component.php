<?if(!defined("B_PROLOG_INCLUDED") && isset($_REQUEST["AJAX_CALL"]) && $_REQUEST["AJAX_CALL"]=="Y")
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	
	if(CModule::IncludeModule("iblock")){
		if(strlen($_REQUEST["ACTION"])>0 && $_REQUEST["REVIEW_ID"]>0){ //rating update
			
			if(!is_array($_SESSION["REVIEW_RATING"])){
				$_SESSION["REVIEW_RATING"] = Array();
			}
			$reviewID = $_REQUEST["REVIEW_ID"];

			if(!array_key_exists($reviewID, $_SESSION["REVIEW_RATING"])){
				$_SESSION["REVIEW_RATING"][$reviewID]=true;
				if($_REQUEST["ACTION"]=="plus"){
					$rsProp = CIBlockElement::GetProperty(38, $reviewID, array(), $arFilter = array("CODE"=>"PLUS"));
					if($arProp = $rsProp->Fetch()){
						$cnt = $arProp["VALUE"]+1;
					}
					CIBlockElement::SetPropertyValuesEx($reviewID, 38, array("PLUS" => $cnt));
				}elseif($_REQUEST["ACTION"]=="minus"){
					$rsProp = CIBlockElement::GetProperty(38, $reviewID, array(), $arFilter = array("CODE"=>"MINUS"));
					if($arProp = $rsProp->Fetch()){

						$cnt = $arProp["VALUE"]+1;
					}
					CIBlockElement::SetPropertyValuesEx($reviewID, 38, array("MINUS" => $cnt));
					CIBlock::clearIblockTagCache(38);
				}
			}
			echo ($cnt);

		}elseif(isset($_REQUEST["PAGEN_3"])){ // paginator
			$APPLICATION->IncludeComponent("alfateam:review.list", ".default", array("IBLOCK_ID"=>38, "ELEMENT_ID"=>$_REQUEST["ELEMENT_ID"], "PAGE_COUNT"=>$_REQUEST["PAGE_COUNT"]));
		}
	}

	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
	die();
}
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$IBLOCKID = $arParams["IBLOCK_ID"];
$arParams["IBLOCK_ID"] = intval($arParams["PRODUCT_IBLOCK_ID"]);
$arParams["ELEMENT_ID"] = intval($arParams["ELEMENT_ID"]);
$arParams["PRODUCT_IBLOCK_ID"] = intval($IBLOCKID);

$arNavParams = array(
	"nPageSize" => $arParams["PAGE_COUNT"],
);
$arNavigation = CDBResult::GetNavParams($arNavParams);
if($this->StartResultCache(false,array($arNavigation))){
	if(CModule::IncludeModule("iblock")){

		$userAUTH = $USER->IsAuthorized();

		$arResult['HAS_REVIEW'] = false;
		$rsElement = CIBlockElement::GetList(array("DATE_ACTIVE_FROM"=>"desc"),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"PROPERTY_PRODUCT_ID"=>$arParams["ELEMENT_ID"],"ACTIVE"=>"Y"),false,$arNavParams,array("ID","NAME","DETAIL_TEXT","DATE_ACTIVE_FROM","PROPERTY_RATING","PROPERTY_PLUS","PROPERTY_MINUS","PROPERTY_USER_ID", "PROPERTY_USER_NAME", "PROPERTY_USER_PHOTO"));
		while($arElement = $rsElement->GetNext()){
			$rsUser = CUser::GetByID($arElement["PROPERTY_USER_ID_VALUE"]);
//			if (CUser::GetID() == $arElement["PROPERTY_USER_ID_VALUE"]) {$arResult['HAS_REVIEW'] = true;}
			if($arUser = $rsUser->Fetch()){
				$photo = isset($arUser["PROPERTY_USER_PHOTO_VALUE"])?CFile::GetPath($arUser["PROPERTY_USER_PHOTO_VALUE"]):SITE_TEMPLATE_PATH."/images/nophoto.jpg";
			}
			$arResult["ITEMS"][] = array(
				"ID" => $arElement["ID"],
				"USER_NAME" => $arElement["PROPERTY_USER_NAME_VALUE"],
				"USER_PHOTO" => $photo,
				"TEXT" => $arElement["DETAIL_TEXT"],
				"RATING" => round($arElement["PROPERTY_RATING_VALUE"]),
				"PLUS" => $arElement["PROPERTY_PLUS_VALUE"],
				"MINUS" => $arElement["PROPERTY_MINUS_VALUE"],
//				"VOTED" => (array_key_exists($arElement["ID"], $_SESSION["REVIEW_RATING"]) || !$userAUTH)?1:0,
				"DATE" => $arElement["DATE_ACTIVE_FROM"],
			);
		}

		$arResult["COUNT"] = $rsElement->SelectedRowsCount();

		$rsElement->nPageWindow = 3;
		$arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, "", "reviews");
		$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
		$arResult["NAV_RESULT"] = $rsElement;
		$arResult["PAGEN_END"] = $rsElement->nEndPage;
		$this->SetResultCacheKeys(array(
			"NAV_CACHED_DATA",
			//"RATING",
			"COUNT",
				"PLUS",
				"MINUS"
		));
		$this->IncludeComponentTemplate();
		}
	}
?>
