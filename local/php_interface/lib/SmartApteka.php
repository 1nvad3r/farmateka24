<?php

	namespace lib;

	use \Bitrix\Main\Loader;
	use \Bitrix\Main,
		\Bitrix\Main\Localization\Loc as Loc,
		\Bitrix\Main\Application,
		\Bitrix\Currency,
		\Bitrix\Sale\Delivery,
		\Bitrix\Sale\PaySystem,
		\Bitrix\Sale,
		\Bitrix\Sale\Order,
		\Bitrix\Sale\Affiliate,
		\Bitrix\Sale\DiscountCouponsManager,
		\Bitrix\Main\Context;
		\Bitrix\Main\Loader::includeModule("sale");

	class SmartApteka
	{
		private $reader;
		private $tag;

		public static function pricesParse($file)
		{

			$reader = new \XMLReader();
			$reader->open($_SERVER['DOCUMENT_ROOT'] . $file);
			$step = 1;
			if ($_REQUEST['start']) {
				$start = $_REQUEST['start'];
			} else {
				$start = 0;
			}
			$end = $start + $step;
			$i = 0;

			while ($reader->read()) {

				if ($i >= $start) {
					if ($i <= $end) {
//						echo $i . '<br>';

						if ($reader->nodeType == \XMLReader::ELEMENT) {
							// если находим элемент <card>
							if ($reader->localName == 'R') {
								$priceItem = [];

								$priceItem['PLID'] = $reader->getAttribute('PLID'); // ИД прайс-листа в ПО СмартАптека
								$priceItem['PLDATE'] = $reader->getAttribute('PLDATE');
								$priceItem['SUPPLIER_INN'] = $reader->getAttribute('SUPPLIER_INN');
								$priceItem['SUPPLIER_NAME'] = $reader->getAttribute('SUPPLIER_NAME');
								$priceItem['REGION_CODE'] = $reader->getAttribute('REGION_CODE');
								$priceItem['NUM'] = $reader->getAttribute('NUM');
								$priceItem['MED_ID'] = $reader->getAttribute('MED_ID'); // ID по СмартАптеке
								$priceItem['MED_NAME'] = $reader->getAttribute('MED_NAME');
								$priceItem['VENDOR_NAME'] = $reader->getAttribute('VENDOR_NAME');
								$priceItem['COUNTRY_NAME'] = $reader->getAttribute('COUNTRY_NAME');
								$priceItem['MED_VBARCODE'] = $reader->getAttribute('MED_VBARCODE');
								$priceItem['KIND_NAME'] = $reader->getAttribute('KIND_NAME');
								$priceItem['SPRICE'] = $reader->getAttribute('SPRICE');
								$priceItem['RPRICE'] = $reader->getAttribute('RPRICE');
								$priceItem['VALID_DATE'] = $reader->getAttribute('VALID_DATE');
								$priceItem['PACKQTTY'] = $reader->getAttribute('PACKQTTY');
								$priceItem['MINQTTY'] = $reader->getAttribute('MINQTTY');
								$priceItem['PLQTTY'] = $reader->getAttribute('PLQTTY');

								$reader->read();
								$arRows[] = $priceItem;

								if($_REQUEST['add'] != 'Y') {
									self::importPrices($priceItem, $priceItem['MED_ID']);
								}

								if($_REQUEST['add'] == 'Y'){

										self::addPrices();

								}

							}
						}
					}
				}
			}
		}
		public static function stockParse($file, $stock)
		{

//			$xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . $file);

			$reader = new \XMLReader();

			$reader->open($_SERVER['DOCUMENT_ROOT'] . $file);

			$step = 2000;


			if ($_REQUEST['start']) {
				$start = $_REQUEST['start'];
			} else {
				$start = 0;
			}
			$end = $start + $step;
			$i = 0;

			while ($reader->read()) {

				if ($i >= $start) {
					if ($i <= $end) {
//						echo $i . '<br>';

						if ($reader->nodeType == \XMLReader::ELEMENT) {
							// если находим элемент <card>
							if ($reader->localName == 'ST') {
								$stItem = [];
								// считываем аттрибут number
								$stItem['MED_ID'] = $reader->getAttribute('MED_ID');
								$stItem['MED_NAME'] = $reader->getAttribute('MED_NAME');
								$stItem['IID'] = $reader->getAttribute('IID');
								$stItem['SERIA'] = $reader->getAttribute('SERIA');
								$stItem['SUPPLIER_NAME'] = $reader->getAttribute('SUPPLIER_NAME');
								$stItem['MED_ID'] = $reader->getAttribute('MED_ID');
								$stItem['MED_NAME'] = $reader->getAttribute('MED_NAME');
								$stItem['MED_VBARCODE'] = $reader->getAttribute('MED_VBARCODE');
								$stItem['VENDOR_NAME'] = $reader->getAttribute('VENDOR_NAME');
								$stItem['COUNTRY_NAME'] = $reader->getAttribute('COUNTRY_NAME');
								$stItem['KIND_ID'] = $reader->getAttribute('KIND_ID');
								$stItem['KIND_NAME'] = $reader->getAttribute('KIND_NAME'); //вид товара основной раздел
								$stItem['VENDORBARCODE'] = $reader->getAttribute('VENDORBARCODE');
								$stItem['RPRICE'] = $reader->getAttribute('RPRICE');
								$stItem['PART_ID'] = $reader->getAttribute('PART_ID');
								$stItem['DPRICE'] = $reader->getAttribute('DPRICE');
								$stItem['QTTY'] = $reader->getAttribute('QTTY');
								$stItem['PREPAY_ENABLE'] = $reader->getAttribute('PREPAY_ENABLE');
								$stItem['INTER_ID'] = $reader->getAttribute('INTER_ID');
								$stItem['INTER_NAME'] = $reader->getAttribute('INTER_NAME');
								$stItem['PHARM_ID'] = $reader->getAttribute('PHARM_ID');
								$stItem['PHARM_NAME'] = $reader->getAttribute('PHARM_NAME');
								$stItem['IS_NARC'] = $reader->getAttribute('IS_NARC');
								$stItem['TK_ID'] = $reader->getAttribute('TK_ID');
								$stItem['TK_NAME'] = $reader->getAttribute('TK_NAME'); // Потребительская категория название Третий раздел
								$stItem['TKR_ID'] = $reader->getAttribute('TKR_ID');
								$stItem['TKR_NAME'] = $reader->getAttribute('TKR_NAME'); // Базовая потребительская категория название Второй раздел
								$stItem['ANALOG_ID'] = $reader->getAttribute('ANALOG_ID');
								$stItem['VALID_DATE'] = $reader->getAttribute('VALID_DATE');
								$stItem['NDS'] = $reader->getAttribute('NDS');
								$stItem['GTD'] = $reader->getAttribute('GTD');
								$stItem['LPRICE'] = $reader->getAttribute('LPRICE');
								$stItem['OPRICE'] = $reader->getAttribute('OPRICE');
								$stItem['DIVISOR'] = $reader->getAttribute('DIVISOR');
								$stItem['RQTTY'] = $reader->getAttribute('RQTTY');
								$stItem['BRAND_ID'] = $reader->getAttribute('BRAND_ID');
								$stItem['BRAND_NAME'] = $reader->getAttribute('BRAND_NAME');
								$stItem['MAX_OTP'] = $reader->getAttribute('MAX_OTP');
								$stItem['INVOICE_DATE'] = $reader->getAttribute('INVOICE_DATE');
								$stItem['UK'] = $reader->getAttribute('UK');
								$stItem['JV'] = $reader->getAttribute('JV');
								$stItem['DELIVERY'] = $reader->getAttribute('DELIVERY');
								$stItem['DGO'] = $reader->getAttribute('DGO');
								$stItem['RX'] = $reader->getAttribute('RX');
								// читаем дальше для получения текстового элемента
								$reader->read();
								//$arRows[] = $stItem;
								self::importProduct($stItem, $stock);
							}

							if($reader->localName == 'ORDER'){

								$stItem['ORDER_ID'] = $reader->getAttribute('ORDER_ID');
								$stItem['ORDER_STATE'] = $reader->getAttribute('ORDER_STATE');
								$stItem['END_TIME'] = $reader->getAttribute('END_TIME');
								$stItem['DCARD_OK'] = $reader->getAttribute('DCARD_OK');
								$stItem['COMMENT'] = $reader->getAttribute('COMMENT');
								$stItem['PAYSUM'] = $reader->getAttribute('PAYSUM');

								if ($reader->localName == 'ITEM') {
									$stItem['PRODUCTS'] = [];
									$stItem['PRODUCTS']['IID'] = $reader->getAttribute('IID');
									$stItem['PRODUCTS']['MED_ID'] = $reader->getAttribute('MED_ID');
									$stItem['PRODUCTS']['RPRICE'] = $reader->getAttribute('RPRICE');
									$stItem['PRODUCTS']['DPRICE'] = $reader->getAttribute('DPRICE');
									$stItem['PRODUCTS']['QTTY'] = $reader->getAttribute('QTTY');
								}

								$reader->read();
								$arRows[] = $stItem;
								self::exportOrders($stItem, $stock);
							}

						}
					}

				}
				$i++;
			}

//			echo '<br>'; echo '<br>';
//			self::importProduct($arRows[1211], $stock);

		}

		public static function getFiles($type)
		{

			$dir = '/upload/smartapteka/'; // Папка с xml на сервере
			$f = scandir($_SERVER['DOCUMENT_ROOT'] . $dir);
			if ($type === 'stock') {
				/*foreach ($f as $file) {
					if (preg_match("/^stock_/u", $file)) { // выборка только stock_*.xml
						$arStock = explode('_', str_replace('.xml', '', $file));
						$stock = $arStock[1]; // выбираем код склада

						$filePath = $dir . $file;
						self::stockParse($filePath, $stock);
					}
				}*/
//				$arRows = self::stockParse('/upload/smartapteka/stock_1003.xml', 1003);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1010.xml', 1010);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1011.xml', 1011);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1015.xml', 1015);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1017.xml', 1017);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1018.xml', 1018);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1019.xml', 1019);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1020.xml', 1020);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1023.xml', 1023);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1025.xml', 1025);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1026.xml', 1026);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1027.xml', 1027);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1028.xml', 1028);
			}
			if ($type === 'order'){
				//$arRows = self::stockParse('/upload/smartapteka/stock_1003.xml', 1003);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1010.xml', 1010);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1011.xml', 1011);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1015.xml', 1015);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1017.xml', 1017);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1018.xml', 1018);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1019.xml', 1019);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1023.xml', 1023);
//				$arRows = self::stockParse('/upload/smartapteka/stock_1025.xml', 1025);
			}

			if ($type === 'price'){
				$arRows = self::pricesParse('/upload/smartapteka/ordPrice.xml');
			}
			;
			return $arRows;
		}

		public static function addPrices()
		{
			require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
			if (Loader::includeModule("iblock") && Loader::includeModule("catalog")) {
				$servername = "localhost";
				$username = "root";
				$password = "111111";
				$dbname = "sitemanager";

				$conn = mysqli_connect($servername, $username, $password, $dbname);

				if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
				}

				$sql = "SELECT * FROM OrderPrices";
				$result = mysqli_query($conn, $sql);


				if (mysqli_num_rows($result) > 0) {
					// output data of each row
					while($row = mysqli_fetch_assoc($result)) {
						//echo "id: " . $row["id"]. " - Name: " . $row["MED_ID"]. " " . $row["MED_NAME"]. "<br>";
						require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
						if (Loader::includeModule("iblock") && Loader::includeModule("catalog")) {
							/*$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*"];
							$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "=PROPERTY_MED_ID" => $row["MED_ID"]];
							$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);
							while ($ob = $res->GetNextElement()) {
								$arFields = $ob->GetFields();
								//print_r($arFields);
								$arProps = $ob->GetProperties();
								//print_r($arProps);
							}*/

							/*$el = new CIBlockElement;
							$PROP = array();
							$PROP[12] = "Белый";  // свойству с кодом 12 присваиваем значение "Белый"
							$PROP[3] = 38;        // свойству с кодом 3 присваиваем значение 38
							$arLoadProductArray = Array(
								"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
								"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
								"IBLOCK_ID"      => 18,
								"PROPERTY_VALUES"=> $PROP,
								"NAME"           => "Элемент",
								"ACTIVE"         => "Y",            // активен
								"PREVIEW_TEXT"   => "текст для списка элементов",
								"DETAIL_TEXT"    => "текст для детального просмотра",
								"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
							);
							if($PRODUCT_ID = $el->Add($arLoadProductArray))
								echo "New ID: ".$PRODUCT_ID;
							else
								echo "Error: ".$el->LAST_ERROR;*/
						}

					}
				} else {
					echo "0 results";
				}

			}
		}

		public static function importPrices($priceRow, $medId)
		{
			require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
			if (Loader::includeModule("iblock") && Loader::includeModule("catalog")) {

				$servername = "localhost";
				$username = "root";
				$password = "111111";
				$dbname = "sitemanager";

// Create connection
				$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
				if (!$conn) {
					die("Connection failed: " . mysqli_connect_error());
				}
/*
// sql to create table
				$sql = "CREATE TABLE OrderPrices (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
PLID VARCHAR(30) NOT NULL,
PLDATE VARCHAR(30) NOT NULL,
SUPPLIER_INN VARCHAR(50),
SUPPLIER_NAME VARCHAR(50),
REGION_CODE VARCHAR(50),
NUM VARCHAR(50),
MED_ID VARCHAR(50),
MED_NAME VARCHAR(50),
VENDOR_NAME VARCHAR(50),
COUNTRY_NAME VARCHAR(50),
MED_VBARCODE VARCHAR(50),
KIND_NAME VARCHAR(50),
SPRICE VARCHAR(50),                        
RPRICE VARCHAR(50),
VALUD_DATE VARCHAR(50),
QTTY VARCHAR(50),
PACKQTTY VARCHAR(50),
MINQTTY VARCHAR(50)
)";

				if (mysqli_query($conn, $sql)) {
					echo "Table MyGuests created successfully";
				} else {
					echo "Error creating table: " . mysqli_error($conn);
				}*/

				/*

				$sql = "SELECT * FROM OrderPrices";
				$result = mysqli_query($conn, $sql);

				d(mysqli_num_rows($result));

				if (mysqli_num_rows($result) > 0) {
					// output data of each row
					while($row = mysqli_fetch_assoc($result)) {
						echo "id: " . $row["id"]. " - Name: " . $row["MED_ID"]. " " . $row["MED_NAME"]. "<br>";
					}
				} else {
					echo "0 results";
				}*/

				/*
					PLID, PLDATE, SUPPLIER_INN, SUPPLIER_NAME, REGION_CODE, NUM, MED_ID, MED_NAME, VENDOR_NAME, COUNTRY_NAME, MED_VBARCODE,KIND_NAME, SPRICE, RPRICE, VALUD_DATE, QTTY, PACKQTTY, MINQTTY
				*/

				$sql = "INSERT INTO OrderPrices (PLID, PLDATE, SUPPLIER_INN, SUPPLIER_NAME, REGION_CODE, NUM, MED_ID, MED_NAME, VENDOR_NAME, COUNTRY_NAME, MED_VBARCODE,KIND_NAME, SPRICE, RPRICE, VALUD_DATE, QTTY, PACKQTTY, MINQTTY)
VALUES ('".$priceRow['PLID']."', '".$priceRow['PLDATE']."', '".$priceRow['SUPPLIER_INN']."', '".$priceRow['SUPPLIER_NAME']."', '".$priceRow['REGION_CODE']."', '".$priceRow['NUM']."', '".$priceRow['MED_ID']."', '".$priceRow['MED_NAME']."', '".$priceRow['VENDOR_NAME']."', '".$priceRow['COUNTRY_NAME']."', '".$priceRow['MED_VBARCODE']."', '".$priceRow['KIND_NAME']."', '".$priceRow['SPRICE']."', '".$priceRow['RPRICE']."', '".$priceRow['VALUD_DATE']."', '".$priceRow['QTTY']."', '".$priceRow['PACKQTTY']."', '".$priceRow['MINQTTY']."')";

				if (mysqli_query($conn, $sql)) {
					echo "New record created successfully";
				} else {
					echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}

				mysqli_close($conn);
			}
		}
		public static function importProduct($product, $stock)
		{
			require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

			if (Loader::includeModule("iblock") && Loader::includeModule("catalog")) {
				$arSelect = ["*", "PROPERTIES_*"];
				$arFilter = ["IBLOCK_ID" => 4, "PROPERTY_MED_ID" => $product['MED_ID']];
				$res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
				while($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$arFields['PROPERTIES'] = $ob->GetProperties();
				}

				$arParams = ["replace_space" => "-", "replace_other" => "-"];
				$trans = \Cutil::translit($product['MED_NAME'], "ru", $arParams);

				$el = new \CIBlockElement;

				$PROP = [];

				$PROP[84] = $product['IID'];
				$PROP[85] = $product['MED_ID'];
				$PROP[86] = $product['MED_NAME'];
				$PROP[87] = $product['VENDOR_NAME'];
				$PROP[88] = $product['COUNTRY_NAME'];
				$PROP[89] = $product['KIND_NAME'];
				$PROP[90] = $product['VENDORBARCODE'];
				$PROP[91] = $product['RPRICE'];
				$PROP[92] = $product['DPRICE'];
				$PROP[93] = $product['QTTY'];
				$PROP[94] = $product['PREPAY_ENABLE'];
				$PROP[95] = $product['INTER_ID'];
				$PROP[96] = $product['INTER_NAME'];
				$PROP[97] = $product['PHARM_ID'];
				$PROP[98] = $product['PHARM_NAME'];
				$PROP[99] = $product['TK_ID'];
				$PROP[100] = $product['TK_NAME'];
				$PROP[101] = $product['TKR_ID'];
				$PROP[102] = $product['TKR_NAME'];
				$PROP[103] = $product['IS_NARC'];
				$PROP[104] = $product['ANALOG_ID'];
				$PROP[105] = $product['VALID_DATE'];
				$PROP[106] = $product['SERIA'];
				$PROP[107] = $product['NDS'];
				$PROP[108] = $product['GTD'];
				$PROP[109] = $product['BRAND_ID'];
				$PROP[110] = $product['BRAND_NAME'];
				$PROP[111] = $product['MAX_OTP'];
				$PROP[112] = $product['INVOICE_DATE'];
				$PROP[113] = $product['LPRICE'];
				$PROP[114] = $product['OPRICE'];
				$PROP[115] = $product['MED_VBARCODE'];
				$PROP[116] = $product['DIVISOR'];
				$PROP[117] = $product['RQTTY'];
				$PROP[118] = $product['SUPPLIER_NAME'];
				$PROP[119] = $product['EXT_CODE'];
				$PROP[133] = $product['RX'];

				// Первый уровень
				if ($product['KIND_NAME']) {
//						if find section by name
					$sectionId = \CIBlockSection::GetList([], ['IBLOCK_ID' => 4, 'NAME' => $product['KIND_NAME'], 'DEPTH_LEVEL' => 1])->Fetch()['ID'];
//						if empty section add
					if (!$sectionId) {
						$bs = new \CIBlockSection;
						$arFields = [
							"ACTIVE" => "Y",
							"IBLOCK_ID" => 4,
							"NAME" => $product['KIND_NAME'],
						];

						$sectionId = $bs->Add($arFields);
						if (!$res)
							echo $bs->LAST_ERROR;

					}
				}
				// Второй уровень
				if ($product['TKR_NAME']) {
//						if fi nd section by name
					$tkrSectionId = \CIBlockSection::GetList([], ['IBLOCK_ID' => 4, 'NAME' => $product['TKR_NAME'], 'SECTION_ID' => $sectionId, 'DEPTH_LEVEL' => 2])->Fetch()['ID'];
//						if empty section add
					if (!$tkrSectionId) {
						$bs = new \CIBlockSection;
						$arFields = [
							"ACTIVE" => "Y",
							"IBLOCK_ID" => 4,
							"IBLOCK_SECTION_ID" => $sectionId,
							"NAME" => $product['TKR_NAME'],
						];

						$tkrSectionId = $bs->Add($arFields);
						if (!$res)
							echo $bs->LAST_ERROR;

					}
				}
				// Третий уровень
				if ($product['TK_NAME']) {
//						if find section by name
					$tkSectionId = \CIBlockSection::GetList([], ['IBLOCK_ID' => 4, 'NAME' => $product['TK_NAME'], 'SECTION_ID' => $tkrSectionId, 'DEPTH_LEVEL' => 3])->Fetch()['ID'];
//						if empty section add
					if (!$tkSectionId) {
						$bs = new \CIBlockSection;
						$arFields = [
							"ACTIVE" => "Y",
							"IBLOCK_ID" => 4,
							"IBLOCK_SECTION_ID" => $tkrSectionId,
							"NAME" => $product['TK_NAME'],
						];

						$tkSectionId = $bs->Add($arFields);
						if (!$res)
							echo $bs->LAST_ERROR;

					}
				}
				if($tkSectionId){
					$secId = $tkSectionId;
				}elseif ($tkrSectionId > 0 || empty($tkSectionId)){
					$secId = $tkrSectionId;
				}elseif (empty($tkSectionId) || empty($tkrSectionId)){
					$secId = $sectionId;
				}

				$arLoadProductArray = [
					'MODIFIED_BY' => $GLOBALS['USER']->GetID(), // элемент изменен текущим пользователем
					'IBLOCK_ID' => 4,
					'CODE' => $trans,
					'IBLOCK_SECTION_ID' => $secId,
					'PROPERTY_VALUES' => $PROP,
					'NAME' => $product['MED_NAME'],
					'ACTIVE' => 'Y', // активен
				];

				if ($arFields) {

					//					Обновляем товар
					echo '1 Обновляем товар ' . $arFields['ID'] . ' - ' . $product['RPRICE'] . '<br>';
					$productId = $arFields['ID'];

					if ($res = $el->Update($productId, $arLoadProductArray)) {
						$arFieldsProduct = [
							"ID" => $productId,
							"QUANTITY" => (int)$product['QTTY'],
						];
						echo (int)$product['QTTY'] . '<br/>';
						if (\CCatalogProduct::Update($productId, $arFieldsProduct)) {
							echo "2 Обновили параметры товара к элементу каталога " . $productId . '<br>';

							$dbResult = \CCatalogStore::GetList(
								[],
								['ACTIVE' => 'Y', "PRODUCT_ID" => $productId,  ["=UF_DEP_ID" => $stock]],
								false,
								false,
								["*", "UF_*"]
							);

							$arStockProd = $dbResult->Fetch();

							if ($arStockProd) {
								$arFieldsStockProduct = [
									"PRODUCT_ID" => $productId,
									"STORE_ID" => $arStockProd['ID'],
									"AMOUNT" => (int)$product['QTTY'],
								];

								// обновляем товарное количество данного склада

								$dbResultF = \CCatalogStoreProduct::GetList(
									[],
									['ACTIVE' => 'Y', "PRODUCT_ID" => $productId, "STORE_ID" => $arStockProd['ID']],
									false,
									false,
									["*", "UF_*"]
								);

								$arStockProdF = $dbResultF->Fetch();

								if($arStockProdF){
									$ID = \CCatalogStoreProduct::Update($arStockProd['ID'], $arFieldsStockProduct);
									echo 'update store' . $ID . '<br>';
								} else {
									$ID = \CCatalogStoreProduct::Add($arFieldsStockProduct);
								}

							}

							$arFieldsPrice = [
								"PRODUCT_ID" => $productId,
								"CATALOG_GROUP_ID" => 1,
								"PRICE" => (string)$product['RPRICE'],
								"CURRENCY" => "RUB",
								"QUANTITY_FROM" => false,
								"QUANTITY_TO" => false,
							];

							$res = \CPrice::GetList(
								[],
								[
									"PRODUCT_ID" => $productId,
									"CATALOG_GROUP_ID" => 1,
								]
							);

							$obPrice = new \CPrice();

							if ($arr = $res->Fetch()) {
								$obPrice->Update($arr["ID"], $arFieldsPrice);
								echo 'Обновляем прайс товара' . $arr["ID"] . '<br>';
							} else {
								$priceID = $obPrice->Add($arFieldsPrice, true);
								echo 'Добавляем прайс' . $priceID . '<br>';
							}

						} else {
							echo 'Ошибка добавления параметров<br>';
						}
						return true;
					} else {
						echo 'Error: ' . $el->LAST_ERROR;
					}
				} else {
//					Добавляем товар

					if ($productId = $el->Add($arLoadProductArray)) {
						echo 'New ID: ' . $productId . '<br>';
						$arFields = [
							"ID" => $productId,
						];
						if (\CCatalogProduct::Add($arFields)) {
							echo "Добавили параметры товара к элементу каталога " . $productId . '<br>';

							$dbResult = \CCatalogStore::GetList(
								[],
								['ACTIVE' => 'Y', "PRODUCT_ID" => $productId,  ["=UF_DEP_ID" => $stock]],
								false,
								false,
								["*", "UF_*"]
							);

							$arStockProd = $dbResult->Fetch();

							if ($arStockProd) {
								$arFieldsStockProduct = [
									"PRODUCT_ID" => $productId,
									"STORE_ID" => $arStockProd['ID'],
									"AMOUNT" => (int)$product['QTTY'],
								];

								// обновляем товарное количество данного склада

								$dbResultF = \CCatalogStoreProduct::GetList(
									[],
									['ACTIVE' => 'Y', "PRODUCT_ID" => $productId, "STORE_ID" => $arStockProd['ID']],
									false,
									false,
									["*", "UF_*"]
								);

								$arStockProdF = $dbResultF->Fetch();

								if($arStockProdF){
									$ID = \CCatalogStoreProduct::Update($arStockProd['ID'], $arFieldsStockProduct);
									echo 'update store' . $ID . '<br>';
								} else {
									$ID = \CCatalogStoreProduct::Add($arFieldsStockProduct);
								}
							}

							$arFieldsPrice = [
								"PRODUCT_ID" => $productId,
								"CATALOG_GROUP_ID" => 1,
								"PRICE" => (string)$product['RPRICE'],
								"CURRENCY" => "RUB",
								"QUANTITY_FROM" => false,
								"QUANTITY_TO" => false,
							];

							$res = \CPrice::GetList(
								[],
								[
									"PRODUCT_ID" => $productId,
									"CATALOG_GROUP_ID" => 1,
								]
							);

							$obPrice = new \CPrice();

							if ($arr = $res->Fetch()) {
								$obPrice->Update($arr["ID"], $arFieldsPrice);
								echo 'Обновляем прайс товара' . $arr["ID"] . '<br>';
							} else {
								$priceID = $obPrice->Add($arFieldsPrice, true);
								echo 'Добавляем прайс' . $priceID . '<br>';
							}

						} else {
							echo 'Ошибка добавления параметров<br>';
						}

						return true;
					} else {
						echo 'Error: ' . $el->LAST_ERROR;
					}
				}
			}
		}

		public static function exportOrders($order, $stock){
			$dbRes = \Bitrix\Sale\Order::getList([
				'select' => ['ID'],
				'filter' => [
					"STATUS_ID" =>$statusId, //по статусу
					"PAYED" => "Y", //оплаченные
					"CANCELED" =>"N", //не отмененные
					"PROPERTY.ORDER_PROPS_ID" => $propId, //по свойству
					"PROPERTY.VALUE" => 'значение', //и по его значению
				],
				'order' => ['ID' => 'DESC']
			]);
			while ($order = $dbRes->fetch()){
				var_dump($order);
			}
		}

		public static function importOrders($order, $stock){

		}

	}
