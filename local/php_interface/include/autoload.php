<?php
	\Bitrix\Main\Loader::registerAutoLoadClasses(
		null,
		[
			'lib\SmartApteka' => '/local/php_interface/lib/SmartApteka.php',
			'Dadata\DadataClient' => '/local/php_interface/lib/dadata/src/DadataClient.php',
		]
	);
