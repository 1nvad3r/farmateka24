<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

	use \Bitrix\Main\Loader;
	use \Bitrix\Iblock;

	global $USER;

	$eventManager = \Bitrix\Main\EventManager::getInstance();

	/**
	 * load common module
	 */
	AddEventHandler("main", "OnPageStart", "loadLocalModule", 1);
	function loadLocalModule()
	{
		Loader::includeModule("local.common");
	}

	/**
	 * Loading composer
	 */
	//if (file_exists($_SERVER['DOCUMENT_ROOT'].'/../vendor/autoload.php')) {
	//    require_once $_SERVER['DOCUMENT_ROOT'].'/../vendor/autoload.php';
	//}

	/**
	 * part for event handlers
	 */

	// USER
	// $eventManager->addEventHandler("main", "OnBeforeUserRegister", ["\\Local\\Common\\Handlers\\User", "beforeRegister"]);
	// $eventManager->addEventHandler("main", "OnBeforeUserAdd", ["\\Local\\Common\\Handlers\\User", "beforeAdd"]);
	// $eventManager->addEventHandler("main", "OnBeforeUserLogin", ["\\Local\\Common\\Handlers\\User", "beforeLogin"]);

	//$eventManager->addEventHandler("main", "OnEpilog", ["CDecPage", "handlerOnEpilog"]);

	//Добавляем товарам DESC_ID и Фото РЛС

	AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "SetPropertyHarmony");
	AddEventHandler("iblock", "OnAfterIBlockElementAdd", "SetPropertyHarmony");

	function updateAddress($id, $city, $street, $house, $litera, $flat, $front, $floor, $code, $comment, $user)
	{
		$el = new CIBlockElement;
		$PROP = [];
		$PROP[67] = $city;
		$PROP[68] = $street;
		$PROP[69] = $house;
		$PROP[70] = $litera;
		$PROP[71] = $flat;
		$PROP[72] = $front;
		$PROP[73] = $floor;
		$PROP[74] = $code;
		$PROP[75] = $comment;
		$PROP[76] = $user;

		$name = $city.', '.$street.', '.$house;

		$arLoadAddressArray = [
			'MODIFIED_BY' => $user,
			'CREATED_BY' => $user,
			'IBLOCK_ID' => 16,
			'PROPERTY_VALUES' => $PROP,
			'NAME' => $name,
			'ACTIVE' => 'Y',
		];

		if ($PRODUCT_ID = $el->Update($id, $arLoadAddressArray)) {
			echo 'Update Address ID: ' . $PRODUCT_ID;
		} else {
			echo 'Error: ' . $el->LAST_ERROR;
		}
	}

	if($_REQUEST['change-address'] == 'Y'){
		CModule::IncludeModule("iblock");
		$result = updateAddress(
			$_REQUEST["id"],
			$_REQUEST["city"],
			$_REQUEST["street"],
			$_REQUEST["house"],
			$_REQUEST["korpus"],
			$_REQUEST['flat'],
			$_REQUEST['entrance'],
			$_REQUEST['floor'],
			$_REQUEST['code_house'],
			$_REQUEST['comment'],
			$_REQUEST['user_id']
		);
	}

	function SetPropertyHarmony(&$arFields)
	{
		if ($arFields["IBLOCK_ID"] == 17) {
			if ($arFields['ID']) {
				$medId = $arFields['PROPERTY_VALUES'][120];
				$descId = $arFields['PROPERTY_VALUES'][121];
				$picName = $arFields['PROPERTY_VALUES'][124];
				$packShort = $arFields['PROPERTY_VALUES'][125];
				$prepFull = $arFields['PROPERTY_VALUES'][126];

				if ($arFields['PROPERTY_VALUES'][124]) {
					$photoPath = 'https://rlsaurora10.azurewebsites.net/images/arls/' . $picName;
				}

				if (!empty($medId)) {
					$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*"];
					$arFilter = ["IBLOCK_ID" => 4, "PROPERTY_MED_ID" => $medId];
					$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
					while ($ob = $res->GetNextElement()) {
						$arFieldsProduct = $ob->GetFields();
						$arFieldsProduct['PROPERTIES'] = $ob->GetProperties();
					}

					if ($arFieldsProduct['ID']) {
						// Обновляем товар
						$el = new \CIBlockElement;

						$arLoadProductArray = [
							"DETAIL_PICTURE" => CFile::MakeFileArray($photoPath),
							"PREVIEW_PICTURE" => CFile::MakeFileArray($photoPath),
						];

						$res = $el->Update(IntVal($arFieldsProduct['ID']), $arLoadProductArray);

						// Установим новое значение для данного свойства данного элемента
						CIBlockElement::SetPropertyValuesEx($arFieldsProduct['ID'], false, ["DESC_ID" => $descId, "PICTURE_NAME" => $picName, "PREP_FULL" => $prepFull, "PACKING_SHORT" => $packShort]);

					}
				}

			}
		}
	}

