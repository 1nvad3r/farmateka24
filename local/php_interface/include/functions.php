<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

	use Bitrix\Main\Application,
		Bitrix\Main\Context,
		Bitrix\Main\Request,
		Bitrix\Main\Server;

	function d($var, $die = false)
	{
		echo '<pre>';
		print_r($var);
		echo '</pre>';
		!$die ?: exit;
	}

	function updateProfile()
	{
		global $USER;
		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();

		$email = $request->get("user_email");
		$name = $request->get("user_name");
		$last_name = $request->get("user_last_name");
		$second_name = $request->get("user_second_name");
		if ($request->get("birthday_month")) {
			$birthday = $request->get("birthday_day") . '.' . $request->get("birthday_month") . '.' . $request->get("birthday_year");
		}
		$gender = $request->get("user_gender");
		$phone = $request->get("user_personal_phone");
		if ($phone) {
			$phone = '+' . preg_replace('/\D+/', '', $phone);
		}
		$getNews = $request->get("user_get_news");
		$wayEmail = $request->get("contact_way_email");
		$waySms = $request->get("contact_way_sms");
		$wayCall = $request->get("contact_way_call");

		$ways = [$wayCall, $waySms, $wayEmail];

		$password = $request->get('user_password');

		$user = new CUser;
		$fields = [
			"NAME" => $name,
			"LOGIN" => $email,
			"LAST_NAME" => $last_name,
			"SECOND_NAME" => $second_name,
			"PERSONAL_BIRTHDAY" => $birthday,
			"PERSONAL_GENDER" => $gender,
			"PHONE_NUMBER" => $phone,
			"PERSONAL_MOBILE" => $phone,
			"UF_GET_NEWS" => $getNews,
			"UF_CONTACT_WAYS" => $ways,
			"EMAIL" => $email,
			"PASSWORD" => $password,
			"CONFIRM_PASSWORD" => $password,
		];
		$user->Update($request->get("user_id"), $fields);
		$strError .= $user->LAST_ERROR;
	}

	function formatPhone($phone)
	{
		echo substr($phone, 0, 1);
		echo substr($phone, 1, 1);
		$format =	' (' . substr($phone, 2, 3) . ') ' . substr($phone, 5, 3) . ' ' . substr($phone, 8, 4);
		return $format;
	}

	function endingWord($n, $titles) {
		$cases = array(2, 0, 1, 1, 1, 2);
		return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
	}

	/*if (($_REQUEST['soa-action'] == 'saveOrderAjax') && empty($_REQUEST['ORDER_PROP_2'])) {
		$_REQUEST['ORDER_PROP_2'] = 'user' . mt_rand() . '@pharmateca24.ru';
	}*/