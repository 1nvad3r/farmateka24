<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	use Bitrix\Main\Page\Asset;
	?>
	<!DOCTYPE html>
	<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php $APPLICATION->ShowTitle() ?></title>
		<?php $APPLICATION->ShowHead() ?>
		<?php Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/libs.min.css'); ?>
		<?php Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/choices.min.css'); ?>
		<?php Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/nouislider.min.css'); ?>
		<?php Asset::getInstance()->addString('<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@21.12.0/dist/css/suggestions.min.css" rel="stylesheet" />'); ?>
		<?php Asset::getInstance()->addString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>'); ?>
		<?php Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1.78/?lang=ru_RU"></script>'); ?>
		<?php Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/choices.min.js'); ?>
		<?php Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/libs.min.js'); ?>
		<?php Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/fslightbox.js'); ?>
		<?php Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/slider-initialization.js'); ?>
		<?php Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/nouislider.min.js'); ?>
		<?php Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/main.js'); ?>
		<?php $APPLICATION->AddHeadScript("/bitrix/js/main/ajax.js");?>

		<?php if ($APPLICATION->GetCurPage(false) === '/personal/'): ?>
			<?php $APPLICATION->ShowViewContent('js_pharmacy'); ?>
		<?php endif; ?>
	</head>
<body>
<?php $APPLICATION->ShowPanel();
//d($APPLICATION);
?>
	<!-- ХЕДЕР -->
	<header class="header">
		<div class="header__top">
			<div class="container">
				<div class="header__top-wrap">
					<?php $APPLICATION->IncludeComponent("alfateam:user.location", "",
						[
							"DEFAULT_LOCATION" => "Москва", // Компонент локаций
						]
					); ?>
					<div class="header__top-inner js_header_top_inner">
						<button class="header__icon header__icon-burger js_burger" type="button">
							<svg width="24" height="18">
								<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-burger' ?>"></use>
							</svg>
						</button>
						<div class="header__contacts">
							<ul class="header__social">
								<li>
									<a href="<?= tplvar('twitter'); ?>" target="_blank">
										<svg width="30" height="30">
											<use
													xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-twitter' ?>"></use>
										</svg>
									</a>
								</li>
								<li>
									<a href="<?= tplvar('youtube'); ?>" target="_blank">
										<svg width="30" height="30">
											<use
													xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-youtube' ?>"></use>
										</svg>
									</a>
								</li>
							</ul>
							<a class="header__phone" href="tel:<?= tplvar('phone'); ?>" title="">
								<?= tplvar('phone'); ?>
							</a>
						</div>
						<?php $APPLICATION->IncludeComponent("bitrix:menu", ".default", [
								"ROOT_MENU_TYPE" => "top",
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "top",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_TIME" => "360000",
								"MENU_CACHE_USE_GROUPS" => "Y",
							]
						); ?>
					</div>
					<?php $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd",
						"header",
						[
							"SHOW_ERRORS" => "Y",
						]
					); ?>
					<?php $APPLICATION->IncludeComponent(    // Авторизация в шапке
						"bitrix:system.auth.form",
						"header",
						[
							"REGISTER_URL" => "/auth/index.php?register=yes",
							"FORGOT_PASSWORD_URL" => "",
							"PROFILE_URL" => "/personal/profile/",
							"SHOW_ERRORS" => "Y",
							"HEADER" => "Y",
							"COMPONENT_TEMPLATE" => "header",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						],
						false
					); ?>
					<?php $APPLICATION->IncludeComponent(
						"bitrix:main.register",
						"popup",
						[
							"SHOW_FIELDS" => ["EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_GENDER", "PERSONAL_BIRTHDAY"],
							"REQUIRED_FIELDS" => ["EMAIL", "NAME", "PERSONAL_GENDER"],
							"AUTH" => "Y",
							"USE_BACKURL" => "Y",
							"SUCCESS_PAGE" => "",
							"SET_TITLE" => "N",
							"USER_PROPERTY" => [],
							"USER_PROPERTY_NAME" => "",
							"ajax_mode" => 'Y',
						]
					); ?>
					<!-- ПОП-АП 'ПОДТВЕРЖДЕНИЕ ВЫХОДА ИЗ ЛИЧНОГО КАБИНЕТА' -->
					<div class="popup popup--stop-registration js_popup js_popup_leave" data-popup="leave">
						<div class="popup__wrap">
							<button class="popup__exit js_popup_exit" type="button">
								<svg width="24" height="24">
									<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
								</svg>
							</button>
							<form class="popup__form js_popup_form js_popup_report_form">
								<div class="popup__title">Вы действительно хотите выйти из аккаунта?</div>

								<ul class="popup__stop-btns">
									<li class="popup__stop-btn-item">
										<a href="/?logout=yes&<?= bitrix_sessid_get() ?>"
										   class="popup__btn popup__btn--finish btn btn--tr js_popup_leave_go_away_btn"
										   type="button" title="">Да</a>
									</li>
									<li class="popup__stop-btn-item">
										<button
												class="popup__btn popup__btn--continue btn btn--tr js_popup_leave_stay_here_btn"
												type="button">Нет
										</button>
									</li>
								</ul>
							</form>
						</div>
					</div>
					<?php $APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket.line",
						"mobile",
						[
							"COMPONENT_TEMPLATE" => ".default",
							"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_TOTAL_PRICE" => "Y",
							"SHOW_EMPTY_VALUES" => "Y",
							"SHOW_PERSONAL_LINK" => "N",
							"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
							"SHOW_AUTHOR" => "N",
							"PATH_TO_REGISTER" => SITE_DIR . "auth/",
							"PATH_TO_PROFILE" => SITE_DIR . "personal/",
							"SHOW_PRODUCTS" => "N",
							"POSITION_FIXED" => "N",
							"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
							"PATH_TO_AUTHORIZE" => "",
							"SHOW_REGISTRATION" => "Y",
							"HIDE_ON_BASKET_PAGES" => "Y",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						],
						false
					); ?>
				</div>
			</div>
		</div>
		<div class="header__middle">
			<div class="container">
				<div class="header__middle-wrap">
					<a class="header__logo" href="/" title="На главную">
						<img src="<?= SITE_TEMPLATE_PATH . '/images/logo.svg' ?>" alt="Фарматека">
					</a>
					<?php $APPLICATION->IncludeComponent(
						"bitrix:menu",
						"favorite",
						[
							"ROOT_MENU_TYPE" => "favorite",
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "favorite",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "360000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"USE_EXT" => "Y",
						]
					); ?>
					<div class="header__icons">
						<button class="header__icon js_header_search_btn" type="button">
							<svg width="20" height="20">
								<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-search-mobile' ?>"></use>
							</svg>
						</button>
						<button class="header__icon header__icon-burger js_burger" type="button">
							<svg width="24" height="18">
								<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-burger' ?>"></use>
							</svg>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="header__bottom">
			<div class="container">
				<div class="header__bottom-wrap">
					<a class="header__logo" href="<?= SITE_DIR ?>" title="Главная страница Фарматека">
						<img src="<?= SITE_TEMPLATE_PATH . '/images/logo.svg' ?>" alt="Фарматека">
					</a>
					<div class="header__catalog">
						<?php $APPLICATION->IncludeComponent(
							"bitrix:menu",
							"catalog",
							[
								"ROOT_MENU_TYPE" => "catalog",
								"MAX_LEVEL" => "2",
								"CHILD_MENU_TYPE" => "catalog",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_TIME" => "360000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"USE_EXT" => "Y",
							]
						); ?>
					</div>

					<?php $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						[
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include/headerSearch.php",
						],
						false
					); ?>
					<div class="header-cart-wrap">
					<?php $APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket.line",
						".default",
						[
							"COMPONENT_TEMPLATE" => ".default",
							"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_TOTAL_PRICE" => "Y",
							"SHOW_EMPTY_VALUES" => "Y",
							"SHOW_PERSONAL_LINK" => "N",
							"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
							"SHOW_AUTHOR" => "N",
							"PATH_TO_REGISTER" => SITE_DIR . "auth/",
							"PATH_TO_PROFILE" => SITE_DIR . "personal/",
							"SHOW_PRODUCTS" => "Y",
							"POSITION_FIXED" => "N",
							"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
							"PATH_TO_AUTHORIZE" => "",
							"SHOW_REGISTRATION" => "Y",
							"HIDE_ON_BASKET_PAGES" => "Y",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						],
						false
					); ?>
					</div>
				</div>
			</div>
		</div>
	</header>
<main class="<?php $APPLICATION->ShowProperty("page_class") ?>">
<?php global $WITHOUT_CONTAINER, $HIDEHEAD_BREADCRUMB; ?>
<?php if ($HIDEHEAD_BREADCRUMB != "Y"): ?>
	<?php $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", [
			"START_FROM" => "0",
			"SITE_ID" => "s1",
		]
	); ?>
<?php endif; ?>
<?php if ($APPLICATION->GetCurPage(false) != '/' && $WITHOUT_CONTAINER != "Y"): ?>
<section class="<?php $APPLICATION->ShowProperty("section_class") ?>">
		<div class="container">
<?php endif; ?>