<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
?>

<? global $WITHOUT_CONTAINER, $WITHOUT_VIEWED, $USER; ?>
<? if ($APPLICATION->GetCurPage(false) != '/' && $WITHOUT_CONTAINER != "Y"): ?>
	</div>
	</section>

<? endif; ?>

<?php
	$arViewed = [];
	$basketUserId = (int)CSaleBasket::GetBasketUserID(false);
	if ($basketUserId > 0) {
		$viewedIterator = \Bitrix\Catalog\CatalogViewedProductTable::getList([
			'select' => ['PRODUCT_ID', 'ELEMENT_ID'],
			'filter' => ['=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID],
			'order' => ['DATE_VISIT' => 'DESC'],
			'limit' => 10,
		]);

		while ($arFields = $viewedIterator->fetch()) {
			if ($arFields['ELEMENT_ID'] != $GLOBALS['elementRemove']) {
				$arViewed[] = $arFields['ELEMENT_ID'];
			}
		}
	}

	if ($APPLICATION->GetCurPage(false) != '/' && $WITHOUT_VIEWED != "Y") {
		if (count($arViewed) > 0):
			global $viewedFilter;
			$viewedFilter = ["=ID" => $arViewed];

			$APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"",
			[
				"SHOW_TABS" => "N",
				"TITTLE" => "Недавно просмотренные",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER" => "asc",
				"ELEMENT_SORT_ORDER2" => "asc",
				"FILTER_NAME" => "viewedFilter",
				"IBLOCK_ID" => "4",
				"IBLOCK_TYPE" => "catalog",
				"PRICE_CODE" => ["BASE"],
			]
		);
		endif;
	} ?>
</main>

<footer class="footer">
	<div class="container">
		<div class="footer__wrap">
			<div class="footer__col">
				<a href="/" class="footer__logo">
					<img src="<?= SITE_TEMPLATE_PATH . '/images/logo.svg' ?>" alt="Фарматека">
				</a>
				<div class="footer__contacts">
					<ul class="footer__social">
						<li>
							<a href="<?= tplvar('twitter'); ?>" target="_blank">
								<svg width="40" height="40">
									<use
											xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#footer-twitter' ?>"></use>
								</svg>
							</a>
						</li>
						<li>
							<a href="<?= tplvar('youtube'); ?>" target="_blank">
								<svg width="40" height="40">
									<use
											xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#footer-youtube' ?>"></use>
								</svg>
							</a>
						</li>
					</ul>
					<a class="footer__contact footer__contact-phone" href="tel:<?= tplvar('phone'); ?>">
						<svg width="19" height="20">
							<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#footer-phone' ?>"></use>
						</svg>
						<?= tplvar('phone'); ?>
					</a>
					<a class="footer__contact footer__contact-email" href="mailto:sale@pharmateca24.ru" title="Написать нам">
						<svg width="20" height="19">
							<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#footer-email' ?>"></use>
						</svg>
						sale@pharmateca24.ru
					</a>
				</div>
			</div>
			<div class="footer__col">
				<div class="footer__caption">Каталог</div>
				<? $APPLICATION->IncludeComponent("bitrix:menu", "footer", [
						"ROOT_MENU_TYPE" => "favorite",
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "favorite",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_TIME" => "360000",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"USE_EXT" => "Y",
					]
				); ?>
			</div>
			<div class="footer__col">
				<div class="footer__caption">Помощь</div>
				<? $APPLICATION->IncludeComponent("bitrix:menu", "footer", [
						"ROOT_MENU_TYPE" => "help",
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "help",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_TIME" => "360000",
						"MENU_CACHE_USE_GROUPS" => "Y",
					]
				); ?>
			</div>
			<div class="footer__col">
				<div class="footer__caption">Личный кабинет</div>
				<? global $USER;
					if ($USER->IsAuthorized()):?>
						<a class="footer__auth js_btn_popup" data-btn="leave"
						   href="<?= $APPLICATION->GetCurPageParam("logout=yes&" . bitrix_sessid_get(), [
							   "login",
							   "logout",
							   "register",
							   "forgot_password",
							   "change_password"]); ?>">Выйти</a>
					<? else: ?>
						<a class="footer__auth js_btn_popup" data-btn="log-in" href="#">Вход</a>
						<a class="footer__reg js_btn_popup" data-btn="registration" href="#">Регистрация</a>
					<? endif; ?>
				<address class="footer__address">
					<? $APPLICATION->IncludeComponent("bitrix:main.include", "",
						[
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include/info.php",
							"EDIT_TEMPLATE" => "standard.php",
						], false, ["HIDE_ICONS" => "Y"]
					); ?>
				</address>
				<a class="footer__author" href="https://nologostudio.ru/" target="_blank">
					<img src="<?= SITE_TEMPLATE_PATH . '/images/nologostudio.svg' ?>" alt="Nologostudio">
					<div class="footer__author-text">
						Создание сайта:
						<span>nologostudio.ru</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</footer>
<?php
	global $USER;
	if (!$USER->IsAuthorized()):
		?>
		<div class="cookie js_cookie">
			<div class="container">
				<div class="cookie__wrap">
					<p class="cookie__text">
						Сайт использует файлы cookie для персонализации сервисов и удобства пользователей. Оставаясь на
						нашем сайте, вы соглашаетесь с <a href="/about/privacy-policy/" title="">Политикой обработки
							персональных данных</a> и принимаете условия нашего <a href="/about/user-rules/" title="">Пользовательского
							соглашения</a>. Если вы не хотите
						использовать файлы cookie, измените настройки браузера.
					</p>

					<button class="cookie__btn js_cookie_btn popup__btn-with-loader">
						<span class="popup__btn-text">Принять</span>
						<div class="loader"></div>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php
	/*
	 * Поп-апы для разных страниц
	 */
	if ($APPLICATION->GetCurPage(false) === '/personal/reviews/') {
		$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			[
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include/reviews_popup.php",
			],
			false
		);
	} elseif ($APPLICATION->GetCurPage(false) === '/personal/') {
		$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			[
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include/profile_popup.php",
				"USER_ID" => $USER->GetId(),
			],
			false
		);
	} elseif ($APPLICATION->GetCurPage(false) === '/help/vacancies/') {
		$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			[
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include/vacancies_popup.php",
				"USER_ID" => $USER->GetId(),
			],
			false
		);
	} elseif ($APPLICATION->GetCurPage(false) === '/personal/order/') {
		$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			[
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include/orders.php",
			],
			false
		);
	} elseif (($APPLICATION->GetCurPage(false) === '/contacts/') || ($APPLICATION->GetCurPage(false) === '/feedback/')) {
		$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			[
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include/feedback.php",
			],
			false
		);
	}
?>
</body>
</html>