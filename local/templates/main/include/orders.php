<?$APPLICATION->IncludeComponent(
	"bitrix:sale.personal.order.list",
	"popup",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ALLOW_INNER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"DEFAULT_SORT" => "STATUS",
		"DISALLOW_CANCEL" => "N",
		"HISTORIC_STATUSES" => array("F"),
		"ID" => $ID,
		"NAV_TEMPLATE" => "",
		"ONLY_INNER_FULL" => "N",
		"ORDERS_PER_PAGE" => "20",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_CANCEL" => "",
		"PATH_TO_CATALOG" => "/catalog/",
		"PATH_TO_COPY" => "",
		"PATH_TO_DETAIL" => "",
		"PATH_TO_PAYMENT" => "/personal/order/payment/",
		"REFRESH_PRICES" => "N",
		"RESTRICT_CHANGE_PAYSYSTEM" => array("0"),
		"SAVE_IN_SESSION" => "Y",
		"SET_TITLE" => "N",
		"STATUS_COLOR_CN" => "gray",
		"STATUS_COLOR_F" => "gray",
		"STATUS_COLOR_N" => "green",
		"STATUS_COLOR_P" => "yellow",
		"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
		"STATUS_COLOR_RD" => "gray"
	)
);?>
<!-- ПОП-АП 'ОЦЕНИТЕ КАЧЕСТВО ЗАКАЗА Самовывоз' -->
<div class="popup popup--rate-an-order js_popup js_popup_rate_an_order_pickup" data-popup="rate-an-order-pickup">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Оцените качество заказа</div>

			<div class="popup__order-wrapper">
				<ul class="my-orders__table-titles-list">
					<li class="my-orders__table-title my-orders__table-title--1">
						Номер заказа
					</li>
					<li class="my-orders__table-title  my-orders__table-title--2">
						Дата заказа
					</li>
					<li class="my-orders__table-title  my-orders__table-title--3">
						Статус заказа
					</li>
					<li class="my-orders__table-title  my-orders__table-title--4">
						Товары в заказе
					</li>
				</ul>

				<ul class="my-orders__list">
					<li class="my-orders__item">
						<a class="my-orders__number" href="#">
							№1354/345
						</a>

						<span class="my-orders__date">
                                от 22.10.2022
                            </span>

						<div class="my-orders__status-wrapper">
							<!-- для смены цвета добавь my-orders__status--tomato / --green / --salad / --yellow -->
							<span class="my-orders__status my-orders__status--salad">
                                    Доставлен
                                </span>
						</div>


						<ul class="my-orders__product-list">
							<li class="my-orders__product">
								Венарус 1000 таблетки покрыт.плен.об. 1000 мг, 60 шт.
							</li>
							<li class="my-orders__product">
								Венарус 1000 таблетки покрыт.плен.об. 1000 мг, 60 шт.
							</li>
							<li class="my-orders__product">
								Венарус 1000 таблетки покрыт.плен.об. 1000 мг, 60 шт.
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="popup__bottom">
				<div class="popup__stars-wrapper">
					<label class="personal-data__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="personal-data__clients-title">
                                Оцените качество упаковки заказа:
                            </span>

						<!-- чтобы у пользователя была возможность менять рейтинг добавь класс js_active_rating -->
						<!-- если рейтинг статичен то просто впиши значение в data-total-value="5" -->
						<div class="product__rating rating js_validation_rating_1 js_active_rating"
							 data-total-value="">
							<div class="rating__item js_rating_item" data-item-value="5"></div>
							<div class="rating__item js_rating_item" data-item-value="4"></div>
							<div class="rating__item js_rating_item" data-item-value="3"></div>
							<div class="rating__item js_rating_item" data-item-value="2"></div>
							<div class="rating__item js_rating_item" data-item-value="1"></div>
						</div>
					</label>

					<label class="personal-data__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="personal-data__clients-title">
                                Оценить качество работы фармацевта:
                            </span>

						<!-- чтобы у пользователя была возможность менять рейтинг добавь класс js_active_rating -->
						<!-- если рейтинг статичен то просто впиши значение в data-total-value="5" -->
						<div class="product__rating rating js_validation_rating_2 js_active_rating"
							 data-total-value="">
							<div class="rating__item js_rating_item" data-item-value="5"></div>
							<div class="rating__item js_rating_item" data-item-value="4"></div>
							<div class="rating__item js_rating_item" data-item-value="3"></div>
							<div class="rating__item js_rating_item" data-item-value="2"></div>
							<div class="rating__item js_rating_item" data-item-value="1"></div>
						</div>
					</label>

					<label class="personal-data__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="personal-data__clients-title">
                                Оценить качество сборки заказа:
                            </span>

						<!-- чтобы у пользователя была возможность менять рейтинг добавь класс js_active_rating -->
						<!-- если рейтинг статичен то просто впиши значение в data-total-value="5" -->
						<div class="product__rating rating js_validation_rating_3 js_active_rating"
							 data-total-value="">
							<div class="rating__item js_rating_item" data-item-value="5"></div>
							<div class="rating__item js_rating_item" data-item-value="4"></div>
							<div class="rating__item js_rating_item" data-item-value="3"></div>
							<div class="rating__item js_rating_item" data-item-value="2"></div>
							<div class="rating__item js_rating_item" data-item-value="1"></div>
						</div>
					</label>
				</div>

				<label class="popup__review-label--textarea" for="">
                        <span class="popup__review-label-text">
                            Ваш отзыв
                        </span>

					<textarea class="popup__review-textarea js_write_a_review_input" name="" id="" cols="30"
							  rows="10"></textarea>

					<span class="popup__review-description">
                            500 символов
                        </span>
				</label>
			</div>


			<button class="popup__btn btn btn--tr 
                js_write_a_review_add">Опубликовать</button>
		</form>
	</div>
</div>

<!-- ПОП-АП 'ОЦЕНИТЕ КАЧЕСТВО ЗАКАЗА Благодарность' -->
<div class="popup popup--change-password  js_popup js_popup_rate_an_order_thanks" data-popup="rate-an-order-thanks">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Спасибо за вашу оценку!</div>
			<div class="popup__title">Вы помогаете нам стать лучше!</div>


		</form>
	</div>
</div>