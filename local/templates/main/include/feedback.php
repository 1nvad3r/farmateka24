<!-- ПОП-АП 'СООБЩЕНИЕ УСПЕШНО ОТПРАВЛЕНО' -->
<div class="popup popup--feedback js_popup js_popup_feedback_success" data-popup="feedback-success">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Ваше сообщение успешно отправлено, наш менеджер свяжется с вами в ближайшее время</div>
		</form>
	</div>
</div>
