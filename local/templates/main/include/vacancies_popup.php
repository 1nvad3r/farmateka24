<!-- ПОП-АП 'ОТПРАВИТЬ РЕЗЮМЕ' -->
<div class="popup popup--send-cv js_popup js_popup_send_cv" data-popup="send-cv">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Отправить резюме</div>



			<div class="personal-data__top-label-wrapper">
				<label class="personal-data__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="personal-data__clients-title required">
                            Имя
                        </span>

					<input class="personal-data__client-input js_input_name" type="text">
				</label>

				<label class="personal-data__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="personal-data__clients-title required">
                            Фамилия
                        </span>

					<input class="personal-data__client-input js_input_name js_input_lastname" type="text">
				</label>

				<label class="personal-data__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="personal-data__clients-title required">
                            Email
                        </span>

					<input class="personal-data__client-input js_input_email" type="email">
				</label>

				<label class="personal-data__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="personal-data__clients-title required">
                            Телефон
                        </span>

					<input class="personal-data__client-input js_input_masked_phone" type="text">
				</label>
			</div>

			<label class="popup-cv__input-file-label">
                    <span class="popup-cv__input-title">
                        Загрузите резюме (.dox, .pdf, .jpeg, .png)
                    </span>

				<input class="popup-cv__hidden-file-input
                    js_cv_hidden_files_input" type="file"
					   accept="application/pdf,application/msword,image/jpeg,image/png," multiple>

				<div class="popup-cv__visible-file-input js_cv_visible_files_input">
				</div>
			</label>

			<button class="popup__btn btn js_popup_code_btn btn--tr">Отправить</button>

			<a class="popup-cv__terms" href="#" target="_blank">
				Нажимая кнопку “Отправить” Вы автоматически соглашаетесь на обработку персональных данных.
			</a>
		</form>
	</div>
</div>

<!-- ПОП-АП 'ОТПРАВИТЬ РЕЗЮМЕ УСПЕХ' -->
<div class="popup popup--change-password js_popup js_popup_send_cv_success" data-popup="send-cv-success">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Ваше резюме отправлено</div>
		</form>
	</div>
</div>