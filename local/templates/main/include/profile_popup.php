<!-- ПОП-АП 'ВЫ УВЕРЕНЫ, ЧТО ХОТИТЕ УДАЛИТЬ АДРЕС?' -->
<div class="popup popup--stop-registration js_popup js_popup_delete_address" data-popup="delete-address" data-address="address4">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Вы уверены, что хотите удалить адрес?</div>
			<input type="hidden" class="js_address_id__delete" value="" />
			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--finish btn btn--tr js_ok_btn" type="button">Да</button>
				</li>
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--continue btn btn--tr js_off_btn"
							type="button">Нет</button>
				</li>
			</ul>
		</form>
	</div>
</div>
<!-- ПОП-АП 'ВЫ УВЕРЕНЫ, ЧТО ХОТИТЕ УДАЛИТЬ АДРЕС? УСПЕХ' -->
<div class="popup popup--change-password js_popup js_popup_delete_address_success"
	 data-popup="delete-address-success">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Адрес удален</div>
		</form>
	</div>
</div>
<!-- ПОП-АП 'СОХРАНЕНИЕ АДРЕСА' -->
<div class="popup popup--save-address js_popup js_popup_save_address" data-popup="save-address">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form">
			<div class="popup__title">Сохранение адреса</div>

			<div class="checkout__address-imputs">
				<label class="checkout__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="checkout__clients-title ">
                            Город
                        </span>

					<input class="checkout__client-input js_input_name js_city_input" type="text">
				</label>

				<label class="checkout__client-label checkout__client-label--street" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="checkout__clients-title ">
                            Улица
                        </span>

					<input class="checkout__client-input js_input_name js_street_input" type="text">
				</label>

				<div class="checkout__address-imputs-wrapper">
					<label class="checkout__client-label " for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Дом
                            </span>

						<input class="checkout__client-input js_input_name js_home_input" type="number" min="1">
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Корпус
                            </span>

						<input class="checkout__client-input js_input_name js_home_litera_input" type="text">
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Квартира
                            </span>

						<input class="checkout__client-input js_input_name js_flat_input" type="number" min="1">
					</label>
				</div>

				<div class="checkout__address-imputs-wrapper">
					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Подъезд
                            </span>

						<input class="checkout__client-input js_input_name js_front_door_input" type="text">
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Этаж
                            </span>

						<input class="checkout__client-input js_input_name js_floor_input" type="text">
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Код домофона
                            </span>

						<input class="checkout__client-input js_floor_code_input" type="text">
					</label>
				</div>

				<label class="checkout__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="checkout__clients-title ">
                            Комментарий
                        </span>

					<input class="checkout__client-input js_comment_input" type="text">
				</label>
			</div>
			<input type="hidden" name="user-id" class="user-id" value="<?= $arParams['USER_ID']?>">
			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<button class="popup__btn
                        popup__btn--continue
                        btn btn--tr ">Сохранить</button>
				</li>
				<!-- <li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--cansel-review btn btn--tr js_off_btn"
						type="button">Отменить</button>
				</li> -->
			</ul>
		</form>
	</div>
</div>

<!-- ПОП-АП 'СОХРАНЕННЫЕ АДРЕСА' -->
<div class="popup popup--save-address js_popup js_popup_saved_address" data-popup="saved-address">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form">
			<div class="popup__title">Сохраненные адреса</div>

			<div class="popup__subtitle">Пожалуйста, выберите один из сохраненных адресов для доставки заказа.</div>

			<div class="checkout__address-imputs js_address_card" data-address="address1">
				<label class="checkout__client-label checkout__client-label--street" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="checkout__clients-title">
                            Регион и улица
                        </span>

					<input class="checkout__client-input js_input_name js_input_street" type="text"
						   value="г. Москва, ул. Маршала Малиновского" disabled>
				</label>

				<div class="checkout__address-imputs-wrapper">
					<label class="checkout__client-label required" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Дом
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_home_input" type="number"
							   min="1" value="34" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Корпус
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_home_litera_input"
							   type="text" value="T" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Квартира
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_flat_input" type="number"
							   min="1" value="45" disabled>
					</label>
				</div>

				<div class="checkout__address-imputs-wrapper">
					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Подъезд
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_front_door_input" type="text"
							   value="1" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Этаж
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_floor_input" type="text"
							   value="7" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Код домофона
                            </span>

						<input class="checkout__client-input js_checkout_floor_code_input" type="text" value="777"
							   disabled>
					</label>
				</div>

				<label class="checkout__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="checkout__clients-title ">
                            Комментарий
                        </span>

					<input class="checkout__client-input js_checkout_comment_input" type="text"
						   value="По будним с 9 до 17" disabled>
				</label>

				<ul class="popup__stop-btns">
					<li class="popup__stop-btn-item">
						<button class="popup__btn
                    popup__btn--continue
                    btn btn--tr  js_submit_btn" >Выбрать адрес</button>
					</li>
				</ul>
			</div>

			<div class="checkout__address-imputs js_address_card" data-address="address2">
				<label class="checkout__client-label checkout__client-label--street" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="checkout__clients-title">
                            Регион и улица
                        </span>

					<input class="checkout__client-input js_input_name js_input_street" type="text"
						   value="г. Москва, ул. Свободы" disabled>
				</label>

				<div class="checkout__address-imputs-wrapper">
					<label class="checkout__client-label required" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Дом
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_home_input" type="number"
							   min="1" value="14" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Корпус
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_home_litera_input"
							   type="text" value="F" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Квартира
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_flat_input" type="number"
							   min="1" value="455" disabled>
					</label>
				</div>

				<div class="checkout__address-imputs-wrapper">
					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Подъезд
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_front_door_input" type="text"
							   value="2" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title">
                                Этаж
                            </span>

						<input class="checkout__client-input js_input_name js_checkout_floor_input" type="text"
							   value="8" disabled>
					</label>

					<label class="checkout__client-label" for="">
						<!-- если поле обязательно добавть класс required - добавится звездочка -->
						<span class="checkout__clients-title ">
                                Код домофона
                            </span>

						<input class="checkout__client-input js_checkout_floor_code_input" type="text" value="8777"
							   disabled>
					</label>
				</div>

				<label class="checkout__client-label" for="">
					<!-- если поле обязательно добавть класс required - добавится звездочка -->
					<span class="checkout__clients-title ">
                            Комментарий
                        </span>

					<input class="checkout__client-input js_checkout_comment_input" type="text"
						   value="По будним с 10 до 17" disabled>
				</label>

				<ul class="popup__stop-btns">
					<li class="popup__stop-btn-item">
						<button class="popup__btn
                    popup__btn--continue
                    btn btn--tr  js_submit_btn">Выбрать адрес</button>
					</li>
				</ul>
			</div>


		</form>
	</div>
</div>

<!-- ПОП-АП 'ВЫ УВЕРЕНЫ, ЧТО ХОТИТЕ УДАЛИТЬ АПТЕКУ?' -->
<div class="popup popup--stop-registration js_popup js_popup_delete_pharmacy" data-popup="delete-pharmacy" data-pharmacy="pharmacy4">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Вы уверены, что хотите удалить аптеку?</div>
			<input type="hidden" class="js_pharmacy_id__delete" value="" />
			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--finish btn btn--tr js_ok_btn" type="button">Да</button>
				</li>
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--continue btn btn--tr js_off_btn"
							type="button">Нет</button>
				</li>
			</ul>
		</form>
	</div>
</div>
<!-- ПОП-АП 'ВЫ УВЕРЕНЫ, ЧТО ХОТИТЕ УДАЛИТЬ АПТЕКУ? УСПЕХ' -->
<div class="popup popup--change-password js_popup js_popup_delete_pharmacy_success"
	 data-popup="delete-pharmacy-success">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Аптека удалена</div>
		</form>
	</div>
</div>
<!-- ПОП-АП 'ДОБАВЛЕНИЕ АПТЕКИ' -->
<div class="popup popup--add-pharmacy js_popup js_popup_add_pharmacy" data-popup="add-pharmacy">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form">
			<label class="checkout__search-address" for="">
                    <span class="checkout__search-title">
                        Поиск по улице или названию аптеки
                    </span>

				<input class="checkout__search-input js_popup_address_input" type="text">
				<input type="hidden" class="js_pharmacy_id" value=""/>
			</label>

			<div class="popup__map js_checkout_map" id="map9"></div>

			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<button class="popup__btn
                            popup__btn--continue
                            btn btn--tr js_popup_address_submit_btn js_popup_pharmacy_submit_btn" type="button">Сохранить</button>
				</li>
			</ul>
		</form>
	</div>
</div>
<!-- ПОП-АП 'СОХРАНЕННЫЕ АПТЕКИ' -->
<div class="popup popup--select-saved-pharmacy  js_popup js_popup_select_saved_pharmacy"
	 data-popup="select-saved-pharmacy">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form">
			<div class="popup__title">Сохраненные аптеки</div>
			<div class="popup__subtitle">Пожалуйста, выберите одну из сохраненных аптек.</div>

			<div class="popup__pharmacy-card js_pharmacy_card" data-pharmacy="pharmacy6">
				<div class="big-balloon">
					<h3 class="big-balloon__address-title">
						Новые Ватутники
					</h3>

					<span class="big-balloon__address-text js_target_address">
                            г. 1Ватутинки, 1-ая Нововатутинская улица д.3
                        </span>

					<div class="big-balloon__address-open-time-row">
                            <span class="big-balloon__address-open-days">
                                Понедельник-Воскресенье
                            </span>

						<span class="big-balloon__address-open-time">
                                08:00-23:00
                            </span>
					</div>

					<a class="big-balloon__address-phone" href="tel:84955414585">
						+7 495 541-45-85
					</a>
				</div>

				<li class="popup__stop-btn-item">
					<button class="popup__btn
                            popup__btn--continue
                            btn btn--tr js_submit_btn">Выбрать аптеку</button>
				</li>
			</div>

			<div class="popup__pharmacy-card js_pharmacy_card" data-pharmacy="pharmacy9">
				<div class="big-balloon">
					<h3 class="big-balloon__address-title">
						Новые Ватутники
					</h3>

					<span class="big-balloon__address-text js_target_address">
                            г. 2Ватутинки, 1-ая Нововатутинская улица д.3
                        </span>

					<div class="big-balloon__address-open-time-row">
                            <span class="big-balloon__address-open-days">
                                Понедельник-Воскресенье
                            </span>

						<span class="big-balloon__address-open-time">
                                08:00-23:00
                            </span>
					</div>

					<a class="big-balloon__address-phone" href="tel:84955414585">
						+7 495 541-45-85
					</a>
				</div>

				<li class="popup__stop-btn-item">
					<button class="popup__btn
                            popup__btn--continue
                            btn btn--tr js_submit_btn">Выбрать аптеку</button>
				</li>
			</div>

		</form>
	</div>
</div>
<!-- ПОП-АП 'СОХРАНИТЬ АПТЕКУ' -->
<div class="popup popup--select-saved-pharmacy  js_popup js_popup_save_pharmacy" data-popup="save-pharmacy" data-pharmacy="pharmacy4">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form">
			<div class="popup__title">Сохраненные аптеки</div>
			<div class="popup__subtitle">Сохраненные аптеки позволяют быстрее оформлять самовывоз заказов. Вы в
				любое время сможете изменить сохранную аптеку в личном кабинете. Также в
				<a class="popup__subtitle-link" href="#">личном кабинете</a>
				есть возможность удалить аптеку или добавить еще одну.</div>

			<div class="popup__pharmacy-card js_pharmacy_card">
				<div class="big-balloon">
					<h3 class="big-balloon__address-title">
						Новые Ватутники
					</h3>

					<span class="big-balloon__address-text js_target_address">
                            г. 1Ватутинки, 1-ая Нововатутинская улица д.3
                        </span>

					<div class="big-balloon__address-open-time-row">
                            <span class="big-balloon__address-open-days">
                                Понедельник-Воскресенье
                            </span>

						<span class="big-balloon__address-open-time">
                                08:00-23:00
                            </span>
					</div>

					<a class="big-balloon__address-phone" href="tel:84955414585">
						+7 495 541-45-85
					</a>
				</div>

				<li class="popup__stop-btn-item">
					<button class="popup__btn
                            popup__btn--continue
                            btn btn--tr js_submit_btn">Сохранить выбранную аптеку</button>
				</li>
			</div>

		</form>
	</div>
</div>

<!-- ПОП-АП 'ИЗМЕНИТЬ ПАРОЛЬ' -->
<div class="popup popup--update-password js_popup js_popup_update_password" data-popup="update-password">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form">
			<div class="popup__title">Изменить пароль</div>

			<div class="popup__fields">
				<div class="popup__field field">
					<input class="js_input_password" type="password" placeholder="Старый пароль">
				</div>
			</div>

			<div class="popup__fields">
				<div class="popup__field field">
					<input class="js_input_password js_input_new_password_1" type="password"
						   placeholder="Новый пароль">
				</div>
			</div>

			<div class="popup__fields">
				<div class="popup__field field">
					<input class="js_input_password js_input_new_password_2" type="password"
						   placeholder="Повторить новый пароль">
				</div>
			</div>

			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<button class="popup__btn
                        popup__btn--continue
                        btn btn--tr ">Сохранить</button>
				</li>
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--cansel-review btn btn--tr js_off_btn"
							type="button">Отменить</button>
				</li>
			</ul>
		</form>
	</div>
</div>
<!-- ПОП-АП 'ПАРОЛЬ УСПЕШНО ИЗМЕНЕН' -->
<div class="popup popup--change-password js_popup js_popup_update_password_success"
	 data-popup="update-password-success">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Пароль успешно изменен</div>
		</form>
	</div>
</div>
<!-- ПОП-АП 'ДАННЫЕ УСПЕШНО ИЗМЕНЕНЫ' -->
<div class="popup popup--change-password js_popup js_popup_update_info_success" data-popup="update-info-success">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Ваши данные успешно изменены</div>
		</form>
	</div>
</div>