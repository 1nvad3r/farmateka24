<!-- ПОП-АП 'ОСТАВИТЬ ОТЗЫВ О ТОВАРЕ Заполненный отзыв' -->
<div class="popup popup--write-a-review popup--write-a-review-done js_popup js_write_a_review_done"
	 data-popup="write-a-review-done">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>

		<form class="popup__form js_popup_form">
			<div class="popup__title">Оставить отзыв</div>

			<label class="personal-data__client-label" for="">
				<!-- если поле обязательно добавть класс required - добавится звездочка -->
				<span class="personal-data__clients-title">
                        Ваша оценка
                    </span>

				<!-- чтобы у пользователя была возможность менять рейтинг добавь класс js_active_rating -->
				<!-- если рейтинг статичен то просто впиши значение в data-total-value="5" -->
				<div class="product__rating rating js_validation_rating js_active_rating" data-total-value="4">
					<div class="rating__item js_rating_item" data-item-value="5"></div>
					<div class="rating__item js_rating_item" data-item-value="4"></div>
					<div class="rating__item js_rating_item" data-item-value="3"></div>
					<div class="rating__item js_rating_item" data-item-value="2"></div>
					<div class="rating__item js_rating_item" data-item-value="1"></div>
				</div>
			</label>

			<label class="personal-data__client-label personal-data__client-label--name" for="">
				<!-- если поле обязательно добавть класс required - добавится звездочка -->
				<span class="personal-data__clients-title">
                        Имя
                    </span>

				<input class="personal-data__client-input js_input_name" type="text" value="Иван">
			</label>

			<label class="popup__review-label" for="">
                    <span class="popup__review-label-text">
                        Ваш отзыв
                    </span>

				<textarea class="popup__review-textarea js_write_a_review_input" name="" id="2" cols="30" rows="10">Товар великолепен! Все советую двойную дозу!
                    </textarea>
			</label>
			<input type="hidden" name="review-id" class="review-id" value="">
			<input type="hidden" name="review-product-id" class="review-product-id" value="">

			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--continue btn btn--tr popup__btn-with-loader js_yes_btn">
						<span class="popup__btn-text">Опубликовать</span>
						<? /*<div class="loader"></div>*/ ?>
					</button>
				</li>
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--cansel-review btn btn--tr js_cansel_btn"
							type="button">Отменить
					</button>
				</li>
			</ul>
		</form>
	</div>
</div>

<!-- ПОП-АП 'ОСТАВИТЬ ОТЗЫВ О ТОВАРЕ - Благодарность' -->
<div class="popup popup--change-password  js_popup js_write_a_review_thanks" data-popup="write-a-review-thanks">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Спасибо за ваш отзыв</div>
			<div class="popup__title">Вы помогаете нам стать лучше!</div>


		</form>
	</div>
</div>

<!-- ПОП-АП 'ВЫ ДЕЙСТВИТЕЛЬНО ХОТИТЕ УДАЛИТЬ ОТЗЫВ?' -->
<div class="popup popup--stop-registration js_popup js_popup_delete_review" data-popup="delete-review">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Вы действительно хотите удалить отзыв?</div>
			<input type="hidden" class="review-id" name="review-id" value="">
			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn-with-loader popup__btn--finish btn btn--tr js_ok_btn"
							type="button">
						<span class="popup__btn-text">Да</span>
					</button>
				</li>
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--continue btn btn--tr js_off_btn"
							type="button">Нет
					</button>
				</li>
			</ul>
		</form>
	</div>
</div>

<!-- ПОП-АП 'ВЫ ДЕЙСТВИТЕЛЬНО ХОТИТЕ УДАЛИТЬ ОТЗЫВ? УСПЕХ' -->
<div class="popup popup--change-password js_popup js_popup_delete_review_success"
	 data-popup="delete-review-success">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Ваш отзыв успешно удален !</div>
		</form>
	</div>
</div>

<!-- ПОП-АП 'ВЫ ДЕЙСТВИТЕЛЬНО ХОТИТЕ УДАЛИТЬ ОТЗЫВ? НЕ УСПЕХ' -->
<div class="popup popup--change-password js_popup js_popup_delete_review_fail" data-popup="delete-review-fail">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Что-то пошло не так, пожалуйста, попробуйте позже.</div>
		</form>
	</div>
</div>
