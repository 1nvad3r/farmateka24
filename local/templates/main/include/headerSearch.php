<form id="search" action="/catalog/filter/" class="header__search js_header_search" id="search_box">
	<div class="header__search-field">
		<input id="title-search-input" class="js_header_search_input" type="text" name="q"
			   value="<?= htmlspecialcharsbx($_REQUEST["q"]) ?>" autocomplete="off">
		<span class="js_header_search_input-placeholder">По названию или веществу, например <b>Аквадетрим</b>, <b>Акваоптик</b></span>
		<button>
			<svg width="30" height="30">
				<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-search' ?>"></use>
			</svg>
		</button>
	</div>
	<div class="header__search-wrapper js_header_search_list " id="search_box-result"></div>
</form>

<script type="text/javascript">
	$(document).ready(function () {
		var $result = $('#search_box-result');

		$('#title-search-input').on('keyup', function () {
			var search = $(this).val();
			if ((search != '') && (search.length > 1)) {
				$.ajax({
					type: "POST",
					url: "/ajax/search.php",
					data: {'search': search},
					success: function (msg) {
						$result.html(msg);
						$result.addClass('active');
						console.log(msg);
						if (msg != '') {
							$result.fadeIn();
						} else {
							$result.fadeOut(100);
						}
					}
				});
			} else {
				$result.html('');
				$result.fadeOut(100);
			}
		});

		$(document).on('click', function (e) {
			if (!$(e.target).closest('.search_box').length) {
				$result.html('');
				$result.fadeOut(100);
			}
		});

		$(document).on('click', '.search_result-name a', function () {
			$('#title-search-input').val($(this).text());
			$result.fadeOut(100);
			return false;
		});

		$(document).on('click', function (e) {
			if (!$(e.target).closest('.search_box').length) {
				$result.html('');
				$result.fadeOut(100);
			}
		});
	});
</script>