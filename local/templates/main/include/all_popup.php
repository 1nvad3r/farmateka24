<!-- ПОП-АП 'ТОВАР ДОБАВЛЕН В КОРЗИНУ' -->
<div class="popup popup--added-to-cart js_popup js_added_to_card" data-popup="added-to-card">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<div class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Товар добавлен в корзину!</div>

			<div class="cart__product-list-box">
				<?php $APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket.line",
						"ajax",
						[
							"COMPONENT_TEMPLATE" => ".default",
							"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_TOTAL_PRICE" => "Y",
							"SHOW_EMPTY_VALUES" => "Y",
							"SHOW_PERSONAL_LINK" => "N",
							"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
							"SHOW_AUTHOR" => "N",
							"PATH_TO_REGISTER" => SITE_DIR . "auth/",
							"PATH_TO_PROFILE" => SITE_DIR . "personal/",
							"SHOW_PRODUCTS" => "Y",
							"POSITION_FIXED" => "N",
							"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
							"PATH_TO_AUTHORIZE" => "",
							"SHOW_REGISTRATION" => "Y",
							"HIDE_ON_BASKET_PAGES" => "Y",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
						]
					); ?>
			</div>

			<ul class="popup__stop-btns">
				<li class="popup__stop-btn-item">
					<a class="popup__btn popup__btn--accent btn btn--tr"
					   href="/personal/cart/">Перейти в корзину</a>
				</li>
				<li class="popup__stop-btn-item">
					<button class="popup__btn popup__btn--continue btn btn--tr js_added_to_card_continue_shoping"
							type="button">Продолжить покупки
					</button>
				</li>
			</ul>

			<!--Полезные товары -->
			<?php
				global $novFilter;
				$novFilter = ["PROPERTY_USEFUL" => "22"]; ?>
			<?php $APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				"useful",
				[
					"SHOW_TABS" => "N",
					"TITTLE" => "Товары, которые будут Вам полезны",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_ORDER2" => "asc",
					"FILTER_NAME" => "novFilter",
					"IBLOCK_ID" => "4",
					"IBLOCK_TYPE" => "catalog",
					"PRICE_CODE" => ["BASE"],
					"DISPLAY_SHOW_MORE" => "N",
//					"SHOW_MORE_LINK" => "/catalog/list/news/",
				]
			); ?>
		</div>
	</div>
</div>