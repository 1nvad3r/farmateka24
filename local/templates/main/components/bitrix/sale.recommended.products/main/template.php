<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count((array)$arResult["ITEMS"])>0):?>

<section class="slider" id="recomendations">
    <div class="container">
        <div class="slider__wrap">
            <h2 class="slider__title title">С этим товаром покупают</h2>
            <div class="slider__inner js_slider_popular_small">
                <div class="slider__swiper swiper">
                    <div class="swiper-wrapper">
                        <?foreach ($arResult["ITEMS"] as $item):?>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:catalog.element",
                                "",
                                Array(
                                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                    "IBLOCK_ID" =>  $arParams["IBLOCK_ID"],
                                    "ELEMENT_ID" => $item["ID"],
                                    "SHOW_DEACTIVATED" => $arParams["SHOW_DEACTIVATED"],
                                    "SET_TITLE" => "N",
                                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    "PRICE_CODE" => $arParams["PRICE_CODE"]
                                )
                            );?>
                        <?endforeach;?>
                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
</section>
<?endif;?>