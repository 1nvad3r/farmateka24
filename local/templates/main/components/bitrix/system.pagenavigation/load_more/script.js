$(document).ready(function(){
    $(document).on('click', '.about-articles__btn', function(){
        var targetContainer = $('.about-articles__grid'),
            url =  $('.about-articles__btn').attr('data-url');
        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){
                    $('.about-articles__btn').remove();
                    var elements = $(data).find('.articles__card'),
                        pagination = $(data).find('.about-articles__btn');
                    targetContainer.append(elements);
                    targetContainer.after(pagination);
                }
            })
        }
    });
});