<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>


<ul class="category-main__pagination-list js_pagination_list">
    <?if($arResult["nStartPage"] > 1):?>
        <?if($arResult["bSavePage"]):?>
            <li class="category-main__pagination-item">
                <a class="category-main__pagination-link  js_pagination_btn" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a>
            </li>
            <?if($arResult["nStartPage"] > 2):?>
                <li class="category-main__pagination-item">
                    <span class="category-main__pagination-link">...</span>
                </li>
            <?endif;?>
        <?else:?>
            <li class="category-main__pagination-item">
                <a class="category-main__pagination-link  js_pagination_btn" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
            </li>
            <?if($arResult["nStartPage"] > 2):?>
                <li class="category-main__pagination-item">
                    <span class="category-main__pagination-link">...</span>
                </li>
            <?endif;?>
        <?endif?>
    <?endif;?>
    <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

        <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <li class="category-main__pagination-item">
                <a class="category-main__pagination-link  js_pagination_btn  active" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
            </li>
        <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
            <li class="category-main__pagination-item">
                <a class="category-main__pagination-link  js_pagination_btn" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
            </li>
        <?else:?>
            <li class="category-main__pagination-item">
                <a class="category-main__pagination-link  js_pagination_btn" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
            </li>
        <?endif?>
        <?$arResult["nStartPage"]++?>
    <?endwhile?>
    <?if($arResult["nEndPage"] < $arResult["NavPageCount"]):?>
        <?if($arResult["nEndPage"] < ($arResult["NavPageCount"]-1)):?>
            <li class="category-main__pagination-item">
                <span class="category-main__pagination-link">...</span>
            </li>
        <?endif;?>
        <li class="category-main__pagination-item">
            <a class="category-main__pagination-link  js_pagination_btn" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a>
        </li>
    <?endif;?>
</ul>