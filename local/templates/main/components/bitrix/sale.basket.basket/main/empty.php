<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>

<div class="container">
    <div class="cart__empty-row  active js_cart_empty_row">
        <h2 class="cart__empty-title">
            Список корзины пока пуст.
        </h2>
        <a class="cart__empty-btn" href="/catalog/">
            Перейти в каталог
        </a>
    </div>
</div>
