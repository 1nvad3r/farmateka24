<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
    <div class="cart__result-wrapper" data-entity="basket-checkout-aligner">
        <p class="cart__result-title">
            Стоимость заказа
        </p>

        <div class="cart__sum-row">
                                <span class="cart__sum-text">
                                    Стоимость товаров
                                </span>

            <span class="cart__sum">
                                    {{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}
                                </span>
        </div>

        {{#DISCOUNT_PRICE_FORMATED}}
        <div class="cart__sum-row">
                                <span class="cart__sum-text">
                                    Скидка на товар
                                </span>
            <span class="cart__sum cart__sum--accent">
                                    {{{DISCOUNT_PRICE_FORMATED}}}
                                </span>
        </div>
        {{/DISCOUNT_PRICE_FORMATED}}

        {{#PERSONAL_DISCOUNT}}
        <div class="cart__sum-row">
                                <span class="cart__sum-text">
                                    Ваша скидка
                                </span>

            <span class="cart__sum">
                                    {{{PERSONAL_DISCOUNT}}}%
                                </span>
        </div>
        {{/PERSONAL_DISCOUNT}}

        <p class="cart__sale-text">
            Добавьте товаров на 2000 рублей, чтобы увеличить скидку постоянного покупателя
        </p>

        <?
        if ($arParams['HIDE_COUPON'] !== 'Y')
        {
            ?>
            {{^COUPON_ENTERED}}
            <span class="cart__code-title">
                Промокод
            </span>

            <div class="cart__code-wrapper">
                <input class="cart__code-input" id="basket-coupon-input" data-entity="basket-coupon-input" type="text">

                <button class="cart__code-btn"  data-entity="basket-coupon-enter">Применить</button>
            </div>
            {{/COUPON_ENTERED}}
            {{#COUPON_ERROR}}
                <span class="cart__error-info" style="display:block">
                    Промокод не найден
                </span>
            {{/COUPON_ERROR}}
            <?
        }
        ?>


        <div class="cart__result-row">
                                <span class="cart__result-text">
                                    Итого:
                                </span>

            <span class="cart__result-sum"  data-entity="basket-total-price">
                                    {{{PRICE_FORMATED}}}
                                </span>
        </div>

        <p class="cart__result-terms">
            Без учета стоимости доставки
        </p>
    </div>

    <button class="cart__action-btn"
    data-entity="basket-checkout-button">
        Перейти к оформлению заказа
    </button>

    <p class="cart__terms">
        Нажимая на кнопку «Перейти к оформлению заказа» вы соглашаетесь с
        <a class="cart__terms-link" href="#" target="_blank">условиями отпуска товара</a>
        , соглашаетесь на
        <a class="cart__terms-link" href="#" target="_blank">обработку персональных данных</a> и на
        получение информации о статусе заказа по электронной почте и в виде смс-сообщений.
    </p>
</script>