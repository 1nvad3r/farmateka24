<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

$positionClassMap = array(
	'left' => 'basket-item-label-left',
	'center' => 'basket-item-label-center',
	'right' => 'basket-item-label-right',
	'bottom' => 'basket-item-label-bottom',
	'middle' => 'basket-item-label-middle',
	'top' => 'basket-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

//d($arResult);
?>

<script id="basket-item-template" type="text/html">
    <li class="cart__product-item js_cart_product_item" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
        <a class="cart__img-link" href="{{DETAIL_PAGE_URL}}">
            <img class="cart__img" src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=SITE_TEMPLATE_PATH?>/images/notimages.png{{/IMAGE_URL}}" alt="{{NAME}}">
        </a>

        <div class="cart__name-wrapper">
            <a class="cart__product-name" href="{{DETAIL_PAGE_URL}}">
                {{NAME}}
            </a>

            {{#DISCOUNT_PRICE_PERCENT}}
            <span class="cart__sale">
                {{DISCOUNT_PRICE_PERCENT_FORMATED}}
            </span>
            {{/DISCOUNT_PRICE_PERCENT}}
            {{#BRAND}}
            <div class="cart__product-brend-wrapper">
                <span class="cart__product-brend-intro">
                    Бренд:
                </span>
                <a class="cart__product-brend" href="#">
                    {{BRAND}}
                </a>
            </div>
            {{/BRAND}}
            <br>В наличии в {{IN_STOCK_TEXT}} аптеках<br>Под заказ в любой аптеке через 3 дня
        </div>

        <div class="cart__price-row ">
            <div data-entity="basket-item-quantity-block" class="cart__quantity-box quantity-box  {{#NOT_AVAILABLE}} disabled{{/NOT_AVAILABLE}}">
                <button data-entity="basket-item-quantity-minus" class="quantity-box__btn quantity-box__btn--minus "
                        type="button">-</button>

                <input class="quantity-box__input js_product_quantity_input" type="number"
                       min="0" value="{{QUANTITY}}" {{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}}
                        data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field"
                        id="basket-item-quantity-{{ID}}"/>

                <button data-entity="basket-item-quantity-plus" class="quantity-box__btn quantity-box__btn--plus "
                        type="button">+</button>
                {{#SHOW_LOADING}}
                <div class="basket-items-list-item-overlay"></div>
                {{/SHOW_LOADING}}
            </div>

            <div class="cart__btns-wrapper">
                <button class="card__heart offer__heart <?if($USER->IsAuthorized()):?> js_card_heart {{#FAVORITE}}active{{/FAVORITE}}<?else:?>js_btn_popup<?endif;?>"
                        data-btn="log-in" type="button" data-id="{{PRODUCT_ID}}">
                    <svg width="33" height="28">
                        <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#card-heart'?>"></use>
                    </svg>
                    <svg width="33" height="28">
                        <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#card-heart-filled'?>"></use>
                    </svg>
                    <span class="card__heart-description">
                                                добавить в избранное
                                            </span>
                </button>

                <button class="cart__delete-btn js_cart_delete_btn" data-entity="basket-item-delete">
                    <span class="cart__delete-description">
                        удалить из корзины
                    </span>
                </button>
            </div>

            <div class="cart__price-wrapper">
                {{^DISCOUNT_PRICE}}
                    <span class="cart__price" id="basket-item-price-{{ID}}">
                        {{{PRICE}}} ₽
                    </span>
                {{/DISCOUNT_PRICE}}
                {{#DISCOUNT_PRICE}}
                    <span class="cart__price" id="basket-item-price-{{ID}}">
                        {{{PRICE}}} ₽
                    </span>
                    <div class="cart__price-old">
                        {{{FULL_PRICE}}} ₽
                    </div>
                {{/DISCOUNT_PRICE}}
            </div>
        </div>
    </li>
</script>