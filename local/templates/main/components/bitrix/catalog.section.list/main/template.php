<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="categories">
    <div class="container">
        <h1 class="categories__title title">Каталог</h1>
    </div>

    <div class="container categories__container">
        <ul class="categories__list">
            <?php foreach ($arResult["SECTIONS"] as $section): ?>
            <li class="categories__item">
                <?php $picture = $section["PICTURE"]["ID"] ? CFile::GetPath($section["PICTURE"]["ID"]) : '/images/section_default.jpg'; ?>
                <img class="categories__img" src="<?= $picture ?>"
                     alt="<?=$section["PICTURE"]["ALT"]?>">

                <div class="categories__img-mask" style="opacity: 0.55"></div>

                <a class="categories__link" href="<?=$section["SECTION_PAGE_URL"]?>">
                    <?=$section["NAME"]?>
                </a>
            </li>
            <?php endforeach; ?>

        </ul>
    </div>
</section>
