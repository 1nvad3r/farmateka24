<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    /** @var array $arParams */
    /** @var array $arResult */
    /** @global CMain $APPLICATION */
    /** @global CUser $USER */
    /** @global CDatabase $DB */
    /** @var CBitrixComponentTemplate $this */
    /** @var string $templateName */
    /** @var string $templateFile */
    /** @var string $templateFolder */
    /** @var string $componentPath */
    /** @var CBitrixComponent $component */
    $this->setFrameMode(true);
    $res = CIBlockSection::GetByID($arResult["SECTION"]['ID']);
    if ($ar_res = $res->GetNext())
        if ($ar_res['DEPTH_LEVEL'] >= 3) { ?>
            <div class="category-main__popular-product-slider popular-row-product-slider">
                <div class="popular-row-product-slider__top">
                    <h2 class="popular-row-product-slider__title">
                        Популярные товары
                    </h2>

                    <div class="popular-row-product-slider__btn-prev js_popular_row_product_button_prev"></div>
                    <div class="popular-row-product-slider__btn-next js_popular_row_product_button_next"></div>
                </div>
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "section_populars",
                [
                    "SHOW_TABS" => "Y",
                    "TITTLE" => "Популярные товары",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "Y",
                    "ELEMENT_SORT_FIELD" => "PROPERTY_POPULAR",
                    "ELEMENT_SORT_FIELD2" => "SORT",
                    "ELEMENT_SORT_ORDER" => "DESC",
                    "ELEMENT_SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => "",
                    "IBLOCK_ID" => "4",
                    "IBLOCK_TYPE" => "catalog",
                    "PRICE_CODE" => ["BASE"],
                    "CONVERT_CURRENCY" => "Y",
                    "CURRENCY_ID" => "RUB",
                ]
            ); ?>
            </div>
        <?php } else { ?>
            <?php if ($arResult["SECTIONS_COUNT"] > 0): ?>
                <div class="category-main__category-links-wrapper">
                    <h2 class="category-main__category-links-title">
                        Категории
                    </h2>

                    <ul class="category-main__category-links-list js_category_main_category_links_list">
                        <? foreach ($arResult["SECTIONS"] as $section): ?>
                            <li class="category-main__category-link-item">
                                <a class="category-main__category-link" href="<?= $section["SECTION_PAGE_URL"] ?>">
                                    <?= $section["NAME"] ?>
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                    <button class="category-main__category-links-more-btn js_category_main_category_links_more_btn">

                        <span class="category-main__category-links-btn--more">Показать ещё</span>
                        <span class="category-main__category-links-btn--less">Скрыть</span>
                    </button>
                </div>
            <?php endif;
        }
?>