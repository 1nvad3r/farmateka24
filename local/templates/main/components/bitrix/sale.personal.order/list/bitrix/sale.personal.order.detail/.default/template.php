<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

	use Bitrix\Main\Localization\Loc,
		Bitrix\Main\Page\Asset;

?>
<section class="my-order js_my-order">

	<div class="my-order__menu cabinet-menu">
		<h1 class="cabinet-menu__title">
			Мои заказы
		</h1>

		<a class="cabinet-menu__back" href="/personal/order/">Ко всем заказам</a>

		<ul class="cabinet-menu__list  js_cabinet_menu_list">
			<li class="cabinet-menu__item">
				<a class="cabinet-menu__link" href="/personal/">
					Профиль
				</a>
			</li>
			<li class="cabinet-menu__item">
				<a class="cabinet-menu__link " href="/personal/favorites/">
					Избранное
				</a>
			</li>

			<li class="cabinet-menu__item">
				<a class="cabinet-menu__link active" href="/personal/order/">
					Мои заказы
				</a>
			</li>

			<li class="cabinet-menu__item">
				<a class="cabinet-menu__link" href="/personal/reviews/">
					Мои отзывы
				</a>
			</li>

			<li class="cabinet-menu__item">
				<a class="cabinet-menu__link " href="/personal/cart/">
					Корзина
				</a>
			</li>

			<li class="cabinet-menu__item">
				<a class="cabinet-menu__link cabinet-menu__link--exit js_btn_popup" data-btn="leave" href="?logout=yes">
					Выход
				</a>
			</li>
		</ul>

		<button class="cabinet-menu__btn js_cabinet_menu_open_btn">
		</button>
	</div>

	<div class="container">
		<div class="my-order__top">
			<div class="my-order__number-wrapper">
                        <span class="my-order__number">
                            Заказ №<?= $arResult['ACCOUNT_NUMBER'] ?>
                        </span>

				<!-- для смены цвета добавь my-order__status--tomato / --green / --salad / --yellow -->
				<?
					if ($arResult["STATUS_ID"] == "N")
						$color = 'yellow';
					if ($arResult["STATUS_ID"] == "CN")
						$color = 'tomato';
					if ($arResult["STATUS_ID"] == "RD")
						$color = 'green';
					if ($arResult["STATUS_ID"] == "F")
						$color = 'salad';
				?>
				<span class="my-order__status my-order__status--<?= $color ?>">
                            <?= $arResult["STATUS"]["NAME"] ?>
                        </span>
			</div>

			<span class="my-order__date">
                        от <?= $arResult["DATE_INSERT_FORMATED"] ?>
                    </span>
		</div>

		<div class="my-order__main">
			<div class="my-order__price-wrapper">
                        <span class="my-order__price-title">
                            Стоимость заказа
                        </span>

				<div class="my-order__price-row">
                            <span class="my-order__price-text">
                                Стоимость товаров
                            </span>

					<span class="my-order__price-number">
                                <?= $arResult["PRODUCT_SUM_FORMATED"] ?>
                            </span>
				</div>

				<div class="my-order__price-row">
                            <span class="my-order__price-text">
                                Стоимость доставки
                            </span>

					<span class="my-order__price-number">
                                <?= $arResult["PRICE_DELIVERY_FORMATED"] ?>
                            </span>
				</div>

				<div class="my-order__price-row my-order__price-row--result">
                            <span class="my-order__price-text">
                                Итого:
                            </span>

					<span class="my-order__price-number my-order__price-number--result">
                                <?= $arResult["PRICE_FORMATED"] ?>
                            </span>
				</div>
			</div>

			<div class="my-order__details-wrapper">
                        <span class="my-order__price-title">
                            Информация о заказе
                        </span>

				<div class="my-order__details-row">
                            <span class="my-order__price-text">
                                Способ доставки
                            </span>

					<span class="my-order__price-number">
                                <?= $arResult['DELIVERY']["NAME"] ?>
                            </span>
				</div>

				<div class="my-order__details-row">
                            <span class="my-order__price-text">
                                Адрес доставки
                            </span>

					<span class="my-order__price-number my-order__price-number--underlined">
                                ТРЦ Вегас
                            </span>
				</div>

				<div class="my-order__details-row">
                            <span class="my-order__price-text">
                                Дата доставки
                            </span>

					<span class="my-order__price-number my-order__price-number--underlined">
                                <?= $arResult["DELIVERY"]["CONFIG"]["MAIN"]["PERIOD"]["FROM"] ?>-<?= $arResult["DELIVERY"]["CONFIG"]["MAIN"]["PERIOD"]["TO"] ?> дня
                            </span>
				</div>

				<div class="my-order__details-row">
                            <span class="my-order__price-text">
                                Способ оплаты
                            </span>

					<span class="my-order__price-number">
                                <?= $arResult["PAY_SYSTEM"]["NAME"] ?>
                            </span>
				</div>
			</div>
		</div>

		<div class="my-order__products-wrapper">
			<div class="my-order__products-top">
				<h3 class="my-order__products-title">
					Товары в заказе
				</h3>

				<span class="my-order__products-num">
                            (<?= count($arResult["BASKET"]) ?> товара)
                        </span>
			</div>

			<ul class="my-order__products-list">
				<? foreach ($arResult["BASKET"] as $key => $item): ?>
					<li class="my-order__products-item">
						<a class="my-order__product-img-link" href="<?= $item["DETAIL_PAGE_URL"] ?>">
							<img class="my-order__product-img" src="<?= $item["PICTURE"]["SRC"] ?>"
								 alt="<?= $item["NAME"] ?>">
						</a>

						<div class="my-order__products-bottom">
							<a class="my-order__product-name" href="<?= $item["DETAIL_PAGE_URL"] ?>">
								<?= $item["NAME"] ?>
							</a>

							<span class="my-order__product-price">
                                    <?= $item["FORMATED_SUM"] ?>
                                </span>
						</div>
					</li>
				<? endforeach; ?>
			</ul>

			<ul class="my-order__product-btns">
				<li class="my-order__product-btn-item">
					<a href="/catalog/"
					   class="my-order__product-btn my-order__product-btn--accent js_repeat_order_btn"
					   data-orderid="order<?= $arResult["ID"] ?>">
						Повторить заказ
					</a>
				</li>
				<li class="my-order__product-btn-item">
					<button class="my-order__product-btn js_btn_popup" data-btn="rate-an-order-delivery-<?= $arResult["ID"] ?>"
							type="button">
						Оценить качество заказа
					</button>
				</li>
			</ul>
		</div>

	</div>
</section>

