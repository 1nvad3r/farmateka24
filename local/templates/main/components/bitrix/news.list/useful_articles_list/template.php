<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<section class="about-articles">
    <div class="container">
        <h1 class="about-articles__title title-mini">Полезные статьи и акции</h1>
        <div class="about-articles__grid">

            <?foreach ($arResult["ITEMS"] as $item):?>
            <a class="articles__card" href="<?=$item["DETAIL_PAGE_URL"];?>">
                <div class="articles__card-img">
                    <img src="<?=CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]);?>" alt="<?=$item["NAME"]?>">
                </div>
                <h3 class="articles__card-title"><?=$item["NAME"]?></h3>
                <p class="articles__card-text"><?=$item["PREVIEW_TEXT"]?></p>
                <span class="articles__card-more">
                                Читать
                                <svg width="26" height="13">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#articles-card-arrow'?>">
                                    </use>
                                </svg>
                            </span>
            </a>
            <?endforeach;?>

        </div>
        <?=$arResult["NAV_STRING"]?>
    </div>
</section>