<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="intro">
    <div class="container">
        <div class="intro__swiper swiper">
            <div class="swiper-wrapper">
                <?foreach ($arResult["ITEMS"] as $key => $item):?>
                <div class="swiper-slide">
                    <div class="intro__item">
                        <img src="<?=CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]);?>" alt="Изображение-фон">
                        <a class="intro__btn btn btn--green" href="<?=$item["PROPERTIES"]["CATALOG_LINK"]["VALUE"];?>"><?=$item["PROPERTIES"]["BUTTON_TEXT"]["VALUE"];?></a>
                    </div>
                </div>
                <?endforeach;?>
            </div>

            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</section>