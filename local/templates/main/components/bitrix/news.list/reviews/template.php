<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<? if (count($arResult["ITEMS"]) > 0): ?>
	<div class="my-review__top active js_review_top">
                    <span class="my-review__sub-title">
                        (<?= count($arResult["ITEMS"]) ?> отзыва)
                    </span>
	</div>

	<div class="my-review__main-wrapper active js_review_main">
		<ul class="my-review__list">
			<? foreach ($arResult["ITEMS"] as $key => $item): ?>
				<li class="my-review__item js_review_item" id="review-<?= $item['ID']?>" data-rating="<?= $item['PROPERTIES']['RATING']['VALUE'] ?>" data-name="<?= $item['PROPERTIES']['USER_NAME']['VALUE'] ?>" data-text="<?= $item['DETAIL_TEXT']?>" data-productid="<?= $item['PRODUCT']['ID'] ?>">
					<a class="my-review__img-wrapper" href="<?= $item['PRODUCT']['DETAIL_PAGE_URL'] ?>">
						<img class="my-review__img" src="<?= CFile::GetPath($item['PRODUCT']['PREVIEW_PICTURE']); ?>"
							 alt="фото товара">
					</a>

					<div class="my-review__content">
						<a class="my-review__review-title-link" href="<?= $item['PRODUCT']['DETAIL_PAGE_URL'] ?>">
							<h3 class="my-review__reciew-title">
								<?= $item['PRODUCT']['NAME'] ?>
							</h3>
						</a>

						<p class="my-review__text">
							<?= $item['DETAIL_TEXT'] ?>
						</p>

						<ul class="my-review__btn-list">
							<li class="my-review__btn-item">
								<!-- для скрытия добавь класс hide -->
								<button class="my-review__btn js_btn_popup edit-review__btn" data-btn="write-a-review-done" data-review-edit="<?= $item['ID'] ?>">
									Редактировать
								</button>
							</li>

							<li class="my-review__btn-item">
								<button class="my-review__btn js_btn_popup delete-review__btn" data-btn="delete-review" data-review-delete="<?= $item['ID'] ?>">
									Удалить
								</button>
							</li>
						</ul>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
	</div>
<?php else: ?>
	<div class="my-review__empty-row active js_review_empty_row">
		<h2 class="my-review__empty-title">
			Список отзывов пока пуст.
		</h2>

		<div class="my-review__empty-text-wrapper">
			<p class="my-review__empty-text">
				Оставляейте отзывы о товарах, которые Вы покупали.
			</p>
		</div>

		<a class="my-review__empty-btn" href="/catalog/">
			Перейти в каталог
		</a>
	</div>
<?php endif; ?>