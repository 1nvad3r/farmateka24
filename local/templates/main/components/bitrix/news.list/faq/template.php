<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<pre>
<?//print_r($arResult["SECTIONS"]);?>
</pre>

<div class="js_payment_inner">
<ul class="description__links-list description__links-list--faq header__nav-list">
    <?$first = true;?>
    <?foreach ($arResult["SECTIONS"] as $section):?>
    <li class="description__link-item">
        <a class="description__link js_payment_tab_btn <?=$first?"active":"";?>" style="cursor:pointer;" type="button">
            <?$first=false;?>
            <?=$section["NAME"]?>
        </a>
    </li>
    <?endforeach;?>
</ul>
<?$first = true;?>
    <?foreach ($arResult["SECTIONS"] as $section):?>
    <ul class="description__spoilers-list js_payment_item <?=$first?"active":"";?>">
        <?$first=false;?>
        <?foreach ($section["ITEMS"] as $item):?>
        <li class="description__spoiler-item js_description_spoiler_item">
            <div class="description__spoiler-top">
                <h3 class="description__spoiler-title">
                    <?=$item["NAME"]?>
                </h3>
            </div>

            <div class="description__spoiler-bottom">
                <p class="description__spoiler-text">
                    <?=$item["DETAIL_TEXT"]?>
                </p>
            </div>
        </li>
        <?endforeach;?>
    </ul>
    <?endforeach;?>
</div>
