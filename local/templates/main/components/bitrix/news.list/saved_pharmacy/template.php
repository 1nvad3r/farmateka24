<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);

	function deletePharmacy($id)
	{
		global $USER;
		$arUser = CUser::GetByID($USER->GetID())->GetNext();

		$arSelect = ["*", "PROPERTY_*"];
		$arFilter = ["IBLOCK_ID" => 15, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'ID' => $id];
		$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$arFields['PROPERTIES'] = $ob->GetProperties();
		}
//		d($arFields['PROPERTIES']['USER']['VALUE']);
		foreach ($arFields['PROPERTIES']['USER']['VALUE'] as $k => $field){

			if($field == $USER->GetID()){

				unset($arFields['PROPERTIES']['USER']['VALUE'][$k]);
//				d($arFields['PROPERTIES']['USER']['VALUE']);d($id);d($IBLOCK_ID);
//				$arPhopValues[] = ['VALUE' => $arFields['PROPERTIES']['USER']['VALUE'], 'DESCRIPTION' => ''];

//				CIBlockElement::SetPropertyValues($id, $arFields['IBLOCK_ID'], $PROPERTY_VALUE, $PROPERTY_CODE);
//				$el = new CIBlockElement;
//				CIBlockElement::SetPropertyValuesEx($id, $arFields['IBLOCK_ID'], ['USER' => $arFields['PROPERTIES']['USER']['VALUE']], false);

//				CIBlockElement::SetPropertyValuesEx($id, $arFields['IBLOCK_ID'], ['USER' => $arPhopValues]);
//				LocalRedirect('/personal/?pharmacy=ok');
//				return true;

				$el = new CIBlockElement;
				$PROP = array();
				$PROP[63] = $arFields['PROPERTIES']['ADDRESS']['VALUE'];
				$PROP[64] = $arFields['PROPERTIES']['PHONE']['VALUE'];
				$PROP[65] = $arFields['PROPERTIES']['WORK_TIME']['VALUE'];
				$PROP[77] = $arFields['PROPERTIES']['WORK_DAYS']['VALUE'];
				$PROP[134] = $arFields['PROPERTIES']['STORE']['VALUE'];
				$PROP[135] = $arFields['PROPERTIES']['USER']['VALUE'];
				$PROP[78] = $arFields['PROPERTIES']['COORDS']['VALUE'];
				$PROP[79] = $arFields['PROPERTIES']['EMAIL']['VALUE'];
				$PROP[80] = $arFields['PROPERTIES']['AREA']['VALUE'];
				$PROP[81] = $arFields['PROPERTIES']['OPEN_ALL_TIME']['VALUE'];
				$PROP[82] = $arFields['PROPERTIES']['AVAILIBLE_FULL_ORDER']['VALUE'];
				$PROP[83] = $arFields['PROPERTIES']['NOT_AVAILIBLE']['VALUE'];

				$arLoadProductArray = Array(
					"IBLOCK_ID" => $arFields['IBLOCK_ID'],
					"PROPERTY_VALUES" => $PROP,
					"NAME" => $arFields['NAME'],
				);

				$PRODUCT_ID = $id;
				$res = $el->Update($PRODUCT_ID, $arLoadProductArray);

				if($res){
					LocalRedirect('/personal/?pharmacy=ok');
				}
			}
		}
	}
	if ($_REQUEST["action"] === "delete") {
		$result = deletePharmacy($_REQUEST['id']);
	}
?>
<?php if (count($arResult['ITEMS']) > 0): ?>
	<!-- покажется при добавлении класса active -->
	<div class="cabinet__content-item-inner active">
		<h3 class="cabinet__item-title">
			Аптека “Фарматека”
		</h3>

		<ul class="cabinet__address-list pharmacy-list">
			<? foreach ($arResult["ITEMS"] as $key => $item): ?>
				<li class="cabinet__address-item" id="pharmacy-<?= $item['ID'] ?>">
					<div class="cabinet__address-card">
						<h4 class="cabinet__address-title">
							<?= $item['NAME'] ?>
						</h4>

						<p class="cabinet__address">
							<?= $item['PROPERTIES']['ADDRESS']['VALUE'] ?>
						</p>

						<div class="cabinet__address-days-row">
							<span class="cabinet__address-days">
								<?= $item['PROPERTIES']['WORK_DAYS']['VALUE'] ?>
							</span>

							<span class="cabinet__address-time">
								<?= $item['PROPERTIES']['WORK_TIME']['VALUE'] ?>
							</span>
						</div>

						<a class="cabinet__address-tel"
						   href="tel:<?= $item['PROPERTIES']['PHONE']['VALUE'] ?>"><?= formatPhone($item['PROPERTIES']['PHONE']['VALUE']) ?></a>
					</div>

					<div class="cabinet__address-btns-wrapper">
						<a class="cabinet__pharmacy-delet-btn  js_popup_delete_pharmacy"
								data-btn="delete-pharmacy" data-pharmacy-id="<?= $item['ID'] ?>" href="/personal/?pharmacy=ok&id=<?= $item['ID'] ?>&action=delete">
							Удалить сохраненную аптеку
						</a>

						<button class="cabinet__address-add-btn js_btn_popup" data-btn="add-pharmacy" type="button">
							Добавить новую аптеку
						</button>
					</div>
				</li>
			<? endforeach; ?>
		</ul>

		<div class="cabinet__map js_cabinet_map" id="map7"></div>

	</div>
<?php else: ?>
	<!-- покажется при добавлении класса active -->
	<div class="cabinet__content-item-inner active">
		<div class="cabinet__item-title-wrapper">
			<h3 class="cabinet__item-title">
				У Вас нет сохраненных аптек
			</h3>

			<svg class="cabinet__item-title-icon" width="44" height="44" viewBox="0 0 44 44"
				 fill="none" xmlns="http://www.w3.org/2000/svg">
				<path
						d="M22 1.375C10.6092 1.375 1.375 10.6092 1.375 22C1.375 33.3908 10.6092 42.625 22 42.625C33.3908 42.625 42.625 33.3908 42.625 22C42.625 10.6092 33.3908 1.375 22 1.375ZM34.6395 34.6395C31.5054 37.7643 27.3202 39.6102 22.8994 39.8178C18.4786 40.0253 14.1388 38.5795 10.7258 35.7621C7.31275 32.9447 5.07088 28.9575 4.43703 24.5775C3.80317 20.1974 4.82273 15.7383 7.29705 12.0689C9.77137 8.39948 13.5232 5.78272 17.8215 4.72847C22.1198 3.67423 26.6566 4.25801 30.548 6.36608C34.4393 8.47414 37.4065 11.9555 38.8712 16.1317C40.336 20.308 40.1935 24.88 38.4714 28.9569C37.5739 31.0812 36.2725 33.0111 34.6395 34.6395Z"
						fill="black"/>
				<path
						d="M13.0625 17.1875H16.5V20.625H13.0625V17.1875ZM27.5 17.1875H30.9375V20.625H27.5V17.1875ZM22 24.0625C19.6296 24.0625 17.3563 25.0041 15.6802 26.6802C14.0041 28.3563 13.0625 30.6296 13.0625 33H30.9375C30.9375 30.6296 29.9959 28.3563 28.3198 26.6802C26.6437 25.0041 24.3704 24.0625 22 24.0625Z"
						fill="black"/>
			</svg>
		</div>

		<button class="cabinet__address-add-btn  js_btn_popup" data-btn="add-pharmacy"
				type="button">
			Добавить новую аптеку
		</button>

	</div>
<?php endif; ?>
<?php
	CModule::IncludeModule('catalog');
	$resStores = CCatalogStore::GetList(
		['PRODUCT_ID' => 'ASC', 'ID' => 'ASC'],
		['ACTIVE' => 'Y'],
		false,
		false,
		["UF_*"]
	);
	while ($arStore = $resStores->Fetch()) {
		$arResult['STORES'][] = $arStore;
	}
?>
<? $this->SetViewTarget('js_pharmacy'); ?>
	<script type="text/javascript">
		window.addEventListener("DOMContentLoaded", () => {
			// тут храним всю информацию об аптеках
			const pharmInfo = [
				<?
				foreach ($arResult['STORES'] as $key => $store) {
					$xCoords = trim($store['GPS_N']);
					$yCoords = trim($store['GPS_S']);
					!$store["UF_OPENALLTIME"] ? $allTime = 'false' : $allTime = 'true';
					!$store["UF_AVAILIBLE_FULL_ORDER"] ? $fullOrder = 'false' : $fullOrder = 'true';

					echo '
					{
						x: ' . $xCoords . ',
						y: ' . $yCoords . ',
						id: ' . $store["ID"] . ',
						address: "' . $store["ADDRESS"] . '",
						title: "' . $store["TITLE"] . '",
						openDays: "' . $store['UF_OPENDAYS'] . '",
						openDaysShort: "' . $store['UF_OPENDAYS_SHRT'] . '",
						area: "' . $store["DESCRIPTION"] . '",
						openTime: "' . $store['UF_OPENTIME'] . '",
						phone: "' . formatPhone($store["PHONE"]) . '",
						phoneForLink: ' . $store["PHONE"] . ',
						mail: "' . $store["EMAIL"] . '",
						isOpenAllTime: ' . $allTime . ',
						isAvailibleFullOrder: ' . $fullOrder . ',
						notAvailible: ["Антигриппин таблетки шипучие 30 шт"],
					},
				';
				}
				?>
			];

			// тут храним всю информацию о Сохраенных аптеках
			const myPharmInfo = [
				<?
				foreach ($arResult['ITEMS'] as $key => $item):
					$coords = explode(",", $item["PROPERTIES"]["COORDS"]['VALUE']);
					$xCoords = trim($coords[0]);
					$yCoords = trim($coords[1]);
					!$item["PROPERTIES"]["OPEN_ALL_TIME"]["VALUE"] ? $allTime = 'false' : $allTime = 'true';
					!$item["PROPERTIES"]["AVAILIBLE_FULL_ORDER"]["VALUE"] ? $fullOrder = 'false' : $fullOrder = 'true';
					echo '
					{
						x: ' . $xCoords . ',
						y: ' . $yCoords . ',
						id: ' . $item["ID"] . ',
						address: "' . $item["PROPERTIES"]["ADDRESS"]["VALUE"] . '",
						title: "' . $item["NAME"] . '",
						openDays: "' . $item["PROPERTIES"]["WORK_DAYS"]["VALUE"] . '",
						openDaysShort: "' . $item["PROPERTIES"]["WORK_DAYS"]["VALUE"] . '",
						area: "' . $item["PROPERTIES"]["AREA"]["VALUE"] . '",
						openTime: "' . $item["PROPERTIES"]["WORK_TIME"]["VALUE"] . '",
						phone: "' . formatPhone($item["PROPERTIES"]["PHONE"]["VALUE"]) . '",
						phoneForLink: ' . $item["PROPERTIES"]["PHONE"]["VALUE"] . ',
						mail: "' . $item["PROPERTIES"]["EMAIL"]["VALUE"] . '",
						isOpenAllTime: ' . $allTime . ',
						isAvailibleFullOrder: ' . $fullOrder . ',
						notAvailible: ["Антигриппин таблетки шипучие 30 шт"],
					},
				';
				endforeach;
				?>
			];

			// карта на вкладке Сохраненные аптеки в личном кабинете

			let myMapInCabinet; //карта в личном кабинете (перемнная должна быть над попапом)

			let initCobinetCard; //инициализация карты в личном кабинете (перемнная должна быть над попапом)

			if (document.querySelector(".js_cabinet_map")) {
				initCobinetCard = function () {
					myMapInCabinet = new ymaps.Map("map7", {
						// Создание экземпляра карты и его привязка к контейнеру с заданным id;
						center: [55.62728, 37.586311], // При инициализации карты обязательно нужно указать её центр и коэффициент масштабирования;
						zoom: 10,
						controls: ['zoomControl'], // Скрываем элементы управления на карте;
					});

					let collection = new ymaps.GeoObjectCollection(null, {
						// Создаём коллекцию, в которую будемпомещать метки (что-то типа массива);
					});

					// из всей информации об аптеках вытаскиваем только координаты
					let collectionCoords = myPharmInfo.map((item) => {
						return [item.x, item.y];
					});

					for (let i = 0, l = collectionCoords.length; i < l; i++) {
						// C помощью цикла добавляем все метки в коллекцию;
						// находим по коррдинатам полную информацию об аптеке для вставки в балун
						const ourPoint = myPharmInfo.filter((item) => {
							return item.x === collectionCoords[i][0];
						});

						collection.add(
							new ymaps.Placemark(collectionCoords[i], {
								balloonContent: `
                        <div class="big-balloon">
                            <h3 class="big-balloon__address-title">
                            ${ourPoint[0]["title"]}
                            </h3>

                            <span class="big-balloon__address-text js_baloon_address">
                                ${ourPoint[0]["address"]}
                            </span>

                            <div class="big-balloon__address-open-time-row">
                                <span class="big-balloon__address-open-days">
                                    ${ourPoint[0]["openDays"]}
                                </span>

                                <span class="big-balloon__address-open-time">
                                    ${ourPoint[0]["openTime"]}
                                </span>
                            </div>

                            <a class="big-balloon__address-phone" href="tel:${ourPoint[0]["phoneForLink"]}">
                                ${ourPoint[0]["phone"]}
                            </a>
                        </div>
                        `,
							})
						);
						collection.get(i).properties.set("iconContent", `${i + 1}`); // Чтоб в дальнейшем к ней можно было как-то обратиться к нужной метке, добавляем каждой порядковый номер, записываем его в свойство 'iconContent';
					}

					myMapInCabinet.geoObjects.add(collection); // Добавляем коллекцию с метками на карту;

					collection.options.set("iconLayout", "default#image"); // Необходимо указать данный тип макета;
					collection.options.set("iconImageHref", "<?= SITE_TEMPLATE_PATH?>/images/mark.svg"); // Своё изображение иконки метки;
					collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
					collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
					collection.options.set("hideIcon", false); // Размеры метки;

					//перкскакиваем к нужному адресу
					const storeList = document.querySelector(".store__list"); // Список адресов
				};

				ymaps.ready(initCobinetCard); // Дождёмся загрузки API и готовности DOM;
			}

			// попап Добавить аптеку в личном кабинете

			if (document.querySelector('.js_popup_add_pharmacy')) {
				const popup = document.querySelector('.js_popup_add_pharmacy');
				const popupAddressInput = document.querySelector('.js_popup_address_input') // поле поиска
				const pharmacyId = document.querySelector('.js_pharmacy_id')
				const popupSubmitBtn = document.querySelector('.js_popup_address_submit_btn') // кнопка подтверждения
				let filterdPharmInfo = pharmInfo // тут храним всю информацию об аптека, но уже отфильтрованную именно под этот попап

				ymaps.ready(init); // Дождёмся загрузки API и готовности DOM;

				let myMap //тут храним карту

				function init() {

					myMap = new ymaps.Map('map9', { // Создание экземпляра карты и его привязка к контейнеру с заданным id;
						center: [55.627280, 37.586311], // При инициализации карты обязательно нужно указать её центр и коэффициент масштабирования;
						zoom: 10,
						controls: [] // Скрываем элементы управления на карте;
					});

					let collection = new ymaps.GeoObjectCollection(null, { // Создаём коллекцию, в которую будем помещать метки (что-то типа массива);
					});

					// из всей информации об аптеках вытаскиваем только координаты
					let collectionCoords = filterdPharmInfo.map(item => {
						return [item.x, item.y]
					})

					for (let i = 0, l = collectionCoords.length; i < l; i++) { // C помощью цикла добавляем все метки в коллекцию;
						// находим по коррдинатам полную информацию об аптеке для вставки в балун
						const ourPoint = filterdPharmInfo.filter(item => {
							return item.x === collectionCoords[i][0]
						})

						collection.add(new ymaps.Placemark(collectionCoords[i], {
								balloonContent: `
                            <div class="big-balloon" data-pharmacy-id="${ourPoint[0]['id']}">
                            <h3 class="big-balloon__address-title">
                               ${ourPoint[0]['title']}
                            </h3>

                            <span class="big-balloon__address-text js_baloon_address">
                                ${ourPoint[0]['address']}
                            </span>

                            <div class="big-balloon__address-open-time-row">
                                <span class="big-balloon__address-open-days">
                                    ${ourPoint[0]['openDays']}
                                </span>

                                <span class="big-balloon__address-open-time">
                                    ${ourPoint[0]['openTime']}
                                </span>
                            </div>

                            <a class="big-balloon__address-phone" href="tel:${ourPoint[0]['phoneForLink']}">
                                ${ourPoint[0]['phone']}
                            </a>

                            <button class="big-balloon__choose-btn js_ballon_accept_btn" type="button" data-pharmacy-id="${ourPoint[0]['id']}">
                                Выбрать аптеку
                            </button>
                        </div>
                        `
							}
						));
						collection.get(i).properties.set('iconContent', `${i + 1}`); // Чтоб в дальнейшем к ней можно было как-то обратиться к нужной метке, добавляем каждой порядковый номер, записываем его в свойство 'iconContent';
					}

					myMap.geoObjects.add(collection); // Добавляем коллекцию с метками на карту;

					collection.options.set('iconLayout', 'default#image'); // Необходимо указать данный тип макета;
					collection.options.set('iconImageHref', '<?= SITE_TEMPLATE_PATH?>/images/mark.svg'); // Своё изображение иконки метки;
					collection.options.set('iconImageSize', [28, 37]); // Размеры метки;
					collection.options.set('iconImageSize', [28, 37]); // Размеры метки;
					collection.options.set('hideIcon', false); // Размеры метки;

					// при нажатии Выбрать аптеку в балуне
					let map = document.querySelector('.js_checkout_map')

					if (map) {
						map.addEventListener('click', (e) => {
							// проверяем что кликнули в кнопку
							if (e.target.classList.contains('js_ballon_accept_btn')) {
								const targetBalloon = e.target.parentNode

								const address = targetBalloon.querySelector('.js_baloon_address')

								popupAddressInput.value = address.innerHTML.trim()

								pharmacyId.value = $(e.target).data('pharmacy-id')

								// закрываем балун
								myMap.balloon.close()

								// отображаем только одну метку на карте
								// filterPoints(address.innerHTML.trim())
								filterPoints(pharmacyId.value);
							}
						})
					}

					// при выборе аптеки отобраажем на карте только её
					function filterPoints(id) {
						filterdPharmInfo = pharmInfo.filter(item => item.id == id)

						myMap.destroy()

						init()
					}

					//перкскакиваем к нужному адресу
					const storeList = document.querySelector('.store__list'); // Список адресов

					$('.js_popup_pharmacy_submit_btn').click(function () {
						if (popupAddressInput.value === '') {
							popupAddressInput.classList.add('error');
						} else {
							//console.log('надо бы сохранить', filterdPharmInfo[0]);
							myPharmInfo.push(filterdPharmInfo[0]);
							console.log(pharmacyId.value);

							// отправляем запрос
							BX.ajax({
								url: '/ajax/pharmacy.php',
								method: 'POST',
								data: {
									action: 'add',
									id: pharmacyId.value
								},
								dataType: 'json',
								timeout: 30,
								async: true,
								onsuccess: function (data) {
									window.location.href = '/personal/?pharmacy=ok';
									//
									// $("#pharmacy-" + pharmacyId.value).length ? '' : console.log(filterdPharmInfo[0]);
									console.log(data);
									if ($("#pharmacy-" + pharmacyId.value).length) {

									} else {
										console.log(filterdPharmInfo[0]);
									}

								},
							});

							// перерисовываем карту на вкладке "сохраненные аптеки"
							myMapInCabinet.destroy();
							initCobinetCard();

							popup.classList.remove("active");
							document.body.removeAttribute("data-body-scroll-fix");
							document.body.removeAttribute("style");
						}
					});

				}

				// ищем подходящте под поиск аптеки
				function searchPoint() {
					filterdPharmInfo = pharmInfo.filter(item => item.address.includes(popupAddressInput.value) || item.title.includes(popupAddressInput.value))

					myMap.destroy()

					init()
				}

				// оборачиваем поиск в дебаунс чтобы откинуть лишние запросы
				let debouncedSearchPoint = debounce(searchPoint, 1000);

				// запускаем поиск по вводу
				popupAddressInput.addEventListener('keyup', (e) => {
					debouncedSearchPoint()
				})

				// валидируем попап
				clearInputError(popupAddressInput)
			}
		});
	</script>
<? $this->EndViewTarget(); ?>