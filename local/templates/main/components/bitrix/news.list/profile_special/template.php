<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="category-slider">
	<div class="container category-slider__container">
		<div class="swiper category-slider__swiper js_category_slider_swiper">
			<div class="swiper-wrapper category-slider__swiper-wrapper">

				<?foreach ($arResult["ITEMS"] as $key => $item):?>
					<div class="swiper-slide category-slider__swiper-slide">
						<a class="category-slider__link" href="<?=$item["PROPERTIES"]["CATALOG_LINK"]["VALUE"];?>">
							<img class="category-slider__img" src="<?=CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]);?>"
								 alt="<?=$item["NAME"];?>">
						</a>
					</div>
				<?endforeach;?>
			</div>

			<div class="swiper-button-prev category-slider__swiper-button-prev category_slider_button_prev">
			</div>
			<div class="swiper-button-next category-slider__swiper-button-next category_slider_button_next">
			</div>
		</div>
	</div>
</section>
