<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
	global $USER;
?>
<?php if (count($arResult['ITEMS']) > 0): ?>
	<!-- покажется при добавлении класса active -->
	<div class="cabinet__content-item-inner active">
		<ul class="cabinet__address-list">
			<?php foreach ($arResult["ITEMS"] as $key => $item): ?>
				<li class="cabinet__saved-address-item" id="address-<?= $item['ID'] ?>">
					<p class="cabinet__saved-address">
						<?= $item['PROPERTIES']['CITY']['VALUE'] ?>, <?= $item['PROPERTIES']['STREET']['VALUE'] ?>
						, <?= $item['PROPERTIES']['HOUSE']['VALUE'] ?> <?= $item['PROPERTIES']['FLAT']['VALUE'] ?>
					</p>

					<div class="cabinet__saved-address-btns-wrapper">
						<button class="cabinet__address-delet-btn js_btn_popup"
								data-btn="delete-address" type="button" data-address-id="<?= $item['ID'] ?>">
							Удалить
						</button>

						<button class="cabinet__address-change-btn  js_btn_popup"
								data-btn="change-address-<?= $item['ID'] ?>" type="button" data-address-id="<?= $item['ID'] ?>" >
							Редактировать
						</button>

						<button class="cabinet__address-add-btn  js_btn_popup"
								data-btn="save-address" type="button">
							Добавить новый адрес
						</button>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
	</div>
	<?php foreach ($arResult["ITEMS"] as $key => $item): ?>
		<!-- ПОП-АП 'ИЗМЕНЕНИЕ АДРЕСА' -->
		<div class="popup popup--save-address js_popup js_popup_change_address" data-popup="change-address-<?= $item['ID'] ?>">
			<div class="popup__wrap">
				<button class="popup__exit js_popup_exit" type="button">
					<svg width="24" height="24">
						<use xlink:href="/local/templates/main/images/sprite.svg#exit"></use>
					</svg>
				</button>
				<form class="popup__form js_popup_form" method="post" action="/personal/?address=ok">
					<input type="hidden" name="change-address" value="Y">
					<div class="popup__title">Изменить адрес</div>

					<div class="checkout__address-imputs">
						<label class="checkout__client-label" for="">
							<!-- если поле обязательно добавть класс required - добавится звездочка -->
							<span class="checkout__clients-title required" >
                            Город
                        </span>

							<input class="checkout__client-input js_city_input" type="text" value="<?= $item['PROPERTIES']['CITY']['VALUE'] ?>" name="city">
						</label>

						<label class="checkout__client-label checkout__client-label--street" for="">
							<!-- если поле обязательно добавть класс required - добавится звездочка -->
							<span class="checkout__clients-title required">
                            Улица
                        </span>

							<input class="checkout__client-input js_city_street" type="text"
								   value="<?= $item['PROPERTIES']['STREET']['VALUE'] ?>" name="street">
						</label>

						<div class="checkout__address-imputs-wrapper">
							<label class="checkout__client-label required" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title required">
                                Дом
                            </span>

								<input name="house" class="checkout__client-input js_home_input" type="number" min="1" value="<?= $item['PROPERTIES']['HOUSE']['VALUE'] ?>">
							</label>

							<label class="checkout__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title required">
                                Корпус
                            </span>

								<input name="korpus" class="checkout__client-input js_korpus_input" type="text" value="<?= $item['PROPERTIES']['KORPUS']['VALUE'] ?>">
							</label>

							<label class="checkout__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title required">
                                Квартира
                            </span>

								<input name="flat" class="checkout__client-input js_flat_input" type="number" min="1" value="<?= $item['PROPERTIES']['FLAT']['VALUE'] ?>">
							</label>
						</div>

						<div class="checkout__address-imputs-wrapper">
							<label class="checkout__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title required">
                                Подъезд
                            </span>

								<input name="entrance" class="checkout__client-input js_front_door_input" type="text" value="<?= $item['PROPERTIES']['ENTRANCE']['VALUE'] ?>">
							</label>

							<label class="checkout__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title required">
                                Этаж
                            </span>

								<input name="floor" class="checkout__client-input js_floor_name js_floor_input" type="text" value="<?= $item['PROPERTIES']['FLOOR']['VALUE'] ?>">
							</label>

							<label class="checkout__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title ">
                                Код домофона
                            </span>

								<input name="code_house" class="checkout__client-input js_floor_code_input" type="text"  value="<?= $item['PROPERTIES']['CODE_HOUSE']['VALUE'] ?>">
							</label>
						</div>

						<label class="checkout__client-label" for="">
							<!-- если поле обязательно добавть класс required - добавится звездочка -->
							<span class="checkout__clients-title ">
                            Комментарий
                        </span>

							<input name="comment" class="checkout__client-input js_comment_input" type="text" value="<?= $item['PROPERTIES']['COMMENT']['VALUE'] ?>">
						</label>
					</div>
					<input class="js_user-id" type="hidden" name="user_id" value="<?= $USER->GetID(); ?>">
					<input class="js_id_input" type="hidden" name="id" value="<?= $item['ID'] ?>">
					<ul class="popup__stop-btns">
						<li class="popup__stop-btn-item">
							<button class="popup__btn
                        popup__btn--continue
                        btn btn--tr js_ok_btn">Сохранить
							</button>
						</li>
						<li class="popup__stop-btn-item">
							<button class="popup__btn popup__btn--cansel-review btn btn--tr js_off_btn"
									type="button">Отменить
							</button>
						</li>
					</ul>
				</form>
			</div>
		</div>
	<?php endforeach; ?>
<?php else: ?>
	<!-- покажется при добавлении класса active -->
	<div class="cabinet__content-item-inner active">
		<div class="cabinet__item-title-wrapper">
			<h3 class="cabinet__item-title">
				У Вас нет сохраненных адресов
			</h3>

			<svg class="cabinet__item-title-icon" width="44" height="44" viewBox="0 0 44 44"
				 fill="none" xmlns="http://www.w3.org/2000/svg">
				<path
						d="M22 1.375C10.6092 1.375 1.375 10.6092 1.375 22C1.375 33.3908 10.6092 42.625 22 42.625C33.3908 42.625 42.625 33.3908 42.625 22C42.625 10.6092 33.3908 1.375 22 1.375ZM34.6395 34.6395C31.5054 37.7643 27.3202 39.6102 22.8994 39.8178C18.4786 40.0253 14.1388 38.5795 10.7258 35.7621C7.31275 32.9447 5.07088 28.9575 4.43703 24.5775C3.80317 20.1974 4.82273 15.7383 7.29705 12.0689C9.77137 8.39948 13.5232 5.78272 17.8215 4.72847C22.1198 3.67423 26.6566 4.25801 30.548 6.36608C34.4393 8.47414 37.4065 11.9555 38.8712 16.1317C40.336 20.308 40.1935 24.88 38.4714 28.9569C37.5739 31.0812 36.2725 33.0111 34.6395 34.6395Z"
						fill="black"/>
				<path
						d="M13.0625 17.1875H16.5V20.625H13.0625V17.1875ZM27.5 17.1875H30.9375V20.625H27.5V17.1875ZM22 24.0625C19.6296 24.0625 17.3563 25.0041 15.6802 26.6802C14.0041 28.3563 13.0625 30.6296 13.0625 33H30.9375C30.9375 30.6296 29.9959 28.3563 28.3198 26.6802C26.6437 25.0041 24.3704 24.0625 22 24.0625Z"
						fill="black"/>
			</svg>
		</div>

		<button class="cabinet__address-add-btn js_btn_popup" data-btn="save-address"
				type="button">
			Добавить новый адрес
		</button>

	</div>
<?php endif; ?>