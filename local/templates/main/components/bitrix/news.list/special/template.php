<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="special">
    <div class="container">
        <div class="special__wrap">
            <h2 class="special__title title">Специальные предложения</h2>
            <div class="special__inner">
                <?foreach ($arResult["ITEMS"] as $key => $item):?>
                    <a href="<?=$item["PROPERTIES"]["CATALOG_LINK"]["VALUE"];?>" class="special__item">
                        <img src="<?=CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]);?>">
                    </a>
                <?endforeach;?>
            </div>
        </div>
    </div>
</section>