<?

global $USER_FIELD_MANAGER;
global $USER;
$userID = $USER->GetID();
$rsUser = \CUser::GetByID($userID);
$arUser = $rsUser->Fetch();
$arProducts = $arUser["UF_FAVORITE_PRODUCTS"];

foreach ($arResult["ITEMS"] as $key => $item){
    if(count((array)$item["PROPERTIES"]["ACTION"]["VALUE"]) > 0){
        $arSelect = array("ID", "NAME", "PROPERTY_COLOR", "PREVIEW_TEXT", "PREVIEW_PICTURE");
        $arFilter = array("IBLOCK_ID" => $item["PROPERTIES"]["ACTION"]["LINK_IBLOCK_ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $item["PROPERTIES"]["ACTION"]["VALUE"]);
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $item["PROPERTIES"]["ACTION"]["VALUE_EXTRA"][] = $arFields;
            $arResult["ITEMS"][$key] = $item;
        }
    }

    $index = array_search($item["ID"], (array)$arProducts);

    if($index !== false){
        $arResult["ITEMS"][$key]["FAVORITE"] = true;
    }
}

//d($arResult);