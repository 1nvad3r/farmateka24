<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<?$this->SetViewTarget('countSearch');?>
<?php echo $arResult['NAV_RESULT']->NavRecordCount; ?>
<?$this->EndViewTarget();?>

	<div class="loader js_category_loader"></div>
	<? foreach ($arResult["ITEMS"] as $item): ?>
		<li class="category-main__product-item">
			<div class="card js_product_card" data-productid="productid<?= $item["ID"] ?>">
				<a class="card__img" href="<?= $item["DETAIL_PAGE_URL"] ?>">
					<img src="<?= ($item["PREVIEW_PICTURE"]) ? CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]) : SITE_TEMPLATE_PATH . '/images/notimages.png'; ?>"
						 alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>">
					<div class="card__labels">
						<? foreach ($item["PROPERTIES"]["ACTION"]["VALUE_EXTRA"] as $action): ?>
							<object>
								<a class="card__action-wrapper"
								   style="background-color: #<?= $action["PROPERTY_COLOR_VALUE"] ?>;"
								   href="#">
									<p class="card__action">
										<?= $action["NAME"] ?>
									</p>

									<p class="card__action-description">
										<? if (!empty($action["PREVIEW_PICTURE"])): ?>
											<img class="row-card__bonus-img"
												 src="<?= CFile::GetPath($action["PREVIEW_PICTURE"]); ?>">
										<? endif; ?>
										<?= $action["PREVIEW_TEXT"] ?>
									</p>
								</a>
							</object>
						<? endforeach; ?>
					</div>
				</a>
				<a class="card__title" href="<?= $item["DETAIL_PAGE_URL"] ?>"><?= $item["NAME"] ?></a>
				<? if (!empty($item["PROPERTIES"]["BRAND"]["VALUE"])): ?>
					<div class="card__brand">
						Бренд:
						<a href="#"><?= $item["PROPERTIES"]["BRAND"]["VALUE"] ?></a>
					</div>
				<? endif; ?>
				<div class="card__bottom">
					<? if (!empty($item["PRICES"]["BASE"]["DISCOUNT_VALUE"]) && $item["PRICES"]["BASE"]["DISCOUNT_VALUE"] != $item["PRICES"]["BASE"]["VALUE"]): ?>
						<div class="card__price"><?= $item["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
						<div class="card__old-price"><?= $item["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"] ?></div>
					<? else: ?>
						<div class="card__price"><?= $item["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
					<? endif; ?>
					<button class="card__heart js_card_heart" type="button">
						<svg width="33" height="28">
							<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart' ?>"></use>
						</svg>
						<svg width="33" height="28">
							<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart-filled' ?>"></use>
						</svg>
						<span class="card__heart-description">
                                                добавить в избранное
                                            </span>
					</button>
				</div>
				<div class="card__buttons">
					<div class="card__btn card__btn-1 btn btn--green js_btn_popup"
						 data-btn="added-to-card"
						 data-action="<?= $item["ADD_URL"] ?>"
						 id="add<?= $item["ID"] ?>"
						 rel="nofollow"
						 onclick="addToCart(this, $('#<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?><?= $item["ID"] ?>').val(), <?= $item["PRICES"]["BASE"]["DISCOUNT_VALUE"] ?>); return false;">
                                            <span>
                                                В корзину
                                                <svg width="27" height="22">
                                                    <use
														xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-cart' ?>"></use>
                                                </svg>
                                            </span>
					</div>
					<div class="card__btn card__btn-2 btn btn--green js_btn_popup"
						 data-btn="report">
						Сообщить о поступлении
					</div>
					<div class="card__btn card__btn-3 btn btn--green">
						Вы узнаете о поступлении
					</div>
					<div class="card__btn card__btn-4 btn btn--green js_btn_popup" data-btn="order">
						Заказать
					</div>
				</div>
			</div>
		</li>
	<? endforeach; ?>




<?php// $arResult["NAV_STRING"] ?>
