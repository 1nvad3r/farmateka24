<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>

<div class="swiper popular-row-product-slider__swiper js_popular_row_product_slider_swiper swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden">
	<div class="swiper_white_bottoms"></div>
	<div class="swiper-wrapper">

		<? foreach ($arResult["ITEMS"] as $item): ?>
			<? $APPLICATION->IncludeComponent(
				"bitrix:catalog.element",
				"section_popular",
				[
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_ID" => $item["ID"],
					"SHOW_DEACTIVATED" => $arParams["SHOW_DEACTIVATED"],
					"SET_TITLE" => "N",
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"ADD_SECTIONS_CHAIN" => "N",
					"ADD_ELEMENT_CHAIN" => "N",
				]
			); ?>
		<? endforeach; ?>

		<? if ($arParams["DISPLAY_SHOW_MORE"] == "Y"): ?>
			<div class="swiper-slide">
				<div class="card card-link">
					<a href="<?= $arParams["SHOW_MORE_LINK"] ?>" class="card__more">
						<svg width="43" height="42">
							<use
								xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-more' ?>"></use>
						</svg>
						Показать больше
					</a>
				</div>
			</div>
		<? endif; ?>
	</div>
	<div class="swiper-scrollbar"></div>
</div>