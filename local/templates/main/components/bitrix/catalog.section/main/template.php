<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>

<? $this->SetViewTarget('countSearch'); ?>
<?php echo $arResult['NAV_RESULT']->NavRecordCount; ?>
<? $this->EndViewTarget(); ?>

<ul class="category-main__product-list rows js_category_main_product_list">
	<div class="loader js_category_loader"></div>
	<? foreach ($arResult["ITEMS"] as $item): ?>
		<li class="category-main__product-row-item">
			<div class="row-card js_product_card" data-productid="productid<?= $item["ID"] ?>">
				<a class="row-card__img-link" href="<?= $item["DETAIL_PAGE_URL"] ?>">
					<img class="row-card__img"
						 src="<?= ($item["PREVIEW_PICTURE"]) ? CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]) : SITE_TEMPLATE_PATH . '/images/notimages.png'; ?>"
						 alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>">
				</a>
				<div class="row-card__content">
					<a class="row-card__title" href="<?= $item["DETAIL_PAGE_URL"] ?>">
						<?= $item["NAME"] ?>
					</a>

					<div class="row-card__terms-wrapper">
						<div class="row-card__delivery-and-availability">
							<p class="row-card__delivery">
								<? if ($item['PROPERTIES']['RX']['VALUE']): ?>
									Самовывоз
								<? else: ?>
									Самовывоз, доставка курьером
								<? endif; ?>
							</p>

							<? if ($item["PRODUCT"]["QUANTITY"] > 0): ?>
								<p class="row-card__availability">
									В наличии
								</p>
							<? endif; ?>
						</div>

						<div class="row-card__prices-wrapper">
							<? if ($item["PRICES"]["BASE"]["VALUE"]): ?>
								<span class="row-card__price-text">от</span>
							<? endif; ?>
							<? if (!empty($item["PRICES"]["BASE"]["DISCOUNT_VALUE"]) && $item["PRICES"]["BASE"]["DISCOUNT_VALUE"] != $item["PRICES"]["BASE"]["VALUE"]): ?>
								<span class="row-card__price"><?= $item["PRICES"]["BASE"]["PRINT_VALUE"] ?></span>
								<span
										class="row-card__sale"><?= $item["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"] ?>%</span>
								<span
										class="row-card__old-price"><?= $item["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"] ?></span>
							<? else: ?>
								<span class="row-card__price"><?= $item["PRICES"]["BASE"]["PRINT_VALUE"] ?></span>
							<? endif; ?>
						</div>
					</div>

					<ul class="row-card__offers-list">
						<? foreach ($item["PROPERTIES"]["ACTION"]["VALUE_EXTRA"] as $action): ?>
							<li class="row-card__offer-item">
								<a class="row-card__offer"
								   style="background-color: #<?= $action["PROPERTY_COLOR_VALUE"] ?>;"
								   href="#"><?= $action["NAME"] ?></a>
								<p class="row-card__offer-description">
									<? if (!empty($action["PREVIEW_PICTURE"])): ?>
										<img class="row-card__bonus-img"
											 src="<?= CFile::GetPath($action["PREVIEW_PICTURE"]); ?>">
									<? endif; ?>
									<?= $action["PREVIEW_TEXT"] ?>
								</p>
							</li>
						<? endforeach; ?>
					</ul>

					<ul class="row-card__characteristics-list">
						<? if (!empty($item["PROPERTIES"]["BRAND"]["VALUE"])): ?>
							<li class="row-card__characteristics-item">
                                                <span class="row-card__characteristics-name">
                                                    Бренд:
                                                </span>

								<!-- для нижнего подчеркивания добавь модификатор --underline -->
								<a class="row-card__characteristic--underline" href="#">
									<?= $item["PROPERTIES"]["BRAND"]["VALUE"] ?>
								</a>
							</li>
						<? endif; ?>

						<? if (!empty($item["PROPERTIES"]["BRAND_NAME"]["VALUE"])): ?>
							<li class="row-card__characteristics-item">
                                                <span class="row-card__characteristics-name">
                                                    Бренд:
                                                </span>

								<!-- для нижнего подчеркивания добавь модификатор --underline -->
								<a class="row-card__characteristic--underline" href="#">
									<?= $item["PROPERTIES"]["BRAND_NAME"]["VALUE"] ?>
								</a>
							</li>
						<? endif; ?>

						<? if (!empty($item["PROPERTIES"]["VENDOR_NAME"]["VALUE"])): ?>
							<li class="row-card__characteristics-item">
                                                <span class="row-card__characteristics-name">
                                                    Производитель:
                                                </span>

								<!-- для нижнего подчеркивания добавь модификатор --underline -->
								<a class="row-card__characteristic" href="#">
									<?= $item["PROPERTIES"]["VENDOR_NAME"]["VALUE"] ?>
								</a>
							</li>
						<? endif; ?>
						<? if (!empty($item["PROPERTIES"]["MANUFACTURER"]["VALUE"])): ?>
							<li class="row-card__characteristics-item">
                                                <span class="row-card__characteristics-name">
                                                    Производитель:
                                                </span>

								<!-- для нижнего подчеркивания добавь модификатор --underline -->
								<a class="row-card__characteristic" href="#">
									<?= $item["PROPERTIES"]["MANUFACTURER"]["VALUE"] ?>
								</a>
							</li>
						<? endif; ?>

						<? if (!empty($item["PROPERTIES"]["COUNTRY_NAME"]["VALUE"])): ?>
							<li class="row-card__characteristics-item">
                                                <span class="row-card__characteristics-name">
                                                    Страна производства:
                                                </span>

								<!-- для нижнего подчеркивания добавь модификатор --underline -->
								<a class="row-card__characteristic" href="#">
									<?= $item["PROPERTIES"]["COUNTRY_NAME"]["VALUE"] ?>
								</a>
							</li>
						<? endif; ?>

						<? if (!empty($item["PROPERTIES"]["AGENT"]["VALUE"])): ?>
							<li class="row-card__characteristics-item">
                                                <span class="row-card__characteristics-name">
                                                    Действующее вещ-во:
                                                </span>

								<!-- для нижнего подчеркивания добавь модификатор --underline -->
								<a class="row-card__characteristic" href="#">
									<?= $item["PROPERTIES"]["AGENT"]["VALUE"] ?>
								</a>
							</li>
						<? endif; ?>

						<? if (!empty($item["PROPERTIES"]["DISPENSING"]["VALUE"])): ?>
						<li class="row-card__characteristics-item">
                                                <span class="row-card__characteristics-name">
                                                    Порядок отпуска:
                                                </span>

							<!-- для нижнего подчеркивания добавь модификатор --underline -->
							<a class="row-card__characteristic" href="#">
								<?= $item["PROPERTIES"]["DISPENSING"]["VALUE"] ?>
							</a>
						</li>
					</ul>
					<? endif; ?>
					<? if ($item["PRICES"]["BASE"]["VALUE"]): ?>
						<div class="row-card__bottom">

							<div class="card__buttons row-card__buttons">
								<div class="card__btn card__btn-1 btn btn--green js_btn_popup"
									 data-btn="added-to-card"
									 data-action="<?= $item["ADD_URL"] ?>"
									 id="add<?= $item["ID"] ?>"
									 rel="nofollow"
									 onclick="addToCart(this, $('#<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?><?= $item["ID"] ?>').val(), <?= $item["PRICES"]["BASE"]["DISCOUNT_VALUE"] ?>); return false;">
                                                    <span>
                                                        В корзину
                                                        <svg width="27" height="22">
                                                            <use
																	xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-cart' ?>"></use>
                                                        </svg>
                                                    </span>
								</div>
								<div class="card__btn card__btn-2 btn btn--green js_btn_popup"
									 data-btn="report">
									Сообщить о поступлении
								</div>
								<div class="card__btn card__btn-3 btn btn--green">
									Вы узнаете о поступлении
								</div>
								<div class="card__btn card__btn-4 btn btn--green js_btn_popup"
									 data-btn="order">
									Заказать
								</div>
							</div>

							<button class="card__heart row-card__heart
                        <? if ($USER->IsAuthorized()): ?> js_card_heart <?= $item["FAVORITE"] ? "active" : "" ?><? else: ?>js_btn_popup<? endif; ?>"
									data-btn="log-in" data-id="<?= $item["ID"] ?>" type="button" <? if (!$USER->IsAuthorized()): ?>onclick="popUpAuth();"<? endif; ?>>
								<svg width="33" height="28">
									<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart' ?>"></use>
								</svg>
								<svg width="33" height="28">
									<use
											xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart-filled' ?>"></use>
								</svg>
								<span class="card__heart-description">
                                                    добавить в избранное
                                                </span>
							</button>
						</div>
					<? endif; ?>
				</div>
			</div>
		</li>
	<? endforeach; ?>
	<? foreach ($arResult["ITEMS"] as $item): ?>
		<li class="category-main__product-item">
			<div class="card js_product_card" data-productid="productid<?= $item["ID"] ?>">
				<a class="card__img" href="<?= $item["DETAIL_PAGE_URL"] ?>">
					<img src="<?= ($item["PREVIEW_PICTURE"]) ? CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]) : SITE_TEMPLATE_PATH . '/images/notimages.png'; ?>"
						 alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>" Ы>
					<div class="card__labels">
						<? foreach ($item["PROPERTIES"]["ACTION"]["VALUE_EXTRA"] as $action): ?>
							<object>
								<a class="card__action-wrapper"
								   style="background-color: #<?= $action["PROPERTY_COLOR_VALUE"] ?>;"
								   href="#">
									<p class="card__action">
										<?= $action["NAME"] ?>
									</p>

									<p class="card__action-description">
										<? if (!empty($action["PREVIEW_PICTURE"])): ?>
											<img class="row-card__bonus-img"
												 src="<?= CFile::GetPath($action["PREVIEW_PICTURE"]); ?>">
										<? endif; ?>
										<?= $action["PREVIEW_TEXT"] ?>
									</p>
								</a>
							</object>
						<? endforeach; ?>
					</div>
				</a>
				<a class="card__title" href="<?= $item["DETAIL_PAGE_URL"] ?>"><?= $item["NAME"] ?></a>
				<? if (!empty($item["PROPERTIES"]["BRAND"]["VALUE"])): ?>
					<div class="card__brand">
						Бренд:
						<a href="#"><?= $item["PROPERTIES"]["BRAND"]["VALUE"] ?></a>
					</div>
				<? endif; ?>
				<? if (!empty($item["PROPERTIES"]["VENDOR_NAME"]["VALUE"])): ?>
					<div class="card__brand">
						Производитель:
						<a href="#"><?= $item["PROPERTIES"]["VENDOR_NAME"]["VALUE"] ?></a>
					</div>
				<? endif; ?>
				<? if ($item["PRICES"]["BASE"]["VALUE"]): ?>
				<div class="card__bottom">
					<? if (!empty($item["PRICES"]["BASE"]["DISCOUNT_VALUE"]) && $item["PRICES"]["BASE"]["DISCOUNT_VALUE"] != $item["PRICES"]["BASE"]["VALUE"]): ?>
						<div class="card__price"><?= $item["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
						<div class="card__old-price"><?= $item["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"] ?></div>
					<? else: ?>
						<div class="card__price"><?= $item["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
					<? endif; ?>
					<button class="card__heart row-card__heart
                        <? if ($USER->IsAuthorized()): ?> js_card_heart <?= $item["FAVORITE"] ? "active" : "" ?><? else: ?>js_btn_popup<? endif; ?>"
							data-btn="log-in" data-id="<?= $item["ID"] ?>" type="button" <? if (!$USER->IsAuthorized()): ?>onclick="popUpAuth();"<? endif; ?>>
						<svg width="33" height="28">
							<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart' ?>"></use>
						</svg>
						<svg width="33" height="28">
							<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart-filled' ?>"></use>
						</svg>
						<span class="card__heart-description">
                                                добавить в избранное
                                            </span>
					</button>
				</div>
				<div class="card__buttons empty-1">
					<div class="card__btn card__btn-1 btn btn--green js_btn_popup"
						 data-btn="added-to-card"
						 data-action="<?= $item["ADD_URL"] ?>"
						 id="add<?= $item["ID"] ?>"
						 rel="nofollow"
						 onclick="addToCart(this, $('#<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?><?= $item["ID"] ?>').val(), <?= $item["PRICES"]["BASE"]["DISCOUNT_VALUE"] ?>); return false;">
                                            <span>
                                                В корзину
                                                <svg width="27" height="22">
                                                    <use
															xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-cart' ?>"></use>
                                                </svg>
                                            </span>
					</div>
					<div class="card__btn card__btn-2 btn btn--green js_btn_popup"
						 data-btn="report">
						Сообщить о поступлении
					</div>
					<div class="card__btn card__btn-3 btn btn--green">
						Вы узнаете о поступлении
					</div>
					<div class="card__btn card__btn-4 btn btn--green js_btn_popup" data-btn="order">
						Заказать
					</div>
				</div>
				<? endif; ?>
			</div>
		</li>
	<? endforeach; ?>


</ul>

<?= $arResult["NAV_STRING"] ?>
