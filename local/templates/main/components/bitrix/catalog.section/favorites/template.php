<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<div class="favorites__main-wrapper active js_favorites_main">
	<ul class="favorites__tags-list">
		<li class="favorites__tag-item">
			<!-- при клике покажутся товары с атрибутом data-group='1' -->
			<button class="favorites__tag-btn active js_favorites_tag_btn" type="button" data-group='1'>
				В наличии в аптеках
			</button>
		</li>

		<li class="favorites__tag-item">
			<!-- при клике покажутся товары с атрибутом data-group='2' -->
			<button class="favorites__tag-btn js_favorites_tag_btn" type="button" data-group='2'>
				В наличии на складе
			</button>
		</li>
	</ul>

	<ul class="favorites__product-wrapper">

		<? foreach ($arResult["ITEMS"] as $item): ?>
			<li class="favorites__product-item js_favorites_item active" data-group='1'>
				<? $APPLICATION->IncludeComponent(
					"bitrix:catalog.element",
					"",
					[
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"ELEMENT_ID" => $item["ID"],
						"SHOW_DEACTIVATED" => $arParams["SHOW_DEACTIVATED"],
						"SET_TITLE" => "N",
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"ADD_SECTIONS_CHAIN" => "N",
						"ADD_ELEMENT_CHAIN" => "N",
					]
				); ?>
			</li>
		<? endforeach; ?>
	</ul>
</div>
