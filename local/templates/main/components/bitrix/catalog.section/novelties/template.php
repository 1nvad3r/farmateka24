<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="popular popular--mod1">
    <div class="container">
        <div class="popular__wrap">
            <h2 class="popular__title title">Новинки</h2>
            <div class="popular__inner js_popular_inner">

                <div class="popular__tabs">
                    <?$count=true;?>
                    <?foreach ($arResult["SECTIONS"] as $key => $section):?>
                    <?if($key == 42) continue;?>
                        <button class="popular__tab js_popular_tab btn <?=$count?'active':''?>" type="button"
                                style="background-color: <?=$section["COLOR"]?>; color: <?=$section["COLOR"]?>;">
                            <span><?=$section["NAME"]?></span>
                        </button>
                        <?$count=false;?>
                    <?endforeach;?>
                    <?$count=true;?>
                </div>

                <div class="popular__content">

                    <?foreach ($arResult["SECTIONS"] as $section):?>
                        <?if($key == 42) continue;?>
                    <div class="popular__item js_popular_item <?=$count?'active':''?>">
                        <?$count=false;?>
                        <div class="popular__swiper swiper">
                            <div class="swiper-wrapper">

                                <?foreach ($section["ITEMS"] as $item):?>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:catalog.element",
                                        "",
                                        Array(
                                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                            "IBLOCK_ID" =>  $arParams["IBLOCK_ID"],
                                            "ELEMENT_ID" => $item["ID"],
                                            "SHOW_DEACTIVATED" => $arParams["SHOW_DEACTIVATED"],

                                        )
                                    );?>
                                <?endforeach;?>

                                <div class="swiper-slide">
                                    <div class="card card-link">
                                        <a href="#" class="card__more">
                                            <svg width="43" height="42">
                                                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#card-more'?>"></use>
                                            </svg>
                                            Показать больше
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-scrollbar"></div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>