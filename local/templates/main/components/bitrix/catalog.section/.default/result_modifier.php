<?

	$arFilter = ['IBLOCK_ID' => 5];
	$db_list = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, true, ["UF_COLOR"]);

	$sections = [];

	while ($ar_result = $db_list->GetNext()) {
		$sections[$ar_result['ID']]["NAME"] = $ar_result['NAME'];
		if ($ar_result['UF_COLOR']) {
			$rsEnum = CUserFieldEnum::GetList([], ["ID" => $ar_result['UF_COLOR']]);
			if ($arEnum = $rsEnum->GetNext()) {
				$sections[$ar_result['ID']]["COLOR"] = $arEnum["VALUE"];
			}
		}
	}

	foreach ($arResult["ITEMS"] as $key => $item) {
		foreach ($item["PROPERTIES"]["SECTION"]["VALUE"] as $sectionID) {
			$sections[$sectionID]["ITEMS"][$key] = $item;
		}
	}

	foreach ($sections as $key => $section) {
		if (count((array)$section["ITEMS"]) == 0) {
			unset($sections[$key]);
		}
	}

	$arResult["SECTIONS"] = $sections;
?>