<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="popup popup--change-password js_popup js_popup_change_password" data-popup="change-password">
    <div class="popup__wrap">
        <button class="popup__exit js_popup_exit" type="button">
            <svg width="24" height="24">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
            </svg>
        </button>
        <form  name="bform" method="post" target="_top" action="/auth<?=$arResult["AUTH_URL"]?>" class="popup__form js_popup_form js_popup_report_form">
            <?if (strlen($arResult["BACKURL"]) > 0):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif?>
            <div class="popup__title">Восстановление пароля</div>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">
            <div class="popup__fields">
                <div class="popup__field field">
                    <input class="js_input_email" type="email" name="USER_LOGIN" placeholder="Email">
                </div>
            </div>

            <button name="send_account_info" class="popup__btn btn btn--tr">Восстановить</button>

            <!-- чтобы показать ошибку добавь класс active -->
            <p class="popup__warning-text ">
                Нет пользователя с таким e-mail, вы можете
                <a href="#" class="js_btn_popup popup__warning-link" data-btn="registration">зарегистрироваться</a>
            </p>
        </form>
    </div>
</div>

<!-- ПОП-АП 'ВОССТАНОВЛЕНИЕ ПАРОЛЯ Отправлена контрольная строка' -->
<div class="popup popup--change-password  js_popup js_popup_change_password_wait" data-popup="change-password-wait">
    <div class="popup__wrap">
        <button class="popup__exit js_popup_exit" type="button">
            <svg width="24" height="24">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
            </svg>
        </button>
        <form class="popup__form js_popup_form js_popup_report_form">
            <div class="popup__title">Восстановление пароля</div>

            <p class="popup__wait-text">
                Контрольная строка, а также ваши регистрационные данные были высланы на email. Пожалуйста, дождитесь
                письма, так как контрольная строка изменяется при каждом запросе.
            </p>
        </form>
    </div>
</div>

