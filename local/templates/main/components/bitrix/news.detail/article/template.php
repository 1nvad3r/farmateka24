<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
?>
<section class="article">
    <div class="container">
        <div class="article__spoiler-bottom">
            <h1 class="article__title">
                <?=$arResult["NAME"]?>
            </h1>

            <?if(!empty($arResult["DETAIL_PICTURE"])):?>
                <img class="article__spoiler-img" src="<?=CFile::GetPath($arResult["DETAIL_PICTURE"]["ID"]);?>" alt="<?=$arResult["NAME"]?>">
            <?endif;?>

            <?if(!empty($arResult["DETAIL_TEXT"])):?>
                <?=$arResult["DETAIL_TEXT"]?>
            <?endif;?>

            <a class="article__spoiler-back-link" href="<?=$arResult["LIST_PAGE_URL"]?>">Вернуться ко всем статьям</a>
        </div>
    </div>
</section>
