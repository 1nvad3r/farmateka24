<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="preview">
    <div class="container">
        <div class="preview__wrap">
            <img src="<?=CFile::GetPath($arResult["DETAIL_PICTURE"]["ID"]);?>">
            <div class="preview__cnt">
                <div class="preview__descr">
                    <?=$arResult["~DETAIL_TEXT"]?>
                </div>
                <a class="preview__btn" href="<?=$arResult["PROPERTIES"]["BTN_LINK"]["VALUE"]?>"><?=$arResult["PROPERTIES"]["BTN_TEXT"]["VALUE"]?></a>
            </div>
        </div>
    </div>
</section>