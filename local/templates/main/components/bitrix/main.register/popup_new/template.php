<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="popup popup--log-in js_popup js_popup_registration" data-popup="registration">
    <div class="popup__wrap">
        <button class="popup__exit js_popup_exit" type="button">
            <svg width="24" height="24">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
            </svg>
        </button>
        <form class="popup__form js_popup_form"  method="post" target="_top">
            <div class="popup__title">Регистрация</div>

            <ul class="popup__switch-btns-list">
                <li class="popup__switch-btn-item">
                    <button class="popup__switch-btn popup__switch-btn--left js_popup_registration_email_phone_btn"
                            type="button">
                        Телефон
                    </button>
                </li>
                <li class="popup__switch-btn-item">
                    <button class="popup__switch-btn popup__switch-btn--right active" type="button">
                        Email
                    </button>
                </li>
            </ul>

            <div class="popup__fields">
                <div class="popup__field field">
                    <input class="js_input_email" name="REGISTER[LOGIN]" type="email" placeholder="Email">
                </div>
                <div class="popup__field field">
                    <input class="js_input_password" name="REGISTER[PASSWORD]" type="password" placeholder="Пароль">
                </div>
            </div>

            <button class="popup__btn btn btn--tr">Зарегистрироваться</button>

            <!-- чтобы показать ошибку добавь класс active -->
            <p class="popup__warning-text">
                Пользователь с таким e-mail уже зарегистрирован
            </p>

            <p class="popup__text">
                Уже есть личный кабинет?
                <button class="popup__registration js_popup_registration_log_in_btn" type="button">
                    Вход
                </button>
            </p>
        </form>
    </div>
</div>

    <div class="popup popup--personal-data js_popup js_popup_personal_data" data-popup="personal-data">
        <div class="popup__wrap">
            <button class="popup__exit js_popup_exit_personal_data" type="button">
                <svg width="24" height="24">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
                </svg>
            </button>
            <form class="popup__form js_popup_form js_popup_report_form" method="post" target="_top" action="/registration/">
                <div class="popup__title">Персональная информация</div>



                <div class="personal-data__top-label-wrapper">
                    <label class="personal-data__client-label" for="">
                        <span class="personal-data__clients-title required">
                            Имя
                        </span>

                        <input class="personal-data__client-input js_input_name js_personal_data_name_input" name="REGISTER[NAME]"
                               type="text">
                    </label>

                    <label class="personal-data__client-label" for="">
                        <span class="personal-data__clients-title">
                            Фамилия
                        </span>

                        <input class="personal-data__client-input js_checkout_client_name_input" name="REGISTER[SECOND_NAME]" type="text">
                    </label>

                    <label class="personal-data__client-label" for="">
                        <span class="personal-data__clients-title">
                            Отчество
                        </span>

                        <input class="personal-data__client-input js_checkout_client_name_input" name="REGISTER[LAST_NAME]" type="text">
                    </label>

                    <label class="personal-data__client-label" for="">
                        <span class="personal-data__clients-title">
                            Дата рождения
                        </span>

                        <div class="personal-data__birthday-wrapper">
                            <div class="personal-data__birthday-select-day my-select my-select--wrappered">
                                <select class="js_my_select" name="date_day" id="date_day">
                                    <option value="">День</option>
                                    <option value="01">1</option>
                                    <option value="02">2</option>
                                    <option value="03">3</option>
                                    <option value="04">4</option>
                                    <option value="05">5</option>
                                    <option value="06">6</option>
                                    <option value="07">7</option>
                                    <option value="08">8</option>
                                    <option value="09">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>

                            <div class="personal-data__birthday-select-month my-select my-select--wrappered">
                                <select class="js_my_select" name="date_mouth" id="date_mouth">
                                    <option value="">Месяц</option>
                                    <option value="01">январь</option>
                                    <option value="02">февраль</option>
                                    <option value="03">март</option>
                                    <option value="04">апрель</option>
                                    <option value="05">май</option>
                                    <option value="06">июнь</option>
                                    <option value="07">июль</option>
                                    <option value="08">август</option>
                                    <option value="09">сентябрь</option>
                                    <option value="10">октябрь</option>
                                    <option value="11">ноябрь</option>
                                    <option value="12">декабрь</option>
                                </select>
                            </div>

                            <div class="personal-data__birthday-select-year my-select my-select--wrappered">
                                <select class="js_my_select" name="date_year" id="date_year">
                                    <option value="">Год</option>
                                </select>
                            </div>
                            <input type="hidden" id="birthday_date" name="REGISTER[PERSONAL_BIRTHDAY]">
                        </div>

                        <script>
                            /*function howMuchDays (year, month) {
                                var date1 = new Date(year, month-1, 1);
                                var date2 = new Date(year, month, 1);
                                return Math.round((date2 - date1) / 1000 / 3600 / 24);
                            }

                            choiseDay = new Choices(document.getElementById('date_day'), {
                                searchEnabled: false,
                                    searchPlaceholderValue: null,
                                    itemSelectText: '',
                                    placeholder: true,
                                    shouldSort: false
                            });
                            choiseMouth = new Choices(document.getElementById('date_mouth'), {
                                searchEnabled: false,
                                searchPlaceholderValue: null,
                                itemSelectText: '',
                                placeholder: true,
                                shouldSort: false
                            });
                            choiseYear = new Choices(document.getElementById('date_year'), {
                                searchEnabled: false,
                                searchPlaceholderValue: null,
                                itemSelectText: '',
                                placeholder: true,
                                shouldSort: false
                            });
                            var yearsArray = [];
                            for(var i = new Date().getFullYear(); i >= 1950; i--){
                                yearsArray.push( { value: (i).toString(), label: (i).toString() });
                            }
                            choiseYear.setChoices(yearsArray, 'value', 'label', true);

                            choiseDay.passedElement.element.addEventListener(
                                'change',
                                function(){
                                    $('#birthday_date').val(choiseDay.getValue(true)+"."+choiseMouth.getValue(true)+"."+choiseYear.getValue(true));
                                },
                                false,
                            );
                            choiseMouth.passedElement.element.addEventListener(
                                'change',
                                function(){
                                    setDays();
                                    $('#birthday_date').val(choiseDay.getValue(true)+"."+choiseMouth.getValue(true)+"."+choiseYear.getValue(true));
                                },
                                false,
                            );
                            choiseYear.passedElement.element.addEventListener(
                                'change',
                                function(){
                                    setDays();
                                    $('#birthday_date').val(choiseDay.getValue(true)+"."+choiseMouth.getValue(true)+"."+choiseYear.getValue(true));
                                },
                                false,
                            );
                            function setDays(){
                                var days = howMuchDays (choiseYear.getValue(true), choiseMouth.getValue(true));
                                var daysArray = [];
                                for(var i = 1; i <= days; i++){
                                    daysArray.push( { value: "0" + (i).toString().slice(-2), label: (i).toString() });
                                }
                                choiseDay.setChoices(daysArray, 'value', 'label', true);
                            }*/
                        </script>


                    </label>
                </div>

                <div class="personal-data__client-label personal-data__client-label--gender" for="">
                    <span class="personal-data__clients-title required">
                        Пол
                    </span>

                    <div class="personal-data__gender-inputs">
                        <label class="check personal-data__radio-check">
                            <input class="check__input js_boy_input" value="M" type="radio" name="REGISTER[PERSONAL_GENDER]">
                            <span class="check__box"></span>
                            Мужской
                        </label>

                        <label class="check personal-data__radio-check">
                            <input class="check__input js_girl_input" value="F" type="radio" name="REGISTER[PERSONAL_GENDER]">
                            <span class="check__box"></span>
                            Женский
                        </label>
                    </div>
                </div>
                <input type="hidden" name="TYPE" value="REGISTRATION"/>
                <input type="hidden" name="register_submit_button" value="Y"/>

                <button class="popup__btn btn js_popup_code_btn btn--tr">Сохранить</button>

            </form>
        </div>
    </div>

    <!-- ПОП-АП 'ОСТАНОВКА РЕГИСТРАЦИИ' -->
    <div class="popup popup--stop-registration js_popup js_popup_stop_registration" data-popup="stop-registration">
        <div class="popup__wrap">
            <button class="popup__exit js_popup_exit" type="button">
                <svg width="24" height="24">
                    <use xlink:href="images/sprite.svg#exit"></use>
                </svg>
            </button>
            <form class="popup__form js_popup_form js_popup_report_form">
                <div class="popup__title">Вы уверены, что хотите прекратить регистрацию?</div>

                <ul class="popup__stop-btns">
                    <li class="popup__stop-btn-item">
                        <button class="popup__btn popup__btn--finish btn btn--tr js_stop_registration_finish_btn"
                                type="button">Да, прекратить</button>
                    </li>
                    <li class="popup__stop-btn-item">
                        <button class="popup__btn popup__btn--continue btn btn--tr js_stop_registration_continue_btn"
                                type="button">Нет, продолжить</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>

    <!-- ПОП-АП 'РЕГИСТРАЦИЯ СМС' -->
    <div class="popup popup--log-in js_popup js_popup_registration_sms" data-popup="registration-sms">
        <div class="popup__wrap">
            <button class="popup__exit js_popup_exit" type="button">
                <svg width="24" height="24">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
                </svg>
            </button>
            <form class="popup__form js_popup_form js_popup_report_form">
                <div class="popup__title">Регистрация</div>
				<?
					$userTemp = 'user'.mt_rand();
					$mailTemp = $userTemp.'@pharmateca24.ru';

					if($_REQUEST['register_submit_button']){
						$arResult["VALUES"]['LOGIN'] = $_REQUEST['login'];
						$arResult["VALUES"]['EMAIL'] = $_REQUEST['email'];
						$arResult["VALUES"]['PASSWORD'] = $_REQUEST['pass'];
						$arResult["VALUES"]['CONFIRM_PASSWORD'] = $_REQUEST['ch_pass'];
					}

				?>
				<input type="hidden" name="REGISTER[LOGIN]" value="<?= $userTemp ?>" class="register-field__login"/>
				<input type="hidden" name="REGISTER[EMAIL]" value="<?= $mailTemp ?>" class="register-field__email"/>
				<input type="hidden" name="REGISTER[PASSWORD]" value="<?= $userTemp ?>" class="register-field__pass"/>
				<input type="hidden" name="REGISTER[CONFIRM_PASSWORD]" value="<?= $userTemp ?>" class="register-field__pass_ch"/>


                <ul class="popup__switch-btns-list">
                    <li class="popup__switch-btn-item">
                        <button class="popup__switch-btn popup__switch-btn--left active" type="button">
                            Телефон
                        </button>
                    </li>
                    <li class="popup__switch-btn-item">
                        <button class="popup__switch-btn popup__switch-btn--right js_popup_registration_email_btn"
                                type="button">
                            Email
                        </button>
                    </li>
                </ul>

                <div class="popup__fields">
                    <div class="popup__field field">
                        <input class="js_input_masked_phone register-field__phone" type="text" placeholder="Телефон" name="REGISTER[PHONE_NUMBER]">
                    </div>
                </div>

                <button class="popup__btn btn btn--tr">Получить код по смс</button>

                <!-- чтобы показать ошибку добавь класс active -->
                <p class="popup__warning-text">
                    Пользователь с таким номером телефона уже зарегистрирован
                </p>

                <p class="popup__text">
                    Нет личного кабинета?
                    <button class="popup__registration js_popup_log_in_sms_registration_btn" type="button"
                            type="button">
                        Зарегистрируйтесь
                    </button>
                </p>
            </form>
        </div>
    </div>

    <!-- ПОП-АП 'РЕГИСТРАЦИЯ СМС КОД' -->
    <div class="popup popup--log-in js_popup js_popup_registration_sms_code" data-popup="registration-sms-code">
        <div class="popup__wrap">
            <button class="popup__exit js_popup_exit" type="button">
                <svg width="24" height="24">
                    <use xlink:href="images/sprite.svg#exit"></use>
                </svg>
            </button>
            <form class="popup__form js_popup_form js_popup_report_form">
                <div class="popup__title">Код подтверждения</div>

                <p class="popup__predescription-text">
                    На указанный номер был отправлен код. Для подтверждения, введите код из sms.
                </p>

                <ul class="popup__code-list">
<!--                    <li class="popup__code-item">
                        <input class="popup__code-input js_code_input" type="text" placeholder="—" maxlength="1">
                    </li>-->

                    <li class="popup__code-item">
                        <input class="popup__code-input js_code_input"  type="text" placeholder="——————" maxlength="6">
                    </li>

                   <!-- <li class="popup__code-item">
                        <input class="popup__code-input js_code_input" type="text" placeholder="—" maxlength="1">
                    </li>

                    <li class="popup__code-item">
                        <input class="popup__code-input js_code_input" type="text" placeholder="—" maxlength="1">
                    </li>

                    <li class="popup__code-item">
                        <input class="popup__code-input js_code_input" type="text" placeholder="—" maxlength="1">
                    </li>

					<li class="popup__code-item">
						<input class="popup__code-input js_code_input" type="text" placeholder="—" maxlength="1">
					</li>
					-->
                </ul>

                <p class="popup__timer-text js_code_time_text">
                    Введите код в течение
                    <span class="popup__timer-time js_code_time_span">
                        00:15
                    </span>
                </p>

                <button class="popup__get-new-code js_new_code" type="button">
                    Получить новый код
                </button>

                <button class="popup__btn btn js_popup_code_btn btn--tr">Отправить</button>

            </form>
        </div>
    </div>

<!-- ПОП-АП 'УСПЕШНАЯ РЕГИСТРАЦИЯ' -->
<div class="popup popup--registration-success js_popup js_popup_registration_success"
     data-popup="registration-success">
    <div class="popup__wrap">
        <button class="popup__exit js_popup_exit" type="button">
            <svg width="24" height="24">
                <use xlink:href="images/sprite.svg#exit"></use>
            </svg>
        </button>
        <form class="popup__form js_popup_form js_popup_report_form">
            <div class="popup__title">Вы успешно зарегистрировались!</div>

            <p class="popup__successfull-text">
                На адрес
                <span class="popup__successfull-text--accent">

                    </span>
                было отправлено письмо.
            </p>

            <p class="popup__successfull-text">
                Для подтверждения регистрации перейдите по ссылке из письма.
            </p>
        </form>
    </div>
</div>