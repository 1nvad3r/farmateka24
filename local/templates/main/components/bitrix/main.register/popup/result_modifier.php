
<?
if (strlen($_POST['ajax_mode'])) {
    $APPLICATION->RestartBuffer();
    if (!defined('PUBLIC_AJAX_MODE')) {
        define('PUBLIC_AJAX_MODE', true);
    }
    header('Content-type: application/json');
    if ($arResult['ERRORS']) {
        echo json_encode($arResult['ERRORS']);
    } else {
        echo json_encode(array('type' => 'ok',
            'name' => $arResult["USER_NAME"]));
    }
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
    die();
}
?>