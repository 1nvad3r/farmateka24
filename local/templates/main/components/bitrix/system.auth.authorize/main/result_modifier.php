<?
if (strlen($_POST['ajax_mode'])) {
    $APPLICATION->RestartBuffer();
    if (!defined('PUBLIC_AJAX_MODE')) {
        define('PUBLIC_AJAX_MODE', true);
    }
    header('Content-type: application/json');

    if ($arResult['ERROR']) {
        echo json_encode(array(
            'type' => 'error',
            'message' => strip_tags($arResult['ERROR_MESSAGE']['MESSAGE']),
        ));
    } else {
        //echo json_encode(array('type' => 'ok'));
    }
    echo json_encode($arResult);
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
    die();
}
?>