<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
    return "";
$strReturn = '';


$strReturn .= '<div class="breadcrumbs"><div class="container breadcrumbs__container">';

$itemSize = count((array)$arResult);
for($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
    {
        $strReturn .= '<a  id="bx_breadcrumb_'.$index.'" itemprop="itemListElement" itemscope  href="'.$arResult[$index]["LINK"].'">'.$title.'</a>';
    }
}

$strReturn .= '</div></div>';

return $strReturn;
