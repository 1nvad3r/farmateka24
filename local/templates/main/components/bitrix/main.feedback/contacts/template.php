<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<section class="feedback js_feedback" id="feedback">
    <div class="container">
        <h1 class="feedback__title">
            Обратная связь
        </h1>

        <p class="feedback__text">
            Если у Вас есть вопрос по работе сайта или Вы столкнулись с техническими проблемами, пожалуйста,
            оставьте заявку в форме обратной связи и наш менеджер свяжется с Вами в ближайшее рабочее время.
        </p>

        <span class="feedback__warning">
                    Поле обязательно для заполнения
                </span>

        <form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="feedback__form js_popup_form js_feedback_form">
            <?=bitrix_sessid_post()?>
            <div class="personal-data__top-label-wrapper">
                <label class="personal-data__client-label" for="">
                    <!-- если поле обязательно добавть класс required - добавится звездочка -->
                    <span class="personal-data__clients-title required">
                                Имя
                            </span>

                    <input class="personal-data__client-input js_input_name  <?=in_array("Укажите ваше имя.", (array)$arResult["ERROR_MESSAGE"])?"error":""?>" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" type="text">
                </label>

                <div class="feedback__labels-wrapper">
                    <label class="personal-data__client-label" for="">
                        <!-- если поле обязательно добавть класс required - добавится звездочка -->
                        <span class="personal-data__clients-title ">
                                    Номер телефона
                                </span>

                        <input class="personal-data__client-input js_input_masked_phone" name="user_phone" type="text">
                    </label>

                    <label class="personal-data__client-label" for="">
                        <!-- если поле обязательно добавть класс required - добавится звездочка -->
                        <span class="personal-data__clients-title required">
                                    Email
                                </span>

                        <input class="personal-data__client-input js_input_email <?=(in_array("Указанный E-mail некорректен.", (array)$arResult["ERROR_MESSAGE"])||in_array("Укажите E-mail, на который хотите получить ответ.", (array)$arResult["ERROR_MESSAGE"]))?"error":""?>" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" type="text">
                    </label>
                </div>



                <label class="popup__review-label--textarea" for="">
                            <span class="personal-data__clients-title popup__review-label-text required">
                                Сообщение
                            </span>

                    <textarea name="MESSAGE" class="popup__review-textarea js_input_messege <?=in_array("Вы не написали сообщение.", (array)$arResult["ERROR_MESSAGE"])?"error":""?>" id="" cols="30" rows="10"><?=$arResult["MESSAGE"]?></textarea>
                </label>
            </div>

            <!-- чтобы показать добавь класс active -->
            <label class="check feedback__check personal-data__check--terms active">
                <input class="check__input" checked type="checkbox">
                <span class="check__box"></span>
                <p>
                    Я ознакомлен и принимаю условия
                    <a href="/about/privacy-policy/" target="_blank">
                        политики конфиденциальности
                    </a>
                </p>

            </label>

            <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
            <input type="submit" class="feedback__btn btn js_popup_code_btn btn--tr" name="submit_form" value="Отправить">


        </form>
    </div>
</section>
<?
if($arResult["OK_MESSAGE"] <> '')
{?>
<div class="popup popup--feedback js_popup js_popup_feedback_success active" data-popup="feedback-success">
    <div class="popup__wrap">
        <button class="popup__exit js_popup_exit" type="button">
            <svg width="24" height="24">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
            </svg>
        </button>
        <form class="popup__form js_popup_form js_popup_report_form">
            <div class="popup__title"><?=$arResult["OK_MESSAGE"]?></div>
        </form>
    </div>
</div>

    <script>
        const ppups = document.querySelectorAll('.js_popup_feedback_success');
        ppups.forEach(popup => {
            const popupExits = popup.querySelectorAll('.js_popup_exit');
            popupExits.forEach(exit => {
                exit.addEventListener('click', () => {
                    popup.classList.remove('active');
                });

            });
            popup.addEventListener('click', (e) => {
                if (e.target == popup) {
                    popup.classList.remove('active');
                }
            });
        });
    </script>
<?}?>