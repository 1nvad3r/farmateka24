<?

	if (count((array)$arResult["PROPERTIES"]["ACTION"]["VALUE"]) > 0) {
		$arSelect = ["ID", "NAME", "PROPERTY_COLOR", "PREVIEW_TEXT", "PREVIEW_PICTURE"];
		$arFilter = ["IBLOCK_ID" => $arResult["PROPERTIES"]["ACTION"]["LINK_IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $arResult["PROPERTIES"]["ACTION"]["VALUE"]];
		$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$arResult["PROPERTIES"]["ACTION"]["VALUE_EXTRA"][] = $arFields;
		}
	}
	d($arResult["PROPERTIES"]["RELATED"]["VALUE"]);
	if (count((array)$arResult["PROPERTIES"]["RELATED"]["VALUE"]) > 0) {
		$arSelect = ["ID", "NAME", "DETAIL_PAGE_URL"];
		$arFilter = ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $arResult["PROPERTIES"]["RELATED"]["VALUE"]];
		$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$arResult["PROPERTIES"]["RELATED"]["VALUE_EXTRA"][] = $arFields;
		}
	}


	global $USER_FIELD_MANAGER;
	global $USER;
	$userID = $USER->GetID();
	$rsUser = \CUser::GetByID($userID);
	$arUser = $rsUser->Fetch();
	$arProducts = $arUser["UF_FAVORITE_PRODUCTS"];

	$index = array_search($arResult["ID"], (array)$arProducts);

	if ($index !== false) {
		$arResult["FAVORITE"] = true;
	}

	$GLOBALS['elementRemove'] = $arResult['ID'];
