<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
	global $USER;

	$haveOffers = !empty($arResult['OFFERS']);
	if ($haveOffers) {
		$actualItem = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']] ?? reset($arResult['OFFERS']);
		$showSliderControls = false;
	} else {
		$actualItem = $arResult;
	}
?>
	<section class="product">
		<?

			$arPriceGrid = array(
					'BASE' => $arResult["PRICES"]["BASE"]["VALUE_VAT"],
					'BASE_1' => $arResult["PRICES"]["BASE_1"]["VALUE_VAT"],
					'BASE_2' => $arResult["PRICES"]["BASE_2"]["VALUE_VAT"],
				'BASE_3' => $arResult["PRICES"]["BASE_3"]["VALUE_VAT"],
				'BASE_4' => $arResult["PRICES"]["BASE_4"]["VALUE_VAT"],
				'BASE_5' => $arResult["PRICES"]["BASE_5"]["VALUE_VAT"],
				);

			if(min($arPriceGrid)){
				$minPrice =  CurrencyFormat(min($arPriceGrid), "RUB");
			}

		?>
		<div class="container">
			<div class="product__top">
				<div class="product__left-column">
					<h1 class="product__title">
						<?= $arResult["NAME"] ?>
					</h1>

					<div class="swiper product__main-swiper js_product_main_swiper">
						<div class="swiper-wrapper">
							<div class="swiper-slide js_btn_popup" data-btn="product-gallery">
								<div class="product__main-slide-inner">
									<img class="product__img" alt="<?= $arResult["NAME"] ?>"
										 src="<?= ($arResult["DETAIL_PICTURE"]) ? CFile::GetPath($arResult["DETAIL_PICTURE"]["ID"]) : SITE_TEMPLATE_PATH . '/images/notimages.png'; ?>">
								</div>
							</div>
							<? foreach ($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $image): ?>
								<div class="swiper-slide js_btn_popup" data-btn="product-gallery">
									<div class="product__main-slide-inner">
										<img class="product__img" src="<?= CFile::GetPath($image); ?>"
											 alt="<?= $arResult["NAME"] ?>"/>
									</div>
								</div>
							<? endforeach; ?>
						</div>
					</div>

					<div class="product__small-swiper-wrapper">
						<button class="product__small-swiper-button-prev js_product_small_swiper_button_prev"
								type="button"></button>

						<div class="swiper product__small-swiper js_product_small_swiper">
							<div class="swiper-wrapper">
								<? if (!empty($arResult["DETAIL_PICTURE"]["ID"])): ?>
									<div class="swiper-slide">
										<div class="product__small-slide-inner">
											<img class="product__img"
												 src="<?= CFile::GetPath($arResult["DETAIL_PICTURE"]["ID"]); ?>">
										</div>
									</div>
								<? else: ?>
									<div class="swiper-slide">
										<div class="product__small-slide-inner">
											<img class="product__img"
												 src="<?= SITE_TEMPLATE_PATH . '/images/notimages.png' ?>">
										</div>
									</div>
								<? endif; ?>

								<? foreach ($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $image): ?>
									<div class="swiper-slide">
										<div class="product__small-slide-inner">
											<img class="product__img"
												 src="<?= CFile::GetPath($image); ?>">
										</div>
									</div>
								<? endforeach; ?>
							</div>

						</div>

						<button class="product__small-swiper-button-next js_product_small_swiper_button_next"
								type="button"></button>

					</div>
				</div>

				<div class="product__right-column">
					<div class="product__info-row">
						<a class="product__show-all-photos" href="#">
							Показать все фото
						</a>
					</div>


					<div class="product__star-row">
						<? if ($arResult["PRODUCT"]["QUANTITY"] > 0): ?>
							<p class="product__is-on-stock product__is-on-stock--true">
								В наличии
							</p>
						<? else: ?>
							<p class="product__is-on-stock product__is-on-stock--true">
								Нет в наличии
							</p>
						<? endif; ?>

						<?php $APPLICATION->IncludeComponent("alfateam:review.average", "", [
							"IBLOCK_ID" => $arParams["REVIEW_IBLOCK_ID"],
							"ELEMENT_ID" => $arResult["ID"],
							"SHOW_COUNT" => "Y",
							"CUSTOM_CLASS" => "product__rating",
							"CACHE_TYPE" => "N"
						]);
						?>
					</div>

					<? if ($arResult["PRICES"]["BASE"]["VALUE"]): ?>
						<div class="product__price-row">

							<div class="product__left-price-row">
								<div class="product__quantity-box quantity-box js_quantity_box">
									<button class="quantity-box__btn quantity-box__btn--minus js_number_minus"
											type="button">-
									</button>

									<input class="quantity-box__input js_product_quantity_input"
										   id="<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?><?= $arResult["ID"] ?>"
										   type="number" min="0"
										   value="1"/>

									<button class="quantity-box__btn quantity-box__btn--plus js_number_plus"
											type="button">+
									</button>
								</div>
								<? if($minPrice): ?>

									<div class="product__price-wrapper">
										<div class="product__price-left">
                                        <span class="product__price-text">
                                            от
                                        </span>
										</div>
										<div class="product__price-right">
											<div class="product__price-top">
                                            <span class="product__price">
                                               <?= $minPrice?>
                                            </span>
											</div>
										</div>
									</div>


								<? else: ?>


								<? if (!empty($arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"]) && $arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"] != $arResult["PRICES"]["BASE"]["VALUE"]): ?>
									<div class="product__price-wrapper">
										<div class="product__price-left">
                                        <span class="product__price-text">
                                            от
                                        </span>
										</div>

										<div class="product__price-right">
											<div class="product__price-top">
                                            <span class="product__price">
                                                <?= $arResult["PRICES"]["BASE"]["PRINT_VALUE"] ?>
                                            </span>
											</div>

											<div class="product__price-bottom">
                                            <span class="product__sale">
                                                <?= $arResult["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"] ?>%
                                            </span>

												<span class="product__price-old">
                                                <?= $arResult["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"] ?>
                                            </span>
											</div>
										</div>
									</div>
								<? else: ?>
									<div class="product__price-wrapper">
										<div class="product__price-left">
                                        <span class="product__price-text">
                                            от
                                        </span>
										</div>
										<div class="product__price-right">
											<div class="product__price-top">
                                            <span class="product__price">
                                                <?= $arResult["PRICES"]["BASE"]["PRINT_VALUE"] ?>
                                            </span>
											</div>
										</div>
									</div>
								<? endif; ?>

								<? endif; ?>
							</div>

							<div class="product__right-price-row">
								<div class="card__buttons offer__buttons">
									<div class="card__btn card__btn-1 btn btn--green js_btn_popup"
										 data-btn="added-to-card"
										 data-btn="added-to-card"
										 data-action="<?= $arResult["ADD_URL"] ?>"
										 id="add<?= $arResult["ID"] ?>"
										 rel="nofollow"
										 onclick="addToCart(this, $('#<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?><?= $arResult["ID"] ?>').val(), <?= $arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"] ?>); return false;"
									>
										<span>В корзину</span>
									</div>
									<div class="card__btn card__btn-2 btn btn--green js_btn_popup" data-btn="report">
										Сообщить о поступлении
									</div>
									<div class="card__btn card__btn-3 btn btn--green">
										Вы узнаете о поступлении
									</div>
									<div class="card__btn card__btn-4 btn btn--green js_btn_popup" data-btn="order">
										Заказать
									</div>
								</div>

								<button class="card__heart offer__heart
                        		<? if ($USER->IsAuthorized()): ?> js_card_heart <?= $arResult["FAVORITE"] ? "active" : "" ?><? else: ?>js_btn_popup<? endif; ?>"
										data-btn="log-in" data-id="<?= $arResult["ID"] ?>" type="button">
									<svg width="33" height="28">
										<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart' ?>"></use>
									</svg>
									<svg width="33" height="28">
										<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart-filled' ?>"></use>
									</svg>
									<span class="card__heart-description">
									добавить в избранное
								</span>
								</button>
								<?php
									$rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList([
										'filter' => ['=PRODUCT_ID' => $arResult["ID"], 'STORE.ACTIVE' => 'Y'],
										'select' => ['AMOUNT', 'STORE_ID', 'STORE_TITLE' => 'STORE.TITLE'],
									]);
									$cnt = 0;
									while ($arStoreProduct = $rsStoreProduct->fetch()) {
										$cnt += 1;
									}
								?>
								<div class="in__stock">
									<span>В наличии в <?= $cnt ?> аптеке</span>
									<br>Под заказ в любой аптеке
								</div>
							</div>

						</div>
					<? endif; ?>

					<div class="product__offer-row">
						<ul class="offer__offers-list">
							<? foreach ($arResult["PROPERTIES"]["ACTION"]["VALUE_EXTRA"] as $action): ?>
								<li class="offer__offer-item">
									<a class="offer__offer"
									   style="background-color: #<?= $action["PROPERTY_COLOR_VALUE"] ?>;"
									   href="#"><?= $action["NAME"] ?></a>

									<p class="offer__offer-description">
										<? if (!empty($action["PREVIEW_PICTURE"])): ?>
											<img class="row-card__bonus-img"
												 src="<?= CFile::GetPath($action["PREVIEW_PICTURE"]); ?>">
										<? endif; ?>
										<?= $action["PREVIEW_TEXT"] ?>
									</p>
								</li>
							<? endforeach; ?>
							<? if ($arResult["PROPERTIES"]["RX"]["VALUE"] != 0): ?>
								<li class="offer__offer-item">
									<a class="offer__offer" style="background-color: #588459;" href="#">По рецепту</a>

									<!-- это блок с пояснениями во всплывашке -->
									<p class="offer__offer-description">
										«По указу президента РФ № 187 от 17.03.2020 через курьерские службы разрешено
										отправлять безрецептурные препараты. Рецептурные лекарства можно только забрать
										из аптеки самовывозом.»
									</p>
								</li>
							<? endif; ?>
						</ul>
					</div>
					<? if ($arResult["PROPERTIES"]["RX"]["VALUE"] != 0): ?>
						<div class="product__delivery-row">
                            <span class="product__delivery-intro">
                                Возможные способы доставки:
                            </span>
							<span class="product__delivery-item">
                                Самовывоз
                            </span>
						</div>
					<? endif; ?>
					<ul class="product__characteristics-list">
						<? if (!empty($arResult["PROPERTIES"]["BRAND"]["VALUE"])): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Бренд:
                                </span>

								<a class="product__characteristics-value product__characteristics-value--link"
								   href="/catalog/?brand=<?= $arResult["PROPERTIES"]["BRAND"]["VALUE"] ?>" title="">
									<?= $arResult["PROPERTIES"]["BRAND"]["VALUE"] ?>
								</a>
							</li>
						<? elseif (!empty($arResult["PROPERTIES"]["BRAND_NAME"]["VALUE"])): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Бренд:
                                </span>

								<a class="product__characteristics-value product__characteristics-value--link"
								   href="/catalog/?brand_name=<?= $arResult["PROPERTIES"]["BRAND_NAME"]["VALUE"] ?>"
								   title="">
									<?= $arResult["PROPERTIES"]["BRAND_NAME"]["VALUE"] ?>
								</a>
							</li>
						<? endif; ?>

						<? if (!empty($arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"])): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Производитель:
                                </span>

								<span class="product__characteristics-value">
                            <?= $arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"] ?>
                                </span>
							</li>
						<? elseif (!empty($arResult["PROPERTIES"]["VENDOR_NAME"]["VALUE"])): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Производитель:
                                </span>

								<span class="product__characteristics-value">
                            <?= $arResult["PROPERTIES"]["VENDOR_NAME"]["VALUE"] ?>
                                </span>
							</li>
						<? endif; ?>

						<? if (!empty($arResult["PROPERTIES"]["COUNTRY"]["VALUE"])): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Страна производства:
                                </span>

								<span class="product__characteristics-value">
                            <?= $arResult["PROPERTIES"]["COUNTRY"]["VALUE"] ?>
                                </span>
							</li>
						<? elseif (!empty($arResult["PROPERTIES"]["COUNTRY_NAME"]["VALUE"])): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Страна производства:
                                </span>

								<span class="product__characteristics-value">
                            <?= $arResult["PROPERTIES"]["COUNTRY_NAME"]["VALUE"] ?>
                                </span>
							</li>
						<? endif; ?>

						<? if (!empty($arResult["PROPERTIES"]["AGENT"]["VALUE"])): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Действующее вещ-во:
                                </span>

								<span class="product__characteristics-value">
                            <?= $arResult["PROPERTIES"]["AGENT"]["VALUE"] ?>
                                </span>
							</li>
						<? endif; ?>

						<? if ($arResult["PROPERTIES"]["RX"]["VALUE"] == 0): ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Порядок отпуска:
                                </span>

								<a class="product__characteristics-value product__characteristics-value--link"
								   href="/catalog/?is_nark=no">
									Без рецепта
								</a>
							</li>
						<? else: ?>
							<li class="product__characteristics-item">
                                <span class="product__characteristics-name">
                                    Порядок отпуска:
                                </span>

								<a class="product__characteristics-value product__characteristics-value--link"
								   href="/catalog/?is_nark=yes">
									По рецепту
								</a>
							</li>
						<? endif; ?>

					</ul>

					<? if (!empty($arResult["PROPERTIES"]["RELATED"]["VALUE"])): ?>
						<div class="product__another-offers-row">
                            <span class="product__another-offers-title">
                                Варианты товара:
                            </span>

							<div class="product__another-offers-bottom">
								<ul class="product__another-offers-list">
									<? foreach ($arResult["PROPERTIES"]["RELATED"]["VALUE_EXTRA"] as $related): ?>
										<li class="product__another-offers-item">
											<a class="product__another-offers-link"
											   href="<?= $related["DETAIL_PAGE_URL"] ?>">
												<?= $related["NAME"] ?>
											</a>
										</li>
									<? endforeach; ?>
								</ul>

								<a class="product__another-offers-btn" href="#analog">
									Показать аналоги
								</a>
							</div>
						</div>
					<? endif; ?>
				</div>
			</div>
		</div>
	</section>

	<section class="description">
		<div class="container">
			<ul class="description__links-list">
				<? if (!empty($arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="1">
							Краткое описание
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["COMPOUND"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="2">
							Состав
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["APPLAYING"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="3">
							Способ применения
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["CHARACTERISTIC"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="4">
							Свойство
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["INDICATIONS"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="5">
							Показания к применению
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["CONTRAINDICATIONS"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="6">
							Противопоказания
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["SIDE_EFFECTS"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="7">
							Побочные действия
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["INTERACTION"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="8">
							Взаимодействие
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["OVERDOSE"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="9">
							Передозировка
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["STORAGE_CONDITIONS"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="10">
							Условия хранения
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["EXPIRATION_DATE"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="11">
							Срок годности
						</button>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["DISPENSING_TEXT"]["VALUE"])): ?>
					<li class="description__link-item">
						<button class="description__link js_description_link" type="button" data-num="12">
							Порядок отпуска
						</button>
					</li>
				<? endif; ?>
			</ul>

			<h2 class="description__title">
				Инструкция по применению <?= $arResult["NAME"]; ?>
			</h2>

			<p class="description__subtitle">
				Перед применением проконсультируйтесь с врачом!
			</p>

			<?php
				if ($arResult['PROPERTIES']['DESC_ID']['VALUE']):
					// URL страницы, которую открываем
					$url = 'https://rlsaurora10.azurewebsites.net/api/library_solid_description?desc_id=' . $arResult['PROPERTIES']['DESC_ID']['VALUE'];
					$username = 'aptekacelitel';
					$password = '703228143612';

					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);

					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$response = curl_exec($ch);
					curl_close($ch);
					?>
					<div class="rls-desc">
						<?= $response ?>
					</div>
				<? endif; ?>

			<ul class="description__spoilers-list">
				<? if (!empty($arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="1">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Краткое описание
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["COMPOUND"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="2">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Состав
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["COMPOUND"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["APPLAYING"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="3">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Способ применения
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["APPLAYING"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["CHARACTERISTIC"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="4">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Свойство
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["CHARACTERISTIC"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["INDICATIONS"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="5">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Показания к применению
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["INDICATIONS"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["CONTRAINDICATIONS"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="6">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Противопоказания
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["CONTRAINDICATIONS"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["SIDE_EFFECTS"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="7">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Побочные действия
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["SIDE_EFFECTS"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["INTERACTION"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="8">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Взаимодействие
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["INTERACTION"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["OVERDOSE"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="9">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Передозировка
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["OVERDOSE"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["STORAGE_CONDITIONS"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="10">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Условия хранения
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["STORAGE_CONDITIONS"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["EXPIRATION_DATE"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="11">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Срок годности
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["EXPIRATION_DATE"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>

				<? if (!empty($arResult["PROPERTIES"]["DISPENSING_TEXT"]["VALUE"])): ?>
					<li class="description__spoiler-item js_description_spoiler_item" data-target="12">
						<div class="description__spoiler-top">
							<h3 class="description__spoiler-title">
								Порядок отпуска
							</h3>
						</div>

						<div class="description__spoiler-bottom">
							<p class="description__spoiler-text">
								<?= $arResult["PROPERTIES"]["DISPENSING_TEXT"]["VALUE"]["TEXT"] ?>
							</p>
						</div>
					</li>
				<? endif; ?>
			</ul>

		</div>
	</section>

	<div class="popup popup--product-slider js_popup" data-popup="product-gallery">
		<div class="popup__wrap">
			<button class="popup__exit js_popup_exit" type="button">
				<svg width="24" height="24">
					<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
				</svg>
			</button>
			<form class="popup__form js_popup_form">
				<div class="swiper popup__product-slider js_popup_product_slider">
					<div class="swiper-wrapper popup__product-wrapper">

						<? if (!empty($arResult["DETAIL_PICTURE"]["ID"])): ?>
							<div class="swiper-slide popup__product-slide">
								<img class="popup__product-img"
									 src="<?= CFile::GetPath($arResult["DETAIL_PICTURE"]["ID"]); ?>">
							</div>
						<? endif; ?>
						<? foreach ($arResult["PROPERTIES"]["IMAGES"]["VALUE"] as $image): ?>
							<div class="swiper-slide popup__product-slide">
								<img class="popup__product-img"
									 src="<?= CFile::GetPath($image); ?>">
							</div>
						<? endforeach; ?>

					</div>
				</div>

				<div class="js_popup_product_slider_prev swiper-button swiper-button-prev"></div>
				<div class="js_popup_product_slider_next swiper-button swiper-button-next"></div>
			</form>
		</div>
	</div>
<? if ($arResult["PROPERTIES"]["RX"]["VALUE"] != 0): ?>
	<!-- ПОП-АП 'У ВАС ЕСТЬ РЕЦЕПТ НА ДАННЫЙ ПРЕПАРАТ' -->
	<div class="popup popup--stop-registration active js_popup js_popup_do_you_have_prescription"
		 data-popup="do-you-have-prescription">
		<div class="popup__wrap">
			<button class="popup__exit js_popup_exit" type="button">
				<svg width="24" height="24">
					<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#exit"></use>
				</svg>
			</button>
			<form class="popup__form js_popup_form js_popup_report_form">
				<div class="popup__title">У Вас есть рецепт на данный препарат?</div>

				<p class="popup__perscription-text">
					Рецепт на товар должен быть оставлен в аптеке.
				</p>

				<p class="popup__perscription-text">
					Способ доставки рецептурных товаров только самовывоз. Оплата при получении.
				</p>
				<ul class="popup__stop-btns">
					<li class="popup__stop-btn-item">
						<button
								class="popup__btn popup__btn--continue btn btn--tr js_popup_do_you_have_prescription_yes_btn"
								type="button">Да, рецепт есть
						</button>
					</li>
					<li class="popup__stop-btn-item">
						<button
								class="popup__btn popup__btn--finish  btn btn--tr js_popup_do_you_have_prescription_no_btn"
								type="button">Нет рецепта нет
						</button>
					</li>
				</ul>
			</form>
		</div>
	</div>
<?php endif; ?>
<?php
	$arAnalog = $arResult['PROPERTIES']['ANALOGS']['VALUE'];
	if (count((array)$arAnalog) > 0):
	global $analogFilter;
	$analogFilter = ["ID" => $arAnalog];

	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"analog",
		[
			"SHOW_TABS" => "N",
			"TITTLE" => "Аналоги",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
			"ELEMENT_SORT_FIELD2" => "id",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_ORDER2" => "asc",
			"FILTER_NAME" => "analogFilter",
			"IBLOCK_ID" => "4",
			"IBLOCK_TYPE" => "catalog",
			"PRICE_CODE" => ["BASE"],
		]
	);
endif;
