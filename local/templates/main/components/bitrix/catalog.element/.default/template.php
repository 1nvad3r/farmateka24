<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<div class="swiper-slide">
	<div class="card">
		<a class="card__img" href="<?= $arResult["DETAIL_PAGE_URL"] ?>">
			<img
					src="<?= $arResult["PREVIEW_PICTURE"]["ID"] ? CFile::GetPath($arResult["PREVIEW_PICTURE"]["ID"]) : SITE_TEMPLATE_PATH . '/images/notimages.png'; ?>"
					alt="<?= $arResult["NAME"] ?>">
			<div class="card__labels">
				<? foreach ($arResult["PROPERTIES"]["SECTION"]["VALUE"] as $value): ?>
					<span style="background-color: <?= $value["COLOR"] ?>;"><?= $value["NAME"] ?></span>
				<? endforeach; ?>
			</div>
			<? if (!empty($arResult["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"])): ?>
				<div class="card__sale"><?= $arResult["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"] ?>%</div>
			<? endif; ?>
		</a>
		<a class="card__title" href="<?= $arResult["DETAIL_PAGE_URL"] ?>"><?= $arResult["NAME"] ?></a>
		<div class="card__brand">
			<? if ($arResult["PROPERTIES"]["BRAND"]["VALUE"]): ?>
				Бренд:
				<a href="/catalog/?brand=<?= $arResult["PROPERTIES"]["BRAND"]["VALUE"] ?>"
				   title=""><?= $arResult["PROPERTIES"]["BRAND"]["VALUE"] ?></a>
			<? elseif ($arResult["PROPERTIES"]["BRAND_NAME"]["VALUE"]): ?>
				Бренд:
				<a href="/catalog/?brand_name=<?= $arResult["PROPERTIES"]["BRAND_NAME"]["VALUE"] ?>"
				   title=""><?= $arResult["PROPERTIES"]["BRAND_NAME"]["VALUE"] ?></a>
			<? else: ?>
			<? endif; ?>
		</div>

		<? if ($arResult["PRICES"]["BASE"]["VALUE"]): ?>
			<div class="card__bottom">
				<? if (!empty($arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"]) && $arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"] != $arResult["PRICES"]["BASE"]["VALUE"]): ?>
					<div class="card__price"><?= $arResult["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"] ?></div>
					<div class="card__old-price"><?= $arResult["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
				<? else: ?>
					<div class="card__price"><?= $arResult["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
				<? endif; ?>
				<button class="card__heart
                        <? if ($USER->IsAuthorized()): ?> js_card_heart <?= $arResult["FAVORITE"] ? "active" : "" ?><? else: ?>js_btn_popup<? endif; ?>"
						data-btn="log-in" data-id="<?= $arResult["ID"] ?>" type="button">
					<svg width="33" height="28">
						<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart' ?>"></use>
					</svg>
					<svg width="33" height="28">
						<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-heart-filled' ?>"></use>
					</svg>
					<span class="card__heart-description">
                        добавить в избранное
                    </span>
				</button>
			</div>

			<div class="card__buttons">
				<div class="card__btn card__btn-1 btn btn--green js_btn_popup"
					 data-btn="added-to-card"
					 data-action="<?= $arResult["ADD_URL"] ?>"
					 id="add<?= $arResult["ID"] ?>"
					 rel="nofollow"
					 onclick="addToCart(this, $('#<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?><?= $arResult["ID"] ?>').val(), <?= $arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"] ?>); return false;">
                    <span>
                        В корзину
                        <svg width="27" height="22">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#card-cart' ?>"></use>
                        </svg>
                    </span>
				</div>
				<div class="card__btn card__btn-2 btn btn--green js_btn_popup"
					 data-btn="report">
					Сообщить о поступлении
				</div>
				<div class="card__btn card__btn-3 btn btn--green">
					Вы узнаете о поступлении
				</div>
				<div class="card__btn card__btn-4 btn btn--green js_btn_popup"
					 data-btn="order">
					Заказать
				</div>
			</div>
		<? endif; ?>
	</div>
</div>
