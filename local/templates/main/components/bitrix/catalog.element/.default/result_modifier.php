<?
	foreach ($arResult["PROPERTIES"]["SECTION"]["VALUE"] as $key => $sectionID) {
		$arFilter = ['IBLOCK_ID' => 5, 'ID' => $sectionID];
		$db_list = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, true, ["UF_COLOR"]);
		if ($sectionEl = $db_list->GetNext()) {
			$arResult["PROPERTIES"]["SECTION"]["VALUE"][$key] = [
				"ID" => $sectionID,
				"NAME" => $sectionEl['NAME'],
			];
			if ($sectionEl['UF_COLOR']) {
				$rsEnum = CUserFieldEnum::GetList([], ["ID" => $sectionEl['UF_COLOR']]);
				if ($arEnum = $rsEnum->GetNext()) {
					$arResult["PROPERTIES"]["SECTION"]["VALUE"][$key]["COLOR"] = $arEnum["VALUE"];
				}
			}
		}
	}


	global $USER_FIELD_MANAGER;
	global $USER;
	$userID = $USER->GetID();
	$rsUser = \CUser::GetByID($userID);
	$arUser = $rsUser->Fetch();
	$arProducts = $arUser["UF_FAVORITE_PRODUCTS"];

	$index = array_search($arResult["ID"], (array)$arProducts);
	if ($index !== false) {
		$arResult["FAVORITE"] = true;
	}
?>