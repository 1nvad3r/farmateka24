<?
foreach ($arResult["PROPERTIES"]["SECTION"]["VALUE"] as $key => $sectionID){
    $arFilter = Array('IBLOCK_ID'=>5, 'ID'=>$sectionID);
    $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, true, Array("UF_COLOR"));
    if($sectionEl = $db_list->GetNext()){
        $arResult["PROPERTIES"]["SECTION"]["VALUE"][$key] = [
            "ID" => $sectionID,
            "NAME" => $sectionEl['NAME']
        ];
        if($sectionEl['UF_COLOR']){
            $rsEnum = CUserFieldEnum::GetList(array(), array("ID" =>$sectionEl['UF_COLOR']));
            if($arEnum = $rsEnum->GetNext())
            {
                $arResult["PROPERTIES"]["SECTION"]["VALUE"][$key]["COLOR"] = $arEnum["VALUE"];
            }
        }
    }
}


global $USER_FIELD_MANAGER;
global $USER;
$userID = $USER->GetID();
$rsUser = \CUser::GetByID($userID);
$arUser = $rsUser->Fetch();
$arProducts = $arUser["UF_FAVORITE_PRODUCTS"];

$index = array_search($arResult["ID"], (array)$arProducts);
if($index !== false){
    $arResult["FAVORITE"] = true;
}
?>