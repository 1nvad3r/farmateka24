<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<div class="swiper-slide">
	<div class="popular-row-product-slider__inner">
		<a class="popular-row-product-slider__img-link" href="<?= $arResult["DETAIL_PAGE_URL"] ?>">
			<img class="popular-row-product-slider__img"
				 src="<?= $arResult["PREVIEW_PICTURE"]["ID"] ? CFile::GetPath($arResult["PREVIEW_PICTURE"]["ID"]) : SITE_TEMPLATE_PATH . '/images/notimages.png'; ?>"
				 alt="<?= $arResult['NAME'] ?>">
		</a>

		<div class="popular-row-product-slider__content">
			<h3 class="popular-row-product-slider__product-title">
				<?= $arResult['NAME'] ?>
			</h3>

			<div class="popular-row-product-slider__price-row">
                                                <span class="popular-row-product-slider__text">
                                                    от
                                                </span>

				<span class="popular-row-product-slider__price">
                                                    <? if (!empty($arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"]) && $arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"] != $arResult["PRICES"]["BASE"]["VALUE"]): ?>
														<div
															class="card__price"><?= $arResult["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"] ?></div>
														<div
															class="card__old-price"><?= $arResult["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
													<? else: ?>
														<div
															class="card__price"><?= $arResult["PRICES"]["BASE"]["PRINT_VALUE"] ?></div>
													<? endif; ?>
                                                </span>
				<? if (!empty($arResult["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"])): ?>
					<div class="card__sale"><?= $arResult["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"] ?>%</div>
				<? endif; ?>
			</div>

			<div class="popular-row-product-slider__bottom">
				<button
					onclick="addToCart(this, $('#<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?><?= $arResult["ID"] ?>').val(), <?= $arResult["PRICES"]["BASE"]["DISCOUNT_VALUE"] ?>); return false;"
					class="popular-row-product-slider__to-card-btn js_btn_popup" data-btn="added-to-card" type="button">
					В
					корзину
				</button>

				<button class="card__heart row-card__heart js_card_heart <? if ($USER->IsAuthorized()): ?> js_card_heart <?= $arResult["FAVORITE"] ? "active" : "" ?><? else: ?>js_btn_popup<? endif; ?>" type="button" data-id="<?= $arResult["ID"] ?>" data-btn="log-in" >
					<svg width="33" height="28">
						<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#card-heart"></use>
					</svg>
					<svg width="33" height="28">
						<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#card-heart-filled"></use>
					</svg>
					<span class="card__heart-description">
                                                        добавить в избранное
                                                    </span>
				</button>
			</div>
		</div>
	</div>
</div>
