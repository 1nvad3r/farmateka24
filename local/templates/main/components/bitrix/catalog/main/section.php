<?php
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */

	if ($_REQUEST['q']) {
		global $arrFilter;

		$arSelect = ["*", "PROPERTY_*"];
		$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'NAME' => '%' . $_REQUEST['q'] . '%'];
		$res = CIBlockElement::GetList(["NAME" => "ASC"], $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$ids[] = $arFields['ID'];
		}

		$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_INTER_NAME' => '%' . $_REQUEST['q'] . '%'];
		$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$ids[] = $arFields['ID'];
		}

		$arFilter = ["IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_VENDOR_NAME' => '%' . $_REQUEST['q'] . '%'];
		$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$ids[] = $arFields['ID'];
		}

		if(is_array($ids)){
			$arrFilter = ['ID' => $ids];
		}

		$SectList = CIBlockSection::GetList(["NAME" => "ASC"], ["IBLOCK_ID" => 4, "ACTIVE" => "Y", 'NAME' => '%' . $_REQUEST['q'] . '%'], false, ["ID", "IBLOCK_ID", "IBLOCK_TYPE_ID", "IBLOCK_SECTION_ID", "CODE", "SECTION_ID", "NAME", "SECTION_PAGE_URL"]);
		while ($SectListGet = $SectList->GetNext()) {
			$sections[] = $SectListGet;
		}
	}
?>
<?php if (isset($_REQUEST['q']) && is_string($_REQUEST['q'])): ?>
	<h1 class="category-main__title">Поиск по запросу “<?= $_REQUEST['q'] ?>”</h1>
	<span class="category-main__subtitle">( <?php echo (is_array($ids)) ? $APPLICATION->ShowViewContent("countSearch") : '0'; ?> найдено)</span>
<?php endif; ?>
	<section class="category-main">

		<div class="container category-main__container">

			<div class="category-main__top">
				<?php
					$searchQuery = '';
					if (isset($_REQUEST['q']) && is_string($_REQUEST['q']))
						$searchQuery = trim($_REQUEST['q']);
					if ($searchQuery !== '') {
						$APPLICATION->SetDirProperty("section_class", "search");
						$APPLICATION->AddChainItem("Поиск", "");
						$APPLICATION->SetTitle('Поиск по запросу “' . $searchQuery . '”');
//						$arrFilter = ['*SEARCHABLE_CONTENT' => '%' . $searchQuery . '%'];
					}
					unset($searchQuery);
				?>
				<?php $APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter",
					"main",
					[
						"COMPONENT_TEMPLATE" => ".default",
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"HIDE_NOT_AVAILABLE" => "N",
						"DISPLAY_ELEMENT_COUNT" => "Y",
						"SEF_MODE" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "Y",
						"SAVE_IN_SESSION" => "N",
						"INSTANT_RELOAD" => "Y",
						"PRICE_CODE" => [
							0 => "BASE",
						],
						"SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
						"CURRENCY_ID" => "RUB",
					],
					false
				); ?>

				<?php $APPLICATION->IncludeComponent("bitrix:catalog.section.list",
					"min",
					[
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"COUNT_ELEMENTS" => "Y",
						"TOP_DEPTH" => "2",
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"SECTION_FIELDS" => "",
						"SECTION_USER_FIELDS" => "",
						"ADD_SECTIONS_CHAIN" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_NOTES" => "",
						"CACHE_GROUPS" => "Y",
					]
				);
				?>

				<button class="category-main__show-filters-btn js_filters_show_btn" type="button">Все фильтры</button>

			</div>

			<?php
				$searchClass = '';
				if (isset($_REQUEST['q']) && is_string($_REQUEST['q'])) {
					$searchClass = 'search';
				}
			?>
			<? if(is_array($ids)){ ?>
			<div class="category-main__main-wrapper <?= $searchClass; ?>">

				<?php $APPLICATION->ShowViewContent("filterForm"); ?>

				<div class="category-main__product-column category-main__product-column--move-up js_category_main_product-column ">
					<div class="category-main__product-column-top">
						<?php
							$arSort = [
								"shows" => [
									"asc" => "По популярности",
								],
								"CATALOG_PRICE_1" => [
									"asc" => "По возрастанию цены",
									"desc" => "По убыванию цены",
								],
							];
							$sort = $_REQUEST["by"];
							if (strlen($sort) > 0) {
								if (!array_key_exists($sort, $arSort)) {
									$sort = key($arSort);
								}
								$_SESSION["SORT"] = $sort;
							} elseif (strlen($_SESSION["SORT"]) > 0) {
								$sort = $_SESSION["SORT"];
							} else {
								$sort = key($arSort);
							}

							$order = $_REQUEST["order"];
							if (strlen($order) > 0) {
								if (!array_key_exists($order, $arSort[$sort])) {
									$order = key($arSort[$sort]);
								}
								$_SESSION["ORDER"] = $order;
							} elseif (strlen($_SESSION["ORDER"]) > 0) {
								$order = $_SESSION["ORDER"];
							} else {
								$order = key($arSort[$sort]);
							}
						?>

						<div class="category-main__product-column-select my-select">
							<select class="js_my_select js_my_select_sort_products_mode" name="select" id="select">
								<?php foreach ($arSort as $sortKey => $arOrder): ?>
									<?php foreach ($arOrder as $orderKey => $sortName): ?>
										<option value="<?= $sortKey . "/" . $orderKey ?>"
												<?php if ($sortKey == $sort && $orderKey == $order): ?>selected<? endif ?>><?= $sortName ?></option>
									<?php endforeach ?>
								<?php endforeach ?>
							</select>
						</div>
						<button class="category-main__products-by-rows-btn js_category_list_by_row_btn active"
								type="submit"></button>
						<button class="category-main__products-by-table-btn js_category_list_by_table_btn"
								type="button"></button>
					</div>

					<?php $intSectionID = $APPLICATION->IncludeComponent(
						"bitrix:catalog.section",
						"main",
						[
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"USE_FILTER" => "Y",
							"ELEMENT_SORT_FIELD" => $sort,
							"ELEMENT_SORT_ORDER" => $order,
							"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
							"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
							"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
							"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
							"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
							"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
							"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
							"BASKET_URL" => $arParams["BASKET_URL"],
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"FILTER_NAME" => $arParams["FILTER_NAME"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_FILTER" => $arParams["CACHE_FILTER"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SET_TITLE" => $arParams["SET_TITLE"],
							"SET_STATUS_404" => $arParams["SET_STATUS_404"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
							"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
							"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

							"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
							"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
							"PAGER_TITLE" => $arParams["PAGER_TITLE"],
							"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
							"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
							"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
							"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
							"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

							"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
							"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
							"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

							'LABEL_PROP' => $arParams['LABEL_PROP'],
							'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
							'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

							'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
							'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
							'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
							'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
							'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
							'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
							'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
							'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
							'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
							'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

							'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							"ADD_SECTIONS_CHAIN" => "Y",
							"ADD_ELEMENT_CHAIN" => "Y",
							'ADD_TO_BASKET_ACTION' => $basketAction,
							'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
							'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
							"HAS_SUBSECTION" => $arParams["HAS_SUBSECTION"],
							"SHOW_SUBSECTIONS_ELEMENT" => $showSectionElement,
						],
						$component
					); ?>
				</div>
			</div>
			<? } ?>
		</div>
	</section>
<?php
	if ($_REQUEST['q']) {
		if (count($sections) > 0) {
			?>

			<section class="categories">

					<h1 class="categories__title title">Категории</h1>


				<div class=" categories__container">
					<ul class="categories__list">
						<?php foreach ($sections as $section): ?>
							<li class="categories__item">
								<?php $picture = $section["PICTURE"]["ID"] ? CFile::GetPath($section["PICTURE"]["ID"]) : '/images/section_default.jpg'; ?>
								<img class="categories__img" src="<?= $picture ?>"
									 alt="<?= $section["PICTURE"]["ALT"] ?>">

								<div class="categories__img-mask" style="opacity: 0.55"></div>

								<a class="categories__link" href="<?= $section["SECTION_PAGE_URL"] ?>">
									<?= $section["NAME"] ?>
								</a>
							</li>
						<?php endforeach; ?>

					</ul>
				</div>
			</section>

		<?
		}
	}
?>