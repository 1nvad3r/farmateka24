<section class="category-main">
    <div class="container category-main__container qq">
        <div class="category-main__top">

            <?
//				d($_REQUEST['q']);
            $ID_SECTION = 0;
            $rsSections = CIBlockSection::GetList(array(),array('IBLOCK_ID' => 5, '=CODE' => stristr($_REQUEST["CATALOG_LIST"], "/", true)));
            if ($arSection = $rsSections->Fetch())
            {
                $ID_SECTION = $arSection['ID'];
                $APPLICATION->SetTitle($arSection['NAME']);
                $APPLICATION->AddChainItem($arSection['NAME'], "");
            }else{
                $APPLICATION->SetTitle("Каталог");
            }
            global $SectionFilter;
				$searchQuery = '';
				if (isset($_REQUEST['q']) && is_string($_REQUEST['q']))
					$searchQuery = trim($_REQUEST['q']);
				if ($searchQuery !== '')
				{
						$APPLICATION->SetTitle('Поиск по запросу “'.$searchQuery.'”');
						$arParams["SET_TITLE"] = 'Поиск по запросу “'.$searchQuery.'”';
						$arParams["LIST_BROWSER_TITLE"] = 'Поиск по запросу “'.$searchQuery.'”';
						$arParams["PAGER_TITLE"] = 'Поиск по запросу “'.$searchQuery.'”';
						$APPLICATION->AddChainItem('Поиск', "");
						$SectionFilter  = Array("PROPERTY_SECTION" => $ID_SECTION, '*SEARCHABLE_CONTENT' => $searchQuery, 'PROPERTY_INTER_NAME' => $searchQuery);
				}
				unset($searchQuery);?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.smart.filter",
                "main",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
//                    "PREFILTER_NAME" => "SectionFilter",
                    "FILTER_NAME" => 'SectionFilter',
                    "HIDE_NOT_AVAILABLE" => "N",
                    "DISPLAY_ELEMENT_COUNT" => "Y",
                    "SEF_MODE" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "SAVE_IN_SESSION" => "N",
                    "INSTANT_RELOAD" => "Y",
                    "PRICE_CODE" => array(
                        0 => "BASE",
                    ),
                    "SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
                    "CURRENCY_ID" => "RUB"
                ),
                false
            );?>

            <button class="category-main__show-filters-btn js_filters_show_btn" type="button">Все
                фильтры</button>

        </div>

        <div class="category-main__main-wrapper">

            <?$APPLICATION->ShowViewContent("filterForm");?>

            <div
                class="category-main__product-column category-main__product-column--move-up js_category_main_product-column ">
                <div class="category-main__product-column-top">
                    <?
                    $arSort = array(
                        "shows"=> array(
                            "asc" => "По популярности",
                        ),
                        "CATALOG_PRICE_1"=> array(
                            "asc" => "По возрастанию цены",
                            "desc" => "По убыванию цены",
                        ),
                    );
                    $sort = $_REQUEST["by"];

                    if(strlen($sort)>0){
                        if(!array_key_exists($sort, $arSort)){
                            $sort = key($arSort);
                        }
                        $_SESSION["SORT"] = $sort;
                    }elseif(strlen($_SESSION["SORT"])>0){
                        $sort = $_SESSION["SORT"];
                    }else{
                        $sort = key($arSort);
                    }

                    $order = $_REQUEST["order"];
                    if(strlen($order)>0){
                        if(!array_key_exists($order, $arSort[$sort])){
                            $order = key($arSort[$sort]);
                        }
                        $_SESSION["ORDER"] = $order;
                    }elseif(strlen($_SESSION["ORDER"])>0){
                        $order = $_SESSION["ORDER"];
                    }else{
                        $order = key($arSort[$sort]);
                    }

                    ?>
                    <div class="category-main__product-column-select my-select my-select">
                        <select class="js_my_select js_my_select_sort_products_mode" name="select" id="select">
                            <?foreach($arSort as $sortKey => $arOrder):?>
                                <?foreach($arOrder as $orderKey => $sortName):?>
                                    <option onclick="location.href='/catalog/filter/?q='<?= $_REQUEST['q']?>" value="<?=$sortKey."/".$orderKey?>" <?if($sortKey == $sort && $orderKey == $order):?>selected<?endif?>><?=$sortName?></option>
                                <?endforeach?>
                            <?endforeach?>
                        </select>
                    </div>

                    <button class="category-main__products-by-rows-btn js_category_list_by_row_btn"
                            type="submit"></button>
                    <button class="category-main__products-by-table-btn js_category_list_by_table_btn"
                            type="button"></button>
                </div>
                <?$intSectionID = $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "main",
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "USE_FILTER" => "Y",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "ELEMENT_SORT_FIELD" => $sort,
                        "ELEMENT_SORT_ORDER" => $order,
                        "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                        "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                        "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                        "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                        "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                        "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                        "BASKET_URL" => $arParams["BASKET_URL"],
                        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                        "FILTER_NAME" => $arParams["FILTER_NAME"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "SET_TITLE" => $arParams["SET_TITLE"],
                        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                        "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                        "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                        "PRICE_CODE" => $arParams["PRICE_CODE"],
                        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                        "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                        "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                        "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

                        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                        "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                        "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                        "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
                        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                        'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

                        'LABEL_PROP' => $arParams['LABEL_PROP'],
                        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                        'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                        'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                        'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                        'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                        'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                        'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                        'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

                        'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_ELEMENT_CHAIN" => "Y",
                        'ADD_TO_BASKET_ACTION' => $basketAction,
                        'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                        'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
                        "HAS_SUBSECTION" => $arParams["HAS_SUBSECTION"],
                        "SHOW_SUBSECTIONS_ELEMENT" => $showSectionElement,
                    ),
                    $component
                );?>

            </div>
        </div>
    </div>
</section>
