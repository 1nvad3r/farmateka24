<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */

	$buttonId = $this->randString();
?>
<section class="subscr js_subscr_section">
	<div class="container sender-subscribe">
		<?
			$frame = $this->createFrame("sender-subscribe", false)->begin();
		?>
		<div class="subscr__wrap">
			<div class="subscr__cnt">
				<h2 class="subscr__title">Подпишитесь на нашу рассылку, чтобы получать
					лучшие предложения
					первыми</h2>
				<p class="subscr__text">Новинки, акции, спецпредложения - все это в одной
					рассылке!</p>
			</div>
			<form id="bx_subscribe_subform_<?= $buttonId ?>" role="form" method="post"
				  action="<?= $arResult["FORM_ACTION"] ?>" class="subscr__form js_popup_form">
				<?= bitrix_sessid_post() ?>
				<input type="hidden" name="sender_subscription" value="add">
				<input class="js_input_email" name="SENDER_SUBSCRIBE_EMAIL" type="email" placeholder="Email" required>
				<button class="btn btn--green" id="bx_subscribe_btn_<?= $buttonId ?>">Подписаться</button>
			</form>
		</div>
	</div>
</section>
<?
	$frame->beginStub();
?>

<section class="subscr js_subscr_section">
	<div class="container sender-subscribe">
		<div class="subscr__wrap">
			<div class="subscr__cnt">
				<h2 class="subscr__title">Подпишитесь на нашу рассылку, чтобы получать
					лучшие предложения
					первыми</h2>
				<p class="subscr__text">Новинки, акции, спецпредложения - все это в одной
					рассылке!</p>
			</div>
			<form id="bx_subscribe_subform_<?= $buttonId ?>" role="form" method="post"
				  action="<?= $arResult["FORM_ACTION"] ?>" class="subscr__form js_popup_form">
				<?= bitrix_sessid_post() ?>
				<input type="hidden" name="sender_subscription" value="add">
				<input class="js_input_email" name="SENDER_SUBSCRIBE_EMAIL" type="email" placeholder="Email">
				<button class="btn btn--green" id="bx_subscribe_btn_<?= $buttonId ?>">Подписаться</button>
			</form>
			<?
				$frame->end();
			?>
		</div>
	</div>
</section>
