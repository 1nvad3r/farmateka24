<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="category-main__top-left-wrapper">
    <div class="category-main__title-wrapper">
<?if (!isset($_REQUEST['q'])):?>
        <h1 class="category-main__title">
            <?$APPLICATION->ShowTitle(false);?>
        </h1>
        <span class="category-main__subtitle">
            <?
                $countProduct = count((array)$arResult["COMBO"]);
                if($countProduct == 0){
                    $countProduct =  $arResult['NAV_RESULT']->NavRecordCount;
                }

            ?>
            <? if ($countProduct): ?>
                                (<?= $countProduct ?> товаров)
            <? endif; ?>
                            </span>
<? endif; ?>
    </div>

    <div class="category-main__chosen-filters-wrapper js_chosen_filters">
        <h2 class="category-main__filters-title">
            Фильтры
        </h2>

        <?if(count((array)$arResult["ACTIVE"])>0):?>
        <h3 class="category-main__chosen-filters-title">
            Выбранные фильтры
        </h3>

        <ul class="category-main__chosen-filters-list js_chosen_filters_list">
            <?foreach ((array)$arResult["ACTIVE"] as $item):?>
                <?foreach ($item as $value):?>
                    <li class="category-main__chosen-filter-item js_chosen_filter_item"
                        data-info="<?=$value["CONTROL_ID"]?>">
                        <span class="category-main__chosen-filter-name"><?=$value["VALUE"]?></span>
                        <button
                            class="category-main__delete-chosen-filter-btn js_chosen_filter_item_delete_btn"
                            type="button">
                        </button>
                    </li>
                <?endforeach;?>
            <?endforeach;?>
        </ul>
        <?endif;?>
    </div>

</div>

<?ob_start();?>

    <form  name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="category-main__filters-column js_filters_list">
        <?foreach($arResult["HIDDEN"] as $arItem):?>
            <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
        <?endforeach;?>
        <input type="hidden" name="set_filter" value="Y" />


        <?foreach($arResult["ITEMS"] as $key=>$arItem)//prices
        {
        $key = $arItem["ENCODED_ID"];
        if(isset($arItem["PRICE"])):
        if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
            continue;

        $step_num = 4;
        $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
        $prices = array();
        if (Bitrix\Main\Loader::includeModule("currency"))
        {
            for ($i = 0; $i < $step_num; $i++)
            {
                $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
            }
            $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
        }
        else
        {
            $precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
            for ($i = 0; $i < $step_num; $i++)
            {
                $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $precision, ".", "");
            }
            $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
        }
        ?>

        <div class="category-main__filter js_opening_filter">
            <h4 class="category-main__filter-title">
                <?=$arItem["NAME"]?>
            </h4>
            <div class="category-main__filter-inner range-filter">
                <div class="range-filter__range js-range-slider"></div>

                <div class="range-filter__inputs">
                    <label class="range-filter__label">
                        <span class="range-filter__text">От </span>

                        <input class="range-filter__input js-input-0" type="number"
                               min="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
                               max="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
                               name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                               id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                               value="<?= $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"/>
                    </label>

                    <label class="range-filter__label">
                        <span class="range-filter__text">До</span>

                        <input class="range-filter__input js-input-1" type="number"
                               min="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
                               max="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
                               name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                               id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                               value="<?= $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"/>
                    </label>

                </div>
            </div>
        </div>

        <?endif;
				}?>


        <?foreach ($arResult["ITEMS"] as $item):
            if(
                empty($item["VALUES"])
                || isset($item["PRICE"])
            )
                continue;

            if (
                $item["DISPLAY_TYPE"] == "A"
                && (
                    $item["VALUES"]["MAX"]["VALUE"] - $item["VALUES"]["MIN"]["VALUE"] <= 0
                )
            )
                continue;
            ?>
        <div class="category-main__filter js_opening_filter">
            <h4 class="category-main__filter-title">
                <?=$item["NAME"]?>
            </h4>
            <?switch ($item["DISPLAY_TYPE"])
            {
                case "B":?>
                    <div class="category-main__filter-inner range-filter">
                        <div class="range-filter__range js-range-slider"></div>

                        <div class="range-filter__inputs">
                            <label class="range-filter__label">
                                <span class="range-filter__text">От </span>

                                <input class="range-filter__input js-input-0" type="number"
                                       min="<?=$item["VALUES"]["MIN"]["VALUE"]?>"
                                       max="<?=$item["VALUES"]["MAX"]["VALUE"]?>"
                                       name="<?= $item["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                       id="<?= $item["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                       value="<?= $item["VALUES"]["MIN"]["HTML_VALUE"]?>"/>
                            </label>

                            <label class="range-filter__label">
                                <span class="range-filter__text">До</span>

                                <input class="range-filter__input js-input-1" type="number"
                                       min="<?=$item["VALUES"]["MIN"]["VALUE"]?>"
                                       max="<?=$item["VALUES"]["MAX"]["VALUE"]?>"
                                       name="<?= $item["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                       id="<?= $item["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                       value="<?= $item["VALUES"]["MAX"]["HTML_VALUE"]?>"/>
                            </label>

                        </div>
                    </div>
                <?break;
                    default:?>
            <div class="category-main__check-box-wrapper js_filter_options_wrapper">
                <?foreach ($item["VALUES"] as $value):?>
                <?if($value["DISABLED"]==true)continue;?>
                <label class="check category-main__check">
                    <input
                            class="check__input js_filter_item js_filter_point"
                            type="checkbox"
                            value="<?= $value["HTML_VALUE"] ?>"
                            name="<?= $value["CONTROL_NAME"] ?>"
                            id="<?= $value["CONTROL_ID"] ?>"
                        <?=$value["CHECKED"]?'checked="checked"':''?>
                    />
                    <span class="check__box"></span>
                    <?=$value["VALUE"]?>
                </label>
                <?endforeach;?>
            </div>
                <?if(count((array)$item["VALUES"]) > 4):?>
                <button class="category-main__more-options-btn js_filter_more_options_btn" type="button">
                    <span class="category-main__more-options-btn-text category-main__more-options-btn-text--more">
                        Показать все
                    </span>
                    <span class="category-main__more-options-btn-text category-main__more-options-btn-text--less">
                        Скрыть
                    </span>
                </button>
                <?endif;?>
                <?}?>
        </div>
        <?endforeach;?>

        <button class="category-main__apply-filters-btn js_filters_submit_btn">
            Применить фильтры
        </button>
        <button class="category-main__close-filters-btn js_filters_close_btn" aria-label="закрыть фильтры"></button>
    </form>

<?
$FilterForm = ob_get_contents();

ob_end_clean();
?>
<?$APPLICATION->AddViewContent("filterForm", $FilterForm);?>