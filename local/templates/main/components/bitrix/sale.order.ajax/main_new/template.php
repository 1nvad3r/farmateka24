<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

	use Bitrix\Main;
	use Bitrix\Main\Localization\Loc;

	/**
	 * @var array $arParams
	 * @var array $arResult
	 * @var CMain $APPLICATION
	 * @var CUser $USER
	 * @var SaleOrderAjax $component
	 * @var string $templateFolder
	 */

	$context = Main\Application::getInstance()->getContext();
	$request = $context->getRequest();

	if (empty($arParams['TEMPLATE_THEME'])) {
		$arParams['TEMPLATE_THEME'] = Main\ModuleManager::isModuleInstalled('bitrix.eshop') ? 'site' : 'blue';
	}

	if ($arParams['TEMPLATE_THEME'] === 'site') {
		$templateId = Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', $component->getSiteId());
		$templateId = preg_match('/^eshop_adapt/', $templateId) ? 'eshop_adapt' : $templateId;
		$arParams['TEMPLATE_THEME'] = Main\Config\Option::get('main', 'wizard_' . $templateId . '_theme_id', 'blue', $component->getSiteId());
	}

	if (!empty($arParams['TEMPLATE_THEME'])) {
		if (!is_file(Main\Application::getDocumentRoot() . '/bitrix/css/main/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css')) {
			$arParams['TEMPLATE_THEME'] = 'blue';
		}
	}

	$arParams['ALLOW_USER_PROFILES'] = $arParams['ALLOW_USER_PROFILES'] === 'Y' ? 'Y' : 'N';
	$arParams['SKIP_USELESS_BLOCK'] = $arParams['SKIP_USELESS_BLOCK'] === 'N' ? 'N' : 'Y';

	if (!isset($arParams['SHOW_ORDER_BUTTON'])) {
		$arParams['SHOW_ORDER_BUTTON'] = 'final_step';
	}

	$arParams['HIDE_ORDER_DESCRIPTION'] = isset($arParams['HIDE_ORDER_DESCRIPTION']) && $arParams['HIDE_ORDER_DESCRIPTION'] === 'Y' ? 'Y' : 'N';
	$arParams['SHOW_TOTAL_ORDER_BUTTON'] = $arParams['SHOW_TOTAL_ORDER_BUTTON'] === 'Y' ? 'Y' : 'N';
	$arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] = $arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_PAY_SYSTEM_INFO_NAME'] = $arParams['SHOW_PAY_SYSTEM_INFO_NAME'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_DELIVERY_LIST_NAMES'] = $arParams['SHOW_DELIVERY_LIST_NAMES'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_DELIVERY_INFO_NAME'] = $arParams['SHOW_DELIVERY_INFO_NAME'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_DELIVERY_PARENT_NAMES'] = $arParams['SHOW_DELIVERY_PARENT_NAMES'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_STORES_IMAGES'] = $arParams['SHOW_STORES_IMAGES'] === 'N' ? 'N' : 'Y';

	if (!isset($arParams['BASKET_POSITION']) || !in_array($arParams['BASKET_POSITION'], ['before', 'after'])) {
		$arParams['BASKET_POSITION'] = 'after';
	}

	$arParams['EMPTY_BASKET_HINT_PATH'] = isset($arParams['EMPTY_BASKET_HINT_PATH']) ? (string)$arParams['EMPTY_BASKET_HINT_PATH'] : '/';
	$arParams['SHOW_BASKET_HEADERS'] = $arParams['SHOW_BASKET_HEADERS'] === 'Y' ? 'Y' : 'N';
	$arParams['HIDE_DETAIL_PAGE_URL'] = isset($arParams['HIDE_DETAIL_PAGE_URL']) && $arParams['HIDE_DETAIL_PAGE_URL'] === 'Y' ? 'Y' : 'N';
	$arParams['DELIVERY_FADE_EXTRA_SERVICES'] = $arParams['DELIVERY_FADE_EXTRA_SERVICES'] === 'Y' ? 'Y' : 'N';

	$arParams['SHOW_COUPONS'] = isset($arParams['SHOW_COUPONS']) && $arParams['SHOW_COUPONS'] === 'N' ? 'N' : 'Y';

	if ($arParams['SHOW_COUPONS'] === 'N') {
		$arParams['SHOW_COUPONS_BASKET'] = 'N';
		$arParams['SHOW_COUPONS_DELIVERY'] = 'N';
		$arParams['SHOW_COUPONS_PAY_SYSTEM'] = 'N';
	} else {
		$arParams['SHOW_COUPONS_BASKET'] = isset($arParams['SHOW_COUPONS_BASKET']) && $arParams['SHOW_COUPONS_BASKET'] === 'N' ? 'N' : 'Y';
		$arParams['SHOW_COUPONS_DELIVERY'] = isset($arParams['SHOW_COUPONS_DELIVERY']) && $arParams['SHOW_COUPONS_DELIVERY'] === 'N' ? 'N' : 'Y';
		$arParams['SHOW_COUPONS_PAY_SYSTEM'] = isset($arParams['SHOW_COUPONS_PAY_SYSTEM']) && $arParams['SHOW_COUPONS_PAY_SYSTEM'] === 'N' ? 'N' : 'Y';
	}

	$arParams['SHOW_NEAREST_PICKUP'] = $arParams['SHOW_NEAREST_PICKUP'] === 'Y' ? 'Y' : 'N';
	$arParams['DELIVERIES_PER_PAGE'] = isset($arParams['DELIVERIES_PER_PAGE']) ? intval($arParams['DELIVERIES_PER_PAGE']) : 9;
	$arParams['PAY_SYSTEMS_PER_PAGE'] = isset($arParams['PAY_SYSTEMS_PER_PAGE']) ? intval($arParams['PAY_SYSTEMS_PER_PAGE']) : 9;
	$arParams['PICKUPS_PER_PAGE'] = isset($arParams['PICKUPS_PER_PAGE']) ? intval($arParams['PICKUPS_PER_PAGE']) : 5;
	$arParams['SHOW_PICKUP_MAP'] = $arParams['SHOW_PICKUP_MAP'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_MAP_IN_PROPS'] = $arParams['SHOW_MAP_IN_PROPS'] === 'Y' ? 'Y' : 'N';
	$arParams['USE_YM_GOALS'] = $arParams['USE_YM_GOALS'] === 'Y' ? 'Y' : 'N';
	$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
	$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
	$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

	$useDefaultMessages = !isset($arParams['USE_CUSTOM_MAIN_MESSAGES']) || $arParams['USE_CUSTOM_MAIN_MESSAGES'] != 'Y';

	if ($useDefaultMessages || !isset($arParams['MESS_AUTH_BLOCK_NAME'])) {
		$arParams['MESS_AUTH_BLOCK_NAME'] = Loc::getMessage('AUTH_BLOCK_NAME_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_REG_BLOCK_NAME'])) {
		$arParams['MESS_REG_BLOCK_NAME'] = Loc::getMessage('REG_BLOCK_NAME_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_BASKET_BLOCK_NAME'])) {
		$arParams['MESS_BASKET_BLOCK_NAME'] = Loc::getMessage('BASKET_BLOCK_NAME_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_REGION_BLOCK_NAME'])) {
		$arParams['MESS_REGION_BLOCK_NAME'] = Loc::getMessage('REGION_BLOCK_NAME_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_PAYMENT_BLOCK_NAME'])) {
		$arParams['MESS_PAYMENT_BLOCK_NAME'] = Loc::getMessage('PAYMENT_BLOCK_NAME_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_BLOCK_NAME'])) {
		$arParams['MESS_DELIVERY_BLOCK_NAME'] = Loc::getMessage('DELIVERY_BLOCK_NAME_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_BUYER_BLOCK_NAME'])) {
		$arParams['MESS_BUYER_BLOCK_NAME'] = Loc::getMessage('BUYER_BLOCK_NAME_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_BACK'])) {
		$arParams['MESS_BACK'] = Loc::getMessage('BACK_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_FURTHER'])) {
		$arParams['MESS_FURTHER'] = Loc::getMessage('FURTHER_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_EDIT'])) {
		$arParams['MESS_EDIT'] = Loc::getMessage('EDIT_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_ORDER'])) {
		$arParams['MESS_ORDER'] = $arParams['~MESS_ORDER'] = Loc::getMessage('ORDER_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_PRICE'])) {
		$arParams['MESS_PRICE'] = Loc::getMessage('PRICE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_PERIOD'])) {
		$arParams['MESS_PERIOD'] = Loc::getMessage('PERIOD_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_NAV_BACK'])) {
		$arParams['MESS_NAV_BACK'] = Loc::getMessage('NAV_BACK_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_NAV_FORWARD'])) {
		$arParams['MESS_NAV_FORWARD'] = Loc::getMessage('NAV_FORWARD_DEFAULT');
	}

	$useDefaultMessages = !isset($arParams['USE_CUSTOM_ADDITIONAL_MESSAGES']) || $arParams['USE_CUSTOM_ADDITIONAL_MESSAGES'] != 'Y';

	if ($useDefaultMessages || !isset($arParams['MESS_PRICE_FREE'])) {
		$arParams['MESS_PRICE_FREE'] = Loc::getMessage('PRICE_FREE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_ECONOMY'])) {
		$arParams['MESS_ECONOMY'] = Loc::getMessage('ECONOMY_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_REGISTRATION_REFERENCE'])) {
		$arParams['MESS_REGISTRATION_REFERENCE'] = Loc::getMessage('REGISTRATION_REFERENCE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_1'])) {
		$arParams['MESS_AUTH_REFERENCE_1'] = Loc::getMessage('AUTH_REFERENCE_1_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_2'])) {
		$arParams['MESS_AUTH_REFERENCE_2'] = Loc::getMessage('AUTH_REFERENCE_2_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_3'])) {
		$arParams['MESS_AUTH_REFERENCE_3'] = Loc::getMessage('AUTH_REFERENCE_3_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_ADDITIONAL_PROPS'])) {
		$arParams['MESS_ADDITIONAL_PROPS'] = Loc::getMessage('ADDITIONAL_PROPS_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_USE_COUPON'])) {
		$arParams['MESS_USE_COUPON'] = Loc::getMessage('USE_COUPON_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_COUPON'])) {
		$arParams['MESS_COUPON'] = Loc::getMessage('COUPON_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_PERSON_TYPE'])) {
		$arParams['MESS_PERSON_TYPE'] = Loc::getMessage('PERSON_TYPE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PROFILE'])) {
		$arParams['MESS_SELECT_PROFILE'] = Loc::getMessage('SELECT_PROFILE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_REGION_REFERENCE'])) {
		$arParams['MESS_REGION_REFERENCE'] = Loc::getMessage('REGION_REFERENCE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_PICKUP_LIST'])) {
		$arParams['MESS_PICKUP_LIST'] = Loc::getMessage('PICKUP_LIST_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_NEAREST_PICKUP_LIST'])) {
		$arParams['MESS_NEAREST_PICKUP_LIST'] = Loc::getMessage('NEAREST_PICKUP_LIST_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PICKUP'])) {
		$arParams['MESS_SELECT_PICKUP'] = Loc::getMessage('SELECT_PICKUP_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_INNER_PS_BALANCE'])) {
		$arParams['MESS_INNER_PS_BALANCE'] = Loc::getMessage('INNER_PS_BALANCE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_ORDER_DESC'])) {
		$arParams['MESS_ORDER_DESC'] = Loc::getMessage('ORDER_DESC_DEFAULT');
	}

	$useDefaultMessages = !isset($arParams['USE_CUSTOM_ERROR_MESSAGES']) || $arParams['USE_CUSTOM_ERROR_MESSAGES'] != 'Y';

	if ($useDefaultMessages || !isset($arParams['MESS_PRELOAD_ORDER_TITLE'])) {
		$arParams['MESS_PRELOAD_ORDER_TITLE'] = Loc::getMessage('PRELOAD_ORDER_TITLE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_SUCCESS_PRELOAD_TEXT'])) {
		$arParams['MESS_SUCCESS_PRELOAD_TEXT'] = Loc::getMessage('SUCCESS_PRELOAD_TEXT_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_FAIL_PRELOAD_TEXT'])) {
		$arParams['MESS_FAIL_PRELOAD_TEXT'] = Loc::getMessage('FAIL_PRELOAD_TEXT_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TITLE'])) {
		$arParams['MESS_DELIVERY_CALC_ERROR_TITLE'] = Loc::getMessage('DELIVERY_CALC_ERROR_TITLE_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TEXT'])) {
		$arParams['MESS_DELIVERY_CALC_ERROR_TEXT'] = Loc::getMessage('DELIVERY_CALC_ERROR_TEXT_DEFAULT');
	}

	if ($useDefaultMessages || !isset($arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR'])) {
		$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR'] = Loc::getMessage('PAY_SYSTEM_PAYABLE_ERROR_DEFAULT');
	}

	$scheme = $request->isHttps() ? 'https' : 'http';

	switch (LANGUAGE_ID) {
		case 'ru':
			$locale = 'ru-RU';
			break;
		case 'ua':
			$locale = 'ru-UA';
			break;
		case 'tk':
			$locale = 'tr-TR';
			break;
		default:
			$locale = 'en-US';
			break;
	}

	\Bitrix\Main\UI\Extension::load('ui.fonts.opensans');
	$this->addExternalCss('/bitrix/css/main/bootstrap.css');
	$APPLICATION->SetAdditionalCSS('/bitrix/css/main/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css', true);
	$APPLICATION->SetAdditionalCSS($templateFolder . '/style.css', true);
	$this->addExternalJs($templateFolder . '/order_ajax.js');
	$this->addExternalJs($templateFolder . '/order_ajax_ext.js');
	\Bitrix\Sale\PropertyValueCollection::initJs();
	$this->addExternalJs($templateFolder . '/script.js');

?>
	<NOSCRIPT>
		<div style="color:red"><?= Loc::getMessage('SOA_NO_JS') ?></div>
	</NOSCRIPT>
<?

	if ($request->get('ORDER_ID') <> '') {
		include(Main\Application::getDocumentRoot() . $templateFolder . '/confirm.php');
	} elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET']) {
		include(Main\Application::getDocumentRoot() . $templateFolder . '/empty.php');
	} else {
		Main\UI\Extension::load('phone_auth');

		$hideDelivery = empty($arResult['DELIVERY']);

		if($arResult["DELIVERY"][39]["CHECKED"] == 'Y'){
			?>
			<script>

			</script>
			<?php
		}

		$profileId = 0;
		foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $profile) {
			if ($profile["CHECKED"] == "Y") {
				$profileId = $profile["ID"];
			}
		}

		global $USER;
		$user = \Bitrix\Main\UserTable::getList([
			'filter' => [
				'=ID' => $USER->GetID(),
			],
			'limit' => 1,
			'select' => ['*', 'UF_LOCATION'],
		])->fetch();
		?>
		<section class="checkout">
			<div class="container">

				<form action="<?= POST_FORM_ACTION_URI ?>" method="POST" name="ORDER_FORM" id="bx-soa-order-form"
					  enctype="multipart/form-data" class="checkout__inner js_checkout_pickup_client_address">
					<input type="hidden" name="<?= $arParams['ACTION_VARIABLE'] ?>" value="saveOrderAjax">
					<input type="hidden" name="location_type" value="code">
					<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="">
					<input type="hidden" name="PROFILE_ID" id="PROFILE_ID" value="0">

					<?
						echo bitrix_sessid_post();

						if ($arResult['PREPAY_ADIT_FIELDS'] <> '') {
							echo $arResult['PREPAY_ADIT_FIELDS'];
						}
					?>
					<div class="checkout__left-column" id="bx-soa-order">
						<div class="checkout__title-row">
							<a class="checkout__back-btn" href="/personal/cart/"></a>

							<h1 class="checkout__title">Оформление заказа</h1>
						</div>

						<ul class="cart__delivery-list">
							<li class="cart__delivery-item js_cart_delivery_item cart_delivery_item bx-soa-pp-company
                    <?= ($arResult["DELIVERY"][39]["CHECKED"] == "Y") ? "active" : "" ?>" id="pickup_btn"
								onclick="document.querySelector('#ID_DELIVERY_ID_39').checked = true;">
								<h4 class="cart__delivery-title">
									Самовывоз из аптеки
								</h4>

								<span class="cart__delivery-price">
                                    Стоимость доставки - бесплатно
                                </span>

								<span class="cart__delivery-pay-way">
                                    Оплата наличными или онлайн
                                </span>

								<input
										id="ID_DELIVERY_ID_39"
										name="DELIVERY_ID"
										type="radio"
										value="39"
										style="display:none;"
									<?= ($arResult["DELIVERY"][39]["CHECKED"] == "Y") ? "checked" : "" ?>
								/>
							</li>

							<li class="cart__delivery-item js_cart_delivery_item <?= ($arResult["DELIVERY"][39]["CHECKED"] == "Y") ? "" : "active" ?>"
								id="delivery_btn">
								<h4 class="cart__delivery-title">
									Доставка курьером
								</h4>

								<span class="cart__delivery-price">
                                    Бесплатная доставки - от 30 000 р
                                </span>

								<span class="cart__delivery-pay-way">
                                    Оплата онлайн
                                </span>
							</li>
						</ul>

						<div id="checkout__map-store"
							 style="display:<? //=($arResult["DELIVERY"][39]["CHECKED"]=="Y")?"block":"none"
							 ?>">
							<span class="checkout__subtitle">Выберите удобную Вам аптеку</span>

							<ul class="checkout__filters-list">
								<li class="checkout__filter-item">
									<button
											class="checkout__filter-btn checkout__filter-btn--all-time js_checkout__filter_btn js_checkout_filter_btn_all_time"
											type="button" data-group="1">Открыто 24/7
									</button>
								</li>

								<li class="checkout__filter-item">
									<button
											class="checkout__filter-btn checkout__filter-btn--all-order js_checkout__filter_btn js_checkout_filter_btn_all_order"
											type="button" data-group="2">Есть все товары из заказа
									</button>
								</li>
							</ul>

							<label class="checkout__search-address" for="">
                            <span class="checkout__search-title">
                                Поиск по улице или названию аптеки
                            </span>

								<div class="checkout__search-input-wrapper">
									<input class="checkout__search-input js_checkout_address_input" type="text">
									<!-- чтобы показать добавь класс active, но js покажет её при заполнении поля -->
									<? if (false/*$USER->IsAuthorized()*/):?>
										<button class="checkout__save-address js_btn_popup_save_pharmacy js_btn_popup"
												data-btn="save-pharmacy" type="button">Сохранить аптеку
										</button>
									<? endif; ?>
								</div>
							</label>

							<div class="nav_tabs_view">
								<ul>
									<li class="tabs-nav__item ">
										<button data-param_name="type_viem"  data-param_value="map"
												type="button">Показать на карте
										</button>
									</li>
									<li class="tabs-nav__item">
										<button data-param_name="type_viem" class="active" data-param_value="list" type="button">
											Показать списком
										</button>
									</li>
								</ul>
							</div>

							<div class="checkout__map js_checkout_map" id="map" style="display: none"></div>

							<div class="checkout__pharm_list" id="pharm_list" >
								<ul class="cart__product-list">
									<? foreach ($arResult["STORES"] as $store): ?>
										<?php
										foreach ($arResult['BASKET_ITEMS'] as $key => $product):
											$rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList([
												'filter' => ['=PRODUCT_ID' => $product['PRODUCT_ID'], '=STORE.ACTIVE' => 'Y', '=STORE_ID' => $store['ID']],
												'select' => ['AMOUNT', 'STORE_ID', 'STORE_TITLE' => 'STORE.TITLE'],
											]);

											$all = false;

											while ($arStoreProduct = $rsStoreProduct->fetch()) {
												if ($arStoreProduct['AMOUNT'] > 0) {
													$all = true;
												}
											}
										endforeach;
										?>
										<li class="cart__product-item <?= $store["UF_OPENALLTIME"] ? "isOpenAllTime" : "" ?> <?= $all ? "isAvailibleFullOrder" : "" ?>">
											<div class="cart__name-wrapper">
												<a class="cart__product-name" href="">
													<?= $store['TITLE'] ?>
												</a>
												<br><?= $store['PHONE'] ?><br><?= $store['ADDRESS'] ?>
												<div class="big-balloon__address-sum-row">
												<span class="big-balloon__address-sum">
													<?= $arResult["JS_DATA"]["TOTAL"]["ORDER_PRICE_FORMATED"] ?>
												</span>
													<span class="big-balloon__address-availability">
													<?= ($all == true) ? "Все товары в наличии" : "Некоторые товары отсутствуют" ?>
                                				</span>
												</div>
											</div>
											<div class="cart__price-row ">
												<button class="big-balloon__choose-btn" type="button"
														data-pharma-choose="<?= $store['ID'] ?>"
														data-pharma-address="<?= $store['ADDRESS'] ?>"
												>
													Выбрать аптеку
												</button>
											</div>
										</li>
									<? endforeach; ?>
								</ul>
							</div>

						</div>

						<div class="checkout__client-wrapper"
							 style="display:<?= ($arResult["DELIVERY"][39]["CHECKED"] == "Y") ? "none" : "block" ?>"
							 id="checkout__delivery-address">
							<h3 class="checkout__client-title">
								Укажите адрес доставки
							</h3>

							<div class="checkout__address-imputs">

								<label class="checkout__client-label checkout__client-label--bottom" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title ">
                                        Город
                                    </span>
									<?php
										$city = '';
										if ($user['UF_LOCATION']) {
											$city = $user['UF_LOCATION'];
										} else {
											$city = $_COOKIE['BITRIX_SM_LOCATION'];
										}
									?>
									<input class="checkout__client-input js_checkout_city_input" type="text"
										   name="ORDER_PROP_5" id="soa-property-5" data-property-id-row="5"
										   value="<?= $city ?>">
								</label>

								<div class="checkout__client-label-wrapper">
									<label class="checkout__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="checkout__clients-title required">
                                        Улица
                                    </span>

										<input class="checkout__client-input js_checkout_street_input"
											   name="ORDER_PROP_8"
											   id="soa-property-8"
											   autocomplete="address"
											   type="text"
											   data-property-id-row="8">
									</label>


									<? if ($USER->IsAuthorized() && is_array($arResult["USER_PROFILE"])):?>
										<button class="checkout__saved-addresses-btn-desctop js_btn_popup"
												data-btn="saved-address" type="button">
											Сохраненный адрес
										</button>
									<? endif; ?>
								</div>

								<div class="checkout__address-imputs-wrapper">
									<label class="checkout__client-label required" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="checkout__clients-title required">
                                            Дом
                                        </span>

										<input class="checkout__client-input js_checkout_home_input" type="text"
											   name="ORDER_PROP_9"
											   id="soa-property-9"
											   data-property-id-row="9">
									</label>

									<label class="checkout__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="checkout__clients-title">
                                            Корпус
                                        </span>

										<input class="checkout__client-input js_checkout_home_litera_input" type="text"
											   name="ORDER_PROP_10"
											   id="soa-property-10"
											   data-property-id-row="10">
									</label>

									<label class="checkout__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="checkout__clients-title required">
                                            Квартира
                                        </span>

										<input class="checkout__client-input js_checkout_flat_input" type="text"
											   name="ORDER_PROP_11"
											   id="soa-property-11"
											   data-property-id-row="11">
									</label>
								</div>

								<div class="checkout__address-imputs-wrapper">
									<label class="checkout__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="checkout__clients-title">
                                            Подъезд
                                        </span>

										<input class="checkout__client-input js_checkout_front_door_input" type="text"
											   name="ORDER_PROP_12"
											   id="soa-property-12"
											   data-property-id-row="12">
									</label>

									<label class="checkout__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="checkout__clients-title">
                                            Этаж
                                        </span>

										<input class="checkout__client-input js_checkout_floor_input" type="text"
											   name="ORDER_PROP_13"
											   id="soa-property-13"
											   data-property-id-row="13">
									</label>

									<label class="checkout__client-label" for="">
										<!-- если поле обязательно добавть класс required - добавится звездочка -->
										<span class="checkout__clients-title">
                                            Код домофона
                                        </span>

										<input class="checkout__client-input js_checkout_floor_code_input" type="text"
											   name="ORDER_PROP_14"
											   id="soa-property-14"
											   data-property-id-row="14">
									</label>
								</div>

								<label class="checkout__client-label checkout__client-label--bottom" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title ">
                                        Комментарий
                                    </span>

									<input class="checkout__client-input js_checkout_comment_input" type="text"
										   name="ORDER_PROP_15"
										   id="soa-property-15"
										   data-property-id-row="15">
								</label>

								<button class="checkout__saved-addresses-btn-mobile js_btn_popup"
										data-btn="saved-address" type="button">
									Сохраненный адрес
								</button>
							</div>
						</div>
						<div class="checkout__pay" id="checkout__delivery"
							 style="display:<?= ($arResult["DELIVERY"][39]["CHECKED"] == "Y") ? "none" : "block" ?>">
							<h3 class="checkout__delivery-title">
								Выберите курьерскую компанию
							</h3>

							<ul class="cart__delivery-list" id="bx-soa-delivery">
								<? foreach ($arResult["DELIVERY"] as $delivery):?>
									<? if ($delivery["ID"] == 39) continue; ?>
									<li class="checkout__pay-way-item cart_delivery_item bx-soa-pp-company <?= ($delivery["CHECKED"] == "Y") ? "bx-selected" : "" ?>"
										data-value="<?= $delivery["ID"] ?>"
										onclick="document.querySelector('#ID_DELIVERY_ID_<?= $delivery["ID"] ?>').checked = true;">
										<button class="checkout__delivery-btn full <?= ($delivery["CHECKED"] == "Y") ? "active" : "" ?>"
												type="button">
                                    <span class="checkout__delivery-btn-name" type="button">
                                        <?= $delivery["NAME"] ?>
                                    </span>
											<div class="checkout__delivery-btn-price">
												<?= $delivery["PRICE_FORMATED"] ?>
											</div>

											<span class="checkout__delivery-btn-info">
                                            Ближайшая доставка:
                                        </span>

											<span class="checkout__delivery-btn-time">
											<? if ($delivery['PERIOD_TEXT']) {
												if ($arResult["DAY_DELIVERY"]["HOUR"] >= 21) {
													echo 'Завтра';
												}else{
													echo $delivery['PERIOD_TEXT'];
												}
											} else {
												echo $arResult["DAY_DELIVERY"]["TEXT"];
											} ?>
                                        </span>
											<span class="checkout__delivery-btn-time-not_av">
                                            От 1-3 дней
                                        </span>
										</button>
										<input
												id="ID_DELIVERY_ID_<?= $delivery["ID"] ?>"
												name="DELIVERY_ID"
												type="radio"
												value="<?= $delivery["ID"] ?>"
												style="display:none;"
										/>
									</li>
								<? endforeach; ?>
							</ul>
						</div>

						<div class="checkout__time"
							 style="display:<?= ($arResult["DELIVERY"][39]["CHECKED"] == "Y") ? "none" : "block" ?>">
							<h3 class="checkout__delivery-title">
								Выберите дату и время доставки
							</h3>
							<?php
								// Если поздний час доставка со следующего дня
								if ($arResult["DAY_DELIVERY"]["HOUR"] >= 21) {
									unset($arResult["DAY_DELIVERY"]["ITEMS"][0]);
								}
							?>
							<div class="checkout__date-list-wrapper">
								<div class="swiper js_checkout_date_list">
									<div class="swiper-wrapper">
										<? $check = true; ?>
										<? $dayArray = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС']; ?>

										<? foreach ($arResult["DAY_DELIVERY"]["ITEMS"] as $id => $day):?>
											<div class="swiper-slide">
												<li class="checkout__date-item">
													<button class="checkout__date-btn js_checkout_date_btn
															<? if ($check) {
														echo "active";
														$check = false;
													} ?>"
															id="date_btn_<?= $id ?>"
															type="button">
																<span class="checkout__date-btn-day">
																	<?= $day["DAY_OF_WEAK"] ?>
																</span>

														<span class="checkout__date-btn-date"><?= $day["DATE"] ?></span>
													</button>
												</li>
											</div>
										<? endforeach; ?>
									</div>
									<div
											class="swiper-button-prev checkout__date-scroll-btn checkout__date-scroll-btn--prev">
									</div>
									<div
											class="swiper-button-next checkout__date-scroll-btn checkout__date-scroll-btn--next">
									</div>
								</div>
							</div>

							<div class="checkout__date-list-wrapper">
								<ul class="checkout__date-list">
									<? $check = true; ?>
									<? foreach ($arResult["TIME_DELIVERY"] as $id => $time):?>
										<li class="checkout__date-item checkout__date-item--big">
											<button class="checkout__date-btn js_checkout_time_btn
                                    <? if ($check) {
												echo "active";
												$check = false;
											} ?>"
													id="time_btn_<?= $id ?>"
													type="button">
                                        <span class="checkout__date-btn-day">
                                            <?= $time["TEXT"] ?>
                                        </span>
												<span class="checkout__date-btn-date"><?= $time["TIME"] ?></span>
											</button>
										</li>
									<? endforeach; ?>
								</ul>
							</div>
							<input style="display:none" type="text"
								   name="ORDER_PROP_16"
								   id="soa-property-16"
								   data-property-id-row="16"
								   value="<?= $arResult["DAY_DELIVERY"]["ITEMS"][array_key_first($arResult["DAY_DELIVERY"]["ITEMS"])]["DATE"] . " " .
								   $arResult["TIME_DELIVERY"][array_key_first($arResult["TIME_DELIVERY"])]["TIME"] ?>">
						</div>

						<div class="checkout__pay">
							<h3 class="checkout__delivery-title">
								Выберите способ оплаты
							</h3>

							<ul class="checkout__pay-way-list ">
								<? foreach ($arResult["PAY_SYSTEM"] as $pay_system):?>
									<li class="checkout__pay-way-item"
										onclick="document.querySelector('#ID_PAY_SYSTEM_ID_<?= $pay_system["ID"] ?>').checked = true;">
										<button class="checkout__pay-way-btn js_checkout_pay_way_btn <?= ($pay_system["CHECKED"] == "Y") ? "active" : ""; ?>"
												type="button">
											<?= $pay_system["NAME"] ?>
										</button>
										<input
												id="ID_PAY_SYSTEM_ID_<?= $pay_system["ID"] ?>"
												name="PAY_SYSTEM_ID"
												type="radio"
												value="<?= $pay_system["ID"] ?>"
												style="display:none;"
											<?= ($pay_system["CHECKED"] == "Y") ? "checked" : ""; ?>
										/>
									</li>
								<? endforeach; ?>
							</ul>
						</div>

						<div class="checkout__client-wrapper" <?= $USER->IsAuthorized() ? 'style="display:none"' : '' ?>
							 id="bx-soa-auth">
							<h3 class="checkout__client-title">
								Укажите данные получателя
							</h3>

							<div class="checkout__clients-inputs">
								<label class="checkout__client-label required">
                            <span class="checkout__clients-title required">
                                        Имя
                                    </span>

									<input class="checkout__client-input js_checkout_client_name_input" type="text"
										   name="ORDER_PROP_1"
										   id="soa-property-1"
										   data-property-id-row="1"
										   value="<?= $arResult["ORDER_PROP"]["USER_PROPS_N"][1]["VALUE"] ?>">
								</label>

								<label class="checkout__client-label">
                            <span class="checkout__clients-title">
                                        E-mail
                                    </span>

									<input class="checkout__client-input js_checkout_client_email_input" type="text"
										   name="ORDER_PROP_2"
										   id="soa-property-2"
										   data-property-id-row="2"
										   value="<?= $arResult["ORDER_PROP"]["USER_PROPS_N"][2]["VALUE"] ?>">
								</label>

								<label class="checkout__client-label required">
                            <span class="checkout__clients-title required">
                                        Номер телефона
                                    </span>

									<input class="checkout__client-input js_checkout_client_phone_input js_input_masked_phone"
										   type="text"
										   name="ORDER_PROP_3"
										   id="soa-property-3"
										   data-property-id-row="3"
										   value="<?= $arResult["ORDER_PROP"]["USER_PROPS_N"][3]["VALUE"] ?>">
								</label>

								<a class="checkout__action-btn" id="confirmSmsCodeBtn" style="
                            max-width: 250px;
                            margin: 28px 0 0px 0;
                        ">Подтвердить по sms</a>

								<label class="checkout__client-label" id="confirmSmsCodeInput" style="display: none">
                            <span class="checkout__clients-title required">
                                        Код подтверждения
                                    </span>

									<input class="checkout__client-input js_checkout_client_phone_input" type="number"
										   name="SMS_CODE"
										   max="9999"
										   maxlength="4"
										   id="SMS_CODE">
								</label>
							</div>
						</div>
					</div>

					<div class="checkout__right-column" id="bx-soa-total">
						<div class="checkout__result-wrapper">
							<div class="checkout__result-title-row">
								<p class="checkout__result-title">
									Ваш заказ
								</p>

								<span class="checkout__result-title-num">
                                    (<?= $arResult["JS_DATA"]["TOTAL"]["BASKET_POSITIONS"] ?> товаров)
                                </span>
							</div>
							<? //d($arResult["BASKET_ITEMS"]);
							?>

							<ul class="checkout__product-list js_checkout_product_list">
								<? foreach ($arResult["BASKET_ITEMS"] as $product):?>
									<?
									$rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList([
										'filter' => ['=PRODUCT_ID' => $product['PRODUCT_ID'], '=STORE.ACTIVE' => 'Y'],
										'select' => ['AMOUNT', 'STORE_ID', 'STORE_TITLE' => 'STORE.TITLE'],
									]);

									$store_available = [];

									while ($arStoreProduct = $rsStoreProduct->fetch()) {
										$store_available[] = $arStoreProduct['STORE_ID'];
									}
									?>
									<li class="checkout__product-item"
										data-store_list="[<?= implode(',', $store_available) ?>]">
										<a class="checkout__product-img-link" href="<?= $product["DETAIL_PAGE_URL"] ?>">
											<img class="checkout__product-img"
												 src="<?= $product["PREVIEW_PICTURE"] ? CFile::GetPath($product["PREVIEW_PICTURE"]) : SITE_TEMPLATE_PATH . '/images/notimages.png'; ?>"
												 alt="<?= $product["NAME"] ?>">
										</a>

										<a class="checkout__product-title" href="<?= $product["DETAIL_PAGE_URL"] ?>">
											<?= $product["NAME"] ?>
										</a>

										<div class="checkout__product-num-wrapper">
                                        <span class="checkout__product-price">
                                            <?= $product["PRICE"] ?> ₽
                                        </span>
											<span class="checkout__product-price on__order">
                                            <?= $product["PRICE"] - 10 ?> ₽
                                        </span>

											<span class="not_available-title">Под заказ</span>
										</div>
										<span class="not_available-desc">Заказ можно забрать через 3 дня</span>
									</li>
								<? endforeach; ?>
							</ul>

							<button class="checkout__toggle-product-list-btn js_checkout_toggle_product_list_btn"
									type="button"
									style="<?= (count((array)$arResult["BASKET_ITEMS"]) < 4) ? "display:none" : ""; ?>">
                                <span
										class="checkout__toggle-product-list-text checkout__toggle-product-list-text--show">Раскрыть</span>
								<span
										class="checkout__toggle-product-list-text checkout__toggle-product-list-text--hide">Скрыть</span>
							</button>

							<div class="bx-soa-cart-total-prices">
							</div>


						</div>


						<p class="checkout__terms" id="bx-soa-orderSave">
							Нажимая на кнопку «Перейти к оформлению заказа» вы соглашаетесь с
							<a class="checkout__terms-link" href="/about/user-rules/" target="_blank">условиями отпуска
								товара</a>
							, соглашаетесь на
							<a class="checkout__terms-link" href="/about/privacy-policy/" target="_blank">обработку
								персональных данных</a> и
							на получение информации о статусе заказа по электронной почте и в виде смс-сообщений.
						</p>
						<!--	DUPLICATE MOBILE ORDER SAVE BLOCK	-->
						<div id="bx-soa-total-mobile" style="margin-bottom: 6px;"></div>
					</div>
				</form>

			</div>

		</section>

		<!-- ПОП-АП 'СОХРАНЕННЫЕ АДРЕСА' -->
		<div class="popup popup--save-address js_popup js_popup_saved_address" data-popup="saved-address">
			<div class="popup__wrap">
				<button class="popup__exit js_popup_exit" type="button">
					<svg width="24" height="24">
						<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
					</svg>
				</button>
				<form class="popup__form js_popup_form">
					<div class="popup__title">Сохраненные адреса</div>

					<div class="popup__subtitle">Пожалуйста, выберите один из сохраненных адресов для доставки заказа.
					</div>

					<? foreach ($arResult["USER_PROFILE"] as $profileID => $profile):?>
						<div class="checkout__address-imputs js_address_card" data-profileid="<?= $profileID ?>">
							<label class="checkout__client-label checkout__client-label--street" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title">
                            Регион и улица
                        </span>

								<input class="checkout__client-input js_input_name js_input_street" type="text"
									   value="<?= $profile["PROPS"][8]["VALUE"] ?>" disabled>
							</label>

							<div class="checkout__address-imputs-wrapper">
								<label class="checkout__client-label required" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title">
                                Дом
                            </span>

									<input class="checkout__client-input js_input_name js_checkout_home_input"
										   type="number" min="1" value="<?= $profile["PROPS"][9]["VALUE"] ?>" disabled>
								</label>

								<label class="checkout__client-label" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title">
                                Корпус
                            </span>

									<input class="checkout__client-input js_input_name js_checkout_home_litera_input"
										   type="text" value="<?= $profile["PROPS"][10]["VALUE"] ?>" disabled>
								</label>

								<label class="checkout__client-label" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title">
                                Квартира
                            </span>

									<input class="checkout__client-input js_input_name js_checkout_flat_input"
										   type="number" min="1" value="<?= $profile["PROPS"][11]["VALUE"] ?>" disabled>
								</label>
							</div>

							<div class="checkout__address-imputs-wrapper">
								<label class="checkout__client-label" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title ">
                                Подъезд
                            </span>

									<input class="checkout__client-input js_input_name js_checkout_front_door_input"
										   type="text" value="<?= $profile["PROPS"][12]["VALUE"] ?>" disabled>
								</label>

								<label class="checkout__client-label" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title">
                                Этаж
                            </span>

									<input class="checkout__client-input js_input_name js_checkout_floor_input"
										   type="text" value="<?= $profile["PROPS"][13]["VALUE"] ?>" disabled>
								</label>

								<label class="checkout__client-label" for="">
									<!-- если поле обязательно добавть класс required - добавится звездочка -->
									<span class="checkout__clients-title ">
                                Код домофона
                            </span>

									<input class="checkout__client-input js_checkout_floor_code_input" type="text"
										   value="<?= $profile["PROPS"][14]["VALUE"] ?>" disabled>
								</label>
							</div>

							<label class="checkout__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="checkout__clients-title ">
                            Комментарий
                        </span>

								<input class="checkout__client-input js_checkout_comment_input" type="text"
									   value="<?= $profile["PROPS"][15]["VALUE"] ?>" disabled>
							</label>

							<ul class="popup__stop-btns">
								<li class="popup__stop-btn-item">
									<button class="popup__btn popup__btn--continue btn btn--tr js_submit_btn">Выбрать
										адрес
									</button>
								</li>
							</ul>
						</div>
					<? endforeach; ?>

				</form>
			</div>
		</div>

		<script>
			$(document).ready(function () {
				<?if($arResult['DELIVERY'][39]['CHECKED']=='Y'):?>
				$("#pickup_btn").trigger("click");
				<?endif;?>
				$(".checkout__pharm_list button").click(function () {
					$('.checkout__pharm_list button').removeClass('active');
					$(this).addClass('active');
					var address = $(this).data('pharma-address');
					$('.js_checkout_address_input').val(address).removeClass('error');
					$("#BUYER_STORE").val($(this).data('pharma-choose'));
				});
			});

			const pharmInfo = [
				<?foreach ($arResult["STORES"] as $store):?>
				<?php
				foreach ($arResult['BASKET_ITEMS'] as $key => $product):
					$rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList([
						'filter' => ['=PRODUCT_ID' => $product['PRODUCT_ID'], '=STORE.ACTIVE' => 'Y', '=STORE_ID' => $store['ID']],
						'select' => ['AMOUNT', 'STORE_ID', 'STORE_TITLE' => 'STORE.TITLE'],
					]);

					$all = false;

					while ($arStoreProduct = $rsStoreProduct->fetch()) {
						if ($arStoreProduct['AMOUNT'] > 0) {
							$all = true;
						}
					}
				endforeach;
				?>
				{
					id: <?=$store["ID"]?>,
					x: <?=$store["GPS_N"]?>,
					y: <?=$store["GPS_S"]?>,
					address: "<?=$store["ADDRESS"]?>",
					title: "<?=$store["NAME"]?>",
					openDays: "<?=$store["UF_OPENDAYS"]?>",
					openDaysShort: "<?=$store["UF_OPENDAYS"]?>",
					area: "<?=$store["UF_OPENDAYS_SHRT"]?>",
					openTime: "<?=$store["UF_OPENTIME"]?>",
					phone: "<?=$store["PHONE"]?>",
					phoneForLink: "<?=$store["PHONE"]?>",
					mail: "<?=$store["EMAIL"]?>",
					isOpenAllTime: <?=$store["UF_OPENALLTIME"] ? "true" : "false"?>,
					isAvailibleFullOrder: <?=$all ? "true" : "false"?>,
					notAvailible: [
						<?foreach ($store["NOT_AVAILABLE"] as $NOT_AVAILABLE):?>
						"<?=$NOT_AVAILABLE?>",
						<?endforeach;?>
					],
				},
				<?endforeach;?>
			];

			function debounce(f, ms) {
				let isCooldown = false;

				return function () {
					if (isCooldown) return;

					f.apply(this, arguments);

					isCooldown = true;

					setTimeout(() => (isCooldown = false), ms);
				};
			}

			if (document.querySelector(".js_checkout_map")) {
				let myMap; //сама карта
				let addressInput = document.querySelector(".js_checkout_address_input"); //поле ввода адреса
				let filterBtnAllTime = document.querySelector(
					".js_checkout_filter_btn_all_time"
				); //кнопка фильтра "открыто 24/7"
				let filterBtnAllOrder = document.querySelector(
					".js_checkout_filter_btn_all_order"
				); //кнопка фильтра "всё в наличии"
				let filterdPharmInfo = pharmInfo; // тут храним всю информацию об аптека, но уже отфильтрованную именно под эту карту
				const openBtn = document.querySelector(".js_btn_popup_save_pharmacy");

				// фильтруем аптеки при клике на кнопку  "открыто 24/7"
				filterBtnAllTime.addEventListener("click", () => {
					$('#pharm_list .cart__product-item').toggle();
					$('#pharm_list .cart__product-item.isOpenAllTime').show();
					// если у кнопка ещё не активна
					if (filterBtnAllTime.classList.contains("active")) {
						// если выбран фильтр "все товары"
						if (filterBtnAllOrder.classList.contains("active")) {
							filterdPharmInfo = pharmInfo.filter(
								(item) => item.isAvailibleFullOrder
							);
						} else {
							// если никакие фильтры не выбраны
							// отображаем обратно полный список
							filterdPharmInfo = pharmInfo;
						}

						// в любом случае надо перезапустить карту
						myMap.destroy();

						init();
					} else {
						filterdPharmInfo = filterdPharmInfo.filter(
							(item) => item.isOpenAllTime
						);

						myMap.destroy();

						init();
					}
				});

				// фильтруем аптеки при клике на кнопку  "все товары"
				filterBtnAllOrder.addEventListener("click", () => {
					$('#pharm_list .cart__product-item').toggle();
					$('#pharm_list .cart__product-item.isAvailibleFullOrder').show();
					// если у кнопка ещё не активна
					if (filterBtnAllOrder.classList.contains("active")) {
						// если выбран фильтр "открыто 24/7"
						if (filterBtnAllTime.classList.contains("active")) {
							filterdPharmInfo = pharmInfo.filter((item) => item.isOpenAllTime);
						} else {
							// если никакие фильтры не выбраны
							// отображаем обратно полный список
							filterdPharmInfo = pharmInfo;
						}

						// в любом случае надо перезапустить карту
						myMap.destroy();

						init();
					} else {
						filterdPharmInfo = filterdPharmInfo.filter(
							(item) => item.isAvailibleFullOrder
						);

						myMap.destroy();

						init();
					}

				});

				ymaps.ready(init); // Дождёмся загрузки API и готовности DOM;

				function init() {
					myMap = new ymaps.Map("map", {
						// Создание экземпляра карты и его привязка к контейнеру с заданным id;
						center: [55.62728, 37.586311], // При инициализации карты обязательно нужно указать её центр и коэффициент масштабирования;
						zoom: 10,
						controls: ['zoomControl'], // Скрываем элементы управления на карте;
					});

					let collection = new ymaps.GeoObjectCollection(null, {
						// Создаём коллекцию, в которую будемпомещать метки (что-то типа массива);
						// preset: 'islands#yellowIcon'
					});

					// из всей информации об аптеках вытаскиваем только координаты
					let collectionCoords = filterdPharmInfo.map((item) => {
						return [item.x, item.y];
					});

					for (let i = 0, l = collectionCoords.length; i < l; i++) {
						// C помощью цикла добавляем все метки в коллекцию;
						// находим по коррдинатам полную информацию об аптеке для вставки в балун
						const ourPoint = filterdPharmInfo.filter((item) => {
							return item.x === collectionCoords[i][0];
						});

						// если все товары в наличии создаём один балун
						if (ourPoint[0]["isAvailibleFullOrder"]) {
							collection.add(
								new ymaps.Placemark(collectionCoords[i], {
									balloonContent: `
                        <div class="big-balloon">
                            <h3 class="big-balloon__address-title">
                            ${ourPoint[0]["title"]}
                            </h3>

                            <span class="big-balloon__address-text js_baloon_address">
                                ${ourPoint[0]["address"]}
                            </span>

                            <div class="big-balloon__address-open-time-row">
                                <span class="big-balloon__address-open-days">
                                    ${ourPoint[0]["openDays"]}
                                </span>

                                <span class="big-balloon__address-open-time">
                                    ${ourPoint[0]["openTime"]}
                                </span>
                            </div>

                            <a class="big-balloon__address-phone" href="tel:${ourPoint[0]["phoneForLink"]}">
                                ${ourPoint[0]["phone"]}
                            </a>

                            <div class="big-balloon__address-sum-row">
                                <span class="big-balloon__address-sum">
                                    <?=$arResult["JS_DATA"]["TOTAL"]["ORDER_PRICE_FORMATED"]?>
                                </span>

                                <span class="big-balloon__address-availability" >
                                    Все товары в наличии
                                </span>
                            </div>

                            <button class="big-balloon__choose-btn js_ballon_accept_btn" type="button" data-pharma-choose="${ourPoint[0]["id"]}" >
                                Выбрать аптеку
                            </button>
                        </div>
                        `,
								})
							);
							// collection.get(i).properties.set('iconContent', `${i + 1}`); // Чтоб в дальнейшем к ней можно было как-то обратиться к нужной метке, добавляем каждой порядковый номер, записываем его в свойство 'iconContent';
						} else {
							// если НЕ все товары в наличии создаём один балун

							collection.add(
								new ymaps.Placemark(collectionCoords[i], {
									balloonContent: `
                        <div class="big-balloon big-balloon--without-product">
                            <h3 class="big-balloon__address-title">
                            ${ourPoint[0]["title"]}
                            </h3>

                            <span class="big-balloon__address-text js_baloon_address">
                                ${ourPoint[0]["address"]}
                            </span>

                            <div class="big-balloon__address-open-time-row">
                                <span class="big-balloon__address-open-days">
                                    ${ourPoint[0]["openDays"]}
                                </span>

                                <span class="big-balloon__address-open-time">
                                    ${ourPoint[0]["openTime"]}
                                </span>
                            </div>

                            <a class="big-balloon__address-phone" href="tel:${ourPoint[0]["phoneForLink"]}">
                                ${ourPoint[0]["phone"]}
                            </a>

                            <div class="big-balloon_openDays_without-product-wrapper">
                                <p class="big-balloon__without-product-title">
                                    В данной аптеке нет в налчии:
                                </p>

                                <ul class="big-balloon__without-product-list">
                                    <li class="big-balloon__without-product-item">
                                        ${ourPoint[0]["notAvailible"][0]}
                                    </li>
                                </ul>

                                <p class="big-balloon__without-product-text">
                                    Мы можем оформить доставку товара со склада в данную аптеку. Срок доставки 3 дня.
                                </p>
                            </div>

                            <button class="big-balloon__choose-btn big-balloon__choose-btn--without-product js_ballon_accept_btn"  type="button" data-pharma-choose="${ourPoint[0]["id"]}">
                                Оформить заказ на данную аптеку
                            </button>
                        </div>
                        `,
								})
							);
							// collection.get(i).properties.set('iconContent', `${i + 1}`); // Чтоб в дальнейшем к ней можно было как-то обратиться к нужной метке, добавляем каждой порядковый номер, записываем его в свойство 'iconContent';
						}
					}

					// при нажатии Выбрать аптеку в балуне
					let map = document.querySelector(".js_checkout_map");

					if (map) {
						map.addEventListener("click", (e) => {
							// проверяем что кликнули в кнопку
							if (e.target.classList.contains("js_ballon_accept_btn")) {

								var choosenStore = $(e.target).data('pharma-choose');

								$(".checkout__product-item").each(function () {
									var arStoreList = $(this).data("store_list");

									if (jQuery.inArray(choosenStore, arStoreList) === -1) {
										$(this).addClass("not_available");
										$('.checkout__delivery-btn-time').hide();
										$('.checkout__delivery-btn-time-not_av').show();
									} else {
										$(this).removeClass("not_available");
										$('.checkout__delivery-btn-time').show();
										$('.checkout__delivery-btn-time-not_av').hide();
									}
								});


								const targetBalloon = e.target.parentNode;

								const address = targetBalloon.querySelector(".js_baloon_address");

								addressInput.value = address.innerHTML.trim();

								var storeId = pharmInfo.find(store => store.address === address.innerHTML.trim()).id;
								$("#BUYER_STORE").val(storeId);
								// закрываем балун
								myMap.balloon.close();

								// отображаем только одну метку на карте
								filterPoints(address.innerHTML.trim());

							}
						});
					}

					// при выборе аптеки отобраажем на карте только её
					function filterPoints(address) {
						filterdPharmInfo = pharmInfo.filter((item) => item.address == address);

						myMap.destroy();

						init();
					}

					myMap.geoObjects.add(collection); // Добавляем коллекцию с метками на карту;

					collection.options.set("iconLayout", "default#image"); // Необходимо указать данный тип макета;
					collection.options.set("iconImageHref", "<?=SITE_TEMPLATE_PATH . '/images/mark.svg'?>"); // Своё изображение иконки метки;
					collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
					collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
					collection.options.set("hideIcon", false); // Размеры метки;

					//перкскакиваем к нужному адресу
					const storeList = document.querySelector(".store__list"); // Список адресов
				}

				// ищем подходящте под поиск аптеки
				function searchPoint() {
					filterdPharmInfo = pharmInfo.filter(
						(item) =>
							item.address.includes(addressInput.value) ||
							item.title.includes(addressInput.value)
					);

					myMap.destroy();

					init();
				}

				// оборачиваем поиск в дебаунс чтобы откинуть лишние запросы
				let debouncedSearchPoint = debounce(searchPoint, 1000);

				// запускаем поиск по вводу
				addressInput.addEventListener("keyup", (e) => {
					debouncedSearchPoint();
				});
			}


			<?
			$signer = new Main\Security\Sign\Signer;
			$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.order.ajax');
			$messages = Loc::loadLanguageFile(__FILE__);
			?>
			BX.Sale.OrderAjaxComponentExt.init({
				result: <?=CUtil::PhpToJSObject($arResult['JS_DATA'])?>,
				locations: <?=CUtil::PhpToJSObject($arResult['LOCATIONS'])?>,
				params: <?=CUtil::PhpToJSObject($arParams)?>,
				signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
				siteID: '<?=CUtil::JSEscape($component->getSiteId())?>',
				ajaxUrl: '<?=CUtil::JSEscape($component->getPath() . '/ajax.php')?>',
				templateFolder: '<?=CUtil::JSEscape($templateFolder)?>',
				propertyValidation: true,
				showWarnings: true,
				orderBlockId: 'bx-soa-order',
				authBlockId: 'bx-soa-auth',
				basketBlockId: 'bx-soa-basket',
				regionBlockId: 'bx-soa-region',
				paySystemBlockId: 'bx-soa-paysystem',
				deliveryBlockId: 'bx-soa-delivery',
				pickUpBlockId: 'bx-soa-pickup',
				propsBlockId: 'bx-soa-properties',
				totalBlockId: 'bx-soa-total'
			});

			$(".nav_tabs_view button").click(function () {
				$(".nav_tabs_view button").removeClass('active');
				$(this).addClass('active');
				if ($(this).data('param_value') == 'map') {
					$("#map").show();
					$("#pharm_list").hide();
				}
				if ($(this).data('param_value') == 'list') {
					$("#pharm_list").show();
					$("#map").hide();
				}
			});

			var deliveryBtn = $("#delivery_btn");

			deliveryBtn.click(function () {
				$("#checkout__map-store").hide();
				$(".checkout__time").show();
				$("#checkout__delivery-address").show();
				$("#checkout__delivery").show();
				$(".not_available .not_available-desc, .not_available .not_available-title").hide();
				$("#bx-soa-delivery li").first().click();
			});

			var pickupBtn = $("#pickup_btn");

			pickupBtn.click(function () {
				$("#checkout__map-store").show();
				$(".checkout__time").hide();
				$("#checkout__delivery-address").hide();
				$(".not_available .not_available-desc, .not_available .not_available-title").show();
				$("#checkout__delivery").hide();
			});

			$("#confirmSmsCodeBtn").click(function () {
				if ($("#soa-property-3").val().length >= 17) {
					BX.ajax({
						url: '/ajax/confirmPhone.php',
						data: {
							phone: $("#soa-property-3").val(),
							email: $("#soa-property-2").val(),
							action: "send"
						},
						method: 'POST',
						dataType: 'json',
						timeout: 20,
						onsuccess: function (res) {
							console.log(res)
							$("#confirmSmsCodeBtn").hide();
							$("#confirmSmsCodeInput").show();
							$("#soa-property-3").removeClass("error");
							$("#soa-property-3").prop('readonly', true);
						},
						onfailure: e => {
							console.error(e)
						}
					});
				} else {
					$("#soa-property-3").addClass("error");
				}
			});

			$("#SMS_CODE").on('propertychange input', function () {

				function getRndInteger(min, max) {
					return Math.floor(Math.random() * (max - min + 1) ) + min;
				}

				if ($("#SMS_CODE").val().length >= 4) {
					$("#SMS_CODE").val($("#SMS_CODE").val().substr(0, 4));

					const emailInput = $("#soa-property-2").val();
					if (!emailInput){
						var userLogin = "user" + getRndInteger(1001, 100001);
						var tempMail = userLogin + "@pharmateca24.ru";
						var hiddenInput = '<input type="hidden" name="ORDER_PROP_2" value="'+tempMail+'">';
						$("#bx-soa-order-form").append(hiddenInput);
					}

					BX.ajax({
						url: '/ajax/confirmPhone.php',
						data: {
							phone: $("#soa-property-3").val(),
							code: $("#SMS_CODE").val(),
							email: $("#soa-property-2").val(),
							action: "confirm",
						},
						method: 'POST',
						dataType: 'json',
						timeout: 20,
						onsuccess: function (res) {

							if (res.error) {
								$("#SMS_CODE").addClass("error");
								$("#SMS_CODE").removeClass("success");
							}
							if (res.success) {
								$("#SMS_CODE").addClass("success");
								$("#SMS_CODE").removeClass("error");
								$("#SMS_CODE").prop('disabled', true);
								$("#soa-property-3").prop('readonly', true);
							}
						},
						onfailure: e => {
							console.error(e)
						}
					});
				}
			});

			if ($("#time_btn_0").hasClass('active')) {
				var hour = <?=$arResult["DAY_DELIVERY"]["HOUR"]?>;
				if (hour >= 12) {
					$("#time_btn_0").parent().hide();
					$("#time_btn_1").click();
				}
				if (hour >= 17) {
					$("#time_btn_1").parent().hide();
					$("#time_btn_2").click();
				}
				if (hour >= 21) {
					$("#time_btn_2").parent().hide();
				}
			}

			$(".js_checkout_date_btn").click(function () {
				var Id = $(this).attr('id');
				var hour = <?=$arResult["DAY_DELIVERY"]["HOUR"]?>;
				if (Id == "date_btn_0") {
					console.log(hour);
					console.log($("#time_btn_0").parent());
					if (hour >= 12) {
						$("#time_btn_0").parent().hide();
						$("#time_btn_1").click();
					}
					if (hour >= 17) {
						$("#time_btn_1").parent().hide();
						$("#time_btn_2").click();
					}
					if (hour >= 21) {
						$("#time_btn_2").parent().hide();
					}
				} else {
					$("#time_btn_0").parent().show();
					$("#time_btn_1").parent().show();
					$("#time_btn_2").parent().show();
				}
			})

		</script>
	<? }