<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Catalog;
/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */
//d($_COOKIE);
global $USER;
$arStore = Catalog\StoreTable::getList([
    'select' => ['*'],
    'filter' => ['ACTIVE' => 'Y']
]);
while ($store = $arStore->Fetch()) {
    $store["HASALL"] = false;
    $arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CAT_STORE", $store["ID"]);
    foreach ($arUserFields as $userField){
        $store[$userField["FIELD_NAME"]] = $userField["VALUE"];
    }
    foreach ($arResult["BASKET_ITEMS"] as $product){
        $rsStore = CCatalogStoreProduct::GetList(array(), array('STORE_ID' => $store["ID"], 'PRODUCT_ID' => $product["PRODUCT_XML_ID"]), false, false, array('AMOUNT'));
        if ($ar = $rsStore->Fetch())
        {
            if($ar['AMOUNT'] > 0){
                $store["HASALL"] = true;
            }else{
                $store["HASALL"] = false;
                $store["NOT_AVAILABLE"][] = $product["NAME"];
            }
        }else{
            $store["HASALL"] = false;
            $store["NOT_AVAILABLE"][] = $product["NAME"];
        }
    }
    $arResult["STORES"][$store["ID"]] = $store;
}

$objDateTime = new DateTime();
$dateTime = \Bitrix\Main\Type\DateTime::createFromPhp($objDateTime);
$hour = $objDateTime->format("H");

if($hour < 21){
    $arResult["DAY_DELIVERY"]["TEXT"] = "Сегодня";
}else{
    $arResult["DAY_DELIVERY"]["TEXT"] = "Завтра";
}
$arResult["DAY_DELIVERY"]["HOUR"] = $hour;

for ($i = 0; $i < 7; $i++) {
    if($i == 0 && $hour >= 21){
        continue;
    }
    if($i == 0){
        $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "Сегодня";
    }
    if($i == 1){
        $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "Завтра";
    }
    if($i > 1){
        switch ($objDateTime->format("N")+$i) {
            case 1:
                $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "ПН";
                break;
            case 2:
                $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "ВТ";
                break;
            case 3:
                $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "СР";
                break;
            case 4:
                $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "ЧТ";
                break;
            case 5:
                $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "ПТ";
                break;
            case 6:
                $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "СБ";
                break;
            case 7:
                $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DAY_OF_WEAK"] = "ВС";
                break;
        }
    }
    if($i > 0){
        $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DATE"] = FormatDate("d F", MakeTimeStamp($dateTime->add("1D")->toString()));
    }else{
        $arResult["DAY_DELIVERY"]["ITEMS"][$i]["DATE"] = FormatDate("d F", MakeTimeStamp($dateTime->toString()));
    }
}

    $arResult["TIME_DELIVERY"][0]["TEXT"] = "Утро";
    $arResult["TIME_DELIVERY"][0]["TIME"] = "08:00-12:00";
if($hour > 12){
    $arResult["TIME_DELIVERY"][0]["ACTIVE"] = "N";
}
    $arResult["TIME_DELIVERY"][1]["TEXT"] = "День";
    $arResult["TIME_DELIVERY"][1]["TIME"] = "12:00-17:00";
if($hour > 17){
    $arResult["TIME_DELIVERY"][0]["ACTIVE"] = "N";
}
    $arResult["TIME_DELIVERY"][2]["TEXT"] = "Вечер";
    $arResult["TIME_DELIVERY"][2]["TIME"] = "17:00-21:00";
if($hour > 21){
    $arResult["TIME_DELIVERY"][0]["ACTIVE"] = "N";
}

/*
$db_sales = CSaleOrderUserProps::GetList(
    array("DATE_UPDATE" => "DESC"),
    array("USER_ID" => $USER->GetID())
);

while ($ar_sales = $db_sales->Fetch())
{
    $arResult["USER_PROFILE"][$ar_sales["ID"]] = $ar_sales;
    $db_propVals = CSaleOrderUserPropsValue::GetList(array(), Array("USER_PROPS_ID"=>$ar_sales['ID']));
    while ($arPropVals = $db_propVals->Fetch())
    {
        $arResult["USER_PROFILE"][$ar_sales["ID"]]["PROPS"][$arPropVals["ORDER_PROPS_ID"]] = $arPropVals;
    }
}*/

	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
	$arFilter = Array("IBLOCK_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_USER" => $USER->GetId());
	$res = CIBlockElement::GetList(Array('active_from'=>'desc', 'sort'=>'asc'), $arFilter, false, false, $arSelect);
	$k = 0;
	while($ob = $res->GetNextElement()) {
		$arResult['USER_PROFILE']['SAVED_ADDRESS'][$k] = $ob->GetFields();
		$arResult['USER_PROFILE']['SAVED_ADDRESS'][$k]['PROPS'] = $ob->GetProperties();
		$k++;
	}
//	d($arResult['USER_PROFILE']['SAVED_ADDRESS'] );

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);