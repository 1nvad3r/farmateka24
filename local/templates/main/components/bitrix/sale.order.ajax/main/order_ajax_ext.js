(function () {
    'use strict';

    var editOrderParent = BX.Sale.OrderAjaxComponent.editOrder;
    var bindEventsParent = BX.Sale.OrderAjaxComponent.bindEvents;
    var initParent = BX.Sale.OrderAjaxComponent.init;

    BX.namespace('BX.Sale.OrderAjaxComponentExt');

    BX.Sale.OrderAjaxComponentExt = BX.Sale.OrderAjaxComponent;

    BX.Sale.OrderAjaxComponentExt.init = function (parameters) {
        initParent.apply(this, arguments);

        var deliveryArr = this.orderBlockNode.querySelectorAll('.cart_delivery_item');
        for (var i = 0; i < deliveryArr.length; ++i) {
            BX.bind(deliveryArr[i], 'click', BX.proxy(this.selectDelivery, this));
        }
    };

    BX.Sale.OrderAjaxComponentExt.editOrder = function (parameters) {
        editOrderParent.apply(this, arguments);

        this.orderSaveBlockNode.style.display = 'block';
    };

    BX.Sale.OrderAjaxComponentExt.bindEvents = function (parameters) {
        bindEventsParent.apply(this, arguments);

        BX.bind(this.totalBlockNode.querySelector('[data-save-button]'), 'click', BX.proxy(this.clickOrderSaveAction, this));
    };

    BX.Sale.OrderAjaxComponentExt.getSelectedDelivery = function (parameters) {

        var deliveryCheckbox = this.deliveryBlockNode.querySelector('.cart_delivery_item.active'),
            currentDelivery = false,
            deliveryId, i;

        if (!deliveryCheckbox)
            deliveryCheckbox = this.deliveryBlockNode.querySelector('.cart_delivery_item.active');

        if (!deliveryCheckbox)
            deliveryCheckbox = this.deliveryBlockNode.querySelector('.cart_delivery_item');

        if (deliveryCheckbox)
        {
            deliveryId = deliveryCheckbox.dataset.value;

            for (i in this.result.DELIVERY)
            {
                if (this.result.DELIVERY[i].ID == deliveryId)
                {
                    currentDelivery = this.result.DELIVERY[i];
                    break;
                }
            }
        }

        return currentDelivery;
    };

    BX.Sale.OrderAjaxComponentExt.totalBlockFixFont = function (parameters) {
    };
    BX.Sale.OrderAjaxComponentExt.locationsCompletion = function (parameters) {
    };
    BX.Sale.OrderAjaxComponentExt.checkNotifications = function (parameters) {
    };

    BX.Sale.OrderAjaxComponentExt.getSelectedPaySystem = function (parameters) {
        var paySystemCheckbox = this.paySystemBlockNode.querySelector('input[type=radio][name=PAY_SYSTEM_ID]:checked'),
            currentPaySystem = null, paySystemId, i;

        if (!paySystemCheckbox)
            paySystemCheckbox = this.paySystemHiddenBlockNode.querySelector('input[type=radio][name=PAY_SYSTEM_ID]:checked');

        if (!paySystemCheckbox)
            paySystemCheckbox = this.paySystemHiddenBlockNode.querySelector('input[type=hidden][name=PAY_SYSTEM_ID]');

        if (paySystemCheckbox)
        {
            paySystemId = paySystemCheckbox.value;

            for (i = 0; i < this.result.PAY_SYSTEM.length; i++)
            {
                if (this.result.PAY_SYSTEM[i].ID == paySystemId)
                {
                    currentPaySystem = this.result.PAY_SYSTEM[i];
                    break;
                }
            }
        }

        return currentPaySystem;
    };
})();