<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (!empty($arResult['ITEMS'])):?>
<section class="slider">
    <div class="container">
        <div class="slider__wrap">
            <h2 class="slider__title title">Недавно просмотренные</h2>
            <div class="slider__inner">
                <div class="slider__swiper swiper">
                    <div class="swiper-wrapper">

                        <?foreach ($arResult as $item):?>
                        <div class="swiper-slide">
                            <div class="card">
                                <a class="card__img" href="#">
                                    <img src="images/card/card-img-8.png" alt="Аминокислотный препорат">
                                    <div class="card__labels">
                                        <span style="background-color: #858784;">Для глаз</span>
                                    </div>
                                </a>
                                <a class="card__title" href="#">Аминокислотный препорат</a>
                                <div class="card__brand">
                                    Бренд:
                                    <a href="#">XR47</a>
                                </div>
                                <div class="card__bottom">
                                    <div class="card__price">1 040 ₽</div>
                                    <button class="card__heart js_card_heart" type="button">
                                        <svg width="33" height="28">
                                            <use xlink:href="images/sprite.svg#card-heart"></use>
                                        </svg>
                                        <svg width="33" height="28">
                                            <use xlink:href="images/sprite.svg#card-heart-filled"></use>
                                        </svg>
                                        <span class="card__heart-description">
                                                    добавить в избранное
                                                </span>

                                    </button>
                                </div>
                                <div class="card__buttons">
                                    <div class="card__btn card__btn-1 btn btn--green js_btn_popup"
                                         data-btn="added-to-card">
                                                <span>
                                                    В корзину
                                                    <svg width="27" height="22">
                                                        <use xlink:href="images/sprite.svg#card-cart"></use>
                                                    </svg>
                                                </span>
                                    </div>
                                    <div class="card__btn card__btn-2 btn btn--green js_btn_popup"
                                         data-btn="report">
                                        Сообщить о поступлении
                                    </div>
                                    <div class="card__btn card__btn-3 btn btn--green">
                                        Вы узнаете о поступлении
                                    </div>
                                    <div class="card__btn card__btn-4 btn btn--green js_btn_popup"
                                         data-btn="order">
                                        Заказать
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?endforeach;?>

                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
</section>
<?endif;?>