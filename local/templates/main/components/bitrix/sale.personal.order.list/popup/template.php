<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>

<?php if (count($arResult["ORDERS"]) > 0): ?>
			<? foreach ($arResult["ORDERS"] as $key => $order): ?>

		<!-- ПОП-АП 'ОЦЕНИТЕ КАЧЕСТВО ЗАКАЗА Доставка' -->
		<div class="popup popup--rate-an-order js_popup js_popup_rate_an_order_delivery"
			 data-popup="rate-an-order-delivery-<?= $order['ORDER']['ID'] ?>">
			<div class="popup__wrap">
				<button class="popup__exit js_popup_exit" type="button">
					<svg width="24" height="24">
						<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#exit"></use>
					</svg>
				</button>
				<form class="popup__form js_popup_form js_popup_report_form">
					<div class="popup__title">Оцените качество заказа</div>

					<div class="popup__order-wrapper">
						<ul class="my-orders__table-titles-list">
							<li class="my-orders__table-title my-orders__table-title--1">
								Номер заказа
							</li>
							<li class="my-orders__table-title  my-orders__table-title--2">
								Дата заказа
							</li>
							<li class="my-orders__table-title  my-orders__table-title--3">
								Статус заказа
							</li>
							<li class="my-orders__table-title  my-orders__table-title--4">
								Товары в заказе
							</li>
						</ul>

						<ul class="my-orders__list">
							<li class="my-orders__item">
								<a class="my-orders__number" href="#">
									№<?= $order["ORDER"]["ACCOUNT_NUMBER"] ?>
								</a>

								<span class="my-orders__date">
                                от <?= $order["ORDER"]["DATE_INSERT_FORMATED"] ?>
                            </span>

								<div class="my-orders__status-wrapper">
									<!-- для смены цвета добавь my-orders__status--tomato / --green / --salad / --yellow -->
									<?
										if ($order["ORDER"]["STATUS_ID"] == "N")
											$color = 'yellow';
										if ($order["ORDER"]["STATUS_ID"] == "CN")
											$color = 'tomato';
										if ($order["ORDER"]["STATUS_ID"] == "RD")
											$color = 'green';
										if ($order["ORDER"]["STATUS_ID"] == "F")
											$color = 'salad';
									?>
									<span class="my-orders__status my-orders__status--<?= $color ?>">
										<? if ($order["ORDER"]["STATUS_ID"] === 'RD'): ?>
											Готов к выдаче
										<? else: ?>
											<?= $arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"] ?>
										<? endif; ?>
									</span>
								</div>


								<ul class="my-orders__product-list">
									<? foreach ($order["BASKET_ITEMS"] as $key => $product): ?>
										<li class="my-orders__product">
											<?= $product["NAME"] ?>
											<?
												$product['DISCOUNT_PRICE'] ? $discount = $product['DISCOUNT_PRICE'] : $discount = '';
											?>
										</li>
									<? endforeach; ?>
								</ul>
							</li>
						</ul>
					</div>

					<div class="popup__bottom">
						<div class="popup__stars-wrapper">
							<label class="personal-data__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="personal-data__clients-title">
                                Оцените качество упаковки заказа:
                            </span>

								<!-- чтобы у пользователя была возможность менять рейтинг добавь класс js_active_rating -->
								<!-- если рейтинг статичен то просто впиши значение в data-total-value="5" -->
								<div class="product__rating rating js_validation_rating_1 js_active_rating"
									 data-total-value="">
									<div class="rating__item js_rating_item" data-item-value="5"></div>
									<div class="rating__item js_rating_item" data-item-value="4"></div>
									<div class="rating__item js_rating_item" data-item-value="3"></div>
									<div class="rating__item js_rating_item" data-item-value="2"></div>
									<div class="rating__item js_rating_item" data-item-value="1"></div>
								</div>
							</label>

							<label class="personal-data__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="personal-data__clients-title">
                                Оцените качество работы курьера:
                            </span>

								<!-- чтобы у пользователя была возможность менять рейтинг добавь класс js_active_rating -->
								<!-- если рейтинг статичен то просто впиши значение в data-total-value="5" -->
								<div class="product__rating rating js_validation_rating_2 js_active_rating"
									 data-total-value="">
									<div class="rating__item js_rating_item" data-item-value="5"></div>
									<div class="rating__item js_rating_item" data-item-value="4"></div>
									<div class="rating__item js_rating_item" data-item-value="3"></div>
									<div class="rating__item js_rating_item" data-item-value="2"></div>
									<div class="rating__item js_rating_item" data-item-value="1"></div>
								</div>
							</label>

							<label class="personal-data__client-label" for="">
								<!-- если поле обязательно добавть класс required - добавится звездочка -->
								<span class="personal-data__clients-title">
                                Оцените скорость доставки:
                            </span>

								<!-- чтобы у пользователя была возможность менять рейтинг добавь класс js_active_rating -->
								<!-- если рейтинг статичен то просто впиши значение в data-total-value="5" -->
								<div class="product__rating rating js_validation_rating_3 js_active_rating"
									 data-total-value="">
									<div class="rating__item js_rating_item" data-item-value="5"></div>
									<div class="rating__item js_rating_item" data-item-value="4"></div>
									<div class="rating__item js_rating_item" data-item-value="3"></div>
									<div class="rating__item js_rating_item" data-item-value="2"></div>
									<div class="rating__item js_rating_item" data-item-value="1"></div>
								</div>
							</label>
						</div>

						<label class="popup__review-label--textarea" for="">
                        <span class="popup__review-label-text">
                            Ваш отзыв
                        </span>

							<textarea class="popup__review-textarea js_write_a_review_input" name="" id="" cols="30"
									  rows="10"></textarea>

							<span class="popup__review-description">
                            500 символов
                        </span>
						</label>
					</div>


					<button class="popup__btn btn btn--tr
                js_write_a_review_add">Опубликовать</button>
				</form>
			</div>
		</div>

				<li class="my-orders__item order__item-<?= $order['ORDER']['ID'] ?>">
					<a class="my-orders__number" href="<?= $order["ORDER"]["URL_TO_DETAIL"] ?>">
						№<?= $order["ORDER"]["ACCOUNT_NUMBER"] ?>
					</a>

					<span class="my-orders__date">
						от <?= $order["ORDER"]["DATE_INSERT_FORMATED"] ?>
					</span>

					<div class="my-orders__status-wrapper">
						<!-- для смены цвета добавь my-orders__status--tomato / --green / --salad / --yellow -->
						<?
							if ($order["ORDER"]["STATUS_ID"] == "N")
								$color = 'yellow';
							if ($order["ORDER"]["STATUS_ID"] == "CN")
								$color = 'tomato';
							if ($order["ORDER"]["STATUS_ID"] == "RD")
								$color = 'green';
							if ($order["ORDER"]["STATUS_ID"] == "F")
								$color = 'salad';
						?>
						<span class="my-orders__status my-orders__status--<?= $color ?>">
								<? if ($order["ORDER"]["STATUS_ID"] === 'RD'): ?>
									Готов к выдаче
								<? else: ?>
									<?= $arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"] ?>
								<? endif; ?>
                            </span>
					</div>

					<ul class="my-orders__product-list">
						<? foreach ($order["BASKET_ITEMS"] as $key => $product): ?>
							<li class="my-orders__product">
								<?= $product["NAME"] ?>
								<?
									$product['DISCOUNT_PRICE'] ? $discount = $product['DISCOUNT_PRICE'] : $discount = '';
								?>
							</li>
						<? endforeach; ?>
					</ul>

					<div class="my-orders__price-wrapper">
                            <span class="my-orders__price">
                                <?= $order["ORDER"]["FORMATED_PRICE"] ?>
                            </span>
						<? // if ($order["ORDER"]["DISCOUNT_VALUE"] > 0): ?>
						<div class="my-orders__price-bottom">
                                <span class="my-orders__sale">
									<?php
										$discount = false;
										$oldPrice = false;
									?>
									<? foreach ($order["BASKET_ITEMS"] as $key => $product): ?>
										<?
										$product['DISCOUNT_PRICE'] ? $discount += intval($product['DISCOUNT_PRICE'] * intval($product["QUANTITY"])) : $discount = false;
										$oldPrice += intval($product['BASE_PRICE']) * intval($product["QUANTITY"]);
										?>
									<? endforeach; ?>
									<?= CurrencyFormat($discount, "RUB") ?>
                                </span>

							<span class="my-orders__old-price">
                                    <? echo CurrencyFormat($oldPrice + $order["ORDER"]["PRICE_DELIVERY"], "RUB"); ?>
								</span>
						</div>
						<? // file_get_contents($arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber);// endif; ?>
						<?php
							foreach ($order['PAYMENT'] as $payment) {
								if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y') {
									//d($payment);

									$orderAccountNumber = urlencode(urlencode($order["ORDER"]["ACCOUNT_NUMBER"]));
									$paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
									$url = $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber;

									?>
									<a class="btn btn--green js-btn-payment" onclick="window.open(<?= $url; ?>, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');"
									   href="<?= $arParams["PATH_TO_PAYMENT"] ?>?ORDER_ID=<?= $orderAccountNumber ?>&PAYMENT_ID=<?= $paymentAccountNumber ?>">Оплатить</a>
									<?
									// Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber));
								}
							}
						?>
					</div>

					<button class="my-orders__rate-btn js_btn_popup" data-btn="rate-an-order-delivery" type="button">
						Оценить заказ
					</button>
					<div class="slide--down-payment"></div>
				</li>
			<? endforeach; ?>

		<? echo $arResult["NAV_STRING"]; ?>

<?php else: ?>

<?php endif; ?>
