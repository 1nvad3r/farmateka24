<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<style type="text/css">
    .my-orders__price-wrapper .btn {
        padding: 0px 24px;
        position: absolute;
        bottom: 85px;
        max-height: 50px;
        right: 50px;
        line-height: 50px;
        font-size: 18px;
        font-weight: 600;
    }
</style>
<?php if (count($arResult["ORDERS"]) > 0): ?>
	<div class="container">
		<div class="my-orders__top-row">
			<ul class="my-orders__brn-list">
				<li class="my-orders__btn-item">
					<button class="my-orders__tag-btn my-orders__tag-btn--tomato active js_my_order_tag_btn"
							type="button" data-order_color="tomato">
                                <span class="my-orders__btn-span">
                                    Аннулирован
                                </span>

						<span class="my-orders__btn-off"></span>
					</button>
				</li>

				<li class="my-orders__btn-item">
					<button class="my-orders__tag-btn my-orders__tag-btn--green active js_my_order_tag_btn"
							type="button" data-order_color="green">
                                <span class="my-orders__btn-span">
                                    Готов к выдаче
                                </span>

						<span class="my-orders__btn-off"></span>
					</button>
				</li>

				<li class="my-orders__btn-item">
					<button class="my-orders__tag-btn my-orders__tag-btn--salad active js_my_order_tag_btn"
							type="button" data-order_color="salad">
                                <span class="my-orders__btn-span">
                                    Доставлен
                                </span>

						<span class="my-orders__btn-off"></span>
					</button>
				</li>

				<li class="my-orders__btn-item">
					<button class="my-orders__tag-btn my-orders__tag-btn--yellow active js_my_order_tag_btn"
							type="button" data-order_color="yellow">
                                <span class="my-orders__btn-span">
                                    Принят в работу
                                </span>

						<span class="my-orders__btn-off"></span>
					</button>
				</li>
			</ul>

			<form class="my-orders__search-form" action="">
				<input class="my-orders__search-input" name="order_id" type="text" value="<?= $_REQUEST['order_id'] ?>"
					   placeholder="Поиск по номеру заказа">
				<button class="my-orders__search-btn" type="submit"></button>
			</form>
		</div>

		<ul class="my-orders__table-titles-list">
			<li class="my-orders__table-title my-orders__table-title--1">
				Мои заказы
			</li>
			<li class="my-orders__table-title  my-orders__table-title--2">
				Дата заказа
			</li>
			<li class="my-orders__table-title  my-orders__table-title--3">
				Статус заказа
			</li>
			<li class="my-orders__table-title  my-orders__table-title--4">
				Товары в заказе
			</li>
			<li class="my-orders__table-title  my-orders__table-title--5">
				Сумма заказа
			</li>
		</ul>

		<ul class="my-orders__list">
			<? foreach ($arResult["ORDERS"] as $key => $order): ?>

				<li class="my-orders__item order__item-<?= $order['ORDER']['ID'] ?>">
					<a class="my-orders__number" href="<?= $order["ORDER"]["URL_TO_DETAIL"] ?>">
						№<?= $order["ORDER"]["ACCOUNT_NUMBER"] ?>
					</a>

					<span class="my-orders__date">
						от <?= $order["ORDER"]["DATE_INSERT_FORMATED"] ?>
					</span>

					<div class="my-orders__status-wrapper">
						<!-- для смены цвета добавь my-orders__status--tomato / --green / --salad / --yellow -->
						<?
							if ($order["ORDER"]["STATUS_ID"] == "N")
								$color = 'yellow';
							if ($order["ORDER"]["STATUS_ID"] == "CN")
								$color = 'tomato';
							if ($order["ORDER"]["STATUS_ID"] == "RD")
								$color = 'green';
							if ($order["ORDER"]["STATUS_ID"] == "F")
								$color = 'salad';
						?>
						<span class="my-orders__status my-orders__status--<?= $color ?>">
								<? if ($order["ORDER"]["STATUS_ID"] === 'RD'): ?>
									Готов к выдаче
								<? else: ?>
									<?= $arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"] ?>
								<? endif; ?>
                            </span>
					</div>

					<ul class="my-orders__product-list">
						<? foreach ($order["BASKET_ITEMS"] as $key => $product): ?>
							<li class="my-orders__product">
								<?= $product["NAME"] ?>
								<?
									$product['DISCOUNT_PRICE'] ? $discount = $product['DISCOUNT_PRICE'] : $discount = '';
								?>
							</li>
						<? endforeach; ?>
					</ul>

					<div class="my-orders__price-wrapper">
                            <span class="my-orders__price">
                                <?= $order["ORDER"]["FORMATED_PRICE"] ?>
                            </span>
						<? // if ($order["ORDER"]["DISCOUNT_VALUE"] > 0): ?>
						<div class="my-orders__price-bottom">
                                <span class="my-orders__sale">
									<?php
										$discount = false;
										$oldPrice = false;
									?>
									<? foreach ($order["BASKET_ITEMS"] as $key => $product): ?>
										<?
										$product['DISCOUNT_PRICE'] ? $discount += intval($product['DISCOUNT_PRICE'] * intval($product["QUANTITY"])) : $discount = false;
										$oldPrice += intval($product['BASE_PRICE']) * intval($product["QUANTITY"]);
										?>
									<? endforeach; ?>
									<?= CurrencyFormat($discount, "RUB") ?>
                                </span>

							<span class="my-orders__old-price">
                                    <? echo CurrencyFormat($oldPrice + $order["ORDER"]["PRICE_DELIVERY"], "RUB"); ?>
								</span>
						</div>
						<? // file_get_contents($arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber);// endif; ?>
						<?php
							foreach ($order['PAYMENT'] as $payment) {
								if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y') {
									//d($payment);

									$orderAccountNumber = urlencode(urlencode($order["ORDER"]["ACCOUNT_NUMBER"]));
									$paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
									$url = $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber;

									?>
									<a class="btn btn--green js-btn-payment" onclick="window.open(<?= $url; ?>, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');"
									   href="<?= $arParams["PATH_TO_PAYMENT"] ?>?ORDER_ID=<?= $orderAccountNumber ?>&PAYMENT_ID=<?= $paymentAccountNumber ?>">Оплатить</a>
									<?
									// Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber));
								}
							}
						?>
					</div>

					<button class="my-orders__rate-btn js_btn_popup" data-btn="rate-an-order-delivery-<?= $order['ORDER']['ID'] ?>" type="button">
						Оценить заказ
					</button>
					<div class="slide--down-payment"></div>
				</li>
			<? endforeach; ?>
		</ul>
		<? echo $arResult["NAV_STRING"]; ?>
	</div>
<?php else: ?>
	<div class="container">
		<div class="my-orders__empty-row active">
			<h2 class="my-orders__empty-title">
				Список заказов пока пуст.
			</h2>

			<div class="my-orders__empty-text-wrapper">
				<p class="my-orders__empty-text">
					Здесь вы можете отслеживать и поторить свои заказы
				</p>
			</div>

			<a class="my-orders__empty-btn" href="/catalog/">
				Перейти в каталог
			</a>
		</div>
	</div>
<?php endif; ?>
