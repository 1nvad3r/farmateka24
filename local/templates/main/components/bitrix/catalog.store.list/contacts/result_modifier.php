<?
use \Bitrix\Catalog;
$arTabs = array();
foreach ($arResult["STORES"] as $key => $store){
    if(!in_array($store["DESCRIPTION"], $arTabs)){
        $arTabs[] = $store["DESCRIPTION"];
        $arStore = Catalog\StoreTable::getRow([
            'select' => ['EMAIL'],
            'filter' => [
                'ID' => $store["ID"],
            ]
        ]);
        $arResult["STORES"][$key]["EMAIL"] = $arStore["EMAIL"];
    }
}
$arResult["TABS"] = $arTabs;
?>