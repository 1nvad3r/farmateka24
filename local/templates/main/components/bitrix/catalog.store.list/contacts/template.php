<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="store store__contacts-page">
    <div class="container">
        <h2 class="store__title title">Адреса аптек</h2>
        <div class="store__buttons js_store_buttons">
            <?foreach ($arResult["TABS"] as $key => $tab):?>
                <button class="store__btn btn js_store_btn" type="button" data-category="<?=$key?>"><?=$tab?></button>
            <?endforeach;?>
        </div>
        <div class="store__inner">
            <div class="store__cnt">
                <ul class="store__list js_store_list">
                    <?foreach ($arResult["STORES"] as $key => $store):?>
                        <li class="store__list-item" data-number="<?=$store["ID"]?>" data-location="<?=array_search($store["DESCRIPTION"], $arResult["TABS"]);?>">
                            <?=$store["ADDRESS"]?>

                            <?if(!empty($store["PHONE"])):?>
                            <a class="footer__contact footer__contact-phone contacts__phone store__phone"
                               href="tel:<?=$store["PHONE"]?>">
                                <svg width="20" height="20">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#contacts-phone'?>"></use>
                                </svg>
                                <?=$store["PHONE"]?>
                            </a>
                            <?endif;?>

                            <?if(!empty($store["EMAIL"])):?>
                            <a class="footer__contact footer__contact-email contacts__email store__email"
                               href="mailto:<?=$store["EMAIL"]?>">
                                <svg width="20" height="20">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#contacts-email'?>"></use>
                                </svg>
                                <?=$store["EMAIL"]?>
                            </a>
                            <?endif;?>

                        </li>
                    <?endforeach;?>
                </ul>
            </div>
            <div class="store__map" id="map"></div>
        </div>
    </div>
</section>


<script>
    if (document.querySelector('.js_store_buttons')) {

        const storeBtnsWrap = document.querySelector('.js_store_buttons');
        const storeItems = document.querySelectorAll('.js_store_list li');

        storeBtnsWrap.addEventListener('click', (e) => {

            const target = e.target;

            if (target.closest('.js_store_btn')) {

                storeBtnsWrap.querySelectorAll('.js_store_btn').forEach(btn => {
                    btn.classList.remove('active');
                });

                target.classList.add('active');

                if (target.dataset.category == 'all') {

                    storeItems.forEach(item => {
                        item.classList.remove('hidden');
                    });

                } else {
                    storeItems.forEach(item => {
                        item.classList.add('hidden');

                        if (target.dataset.category == item.dataset.location) {
                            item.classList.remove('hidden');
                        }

                    });
                }

            }

        });
    }


    if (document.querySelector('.store__map')) {

        ymaps.ready(init);

        function init() {

            let myMap = new ymaps.Map('map', {
                center: [55.752933963675126, 37.52233749962665],
                zoom: 9,
                controls: []
            });

            let collection = new ymaps.GeoObjectCollection(null, {
            });

            let collectionCoords = [
                <?foreach ($arResult["STORES"] as $key => $store):?>
                [<?=$store["GPS_N"]?>, <?=$store["GPS_S"]?>],
                <?endforeach;?>
            ];

            for (let i = 0, l = collectionCoords.length; i < l; i++) { // C помощью цикла добавляем все метки в коллекцию;
                collection.add(new ymaps.Placemark(collectionCoords[i], {
                        balloonContent: `
                    <div class="balloon">
                        <span class="balloon__time">
                            08:00-18:00 ПН-СБ
                        </span>

                        <a class="balloon__tel" href="tel:84955414585">
                            8 (495) 541-45-85
                        </a>

                        <a class="balloon__email" href="mailto:apteka@apeteka.ru">
                            apteka@apeteka.ru
                        </a>
                    </div>
                    `
                    }
                ));
                collection.get(i).properties.set('iconContent', `${i + 1}`);
            }

            myMap.geoObjects.add(collection);

            collection.options.set('iconLayout', 'default#image');
            collection.options.set('iconImageHref', '<?=SITE_TEMPLATE_PATH.'/images/mark.svg'?>');
            collection.options.set('iconImageSize', [28, 37]);
            collection.options.set('hideIcon', false);


            const storeList = document.querySelector('.store__list');

            storeList.addEventListener('click', (e) => {
                if (e.target && e.target.tagName == "LI") {
                    for (let i = 0, l = collection.getLength(); i < l; i++) {
                        if (e.target.dataset.number == collection.get(i).properties.get('iconContent')) {
                            myMap.setZoom(16);
                            myMap.setCenter(collection.get(i).geometry.getCoordinates());
                        }
                    }
                }
            });
        }
    }
</script>
