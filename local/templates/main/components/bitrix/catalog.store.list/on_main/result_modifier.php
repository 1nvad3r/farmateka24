<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	use \Bitrix\Catalog;

	$arTabs = [];
	foreach ($arResult["STORES"] as $store) {
		if (!in_array($store["DESCRIPTION"], $arTabs)) {
			$arTabs[] = $store["DESCRIPTION"];
		}
	}
	$arResult["TABS"] = $arTabs;

	$arStore = Catalog\StoreTable::getList([
		'select' => ['*'],
		'filter' => ['ACTIVE' => 'Y']
	]);

	while ($store = $arStore->Fetch()) {
		$arResult['STORES_EXT'][] = $store;
	}