<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<!-- НАШИ АПТЕКИ -->
<section class="store" id="store_list">
	<div class="container">
		<h2 class="store__title title">Наши аптеки</h2>
		<div class="store__buttons js_store_buttons">
			<? foreach ($arResult["TABS"] as $key => $tab): ?>
				<button class="store__btn btn js_store_btn" type="button"
						data-category="<?= $key ?>"><?= $tab ?></button>
			<? endforeach; ?>
			<a class="store__btn  btn  store__btn--show-all" href="<? $arParams["SHOW_ALL_LINK"] ?>">Показать все</a>
		</div>
		<div class="store__inner">
			<div class="store__cnt">
				<ul class="store__list js_store_list">
					<? foreach ($arResult["STORES"] as $key => $store): ?>
						<li data-number="<?= $store["ID"] ?>"
							data-location="<?= array_search($store["DESCRIPTION"], $arResult["TABS"]); ?>"><?= $store["ADDRESS"] ?></li>
					<? endforeach; ?>
				</ul>
			</div>
			<div class="store__map" id="map"></div>
		</div>
	</div>
</section>
<script type="text/javascript">
	if (document.querySelector('.js_store_buttons')) {
		const storeBtnsWrap = document.querySelector('.js_store_buttons');
		const storeItems = document.querySelectorAll('.js_store_list li');
		storeBtnsWrap.addEventListener('click', (e) => {
			const target = e.target;
			if (target.closest('.js_store_btn')) {
				storeBtnsWrap.querySelectorAll('.js_store_btn').forEach(btn => {
					btn.classList.remove('active');
				});
				target.classList.add('active');
				if (target.dataset.category == 'all') {
					storeItems.forEach(item => {
						item.classList.remove('hidden');
					});
				} else {
					storeItems.forEach(item => {
						item.classList.add('hidden');
						if (target.dataset.category == item.dataset.location) {
							item.classList.remove('hidden');
						}
					});
				}
			}
		});
	}

	if (document.querySelector('.store__map')) {
		const storeInfo = [
			<? foreach ($arResult["STORES_EXT"] as $key => $store):
			if (!$store["SCHEDULE"])
				$store["SCHEDULE"] = '08:00-18:00 Пн-Сб';
			?>
			{
				id: <?= $store["ID"] ?>,
				x: <?= $store["GPS_N"] ?>,
				y: <?= $store["GPS_S"] ?>,
				address: "<?= $store["ADDRESS"] ?>",
				title: "<?= $store["STORE_TITLE"] ?>",
				openDays: "<?= $store["SCHEDULE"] ?>",
				openDaysShort: "<?= $store["UF_OPENDAYS"] ?>",
				area: "<?= $store["UF_OPENDAYS_SHRT"] ?>",
				openTime: "<?= $store["UF_OPENTIME"] ?>",
				phone: "<?= formatPhone($store["PHONE"]) ?>",
				phoneForLink: "<?= formatPhone($store["PHONE"]) ?>",
				mail: "<?= $store["EMAIL"] ?>",
				isOpenAllTime: <?= $store["UF_OPENALLTIME"] ? "true" : "false" ?>,
				isAvailibleFullOrder: <?= $store["HASALL"] ? "true" : "false" ?>,
				notAvailible: [
					<? foreach ($store["NOT_AVAILABLE"] as $NOT_AVAILABLE): ?>
					"<?= $NOT_AVAILABLE ?>",
					<? endforeach; ?>
				],
			},
			<? endforeach; ?>
		];

		ymaps.ready(init);

		function init() {

			let myMap = new ymaps.Map('map', {
				center: [55.752933963675126, 37.52233749962665],
				zoom: 9,
				controls: []
			});

			let collection = new ymaps.GeoObjectCollection(null, {});

			let collectionCoords = [
				<? foreach ($arResult["STORES"] as $key => $store): ?>
				[<?= $store["GPS_N"] ?>, <?= $store["GPS_S"]?>],
				<?endforeach;?>
			];

			for (let i = 0, l = collectionCoords.length; i < l; i++) { // C помощью цикла добавляем все метки в коллекцию;

				collection.add(new ymaps.Placemark(collectionCoords[i], {
						balloonContent: `
                    <div class="balloon">
						<h3 class="balloon__address">
                            ${storeInfo[i]["address"]}
                        </h3><br>
                        <span class="balloon__time">
                            ${storeInfo[i]["openDays"]}
                        </span>
                        <a class="balloon__tel" href="tel:${storeInfo[i]["phone"]}">
                            ${storeInfo[i]["phone"]}
                        </a>
						<a class="balloon__email" href="mailto:${storeInfo[i]["mail"]}">
                            ${storeInfo[i]["mail"]}
                        </a>
                    </div>
                    `
					}
				));
				collection.get(i).properties.set('iconContent', `${i + 1}`);
			}

			myMap.geoObjects.add(collection);

			collection.options.set('iconLayout', 'default#image');
			collection.options.set('iconImageHref', '<?=SITE_TEMPLATE_PATH . '/images/mark.svg'?>');
			collection.options.set('iconImageSize', [28, 37]);
			collection.options.set('hideIcon', false);


			const storeList = document.querySelector('.store__list');

			storeList.addEventListener('click', (e) => {
				if (e.target && e.target.tagName == "LI") {
					for (let i = 0, l = collection.getLength(); i < l; i++) {
						if (e.target.dataset.number == collection.get(i).properties.get('iconContent')) {
							myMap.setZoom(16);
							myMap.setCenter(collection.get(i).geometry.getCoordinates());
						}
					}
				}
			});
		}
	}
</script>
