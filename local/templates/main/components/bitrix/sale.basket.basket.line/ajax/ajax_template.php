<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

	$this->IncludeLangFile('template.php');

	$cartId = $arParams['cartId'];
	if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0) {
		?>
		<div data-role="basket-item-list" class="bx-basket-item-list">
			<div id="<?= $cartId ?>products" class="bx-basket-item-list-container">
				<ul class="cart__product-list" id="basket-item-table">
					<? foreach ($arResult["CATEGORIES"] as $category => $items):
						if (empty($items))
							continue;
						?>
						<? foreach ($items as $v):?>
						<li class="cart__product-item">

							<? if ($v["DETAIL_PAGE_URL"]):?>
							<a href="<?= $v["DETAIL_PAGE_URL"] ?>" class="cart__img-link">
								<?php if ($v["PICTURE_SRC"]): ?>
									<img class="cart__img" src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>"></a>
								<? else: ?>
									<img class="cart__img" src="<?= SITE_TEMPLATE_PATH ?>/images/notimages.png"
										 alt="<?= $v["NAME"] ?>"></a>
								<? endif; ?>
							<? else:?>
								<img class="cart__img" src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>"/>
							<?endif ?>


							<div class="cart__name-wrapper">
								<a class="cart__product-name" href="<?= $v["DETAIL_PAGE_URL"] ?>">
									<?= $v["NAME"] ?>
								</a>

								<!--<span class="cart__sale">
									17%
								</span>

								<div class="cart__product-brend-wrapper">
									<span class="cart__product-brend-intro">
										Бренд:
									</span>

									<a class="cart__product-brend" href="#">
										Гео Органикс
									</a>
								</div>-->
							</div>

							<div class="cart__price-row">
								<div class="cart__quantity-box quantity-box js_quantity_box">
									<button class="quantity-box__btn quantity-box__btn--minus js_number_minus"
											type="button">-
									</button>

									<input class="quantity-box__input js_product_quantity_input" type="number"
										   min="0" value="<?= $v["QUANTITY"] ?>"/>

									<button class="quantity-box__btn quantity-box__btn--plus js_number_plus"
											type="button">+
									</button>
								</div>

								<div class="cart__price-wrapper">
                            <span class="cart__price">
                                <?= $v["PRICE_FMT"] ?>
                            </span>

									<div class="cart__price-old">
										<?= $v["FULL_PRICE"] ?>
									</div>
								</div>
							</div>
						</li>
					<?endforeach ?>
					<?endforeach ?>
				</ul>
			</div>
		</div>
		<?
	}else{
		?>
		<div class="category-main">
			 <div class="loader active"></div>
		</div>
		<?php
	}
?>
