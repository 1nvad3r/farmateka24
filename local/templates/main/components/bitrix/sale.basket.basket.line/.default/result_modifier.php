<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Sale;
\Bitrix\Main\Loader::includeModule('sale');

$basketItems = [];
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
foreach ($basket as $basketItem) {
    $basketItems[$basketItem->getProductId()] = $basketItem->getQuantity();         
}
$arResult['BASKET_ITEMS'] = $basketItems;