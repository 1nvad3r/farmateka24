<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="cabinet-menu__list  js_cabinet_menu_list">
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
        <li class="cabinet-menu__item">
            <a class="cabinet-menu__link <?=$arItem["SELECTED"]?"active":""?>" href="<?=$arItem["LINK"]?>">
                <?=$arItem["TEXT"]?>
            </a>
        </li>
    <?endforeach?>
    <li class="cabinet-menu__item">
        <a class="cabinet-menu__link cabinet-menu__link--exit js_btn_popup" data-btn="leave" href="/?logout=yes&<?= bitrix_sessid_get() ?>">
            Выход
        </a>
    </li>
</ul>

<button class="cabinet-menu__btn js_cabinet_menu_open_btn">
</button>
<?endif;?>