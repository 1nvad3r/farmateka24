<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="popular">
    <div class="container">
        <div class="popular__wrap">
            <h2 class="popular__title title">Популярные товары</h2>
            <div class="popular__inner js_popular_inner">

                <div class="popular__tabs">
                    <?$count=true;?>
                    <?foreach ($arResult["SECTIONS"] as $section):?>
                    <button class="popular__tab js_popular_tab btn <?=$count?'active':''?>" type="button"
                            style="background-color: <?=$section["COLOR"]?>; color: <?=$section["COLOR"]?>;">
                        <span><?=$section["NAME"]?></span>
                    </button>
                        <?$count=false;?>
                    <?endforeach;?>
                    <?$count=true;?>
                </div>

                <div class="popular__content">

                    <?foreach ($arResult["SECTIONS"] as $section):?>
                    <div class="popular__item js_popular_item <?=$count?'active':''?>">
                        <?$count=false;?>
                        <div class="popular__swiper swiper">
                            <div class="swiper-wrapper">

                                <?foreach ($section["ITEMS"] as $item):?>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <a class="card__img" href="<?=$item["DETAIL_PAGE_URL"]?>">
                                            <img src="<?=CFile::GetPath($item["PREVIEW_PICTURE"]["ID"]);?>" alt="<?=$item["NAME"]?>">
                                            <div class="card__labels">
                                                <? foreach ($item["PROPERTIES"]["SECTION"]["VALUE"] as $value):?>
                                                    <span style="background-color: <?=$arResult["SECTIONS"][$value]["COLOR"]?>;"><?=$arResult["SECTIONS"][$value]["NAME"]?></span>
                                                <?endforeach;?>
                                            </div>
                                            <?if(!empty($item["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"])):?>
                                            <div class="card__sale"><?=$item["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"]?>%</div>
                                            <?endif;?>
                                        </a>
                                        <a class="card__title" href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a>
                                        <div class="card__brand">
                                            Бренд:
                                            <a href="#"><?=$item["PROPERTIES"]["BRAND"]["VALUE"]?></a>
                                        </div>
                                        <div class="card__bottom">
                                            <?if(!empty($item["PRICES"]["BASE"]["DISCOUNT_VALUE"]) && $item["PRICES"]["BASE"]["DISCOUNT_VALUE"]!=$item["PRICES"]["BASE"]["VALUE"]):?>
                                                <div class="card__price"><?=$item["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"]?></div>
                                                <div class="card__old-price"><?=$item["PRICES"]["BASE"]["PRINT_VALUE"]?></div>
                                            <?else:?>
                                                <div class="card__price"><?=$item["PRICES"]["BASE"]["PRINT_VALUE"]?></div>
                                            <?endif;?>
                                            <button class="card__heart js_card_heart" type="button">
                                                <svg width="33" height="28">
                                                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#card-heart'?>"></use>
                                                </svg>
                                                <svg width="33" height="28">
                                                    <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#card-heart-filled'?>"></use>
                                                </svg>
                                                <span class="card__heart-description">
                                                            добавить в избранное
                                                        </span>
                                            </button>
                                        </div>
                                        <!-- чтобы показать кнопку 'Сообщить  о поступлении', нужно элементу 'card__buttons' добавить класс 'empty-1' -->
                                        <!-- чтобы показать кнопку 'Вы узнаете о поступлении', нужно элементу 'card__buttons' добавить класс 'empty-2' -->
                                        <!-- чтобы показать кнопку 'Заказать', нужно элементу 'card__buttons' добавить класс 'empty-3' -->
                                        <div class="card__buttons">
                                            <div class="card__btn card__btn-1 btn btn--green js_btn_popup"
                                                 data-btn="added-to-card">
                                                        <span>
                                                            В корзину
                                                            <svg width="27" height="22">
                                                                <use xlink:href="images/sprite.svg#card-cart"></use>
                                                            </svg>
                                                        </span>
                                            </div>
                                            <div class="card__btn card__btn-2 btn btn--green js_btn_popup"
                                                 data-btn="report">
                                                Сообщить о поступлении
                                            </div>
                                            <div class="card__btn card__btn-3 btn btn--green">
                                                Вы узнаете о поступлении
                                            </div>
                                            <div class="card__btn card__btn-4 btn btn--green js_btn_popup"
                                                 data-btn="order">
                                                Заказать
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?endforeach;?>

                            </div>
                            <div class="swiper-scrollbar"></div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>

                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>