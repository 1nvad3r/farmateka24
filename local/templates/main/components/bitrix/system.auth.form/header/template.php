<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
?>
<?php
	global $USER;
	$userName = $USER->GetFirstName();
	if (!$userName)
		$userName = 'Профиль';// $USER->GetLogin();
?>
<button class="header__btn <?= $USER->IsAuthorized() ? 'our-boy' : 'js_btn_popup'; ?>" <?= !$USER->IsAuthorized() ? 'data-btn="log-in"' : ''; ?>
		type="button">
    <span class="header__guest-btn">
        <svg width="19" height="21">
            <use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-login' ?>"></use>
        </svg>
        <svg width="17" height="15">
            <use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#header-login-mobile' ?>"></use>
        </svg>
        Вход
    </span>
	<span class="header__client-btn">
        <img width="20" height="20" src="<?= SITE_TEMPLATE_PATH . '/images/user-icon.svg' ?>" alt="иконка пользователя">
        <img width="17" height="15" src="<?= SITE_TEMPLATE_PATH . '/images/user-icon.svg' ?>" alt="иконка пользователя">
        <?= $userName//$USER->GetFirstName() ?>
    </span>
	<ul class="cabinet-menu-header">
		<?php $APPLICATION->IncludeComponent(
			"bitrix:menu",
			"personal",
			[
				"ROOT_MENU_TYPE" => "personal",
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "personal",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "360000",
				"MENU_CACHE_USE_GROUPS" => "Y",
			]
		); ?>
		<li class="cabinet-menu-header__item">
			<a class="cabinet-menu-header__link cabinet-menu-header__link--exit js_btn_popup" data-btn="leave"
			   href="/?logout=yes&<?= bitrix_sessid_get() ?>">
				Выход
			</a>
		</li>
	</ul>
</button>
<!-- ПОП-АП 'АВТОРИЗАЦИЯ ПОЧТА' -->
<div class="popup popup--log-in  js_popup js_popup_log_in" data-popup="log-in">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
			</svg>
		</button>
		<form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top" action="/auth/"
			  class="popup__form js_popup_form">
			<div class="popup__title">Вход в личный кабинет</div>
			<ul class="popup__switch-btns-list">
				<li class="popup__switch-btn-item">
					<button class="popup__switch-btn popup__switch-btn--left js_popup_log_in_sms_btn" type="button">
						Телефон
					</button>
				</li>
				<li class="popup__switch-btn-item">
					<button class="popup__switch-btn popup__switch-btn--right active" type="button">
						Email
					</button>
				</li>
			</ul>
			<input type="hidden" name="AUTH_FORM" value="Y"/>
			<input type="hidden" name="TYPE" value="AUTH"/>
			<input type="hidden" name="recaptcha_token" value="">
			<div class="popup__fields">
				<div class="popup__field field">
					<input class="js_input_email" name="USER_LOGIN" value="<?= $arResult["USER_LOGIN"] ?>" type="text"
						   placeholder="Email">
				</div>
				<div class="popup__field field">
					<input class="js_input_password" name="USER_PASSWORD" type="password" placeholder="Пароль">
				</div>
			</div>
			<button class="popup__btn btn btn--tr" type="submit" name="Login">Войти</button>
			<!-- чтобы показать ошибку добавь класс active -->
			<p class="popup__warning-text active"></p>
			<button class="popup__ask-password js_ask_password_btn" type="button">
				Я не помню пароль
			</button>
			<p class="popup__text">
				Нет личного кабинета?
				<button class="popup__registration js_popup_log_in_registration_btn" type="button" type="button">
					Зарегистрируйтесь
				</button>
			</p>
		</form>
	</div>
</div>

<!-- ПОП-АП 'АВТОРИЗАЦИЯ СМС' -->
<div class="popup popup--log-in  js_popup js_popup_log_in_sms" data-popup="log-in-sms">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Вход в личный кабинет</div>
			<ul class="popup__switch-btns-list">
				<li class="popup__switch-btn-item">
					<button class="popup__switch-btn popup__switch-btn--left active" type="button">
						Телефон
					</button>
				</li>
				<li class="popup__switch-btn-item">
					<button class="popup__switch-btn popup__switch-btn--right js_popup_log_in_email_btn"
							type="button">
						Email
					</button>
				</li>
			</ul>
			<div class="popup__fields">
				<div class="popup__field field ">
					<input class="js_input_masked_phone login_phone_field " type="text" placeholder="Телефон">
					<span class="popup__error-text">Пользователь не найден</span>
				</div>
			</div>
			<button class="popup__btn btn btn--tr" onclick="sendSMSCode();">Получить код по смс</button>
			<!-- чтобы показать ошибку добавь класс active -->
			<p class="popup__warning-text">
				Нет пользователя с таким номером телефона
			</p>
			<p class="popup__text">
				Уже есть личный кабинет?
				<button class="popup__registration js_popup_log_in_sms_registration_btn" type="button" type="button">
					Вход
				</button>
			</p>
		</form>
	</div>
</div>

<!-- ПОП-АП 'АВТОРИЗАЦИЯ СМС КОД' -->
<div class="popup popup--log-in js_popup js_popup_log_in_sms_code" data-popup="log-in-sms-code">
	<div class="popup__wrap">
		<button class="popup__exit js_popup_exit" type="button">
			<svg width="24" height="24">
				<use xlink:href="<?= SITE_TEMPLATE_PATH . '/images/sprite.svg#exit' ?>"></use>
			</svg>
		</button>
		<form class="popup__form js_popup_form js_popup_report_form">
			<div class="popup__title">Код подтверждения</div>
			<p class="popup__predescription-text">
				На указанный номер был отправлен код. Для подтверждения, введите код из sms.
			</p>
			<ul class="popup__code-list">
				<li class="popup__code-item">
					<input class="popup__code-input js_code_input " type="text" placeholder="——————" maxlength="6">
				</li>
			</ul>

			<p class="popup__timer-text js_code_time_text">
				Введите код в течение
				<span class="popup__timer-time js_code_time_span">
                        00:45
                    </span>
			</p>
			<button class="popup__get-new-code js_new_code" type="button">
				Получить новый код
			</button>
			<button class="popup__btn btn js_popup_code_btn btn--tr">Отправить</button>
		</form>
	</div>
</div>