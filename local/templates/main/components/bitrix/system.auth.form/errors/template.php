<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)    die();

if($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])

    header('Content-type: application/json');
    echo json_encode($arResult['ERROR_MESSAGE']);