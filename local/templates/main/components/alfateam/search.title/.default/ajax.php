<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (empty($arResult["CATEGORIES"]))
	return;
?>
<?php //d($arResult);?>
<div class="header__search-wrapper js_header_search_list active">
    <ul class="header__search-start-list">
        <?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
            <?foreach($arCategory["ITEMS"] as $i => $arItem):?>
            <?if($arItem["NAME"] == "остальные" || !empty($arResult["ELEMENTS"][$arItem["ITEM_ID"]]["PRICES"]))continue;?>
                <? if($arItem['PARAM1'] == 'catalog'): ?>
            <li class="header__search-start-item">
                <a class="header__search-start-link" href="<?=$arItem["URL"]?>">
                    <?=$arItem["NAME"]?>
                </a>
            </li>
            <? endif; ?>
            <?endforeach;?>
        <?endforeach;?>
    </ul>



    <?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
    <?foreach($arCategory["ITEMS"] as $i => $arItem):?>
        <?if($arItem["NAME"] == "остальные"):?>
            <a class="header__search-show-all" href="<?=$arItem["URL"]?>">
        Показать все
    </a>
            <? endif; ?>
        <?endforeach;?>
    <?endforeach;?>
</div>