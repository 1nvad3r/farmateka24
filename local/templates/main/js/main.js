window.addEventListener("DOMContentLoaded", () => {

	// сбрасываем ошибки у инпутов при вводе
	function clearInputError(input) {
		if (input) {
			input.addEventListener("input", () => {
				input.classList.remove("error");
			});
		}
	}

	// отсекает промежуточные вызовы функции поиcка
	function debounce(f, ms) {
		let isCooldown = false;

		return function () {
			if (isCooldown) return;

			f.apply(this, arguments);

			isCooldown = true;

			setTimeout(() => (isCooldown = false), ms);
		};
	}

	// маска для ввода номера телфона
	function setCursorPosition(pos, elem) {
		elem.focus();
		if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
		else if (elem.createTextRange) {
			var range = elem.createTextRange();
			range.collapse(true);
			range.moveEnd("character", pos);
			range.moveStart("character", pos);
			range.select();
		}
	}

	function mask(event) {
		var matrix = "+7 (___) ___ ____",
			i = 0,
			def = matrix.replace(/\D/g, ""),
			val = this.value.replace(/\D/g, "");
		if (def.length >= val.length) val = def;
		this.value = matrix.replace(/./g, function (a) {
			return /[_\d]/.test(a) && i < val.length
				? val.charAt(i++)
				: i >= val.length
					? ""
					: a;
		});
		if (event.type == "blur") {
			if (this.value.length == 2) this.value = "";
		} else setCursorPosition(this.value.length, this);
	}

	if (document.querySelector(".js_input_masked_phone")) {
		const allPhoneInputs = document.querySelectorAll(".js_input_masked_phone");

		// добавляем маски номеру телефона
		allPhoneInputs.forEach((phoneInput) => {
			phoneInput.addEventListener("input", mask, false);
			phoneInput.addEventListener("focus", mask, false);
			phoneInput.addEventListener("blur", mask, false);
		});
	}
	// бургер-меню

	if (document.querySelector(".js_burger")) {
		const burgers = document.querySelectorAll(".js_burger"); // находим все кнопки c бургером (в хедере и в самом меню);
		const headerTopInner = document.querySelector(".js_header_top_inner"); // блок с меню;
		const headerMenuItemCatalog = document.querySelector(
			".js_header_menu_item_catalog"
		); // ссылка в меню на мобилке, при клике на которую открывается меню каталога;
		const menuCatalog = document.querySelector(".js_menu_catalog"); // меню каталога
		const menuCatalogExit = menuCatalog.querySelector(".js_menu_catalog_exit"); // кнопка выхода из меню каталога (крестик) на мобилке;

		burgers.forEach((burger) => {
			// перебираем все кнопки;

			burger.addEventListener("click", () => {
				// при клике;

				headerTopInner.classList.toggle("active");
				// window.scrollTo(0, 0);

				if (headerTopInner.classList.contains("active")) {
					bodyFixPosition();
				} else {
					bodyUnfixPosition();
				}
			});
		});

		headerMenuItemCatalog.addEventListener("click", (e) => {
			e.preventDefault();
			menuCatalog.classList.add("active");
		});

		menuCatalogExit.addEventListener("click", () => {
			console.log("test");
			menuCatalog.classList.remove("active");
		});
	}

	if (document.querySelector(".js_header_search")) {
		const searchForm = document.querySelector(".js_header_search"); //вся форма поиска (инпут и список подсказок)
		const searchList = document.querySelector(".js_header_search_list"); //список подсказок
		const openBtn = document.querySelector(".js_header_search_btn"); //кнопки открытия
		const input = document.querySelector(".js_header_search_input"); //инпут
		const placeholder = document.querySelector(
			".js_header_search_input-placeholder"
		); //плейсхолдер

		if (input.value.length > 0) {
			placeholder.classList.add("hidden");
		} else {
			placeholder.classList.remove("hidden");
		}

		openBtn.addEventListener("click", () => {
			searchForm.classList.add("active");
		});

		input.addEventListener("input", () => {
			searchForm.classList.add("active");
			if (input.value.length > 0) {
				placeholder.classList.add("hidden");
			} else {
				placeholder.classList.remove("hidden");
			}
		});
	}

	// фиксация <body>

	function bodyFixPosition() {
		setTimeout(function () {
			//Ставим необходимую задержку, чтобы не было «конфликта» в случае, если функция фиксации вызывается сразу после расфиксации (расфиксация отменяет действия расфиксации из-за одновременного действия)

			if (!document.body.hasAttribute("data-body-scroll-fix")) {
				// Получаем позицию прокрутки
				let scrollPosition =
					window.pageYOffset || document.documentElement.scrollTop;

				// Ставим нужные стили
				document.body.setAttribute("data-body-scroll-fix", scrollPosition); // Cтавим атрибут со значением прокрутки
				document.body.style.overflow = "hidden";
				document.body.style.position = "fixed";
				document.body.style.top = "-" + scrollPosition + "px";
				document.body.style.left = "0";
				document.body.style.width = "100%";
				document.body.style.paddingRight = "8px";
			}
		}, 15);
	}

	// расфиксация <body>

	function bodyUnfixPosition() {
		if (document.body.hasAttribute("data-body-scroll-fix")) {
			// Получаем позицию прокрутки из атрибута
			let scrollPosition = document.body.getAttribute("data-body-scroll-fix");

			// Удаляем атрибут
			document.body.removeAttribute("data-body-scroll-fix");

			// Удаляем ненужные стили
			document.body.style.overflow = "";
			document.body.style.position = "";
			document.body.style.top = "";
			document.body.style.left = "";
			document.body.style.width = "";
			document.body.style.paddingRight = "";

			// Прокручиваем страницу на полученное из атрибута значение
			window.scroll(0, scrollPosition);
		}
	}

	// кнопка с иконкой 'добавить в избранное' в карточке товара

	if (document.querySelector(".js_card_heart")) {
		cardHeartBtns = document.querySelectorAll(".js_card_heart"); // находим все кнопки;

		cardHeartBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				if ($(btn).hasClass('active')) {
					deleteFavorite($(btn).data("id"));
					btn.classList.remove('active');
				} else {
					addFavorite($(btn).data("id"))
					btn.classList.add('active');
				}
			});
		});
	}

	function addFavorite(id) {

		BX.ajax({
			url: '/ajax/favorites.php',
			method: 'POST',
			data: {
				action: 'add',
				id: id
			},
			dataType: 'json',
			timeout: 30,
			async: true,
			onsuccess: function (data) {
				console.log('333');
				console.log(data);
			},
		});
	}

	function deleteFavorite(id) {
		BX.ajax({
			url: '/ajax/favorites.php',
			method: 'POST',
			data: {
				action: 'delete',
				id: id
			},
			dataType: 'json',
			timeout: 30,
			async: true,
			onsuccess: function (data) {
				console.log(data);
			},
		});
	}

	// запросы при взаимодействии с карточкой товара
	async function actionWithProductCard(type, productId) {
		try {
			// отправляем запрос
			console.log("запрос", type, productId);

			let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
				method: "PUT",
				body: JSON.stringify({
					id: 1,
					title: "foo",
					body: "bar",
					userId: 1,
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
				},
			});

			if (!resp.ok) {
				throw new Error(resp.status);
			}
		} catch (err) {
			console.log(
				"Ошибка взаимодействия с карточкой товара",
				err,
				type,
				productId
			);
		}
	}

	var requestFilter = false;

	// отправляем информацию об отмене фильтра
	async function offFilterCheckbox(id) {

		startLoader();

		const filtersList = $(document.querySelector(".js_filters_list"));
		$(filtersList).find("#" + id).prop('checked', false);
		var actionUrl = filtersList.attr('action');
		updateCatalog(actionUrl + "?" + filtersList.serialize())
	}

	async function onSortMode(data) {
		filtersList = $(document.querySelector(".js_filters_list"));
		if ($(".category-main__main-wrapper").hasClass("search")) {
			var actionUrl = '/catalog/filter/';
		} else {
			var actionUrl = filtersList.attr('action');
		}

		params = data.split("/");
		updateCatalog(actionUrl + "?" + filtersList.serialize() + "&by=" + params[0] + "&order=" + params[1])
	}

	async function onSortModeSearch(data) {

		filtersList = $(document.querySelector(".js_filters_list"));
		var actionUrl = filtersList.attr('action');
		params = data.split("/");
		updateCatalog(actionUrl + "&" + filtersList.serialize() + "&by=" + params[0] + "&order=" + params[1])
	}

	function initchosenFilterItems() {
		chosenFilterItems = document.querySelectorAll(
			".js_chosen_filter_item"
		);
		chosenFilterItems.forEach((item) => {
			item.addEventListener("click", (e) => {
				if (e.target.classList.contains("js_chosen_filter_item_delete_btn")) {
					offFilterCheckbox(item.dataset.info);
					item.remove();
				}
			});
		});
	}

	function updateCatalog(url) {
		if (!requestFilter) {
			requestFilter = true;
			startLoader();
			filtersList = $(document.querySelector(".js_filters_list"));
			try {
				BX.ajax({
					url: url,
					method: 'GET',
					dataType: 'html',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						var newList = $(data).find(".js_category_main_product-column");
						$(".js_category_main_product-column").replaceWith(newList);
						var newFilter = $(data).find(".js_chosen_filters");
						$(".js_chosen_filters").replaceWith(newFilter);
						initchosenFilterItems();
						finichLoader();
						requestFilter = false;
						btns = document.querySelectorAll(".js_pagination_btn");
						btns.forEach((btn) => {
							btn.addEventListener("click", (e) => {
								e.preventDefault();
								if (!btn.classList.contains("active")) {
									openPageOfProductCardLict(btn);
								}
							});
						});
						initSelect();
						initWrapperCatalog();
						sortProductsModeSelect = document.querySelector(
							".js_my_select_sort_products_mode"
						);
						sortProductsModeSelectSearch = document.querySelector(
							".js_my_select_sort_products_mode_search"
						);
						sortProductsModeSelect.addEventListener("change", (e) => {
							onSortMode(e.detail.value);
						});
						sortProductsModeSelectSearch.addEventListener("change", (e) => {
							console.log(e.detail.value);
							onSortModeSearch(e.detail.value);
						});
						cardHeartBtns = document.querySelectorAll(".js_card_heart"); // находим все кнопки;

						cardHeartBtns.forEach((btn) => {
							btn.addEventListener("click", () => {
								if ($(btn).hasClass('active')) {
									deleteFavorite($(btn).data("id"));
									btn.classList.remove('active');
								} else {
									addFavorite($(btn).data("id"))
									btn.classList.add('active');
								}
							});
						});
					},
				});
				BX.ajax({
					url: url + "&ajax=y",
					method: 'GET',
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						console.log(url);
						var text = "товаров";
						if (data.ELEMENT_COUNT < 5) {
							text = "товара";
						}
						if (data.ELEMENT_COUNT == 1) {
							text = "товар";
						}
						$(".category-main__subtitle").text("(" + data.ELEMENT_COUNT + " " + text + ")");
						initSelect();
						for (item in data.ITEMS) {
							var count = 0;
							for (value in data.ITEMS[item].VALUES) {
								if (data.ITEMS[item].VALUES[value].DISABLED == true) {
									var elem = $(filtersList).find("input[name='" + data.ITEMS[item].VALUES[value].CONTROL_NAME + "']");
									elem.parent().hide();
								} else {
									count++
									var elem = $(filtersList).find("input[name='" + data.ITEMS[item].VALUES[value].CONTROL_NAME + "']");
									var wraper = elem.parent().parent();
									if (!wraper.hasClass('active') && count < 5) {
										elem.parent().show();
									}
								}
							}
							if (elem) {
								var element = elem.parent().parent().parent();
								if (count == 0) {
									element.hide();
								} else {
									element.show();
									var showMore = element.find(".js_filter_more_options_btn");
									if (count <= 4) {
										showMore.hide();
									} else {
										showMore.show();
									}
								}
							}
						}
					},
				});
			} catch (err) {
				finichLoader();
				console.log("Ошибка включения чекбокса", err);
			}
		}
	}

	// запрос на смену страницы списка товаров
	async function openPageOfProductCardLict(btn) {
		try {
			updateCatalog($(btn).attr("href"))
		} catch (err) {
			console.log(
				"Ошибка взаимодействия с карточкой товара",
				err,
				type,
				productId
			);
		}
	}

	// страница Категории каталога - связь выбранных фильтров и блока "выбранные фильтры"
	if (document.querySelector(".js_filter_item")) {
		// "фильтр" - то, где ставим галочку
		// "плашка" - это елемент в списке "выбранные фильтры", который добавлятся, когда фильтр выбран
		const sortProductsModeSelect = document.querySelector(
			".js_my_select_sort_products_mode"
		);
		const sortProductsModeSelectSearch = document.querySelector(
			".js_my_select_sort_products_mode_search"
		);
		//  селект типов сортировки карточек товара
		const filterItem = document.querySelector(".js_filter_item"); // один фильтр
		// const filterPoint = document.querySelector('.js_filter_point'); // один фильтр
		const filterPoints = document.querySelectorAll(".js_filter_point"); // фильтры
		const chosenFiltersList = document.querySelector(".js_chosen_filters_list"); // список плашек
		chosenFilterItems = document.querySelectorAll(
			".js_chosen_filter_item"
		); //  плашки


		// отправляем информацию о добавлении фильтра
		async function onFilterCheckbox(id) {
			console.log('fil');

			if ($(".category-main__main-wrapper").hasClass("search")) {
				startLoader();

				const filtersList = $(document.querySelector(".js_filters_list"));
				var actionUrl = '/catalog/filter/';
				var catalogUrl = actionUrl + "?" + filtersList.serialize();
				updateCatalog(catalogUrl);
			} else {
				startLoader();

				const filtersList = $(document.querySelector(".js_filters_list"));
				var actionUrl = filtersList.attr('action');
				var catalogUrl = actionUrl + "?" + filtersList.serialize();
				updateCatalog(catalogUrl);
			}
		}

		// отправляем информацию об отмене фильтра
		async function offFilterCheckbox(id) {
			if ($(".category-main__main-wrapper").hasClass("search")) {
				startLoader();

				const filtersList = $(document.querySelector(".js_filters_list"));
				$(filtersList).find("#" + id).prop('checked', false);
				var actionUrl = '/catalog/filter/';
				var catalogUrl = actionUrl + "?" + filtersList.serialize();
				updateCatalog(catalogUrl);
			} else {
				startLoader();

				const filtersList = $(document.querySelector(".js_filters_list"));
				$(filtersList).find("#" + id).prop('checked', false);
				var actionUrl = filtersList.attr('action');
				var catalogUrl = actionUrl + "?" + filtersList.serialize();
				updateCatalog(catalogUrl);
			}
		}

		// отправляем информацию о выборе способа сортировки

		// выбор способа сортировки
		sortProductsModeSelect.addEventListener("change", (e) => {
			// console.log('change e>>', e.detail.value)
			onSortMode(e.detail.value);
		});

		// выбор способа сортировки
		// sortProductsModeSelectSearch.addEventListener("change", (e) => {
		// 	console.log('change e>>', e.detail.value)
		// 	onSortModeSearch(e.detail.value);
		// });


		// уведомляем сервер об изменении чекбоксов
		filterPoints.forEach((point) => {
			point.addEventListener("change", () => {
				if (point.checked) {
					onFilterCheckbox(point.dataset.info);
				} else {
					offFilterCheckbox(point.dataset.info);
				}
			});
		});

		// удаляем плашки
		chosenFilterItems.forEach((item) => {
			item.addEventListener("click", (e) => {
				if (e.target.classList.contains("js_chosen_filter_item_delete_btn")) {
					offFilterCheckbox(item.dataset.info);
					item.remove();
				}
			});
		});

		filterItem.addEventListener('change', () => {
			if (filterItem.checked) {
				console.log('add');
				// создаём новую плашку из шаблона
				let newChosenFilterItem = chosenFilterItemTemplate.cloneNode(true)

				// закидываем в список плашек
				chosenFiltersList.append(newChosenFilterItem);

				// находи в ней тексовое поле
				let textInNewChosenFilterItem = newChosenFilterItem.querySelector('.js_chosen_filter_item_text'); // список плашек

				textInNewChosenFilterItem.innerHTML = 'САМОЕ новое название'

				// chosenFiltersList.insertAdjacentHTML('beforeend', newChosenFilterItem);
				console.log('newChosenFilterItem >>', newChosenFilterItem);

			} else {
				console.log('delete');
				chosenFilterItemTemplate.remove()
			}
		})
	}

	// пагинация
	if (document.querySelector(".js_pagination_list")) {
		const btns = document.querySelectorAll(".js_pagination_btn");
		// const popupsOpenBtns = document.querySelectorAll('.js_btn_popup')
		btns.forEach((btn) => {
			btn.addEventListener("click", (e) => {
				e.preventDefault();
				// если страница не акивна, отправляем запрос
				if (!btn.classList.contains("active")) {
					openPageOfProductCardLict(btn);
				}
			});
		});
	}

	//кнопки переключения раскладки товаров на странице Категория каталога
	if (document.querySelector(".js_category_main_product_list")) {
		const cardsWrapper = document.querySelector(
			".js_category_main_product_list"
		);
		const listByRowsBtn = document.querySelector(
			".js_category_list_by_row_btn"
		);
		const listByTableBtn = document.querySelector(
			".js_category_list_by_table_btn"
		);

		listByRowsBtn.addEventListener("click", () => {
			listByRowsBtn.classList.add("active");
			listByTableBtn.classList.remove("active");
			cardsWrapper.classList.add("rows");
		});

		listByTableBtn.addEventListener("click", () => {
			listByTableBtn.classList.add("active");
			listByRowsBtn.classList.remove("active");
			cardsWrapper.classList.remove("rows");
		});
	}


	// закрытие поп-апа с куки
	if (document.querySelector(".js_cookie")) {
		const cookie = document.querySelector(".js_cookie"); // находим поп-ап с куки;
		const cookieBtn = cookie.querySelector(".js_cookie_btn"); // находим в поп-апе с куки кнопку закрытия;

		// запрос из попапа Куки
		async function cookieRequest() {
			setCookie('acceptCookie', 'true');
		}

		if (getCookie("acceptCookie") == "true") {
			cookie.classList.add("hidden");
		}

		cookieBtn.addEventListener("click", () => {
			// при клике на кнопку;
			cookieRequest();
			cookie.classList.add("hidden");
		});
	}


	function getCookie(name) {
		let matches = document.cookie.match(new RegExp(
			"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		));
		return matches ? decodeURIComponent(matches[1]) : undefined;
	}

	function setCookie(name, value, options = {}) {

		options = {
			path: '/',
			// при необходимости добавьте другие значения по умолчанию
			...options
		};

		if (options.expires instanceof Date) {
			options.expires = options.expires.toUTCString();
		}

		let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

		for (let optionKey in options) {
			updatedCookie += "; " + optionKey;
			let optionValue = options[optionKey];
			if (optionValue !== true) {
				updatedCookie += "=" + optionValue;
			}
		}

		document.cookie = updatedCookie;
	}

	// <!-- ПОП-АП 'Подтвердить город' -->
	if (document.querySelector(".js_popup_accept_city")) {
		const popup = document.querySelector(".js_popup_accept_city");
		const popupChangeCity = document.querySelector(".js_popup_change_city"); //поп-ап 'Выбрать город'
		const citySpan = document.querySelector(".js_popup_accept_city_span");
		const openBtn = document.querySelector(".js_popup_accept_city_open_btn");
		const okBtn = document.querySelector(".js_popup_accept_city_ok_btn");
		const changeBtn = document.querySelector(
			".js_popup_accept_city_change_btn"
		);
		const closeBtn = document.querySelector(".js_popup_accept_city_close_btn");

		// запрос из попапа Подтвердить город
		async function sendRequest() {
			console.log("запрос из попапа Подтвердить город >>");
			// cookieBtn.classList.add('isLoading');
			try {
				BX.ajax.runComponentAction('alfateam:user.location',
					'saveLocation', {
						mode: 'class',
						data: {location: $(citySpan).text()},
					})
					.then(function (response) {
						if (response.status == 'success') {
							if (response.data == $(citySpan).text()) {
								document.cookie = "LOCATION=" + $(citySpan).text();
								location.reload();
							}
						}
					});
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log("Ошибка запроса из попапа Подтвердить город", err);
			}
		}

		openBtn.addEventListener("click", () => {
			popup.classList.add("active");
			// обнавляем значение города
			citySpan.innerHTML = openBtn.innerHTML;
		});

		okBtn.addEventListener("click", () => {
			sendRequest();
			console.log('adf');
			var cityName = $('.js_popup_accept_city_span').text();
			$('.js_popup_change_city_target_span').text($.trim(cityName));
			popup.classList.remove("active");
		});

		closeBtn.addEventListener("click", () => {
			popup.classList.remove("active");
		});

		changeBtn.addEventListener("click", () => {
			popup.classList.remove("active");
			popupChangeCity.classList.add("active");
		});
	}


	// <!-- ПОП-АП 'Выбрать город' -->
	if (document.querySelector(".js_popup_change_city")) {
		const popup = document.querySelector(".js_popup_change_city");
		const targetSpan = document.querySelector(
			".js_popup_change_city_target_span"
		); //сюда вставится название города
		const popularCities = document.querySelectorAll(
			".js_popup_change_city_city_item"
		);
		const input = document.querySelector(".js_popup_change_city_input");
		const openBtn = document.querySelector(".js_popup_change_city_open_btn");
		const okBtn = document.querySelector(".js_popup_change_city_city_btn_ok");
		const denyBtn = document.querySelector(
			".js_popup_change_city_city_btn_deny"
		);
		const closeBtn = document.querySelector(".js_popup_change_city_close_btn");

		// запрос из попапа Выбрать город
		async function sendRequest() {
			try {
				BX.ajax.runComponentAction('alfateam:user.location',
					'saveLocation', {
						mode: 'class',
						data: {location: input.value},
					})
					.then(function (response) {
						if (response.status == 'success') {
							if (response.data == input.value) {
								document.cookie = "LOCATION=" + input.value;
								location.reload();
							}
						}
					});
			} catch (err) {
				console.log("Ошибка запроса из попапа Выбрать город", err);
			}
		}

		// подсказки в поле
		// Замените на свой API-ключ
		var token = "d376492eda844dbfdd0ed88a911763f03a96ad1c";

		if (openBtn) {
			openBtn.addEventListener("click", () => {
				popup.classList.add("active");
			});
		}

		// следим за инпутом и активируем кнопку
		input.addEventListener("input", () => {
			checkInput();
		});

		function checkInput() {
			var count = FindCity(input.value);
			if (input.value == "" && !count) {
				okBtn.classList.remove("active");
			} else {
				okBtn.classList.add("active");
			}
		}

		function FindCity(city) {
			var count = 0;
			$(popup).find(".js_popup_change_city_city_item").each(function (i, item) {
				if ($(item).text().toLowerCase().indexOf(city.toLowerCase()) + 1) {
					$(item).show();
					count++;
				} else {
					$(item).hide();
				}
			});
			return count;
		}

		//скидываем ошибку с инпута
		input.addEventListener("input", () => {
			input.classList.remove("error");
		});

		//переносим популярный город в инпут по клику
		popularCities.forEach((city) => {
			city.addEventListener("click", () => {
				input.value = city.innerHTML.trim();
				checkInput();
			});
		});

		okBtn.addEventListener("click", () => {
			if (input.value === "") {
				input.classList.add("error");
			} else {
				sendRequest();
				popup.classList.remove("active");
				targetSpan.innerHTML = input.value;
			}
		});

		closeBtn.addEventListener("click", () => {
			popup.classList.remove("active");
		});

		denyBtn.addEventListener("click", () => {
			popup.classList.remove("active");
		});
	}

	// табы (вынес в функцию)

	const setTabs = (wrap, btn, item) => {
		if (document.querySelector(wrap)) {
			const tabsWraps = document.querySelectorAll(wrap); // находим все блоки с табами (обёртки), на случай, если на странице несколько раз переиспользуется вёрстка и могут оказать несколько табов с одниковыми классами;

			tabsWraps.forEach((wrap) => {
				// перебираем все блоки с табами (обёртки) на странице;

				const tabsBtns = wrap.querySelectorAll(btn); // находим все кнопки (триггеры);
				const tabsItems = wrap.querySelectorAll(item); // находим все блоки с контентом, которые будут переключаться;

				tabsBtns.forEach((btn, i) => {
					// перебираем все кнопки;

					btn.addEventListener("click", () => {
						// при клике;

						tabsBtns.forEach((btn) => {
							// перебираем все кнопки;
							btn.classList.remove("active");
						});

						btn.classList.add("active");

						tabsItems.forEach((item) => {
							// перебираем все блоки с контентом;
							item.classList.remove("active");
						});

						tabsItems[i].classList.add("active");
					});
				});
			});
		}
	};

	// табы на главной странице в секции 'популярные товары'
	setTabs(".js_popular_inner", ".js_popular_tab", ".js_popular_item");

	// выпадающее меню каталога в хедере на дестопе

	if (document.querySelector(".js_menu_catalog")) {
		const menuCatalog = document.querySelector(".js_menu_catalog"); // выпадающее меню;
		const menuCatalogListLinks = menuCatalog.querySelectorAll(
			".js_menu_catalog_list a"
		); // ссылки 1-го уровня;
		const menuCatalogSublists = menuCatalog.querySelectorAll(
			".js_menu_catalog_sublist"
		); // все списки с сылками 2-го уровня;

		menuCatalogListLinks.forEach((link, i) => {
			link.addEventListener("mouseenter", () => {
				// при наведении курсора;

				menuCatalogListLinks.forEach((link) => {
					link.classList.remove("active");
				});

				link.classList.add("active");

				menuCatalogSublists.forEach((sublist) => {
					sublist.classList.remove("active");
				});

				menuCatalogSublists[i].classList.add("active");
			});
		});
	}

	// липкий хедер

	window.addEventListener("scroll", () => {
		if (window.pageYOffset > 400) {
			document.querySelector(".header").classList.add("fixed");
		} else {
			document.querySelector(".header").classList.remove("fixed");
		}
	});

	// открытие/закрытие поп-апов с классом 'js_btn_popup'

	if (document.querySelector(".js_btn_popup")) {
		const popups = document.querySelectorAll(".js_popup"); // находим все поп-апы с классом 'js_popup';
		const popupShowBtns = document.querySelectorAll(".js_btn_popup"); // находим все элементы (кнопки или ссылки) с классом 'js_btn_popup', открывающие поп-апы с классом 'js_popup';

		popupShowBtns.forEach((btn) => {
			// перебираем все элементы;

			btn.addEventListener("click", (e) => {
				// навешиваем событие клика на каждый элемент;

				e.preventDefault(); // отменяем стандартное поведение браузера если элементом является ссылка;
				popups.forEach((popup) => {
					// перебираем все поп-апы

					popup.classList.remove("active"); // если какойто поп-ап открыт, то закрываем его;
					bodyUnfixPosition();

					if (
						popup.dataset.popup == btn.dataset.btn &&
						!btn.classList.contains("our-boy")
					) {
						popup.classList.add("active"); // при совпадении значений дата-атрибутов у кнопки, на которую мы нажали, и у одного из поп-апов, показываем его;
						bodyFixPosition();
					}
				});
			});
		});

		popups.forEach((popup) => {
			// перебираем все поп-апы

			const popupExits = popup.querySelectorAll(".js_popup_exit"); // находим в каждом поп-апе все кнопки закрытия (крестик и др.);

			popupExits.forEach((exit) => {
				// перебираем в каждом поп-апе все кнопки закрытия (крестик и др.);

				exit.addEventListener("click", () => {
					// навешиваем событие клика на каждую из кнопок закрытия поп-апа;
					closePopup(popup); // вызываем функцию, объявленную ниже;
				});
			});

			popup.addEventListener("click", (e) => {
				// навешиваем событие клика на попап

				if (e.target == popup) {
					// условие, при котором клик будет срабатывать только на прозрачной облати (оверлей);
					closePopup(popup); // вызываем функцию, объявленную ниже;
				}
			});
		});

		var closePopup = (popup) => {
			// повторяющийся код вынесен в функцию;

			popup.classList.remove("active"); // скрываем поп-ап;
			bodyUnfixPosition();

			if (popup.querySelector(".js_popup_form")) {
				const popupForm = popup.querySelector(".js_popup_form"); // если поп-ап содержит форму, находим её;

				popupForm.reset(); // очищаем форму;

				if (popup.querySelector(".js_popup_success")) {
					// если после отправки формы предусмотрен показ сообщения об успешной отправке;
					popup.querySelector(".js_popup_success").classList.add("hidden"); // скрываем сообщение об успешной отправке;
					popupForm.classList.remove("hidden"); // показываем форму;
				}
			}
		};
	}

	// функция - валидатор

	const patternPhone =
		/^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/; // рег. выражение для поля 'телефон';
	const patternEmail = /^[a-zA-Z0-9._%+-\.]+@[a-z0-9.-]+\.[a-z]{2,}$/i; // рег. выражение для поля 'e-mail';

	function validateForm(input) {
		// проверяем на правильность заполнения поля 'имя'
		if (input.classList.contains("js_input_name") && input.value == "") {
			input.classList.add("error");
		} else if (input.classList.contains("js_input_name")) {
			input.classList.remove("error");
		}

		// проверяем на правильность заполнения поля 'телефон'
		if (
			input.classList.contains("js_input_phone") &&
			input.value.search(patternPhone) == 0
		) {
			input.classList.remove("error");
		} else if (input.classList.contains("js_input_phone")) {
			input.classList.add("error");
		}
		// проверяем на правильность заполнения поля 'телефон с масокой'
		if (
			input.classList.contains("js_input_masked_phone") &&
			input.value.length === 17
		) {
			input.classList.remove("error");
		} else if (input.classList.contains("js_input_masked_phone")) {
			input.classList.add("error");
		}
		// проверяем на правильность заполнения поля 'почта'
		/*if (
            input.classList.contains("js_input_email") &&
            input.value.search(patternEmail) == 0
        ) {
            input.classList.remove("error");
        } else if (input.classList.contains("js_input_email")) {
            input.classList.add("error");
        }*/
		// проверяем на правильность заполнения поля 'пароль'
		if (
			input.classList.contains("js_input_password") &&
			input.value.length > 6
		) {
			input.classList.remove("error");
		} else if (input.classList.contains("js_input_password")) {
			input.classList.add("error");
		}

		// проверяем на правильность заполнения поля 'код'
		if (input.classList.contains("js_code_input") && input.value !== "") {
			input.classList.remove("error");
		} else if (input.classList.contains("js_code_input")) {
			input.classList.add("error");
		}
		// // проверяем на правильность заполнения поля 'пол'
		// if (input.classList.contains('js_input_gender') && input.isChecked == true) {
		//     input.classList.remove('error');
		// } else if (input.classList.contains('js_input_gender')) {
		//     input.classList.add('error');
		// }
	}

	//страница Категория каталога
	//список категорий
	if (document.querySelector(".js_category_main_category_links_list")) {
		const list = document.querySelector(
			".js_category_main_category_links_list"
		);
		const moreBtn = document.querySelector(
			".js_category_main_category_links_more_btn"
		);
		const cards = document.querySelector(".js_category_main_product-column"); //основной блок с карточками

		moreBtn.addEventListener("click", () => {
			list.classList.toggle("active");
			moreBtn.classList.toggle("hidden");
			cards.classList.toggle("category-main__product-column--move-up");
		});
	}

	const categoryLoader = document.querySelector(".js_category_loader"); // лодер загрузки карточек товара
	// функции для остановки и запуска анимации загрузки
	function startLoader() {
		$(".js_category_loader").addClass("active");
		$(".category-main__product-row-item").hide();
		$(".category-main__product-item").hide();
	}

	function finichLoader() {
		categoryLoader.classList.remove("active");
	}

	//рендж цены
	if (document.querySelector(".js-range-slider")) {
		var rangeSlider = document.querySelector(".js-range-slider");
		var min = $(rangeSlider).parent().find(".js-input-0").attr("min");
		var max = $(rangeSlider).parent().find(".js-input-1").attr("max");
		noUiSlider.create(rangeSlider, {
			start: [min, max],
			connect: true,
			step: 1,
			range: {
				min: parseInt(min),
				max: parseInt(max),
			},
		});

		// отправляем информацию о добавлении фильтра
		async function changePrice(id, info) {
			const filtersList = $(document.querySelector(".js_filters_list"));

			var actionUrl = filtersList.attr('action');

			updateCatalog(actionUrl + "?" + filtersList.serialize());

		}

		// меняем значения инпутов при перетаскивании ползунков
		const input0 = document.querySelector(".js-input-0");
		const input1 = document.querySelector(".js-input-1");
		const inputs = [input0, input1];
		rangeSlider.noUiSlider.on("update", function (values, handle) {
			inputs[handle].value = Math.round(values[handle]);
		});
		rangeSlider.noUiSlider.on("change", function (values, handle) {
			changePrice(values, handle);
		});

		const setRangeSlider = (i, value) => {
			let arr = [null, null];
			arr[i] = value;

			rangeSlider.noUiSlider.set(arr);
		};
		inputs.forEach((el, index) => {
			el.addEventListener("change", (e) => {
				setRangeSlider(index, e.currentTarget.value);
				changePrice(index, e.currentTarget.value);
			});
		});
	}

	// открывающиеся списки фильтров
	if (document.querySelector(".js_opening_filter")) {
		const filterGroups = document.querySelectorAll(".js_opening_filter"); //обертки списков и кнопок "бооольше"

		filterGroups.forEach((group) => {
			group.addEventListener("click", (event) => {
				if (event.target.closest(".js_filter_more_options_btn")) {
					const btn = group.querySelector(".js_filter_more_options_btn");
					btn.classList.toggle("active");

					const filtersWrapper = group.querySelector(
						".js_filter_options_wrapper"
					);
					filtersWrapper.classList.toggle("active");
				}
			});
		});
	}

	//инициалиизруем селекты
	if (document.querySelector(".js_my_select")) {
		const multiDefaultSelect = () => {
			const elements = document.querySelectorAll(".js_my_select");
			elements.forEach((el) => {
				const choices = new Choices(el, {
					searchEnabled: false,
					searchPlaceholderValue: null,
					itemSelectText: "",
				});
			});
		};

		multiDefaultSelect();
	}

	// открытие списка фильтров на мобильных
	if (document.querySelector(".js_filters_show_btn")) {
		const filtersList = document.querySelector(".js_filters_list");
		const chosenFiltersList = document.querySelector(".js_chosen_filters");
		const header = document.querySelector(".js_filters_header");
		const productList = document.querySelector(
			".js_category_main_product_list"
		);

		const openBtn = document.querySelector(".js_filters_show_btn");
		const submitBtn = document.querySelector(".js_filters_submit_btn");
		const closeBtn = document.querySelector(".js_filters_close_btn");

		// при открытии фильтров
		openBtn.addEventListener("click", () => {
			// показываем
			chosenFiltersList.classList.add("active");
			filtersList.classList.add("active");
			submitBtn.classList.add("active");
			closeBtn.classList.add("active");

			// скрываем
			//header.classList.add("hide");
			productList.classList.add("hide");
		});

		// при закрытии фильтров
		closeBtn.addEventListener("click", (e) => {
			//e.preventDefault();

			// скрываем
			chosenFiltersList.classList.remove("active");
			filtersList.classList.remove("active");
			submitBtn.classList.remove("active");
			closeBtn.classList.remove("active");

			// показываем
			//header.classList.remove("hide");
			productList.classList.remove("hide");
		});
		submitBtn.addEventListener("click", (e) => {
			e.preventDefault();

			// скрываем
			chosenFiltersList.classList.remove("active");
			filtersList.classList.remove("active");
			submitBtn.classList.remove("active");
			closeBtn.classList.remove("active");

			// показываем
			//header.classList.remove("hide");
			productList.classList.remove("hide");
		});
	}

	//инициалиизруем селекты
	if (document.querySelector(".js_my_select")) {
		const multiDefaultSelect = () => {
			const elements = document.querySelectorAll(".js_my_select");
			elements.forEach((el) => {
				const choices = new Choices(el, {
					searchEnabled: false,
					searchPlaceholderValue: null,
					itemSelectText: "",
				});
			});
		};

		multiDefaultSelect();
	}

	function initSelect() {
		if (document.querySelector(".js_my_select")) {
			const multiDefaultSelect = () => {
				const elements = document.querySelectorAll(".js_my_select");
				elements.forEach((el) => {
					const choices = new Choices(el, {
						searchEnabled: false,
						searchPlaceholderValue: null,
						itemSelectText: "",
					});
				});
			};

			multiDefaultSelect();
		}
	}


	function initWrapperCatalog() {
		if (document.querySelector(".js_category_main_product_list")) {
			cardsWrapper = document.querySelector(
				".js_category_main_product_list"
			);
			listByRowsBtn = document.querySelector(
				".js_category_list_by_row_btn"
			);
			listByTableBtn = document.querySelector(
				".js_category_list_by_table_btn"
			);

			listByRowsBtn.addEventListener("click", () => {
				listByRowsBtn.classList.add("active");
				listByTableBtn.classList.remove("active");
				cardsWrapper.classList.add("rows");
			});

			listByTableBtn.addEventListener("click", () => {
				listByTableBtn.classList.add("active");
				listByRowsBtn.classList.remove("active");
				cardsWrapper.classList.remove("rows");
			});
		}
	}

	// запросы на изменение количества товара
	async function changeQuontityOfProduct(product, quontity) {
		try {
			// отправляем запрос
			console.log("запрос на изменение количества товара", product, quontity);

			let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
				method: "PUT",
				body: JSON.stringify({
					id: 1,
					title: "foo",
					body: "bar",
					userId: 1,
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
				},
			});

			if (!resp.ok) {
				throw new Error(resp.status);
			}
		} catch (err) {
			console.log(
				"Ошибка взаимодействия с карточкой товара",
				err,
				type,
				productId
			);
		}
	}

	// активация кнопок ввода количества товара
	if (document.querySelector(".js_quantity_box")) {
		let boxes = document.querySelectorAll(".js_quantity_box");
		let minValue = 0; //ниже этой цифры счетчик не уйдет
		let maxValue = 99; //выше этой цифры счетчик не уйдет

		boxes.forEach((box) => {
			box.addEventListener("click", (e) => {
				let minusBtn = box.querySelector(".js_number_minus");
				let input = box.querySelector(".js_product_quantity_input");
				let plusBtn = box.querySelector(".js_number_plus");

				// добавляем или убираем количество, если не уперлись в ограничители
				if (e.target.closest(".js_number_minus")) {
					if (input.value > minValue) {
						input.stepDown();
						changeQuontityOfProduct(input.dataset.productid, input.value);
					}
				} else if (e.target.closest(".js_number_plus")) {
					if (input.value < maxValue) {
						input.stepUp();
						changeQuontityOfProduct(input.dataset.productid, input.value);
					}
				}

				// отключаем минус при минимуме
				if (input.value == minValue) {
					minusBtn.classList.add("off");
				} else {
					minusBtn.classList.remove("off");
				}

				// отключаем плюс при максимуме
				if (input.value == maxValue) {
					plusBtn.classList.add("off");
				} else {
					plusBtn.classList.remove("off");
				}
			});
		});
	}

	// звездный рейтинг
	if (document.querySelector(".js_active_rating")) {
		let ratingItems = document.querySelectorAll(
			".js_active_rating .js_rating_item"
		);

		ratingItems.forEach((item) => {
			item.addEventListener("click", () => {
				let selectedRate = item.dataset.itemValue;
				item.parentNode.dataset.totalValue = selectedRate;
			});
		});
	}

	// всплывашка для кнопки По рецепту на странице Карточка товара
	if (document.querySelector(".js_product_info_btn")) {
		let btn = document.querySelector(".js_product_info_btn");
		let text = document.querySelector(".js_product_info_text");
		let closeBtn = document.querySelector(".js_product_info_close_btn");

		btn.addEventListener("click", () => {
			text.classList.toggle("active");
		});

		closeBtn.addEventListener("click", () => {
			text.classList.toggle("active");
		});
	}

	// выподашка с акциями

	if (document.querySelector(".offer__offer")) {
		let btns = document.querySelectorAll(".offer__offer");

		btns.forEach((btn) => {
			btn.addEventListener("click", (e) => {
				e.preventDefault();
			});
		});
	}

	// блок Отзывы в на странице Карточка товара (скукоживаем и раскукоживаем)
	if (document.querySelector(".js_reviews")) {
		const btn = document.querySelector(".js_show_more_reviews");
		const section = document.querySelector(".js_reviews");

		if (btn) {
			btn.addEventListener("click", () => {
				btn.classList.toggle("active");
				section.classList.toggle("active");
			});
		}
	}

	// открывающийся пункт описания в Карточке товара
	if (document.querySelector(".js_description_spoiler_item")) {
		let items = document.querySelectorAll(".js_description_spoiler_item"); //спойлеры с поисанием
		let anchors = document.querySelectorAll(".js_description_link"); //якори

		// скролл по якорной ссылке и открытие таба на странице Карточка товара
		anchors.forEach((anchor) => {
			anchor.addEventListener("click", () => {
				// делаем все якоря не активными
				anchors.forEach((anchor) => {
					anchor.classList.remove("active");
				});
				// и делаем нужный якорь активным
				anchor.classList.add("active");

				let a = anchor.dataset.num;
				// находим цель скоролла
				let target = document.querySelector(
					`[data-target="${anchor.dataset.num}"]`
				);

				// скорлим к целе
				target.scrollIntoView({
					block: "center",
					inline: "center",
				});

				// закрываем все цели
				items.forEach((item) => {
					item.classList.remove("active");
				});

				// открываем цель
				target.classList.add("active");
			});
		});
		// открываем спойлер при клике
		items.forEach((item) => {
			item.addEventListener("click", () => {
				// делаем все якоря не активными
				anchors.forEach((anchor) => {
					anchor.classList.remove("active");
				});

				// если кликаем на открытый спойлер, то просто закрываем его
				if (item.classList.contains("active")) {
					item.classList.remove("active");
				}
				// если на закрытый, то закрываем все и открываем кликнутый
				else {
					items.forEach((item) => {
						item.classList.remove("active");
					});

					item.classList.add("active");
				}
			});
		});
	}

	//открывающая мобильная навигация в личном кабинете
	if (document.querySelector(".js_cabinet_menu_list")) {
		let list = document.querySelector(".js_cabinet_menu_list");
		let btn = document.querySelector(".js_cabinet_menu_open_btn");

		btn.addEventListener("click", () => {
			btn.classList.toggle("active");
			list.classList.toggle("active");
		});
	}

	//выбор способа доставки в корзине

	if (document.querySelector(".js_cart_delivery_item")) {
		let items = document.querySelectorAll(".js_cart_delivery_item");

		items.forEach((item) => {
			item.addEventListener("click", (e) => {
				if (!item.classList.contains("js_block")) {
					items.forEach((item) => {
						item.classList.remove("active");
					});

					item.classList.add("active");
				}
			});
		});
	}

	//удаление карточек товара в корзине
	if (document.querySelector(".js_cart_product_item")) {
		let items = document.querySelectorAll(".js_cart_product_item");
		let itemsCounter = items.length;
		let cartTop = document.querySelector(".js_cart_top");
		let cartMain = document.querySelector(".js_cart_main");
		let emptyWrapper = document.querySelector(".js_cart_empty_row");

		items.forEach((item) => {
			item.addEventListener("click", (e) => {
				if (e.target.closest(".js_cart_delete_btn")) {
					item.remove();

					itemsCounter--;
					console.log("itemsCounter", itemsCounter);
				}

				if (itemsCounter === 0) {
					cartTop.classList.remove("active");
					cartMain.classList.remove("active");
					emptyWrapper.classList.add("active");
				}
			});
		});
	}

	//проверяем промокод
	if (document.querySelector(".js_code_form")) {
		const codeWrapper = document.querySelector(".js_code_form");
		const codeInput = document.querySelector(".js_code_input");
		const codeBtn = document.querySelector(".js_code_btn");

		codeBtn.addEventListener("click", () => {
			console.log("rkbr");

			if (codeInput.value !== "") {
				//    происходит что-то хорошее
				codeWrapper.classList.remove("error");
				codeInput.classList.remove("error");
			} else {
				codeWrapper.classList.add("error");
				codeInput.classList.add("error");
			}
		});
	}

	// открываем / закрываем список товаров
	if (document.querySelector(".js_checkout_product_list")) {
		let list = document.querySelector(".js_checkout_product_list");
		let btn = document.querySelector(".js_checkout_toggle_product_list_btn");

		btn.addEventListener("click", () => {
			btn.classList.toggle("active");
			list.classList.toggle("active");
		});
	}


	function selectDeliveryTime() {
		var dateText = $(".js_checkout_date_btn.active").find(".checkout__date-btn-date").text()
		var timeText = $(".js_checkout_time_btn.active").find(".checkout__date-btn-date").text()
		$("#soa-property-16").val(dateText + " " + timeText);
	}

	// Оформление заказа (выбор даты доставки)
	if (document.querySelector(".js_checkout_date_btn")) {
		const btns = document.querySelectorAll(".js_checkout_date_btn");

		btns.forEach((btn) => {
			btn.addEventListener("click", () => {
				btns.forEach((btn) => {
					btn.classList.remove("active");
				});

				btn.classList.add("active");
				selectDeliveryTime();
			});
		});
	}

	// Оформление заказа (скролл списка дат доставки)
	if (document.querySelector(".js_checkout_date_list")) {
		const swiper = new Swiper(".js_checkout_date_list", {
			slidesPerView: 5.7,
			spaceBetween: 5,

			breakpoints: {
				320: {
					slidesPerView: 2.5,
				},
				540: {
					slidesPerView: 4,
				},
				980: {
					slidesPerView: 5.7,
				},
			},

			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
		});
	}

	// Оформление заказа (выбор времени доставки)
	if (document.querySelector(".js_checkout_time_btn")) {
		const btns = document.querySelectorAll(".js_checkout_time_btn");

		btns.forEach((btn) => {
			btn.addEventListener("click", () => {
				btns.forEach((btn) => {
					btn.classList.remove("active");
				});

				btn.classList.add("active");
				selectDeliveryTime()
			});
		});
	}

	// Оформление заказа (самовывоз). Неавторизованный пользователь
	if (document.querySelector(".js_checkout__form")) {
		const form = document.querySelector(".js_checkout__form");
		const filtersBtns = document.querySelectorAll(".js_checkout__filter_btn"); //кнопки фильтрации аптек на карте
		const payBtns = document.querySelectorAll(".js_checkout_pay_way_btn"); //кнопки вида оплаты
		const addressInput = document.querySelector(".js_checkout_address_input");
		const nameInput = document.querySelector(".js_checkout_client_name_input");
		const phoneInput = document.querySelector(
			".js_checkout_client_phone_input"
		);
		const submitBtn = document.querySelector(".js_checkout_submit_btn");
		let result = 0;

		//выбираем фильтры
		filtersBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				btn.classList.toggle("active");
			});
		});

		//выбираем способ оплаты
		payBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				payBtns.forEach((btn) => {
					btn.classList.remove("active");
				});
				btn.classList.add("active");
			});
		});

		// сбрасываем ошибки при клике
		clearInputError(addressInput);
		clearInputError(nameInput);
		clearInputError(phoneInput);

		// добавляем маски номеру телефона
		if (phoneInput) {
			phoneInput.addEventListener("input", mask, false);
			phoneInput.addEventListener("focus", mask, false);
			phoneInput.addEventListener("blur", mask, false);
		}

		// валидируем форму
		submitBtn.addEventListener("click", (e) => {

			e.preventDefault();

			// проверяем адрес
			if (addressInput.value !== "") {
				result++;
			} else {
				addressInput.classList.add("error");
			}

			// проверяем имя
			if (nameInput.value.length > 1) {
				result++;
			} else {
				nameInput.classList.add("error");
			}

			// провеяем телефон
			if (phoneInput.value.length === 17) {
				result++;
			} else {
				phoneInput.classList.add("error");
			}
		});
	}

	// Оформление заказа (самовывоз). Зарегистрированный пользователь (есть сохраненная аптека)
	if (document.querySelector(".js_checkout_pickup_client_address")) {
		const form = document.querySelector(".js_checkout_pickup_client_address");
		const filtersBtns = document.querySelectorAll(".js_checkout__filter_btn"); //кнопки фильтрации аптек на карте
		const payBtns = document.querySelectorAll(".js_checkout_pay_way_btn"); //кнопки вида оплаты
		const addressInput = document.querySelector(".js_checkout_address_input");
		const submitBtn = document.querySelector(".js_checkout_submit_btn");
		let result = 0;

		//выбираем фильтры
		filtersBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				btn.classList.toggle("active");
			});
		});

		//выбираем способ оплаты
		payBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				payBtns.forEach((btn) => {
					btn.classList.remove("active");
				});
				btn.classList.add("active");
			});
		});

		// сбрасываем ошибки при клике
		clearInputError(addressInput);

		// валидируем форму
		submitBtn.addEventListener("click", (e) => {
			e.preventDefault();
			console.log('asdaaaa');

			// проверяем адрес
			if (addressInput.value !== "") {
				result++;
			} else {
				addressInput.classList.add("error");
			}
		});
	}

	// Оформление заказа (доставка курьером). Неавторизованный пользователь
	if (document.querySelector(".js_checkout_delivery_guest__form")) {
		const form = document.querySelector(".js_checkout_delivery_guest__form");

		const streetInput = form.querySelector(".js_checkout_street_input");
		const homeInput = form.querySelector(".js_checkout_home_input");
		const flatInput = form.querySelector(".js_checkout_flat_input");
		const deliveryBtns = document.querySelectorAll(
			".js_checkout_delivery_way_btn"
		);
		const addressInput = document.querySelector(".js_checkout_address_input");
		const nameInput = document.querySelector(".js_checkout_client_name_input");
		const phoneInput = document.querySelector(
			".js_checkout_client_phone_input"
		);
		const emailInput = document.querySelector(
			".js_checkout_client_email_input"
		);
		const submitBtn = form.querySelector(".js_checkout_submit_btn");
		let result = 0;

		// //выбираем способ доставки
		deliveryBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				deliveryBtns.forEach((btn) => {
					btn.classList.remove("active");
				});
				btn.classList.add("active");
			});
		});

		// сбрасываем ошибки при клике
		clearInputError(streetInput);
		clearInputError(homeInput);
		clearInputError(flatInput);
		clearInputError(nameInput);
		clearInputError(phoneInput);
		clearInputError(emailInput);

		// добавляем маски номеру телефона
		phoneInput.addEventListener("input", mask, false);
		phoneInput.addEventListener("focus", mask, false);
		phoneInput.addEventListener("blur", mask, false);

		// активируем поле Дом, когда выбрана улица
		streetInput.addEventListener("change", () => {
			if (streetInput.value !== "") {
				homeInput.classList.remove("off");
				homeInput.disabled = false;
			} else {
				homeInput.classList.add("off");
				homeInput.disabled = true;
			}
		});

		// валидируем форму
		submitBtn.addEventListener("click", (e) => {
			e.preventDefault();

			// проверяем улицу
			if (streetInput.value !== "") {
				result++;
			} else {
				streetInput.classList.add("error");
			}

			// проверяем дом
			if (homeInput.value !== "") {
				result++;
			} else {
				homeInput.classList.add("error");
			}

			// проверяем квартиру
			if (flatInput.value !== "") {
				result++;
			} else {
				flatInput.classList.add("error");
			}

			//     // проверяем адрес
			//     if (addressInput.value !== '') {
			//         result++
			//     } else {
			//         addressInput.classList.add('error')
			//     }

			// проверяем имя
			if (nameInput.value.length > 1) {
				result++;
			} else {
				nameInput.classList.add("error");
			}

			// провеяем телефон
			if (phoneInput.value.length === 17) {
				result++;
			} else {
				phoneInput.classList.add("error");
			}

			// провеяем почту
			if (emailInput.value.search(patternEmail) == 0) {
				result++;
			} else {
				emailInput.classList.add("error");
			}
		});
	}
	// Оформление заказа (доставка курьером). Авторизованный пользователь (нет сохраненных адресов)
	if (document.querySelector(".js_checkout_delivery_client_no_address_form")) {
		const form = document.querySelector(
			".js_checkout_delivery_client_no_address_form"
		);

		const streetInput = form.querySelector(".js_checkout_street_input");
		const homeInput = form.querySelector(".js_checkout_home_input");
		const flatInput = form.querySelector(".js_checkout_flat_input");
		const deliveryBtns = document.querySelectorAll(
			".js_checkout_delivery_way_btn"
		);
		const saveAddressBtn = document.querySelectorAll(".js_save_address_btn");
		const nameInput = document.querySelector(".js_checkout_client_name_input");
		const phoneInput = document.querySelector(
			".js_checkout_client_phone_input"
		);
		const emailInput = document.querySelector(
			".js_checkout_client_email_input"
		);
		const submitBtn = form.querySelector(".js_checkout_submit_btn");
		let result = 0;

		// показываем сохранить адрес, только когда он заполнен
		streetInput.addEventListener("keyup", () => {
			showSaveAddressBtns();
		});
		homeInput.addEventListener("keyup", () => {
			showSaveAddressBtns();
		});
		flatInput.addEventListener("keyup", () => {
			showSaveAddressBtns();
		});

		function showSaveAddressBtns() {
			if (
				streetInput.value !== "" &&
				homeInput.value !== "" &&
				flatInput.value !== ""
			) {
				saveAddressBtn.forEach((btn) => {
					btn.classList.add("active");
				});
			}
		}

		// //выбираем способ доставки
		deliveryBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				deliveryBtns.forEach((btn) => {
					btn.classList.remove("active");
				});
				btn.classList.add("active");
			});
		});

		// сбрасываем ошибки при клике
		clearInputError(streetInput);
		clearInputError(homeInput);
		clearInputError(flatInput);
		clearInputError(nameInput);
		clearInputError(phoneInput);
		clearInputError(emailInput);

		// добавляем маски номеру телефона
		phoneInput.addEventListener("input", mask, false);
		phoneInput.addEventListener("focus", mask, false);
		phoneInput.addEventListener("blur", mask, false);

		// активируем поле Дом, когда выбрана улица
		streetInput.addEventListener("change", () => {
			if (streetInput.value !== "") {
				homeInput.classList.remove("off");
				homeInput.disabled = false;
			} else {
				homeInput.classList.add("off");
				homeInput.disabled = true;
			}
		});

		// валидируем форму
		submitBtn.addEventListener("click", (e) => {
			console.log('asdaaaa2344');
			e.preventDefault();

			// проверяем улицу
			if (streetInput.value !== "") {
				result++;
			} else {
				streetInput.classList.add("error");
			}

			// проверяем дом
			if (homeInput.value !== "") {
				result++;
			} else {
				homeInput.classList.add("error");
			}

			// проверяем квартиру
			if (flatInput.value !== "") {
				result++;
			} else {
				flatInput.classList.add("error");
			}

			//     // проверяем адрес
			//     if (addressInput.value !== '') {
			//         result++
			//     } else {
			//         addressInput.classList.add('error')
			//     }

			// проверяем имя
			if (nameInput.value.length > 1) {
				result++;
			} else {
				nameInput.classList.add("error");
			}

			// провеяем телефон
			if (phoneInput.value.length === 17) {
				result++;
			} else {
				phoneInput.classList.add("error");
			}

			// провеяем почту
			if (emailInput.value.search(patternEmail) == 0) {
				result++;
			} else {
				emailInput.classList.add("error");
			}
		});
	}

	// страница Личный кабинет

	// переключение табов в личном кабинете
	if (document.querySelector(".js_cabinet_tab")) {
		const tabs = document.querySelectorAll(".js_cabinet_tab");
		const btns = document.querySelectorAll(".js_cabinet_tab_btn");

		btns.forEach((btn) => {
			btn.addEventListener("click", () => {
				btns.forEach((btn) => {
					btn.classList.remove("active");
				});

				tabs.forEach((tab) => {
					tab.classList.remove("active");

					if (tab.dataset.num === btn.dataset.num) {
						tab.classList.add("active");
					}
				});

				btn.classList.add("active");
			});
		});
	}

	//проверка формы Личные данные в личном кабинете
	if (document.querySelector(".js_personal_data_form")) {
		const popupOk = document.querySelector(".js_popup_update_info_success");
		const form = document.querySelector(".js_personal_data_form");
		const inputs = form.querySelectorAll("input");
		const nameInput = document.querySelector(".js_personal_data_name_input");
		const emailInput = document.querySelector(".js_personal_data_email_input");
		const passwordInput = document.querySelector(
			".js_personal_data_password_input"
		);
		const phoneInput = document.querySelector(".js_personal_data_phone_input");
		const submitBtn = document.querySelector(".js_personal_data_submit_btn");
		let result = 0;

		// сбрасываем ошибки при клике
		clearInputError(nameInput);
		clearInputError(emailInput);
		clearInputError(passwordInput);
		clearInputError(phoneInput);

		// добавляем маски номеру телефона
		phoneInput.addEventListener("input", mask, false);
		phoneInput.addEventListener("focus", mask, false);
		phoneInput.addEventListener("blur", mask, false);

		// валидируем форму
		submitBtn.addEventListener("click", (e) => {
			e.preventDefault();
			result = 0;

			// проверяем имя
			if (nameInput.value !== "") {
				result++;
			} else {
				nameInput.classList.add("error");
			}

			/*// // проверяем почту и пароль, если что-то одно не пустое
			if (emailInput.value !== "" || passwordInput.value !== "") {
				if (emailInput.value.search(patternEmail) == 0) {
					result++;
				} else {
					emailInput.classList.add("error");
				}

				if (passwordInput.value.length > 5) {
					result++;
				} else {
					passwordInput.classList.add("error");
				}
			}*/

			// проверяем номер телефона, если он не пустой
			if (phoneInput.value !== "") {
				if (phoneInput.value.length === 17) {
					console.log("validated");

					result++;
				} else {
					phoneInput.classList.add("error");
				}
			}

			if (!form.querySelector(".error")) {
				form.submit();
				//popupOk.classList.add("active");

			}
		});
	}

	// шкала скидки (перекрашиваем черточки по мере заполнения)
	if (document.querySelector(".js_sale_scale_main_range")) {
		const ranges = document.querySelectorAll(".js_sale_scale_main_range");

		ranges.forEach((range) => {
			// находим заполненную часть шкалы и её ширину
			let progress = range.querySelector(".js_sale_scale_progress_range");
			let width = parseInt(progress.style.width);

			if (width > 66) {
				// перекрашиваем вторую черту
				range.classList.add("secondDone");
				// перекрашиваем первую черту
				range.classList.add("firstDone");
			} else if (width > 33) {
				// перекрашиваем первую черту
				range.classList.add("firstDone");
			}
		});
	}

	// переключение тегов на странице Избранное
	if (document.querySelector(".js_favorites_tag_btn")) {
		const btns = document.querySelectorAll(".js_favorites_tag_btn");
		const btnsClose = document.querySelectorAll(".js_favorites_tag_btn_close");
		const items = document.querySelectorAll(".js_favorites_item");

		btns.forEach((btn) => {
			btn.addEventListener("click", () => {
				//отключем все кнопки
				btns.forEach((btn) => {
					btn.classList.remove("active");
				});

				// включаем нужную кнопку
				btn.classList.add("active");

				items.forEach((item) => {
					// отключаем все карточки
					item.classList.remove("active");

					// включаем нужные
					if (item.dataset.group === btn.dataset.group) {
						item.classList.add("active");
					}
				});
			});
		});

		btnsClose.forEach((btn) => {
			btn.addEventListener("click", (e) => {
				e.stopPropagation();

				//отключем все кнопки
				btns.forEach((btn) => {
					btn.classList.remove("active");
				});

				items.forEach((item) => {
					// отключаем все карточки
					item.classList.remove("active");
				});
			});
		});
	}

	// сортировка заказов
	if (document.querySelector(".js_my_order_tag_btn")) {
		const btns = document.querySelectorAll(".js_my_order_tag_btn");
		// const btn = document.querySelector('');

		btns.forEach((btn) => {
			/*btn.addEventListener("click", (e) => {
				btn.classList.toggle("active");
				console.log($(e).data("order_color"));
			});*/
			$(btn).click(function () {
				$(this).toggleClass('active');
				var color = $(this).data("order_color");
				$('.my-orders__status--' + color).closest('li').toggle();
			})
		});
	}

	//удаление отзывов в личном кабинете
	if (document.querySelector(".js_review_item")) {
		let items = document.querySelectorAll(".js_review_item");
		let itemsCounter = items.length;
		let cartTop = document.querySelector(".js_review_top");
		let cartMain = document.querySelector(".js_review_main");
		let emptyWrapper = document.querySelector(".js_review_empty_row");

		items.forEach((item) => {
			item.addEventListener("click", (e) => {
				if (e.target.closest(".js_review_delete_btn")) {
					item.remove();

					itemsCounter--;
				}

				if (itemsCounter === 0) {
					cartTop.classList.remove("active");
					cartMain.classList.remove("active");
					emptyWrapper.classList.add("active");
				}
			});
		});
	}

	// попапы авторизации / регистрации
	if (document.querySelector(".js_popup_log_in")) {
		const popupLogIn = document.querySelector(".js_popup_log_in");
		const popupLogInLogInSmsBtn = document.querySelector(
			".js_popup_log_in_sms_btn"
		);
		const popupLogInAskPasswordBtn = document.querySelector(
			".js_ask_password_btn"
		);
		const popupLogInRegistrationBtn = document.querySelector(
			".js_popup_log_in_registration_btn"
		);
		const popupChangePassword = document.querySelector(
			".js_popup_change_password"
		);
		const popupChangePasswordWait = document.querySelector(
			".js_popup_change_password_wait"
		);
		const popupChangePasswordNew = document.querySelector(
			".js_popup_change_password_new"
		);
		const popupChangePasswordSuccess = document.querySelector(
			".js_popup_change_password_success"
		);
		const popupLogInSms = document.querySelector(".js_popup_log_in_sms");
		const popupLogInSmsLogInEmailBtn = document.querySelector(
			".js_popup_log_in_email_btn"
		);
		const popupLogInSmsRegistrationBtn = document.querySelector(
			".js_popup_log_in_sms_registration_btn"
		);
		const popupSmsCode = document.querySelector(".js_popup_log_in_sms_code");
		const popupPersonalData = document.querySelector(".js_popup_personal_data");
		const popupRegistration = document.querySelector(".js_popup_registration");
		const popupRegistrationPhoneBtn = document.querySelector(
			".js_popup_registration_email_phone_btn"
		);
		const popupRegistrationLogInBtn = document.querySelector(
			".js_popup_registration_log_in_btn"
		);
		const popupRegistrationSms = document.querySelector(
			".js_popup_registration_sms"
		);
		const popupRegistrationSmsRegistrationEmailBtn = document.querySelector(
			".js_popup_registration_email_btn"
		);
		const popupRegistrationSmsCode = document.querySelector(
			".js_popup_registration_sms_code"
		);
		const popupRegistrationSuccess = document.querySelector(
			".js_popup_registration_success"
		);
		const popupRegistrationStop = document.querySelector(
			".js_popup_stop_registration"
		);
		const allPhoneInputs = document.querySelectorAll(".js_input_masked_phone");

		// запрос из попапа  Авторизация / регистрация
		async function sendRequest() {
			try {
				var form = $(popupLogIn).find("form");
				BX.ajax({
					url: form.attr("action"),
					method: 'POST',
					data: form.serialize() + "&ajax_mode=Y",
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						if (data.type == "ok") {
							location.reload();
						} else {
							$(".popup__warning-text").text(data.message);
							if (data.message == "Неверный логин или пароль.") {
								$(form).find(".js_input_email").addClass("error");
								$(form).find(".js_input_password").addClass("error");
							}
						}
					},
				});
			} catch (err) {
				console.log("Ошибка запроса из попапа  Авторизация / регистрация", err);
			}
		}

		// сбрасываем ошибки при клике
		const formInputs = document.querySelectorAll("input");
		formInputs.forEach((input) => {
			clearInputError(input);
		});

		// попап Логин Емейл
		{
			popupLogInLogInSmsBtn.addEventListener("click", () => {
				popupLogIn.classList.remove("active");
				popupLogInSms.classList.add("active");
			});

			popupLogInRegistrationBtn.addEventListener("click", () => {
				popupLogIn.classList.remove("active");
				popupRegistration.classList.add("active");
			});

			// восстановить пароль
			popupLogInAskPasswordBtn.addEventListener("click", () => {
				popupLogIn.classList.remove("active");
				popupChangePassword.classList.add("active");
			});

			const popupOrderForm = popupLogIn.querySelector(".js_popup_form");
			// const formInputs = popupLogInSms.querySelectorAll('input')
			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();
				formInputs.forEach((input) => {

					validateForm(input);
				});
				sendRequest();
			});
		}

		// попап ЛогинСМС
		{
			popupLogInSmsLogInEmailBtn.addEventListener("click", () => {
				popupLogInSms.classList.remove("active");
				popupLogIn.classList.add("active");
			});
			popupLogInSmsRegistrationBtn.addEventListener("click", () => {
				popupLogInSms.classList.remove("active");
				popupRegistration.classList.add("active");
			});

			// добавляем маски номеру телефона
			allPhoneInputs.forEach((phoneInput) => {
				phoneInput.addEventListener("input", mask, false);
				phoneInput.addEventListener("focus", mask, false);
				phoneInput.addEventListener("blur", mask, false);
			});

			// сбрасываем ошибки при клике
			const formInputs = popupLogInSms.querySelectorAll("input");
			formInputs.forEach((input) => {
				clearInputError(input);
			});

			const popupOrderForm = popupLogInSms.querySelector(".js_popup_form");

			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();

				formInputs.forEach((input) => {
					// перебираем все инпуты в форме;

					validateForm(input);
				});

				if (!popupOrderForm.querySelector(".error")) {
					//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

					//sendRequest();

					var phone = $('.login_phone_field').val();

					$.ajax({
						method: "POST",
						url: "/ajax/smsAuth.php",
						data: {phone: phone,}
					})
						.done(function (msg) {
							console.log(msg);
							if (msg != 'notFound'){
								let timeText = popupSmsCode.querySelector(".js_code_time_text");
								let timeSpan = popupSmsCode.querySelector(".js_code_time_span");
								let newCodeBtn = popupSmsCode.querySelector(".js_new_code");
								// перезапускам таймер
								timeCounter = 45;

								// показываем таймер
								timeText.classList.remove("hide");
								newCodeBtn.classList.remove("active");

								popupLogInSms.classList.remove("active");
								popupSmsCode.classList.add("active");

								startTimer();
							} else {
								$('.js_popup_log_in_sms .popup__field').addClass("error");
								$('.js_popup_log_in_sms .login_phone_field').addClass("error");
							}
						});


					console.log("validate 993");

				} else {
					console.log("no-validate");
				}
			});
		}

		document.querySelector(".js_popup_log_in_sms .login_phone_field").addEventListener("change",function () {
			$('.js_popup_log_in_sms .popup__field').removeClass("error");
			$('.js_popup_log_in_sms .login_phone_field').removeClass("error");
		});


		// попап ЛогинСМС КОД
		{
			let timeText = popupSmsCode.querySelector(".js_code_time_text");
			let timeSpan = popupSmsCode.querySelector(".js_code_time_span");
			let newCodeBtn = popupSmsCode.querySelector(".js_new_code");

			let codeInputs = popupSmsCode.querySelectorAll(".js_code_input");
			let codeSubmitBtn = popupSmsCode.querySelector(".js_popup_code_btn");

			// перенос фокуса по инпутам
			/*for (let i = 0; i < codeInputs.length; i++) {
				codeInputs[i].addEventListener("input", () => {
					if (codeInputs[i].value !== "" && i !== codeInputs.length - 1) {
						codeInputs[i + 1].focus();
					} else {
						codeSubmitBtn.focus();
					}
				});
			}*/

			// таймер на повторение кода
			let counterStart = 45;
			let timeCounter = counterStart;

			// console.log(newCodeBtn);

			async function startTimerLogin() {

				timeSpan.innerHTML = `00:${timeCounter}`;

				var timerInterval = setInterval(decreaseTimeCounter, 1000);

				function decreaseTimeCounter() {
					if (timeCounter.toString().length === 2) {
						timeSpan.innerHTML = `00:${timeCounter}`;
					} else {
						timeSpan.innerHTML = `00:0${timeCounter}`;
					}

					timeCounter--;

					if (timeCounter === 0) {
						clearInterval(timerInterval);

						timeText.classList.add("hide");
						newCodeBtn.classList.add("active");
					}
				}
			}

			// перезапускам таймер и отправляем новый код
			newCodeBtn.addEventListener("click", () => {
				// отправляем новый код
				sendSMSCode();

				var phone = $('.js_popup_log_in_sms .login_phone_field').val();

				$.ajax({
					method: "POST",
					url: "/ajax/smsAuth.php",
					data: {phone: phone, newcode: 'Y'}
				})
					.done(function (msg) {
						// console.log(msg);
						if (msg != 'notFound'){
							// перезапускам таймер
							timeCounter = counterStart;

							// показываем таймер
							timeText.classList.remove("hide");
							newCodeBtn.classList.remove("active");
						} else {
							let field = popupSmsCode.querySelector(".popup__field");
							let fieldInput = popupSmsCode.querySelector(".login_phone_field");

							field.classList.add("error");
							fieldInput.classList.add("error");
						}
					});
			});

			// валидируем код
			const popupOrderForm = popupSmsCode.querySelector(".js_popup_form");

			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();

				codeInputs.forEach((input) => {
					// перебираем все инпуты в форме;

					validateForm(input);
				});

				if (!popupOrderForm.querySelector(".error")) {
					//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

					//sendRequest();

					// window.location.href = "/personal/";

					console.log("validate чпокс");
					popupSmsCode.classList.remove("active");
				} else {
					console.log("no-validate");
				}
			});
		}

		// попап Регистрация Емейл
		{
			popupRegistrationPhoneBtn.addEventListener("click", () => {
				popupRegistration.classList.remove("active");
				popupRegistrationSms.classList.add("active");
			});

			popupRegistrationLogInBtn.addEventListener("click", () => {
				popupRegistration.classList.remove("active");
				popupLogIn.classList.add("active");
			});

			// восстановить пароль
			popupLogInAskPasswordBtn.addEventListener("click", () => {
				popupLogIn.classList.remove("active");
				popupChangePassword.classList.add("active");
			});

			const popupOrderForm = popupRegistration.querySelector(".js_popup_form");
			// const formInputs = popupLogInSms.querySelectorAll('input')
			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();

				formInputs.forEach((input) => {
					// перебираем все инпуты в форме;
					validateForm(input);
				});

				if (!popupOrderForm.querySelector(".error")) {
					//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);
					try {
						var form = $('.js_popup_registration').find("form");
						BX.ajax({
							url: '/ajax/emailRegister.php',
							method: 'POST',
							data: form.serialize() + "&ajax_mode=Y",
							dataType: 'json',
							timeout: 30,
							async: true,
							onsuccess: function (data) {
								// console.log(data);
								if (data.type == "ok") {
									// location.reload();
								} else {
									$(".popup__warning-text").text(data.message);
									if (data.message == "Неверный логин или пароль.") {
										$(form).find(".js_input_email").addClass("error");
										$(form).find(".js_input_password").addClass("error");
									}
								}
							},
						});
					} catch (err) {
						console.log("Ошибка запроса из попапа  Авторизация / регистрация", err);
					}

					console.log("validate email");
					popupRegistration.classList.remove("active");
					popupPersonalData.classList.add("active");
				} else {
					console.log("no-validate");
				}
			});
		}

		// попап Регистрация СМС
		{
			popupRegistrationSmsRegistrationEmailBtn.addEventListener("click", () => {
				popupRegistrationSms.classList.remove("active");
				popupRegistration.classList.add("active");
			});
			popupLogInSmsRegistrationBtn.addEventListener("click", () => {
				popupLogInSms.classList.remove("active");
				popupRegistration.classList.add("active");
			});

			// добавляем маски номеру телефона
			allPhoneInputs.forEach((phoneInput) => {
				phoneInput.addEventListener("input", mask, false);
				phoneInput.addEventListener("focus", mask, false);
				phoneInput.addEventListener("blur", mask, false);
			});

			// сбрасываем ошибки при клике
			const formInputs = popupRegistrationSms.querySelectorAll("input");
			formInputs.forEach((input) => {
				clearInputError(input);
			});

			const popupOrderForm =
				popupRegistrationSms.querySelector(".js_popup_form");

			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();

				formInputs.forEach((input) => {
					// перебираем все инпуты в форме;

					validateForm(input);
				});

				if (!popupOrderForm.querySelector(".error")) {
					//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

					//sendRequest();

					// console.log("validate 99");
					popupRegistrationSms.classList.remove("active");
					popupRegistrationSmsCode.classList.add("active");

					var login = $('.register-field__login').val();
					var email = $('.register-field__email').val();
					var pass = $('.register-field__pass').val();
					var pass_ch = $('.register-field__pass_ch').val();
					var phone = $('.register-field__phone').val();

					$.ajax({
						method: "POST",
						url: "/ajax/smsRegister.php",
						data: {
							login: login,
							email: email,
							phone: phone,
							pass: pass,
							ch_pass: pass_ch,
							register_submit_button: 'Регистрация'
						},
						success: function(msg){
							console.dir(phone);
						}
					}).done(function (msg) {
						console.log(msg);
					});

					startTimer();
				} else {
					console.log("no-validate");
				}
			});
		}

		// попап Регистрация СМС КОД
		{
			let timeText =
				popupRegistrationSmsCode.querySelector(".js_code_time_text");
			let timeSpan =
				popupRegistrationSmsCode.querySelector(".js_code_time_span");
			let newCodeBtn = popupRegistrationSmsCode.querySelector(".js_new_code");

			let codeInputs =
				popupRegistrationSmsCode.querySelectorAll(".js_code_input");
			let codeSubmitBtn =
				popupRegistrationSmsCode.querySelector(".js_popup_code_btn");

			/*			// перенос фокуса по инпутам
						for (let i = 0; i < codeInputs.length; i++) {
							codeInputs[i].addEventListener("input", () => {
								if (codeInputs[i].value !== "" && i !== codeInputs.length - 1) {
									codeInputs[i + 1].focus();
								} else {
									codeSubmitBtn.focus();
								}
							});
						}*/

			// таймер на повторение кода
			let counterStart = 44;
			let timeCounter = counterStart;

			function startTimer() {
				timeSpan.innerHTML = `00:${timeCounter}`;

				var timerInterval = setInterval(decreaseTimeCounter, 1000);

				function decreaseTimeCounter() {
					if (timeCounter.toString().length === 2) {
						timeSpan.innerHTML = `00:${timeCounter}`;
					} else {
						timeSpan.innerHTML = `00:0${timeCounter}`;
					}

					timeCounter--;

					if (timeCounter === 0) {
						clearInterval(timerInterval);

						timeText.classList.add("hide");
						newCodeBtn.classList.add("active");
					}
				}
			}

			// перезапускам таймер и отправляем новый код
			newCodeBtn.addEventListener("click", () => {
				// отправляем новый код

				// перезапускам таймер
				timeCounter = counterStart;
				startTimer();

				// показываем таймер
				timeText.classList.remove("hide");
				newCodeBtn.classList.remove("active");
			});

			// валидируем код
			const popupOrderForm =
				popupRegistrationSmsCode.querySelector(".js_popup_form");

			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();

				codeInputs.forEach((input) => {
					// перебираем все инпуты в форме;

					validateForm(input);
				});

				if (!popupOrderForm.querySelector(".error")) {
					//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

					//sendRequest();
					console.log('авторизован');
					console.log("validate");
					// window.location.href = "/personal/";


					popupRegistrationSmsCode.classList.remove("active");
					popupPersonalData.classList.add("active");
				} else {
					console.log("no-validate");
				}
			});
		}

		// попап Персональная информация
		{
			const popupOrderForm = popupPersonalData.querySelector(".js_popup_form");
			const formInputs = popupPersonalData.querySelectorAll("input");

			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();

				formInputs.forEach((input) => {
					// перебираем все инпуты в форме;

					validateForm(input);
				});

				// валидируем выбор пола
				const boyInput = popupPersonalData.querySelector(".js_boy_input");
				const girlInput = popupPersonalData.querySelector(".js_girl_input");

				if (!boyInput.checked && !girlInput.checked) {
					boyInput.classList.add("error");
					girlInput.classList.add("error");
				} else {
					boyInput.classList.remove("error");
					girlInput.classList.remove("error");
				}

				if (!popupOrderForm.querySelector(".error")) {
					//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

					try {
						var form = $('.js_popup_personal_data').find("form");
						BX.ajax({
							url: '/update_user.php',
							method: 'POST',
							data: form.serialize() + "&ajax_mode=Y",
							dataType: 'json',
							timeout: 30,
							async: true,
							onsuccess: function (data) {
								location.reload();
								if (data.type == "ok") {

								} else {
									$(".popup__warning-text").text(data.message);
									if (data.message == "Неверный логин или пароль.") {
										$(form).find(".js_input_email").addClass("error");
										$(form).find(".js_input_password").addClass("error");
									}
								}
							},
						});
					} catch (err) {
						console.log("Ошибка запроса из попапа  Авторизация / регистрация", err);
					}

					console.log("validate");
					popupPersonalData.classList.remove("active");
					popupRegistrationSuccess.classList.add("active");
				} else {
					console.log("no-validate");
				}
			});

			// отмена регистрации
			const exitBtn = popupPersonalData.querySelector(
				".js_popup_exit_personal_data"
			);

			exitBtn.addEventListener("click", () => {
				closePopup(popupPersonalData);

				popupRegistrationStop.classList.add("active");
			});
		}

		// попап Вы уверены, что хотите остановить регистрацию?
		{
			const finishBtn = popupRegistrationStop.querySelector(
				".js_stop_registration_finish_btn"
			);
			const continueBtn = popupRegistrationStop.querySelector(
				".js_stop_registration_continue_btn"
			);

			finishBtn.addEventListener("click", () => {
				closePopup(popupRegistrationStop);
			});

			continueBtn.addEventListener("click", () => {
				closePopup(popupRegistrationStop);
				popupPersonalData.classList.add("active");
			});
		}

		// попап Восстановление пароля
		{
			const popupOrderForm =
				popupChangePassword.querySelector(".js_popup_form");
			const formInputs = popupChangePassword.querySelectorAll("input");

			// валидация
			popupOrderForm.addEventListener("submit", (e) => {
				e.preventDefault();

				formInputs.forEach((input) => {
					// перебираем все инпуты в форме;

					validateForm(input);
				});

				if (!popupOrderForm.querySelector(".error")) {
					//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

					//sendRequest();

					var form = $('.js_popup_change_password').find("form");
					BX.ajax({
						url: '/ajax/change_password.php',
						method: 'POST',
						data: form.serialize() + "&ajax_mode=Y",
						dataType: 'json',
						timeout: 30,
						async: true,
						onsuccess: function (data) {
							console.log("validate");
							closePopup(popupChangePassword);
							popupChangePasswordWait.classList.add("active");
						},
					});
				} else {
					console.log("no-validate");
				}
			});
		}

		// попап Новый пароль
		{
			if (popupChangePasswordNew) {
				const popupOrderForm =
					popupChangePasswordNew.querySelector(".js_popup_form");
				const formInputs = popupChangePasswordNew.querySelectorAll("input");
				const password1Input = document.querySelector(".js_input_new_password_1");
				const password2Input = document.querySelector(".js_input_new_password_2");

				// валидация
				popupOrderForm.addEventListener("submit", (e) => {
					e.preventDefault();

					formInputs.forEach((input) => {
						// перебираем все инпуты в форме;

						validateForm(input);
					});

					// проверяем, что пароли совпадают
					if (password1Input.value !== password2Input.value) {
						password1Input.classList.add("error");
						password2Input.classList.add("error");
					} else {
						password1Input.classList.remove("error");
						password2Input.classList.remove("error");
					}

					if (!popupOrderForm.querySelector(".error")) {
						//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

						//sendRequest();

						console.log("validate");
						closePopup(popupChangePasswordNew);
						popupChangePasswordSuccess.classList.add("active");
					} else {
						console.log("no-validate");
					}
				});
			}

		}
	}

	// попап "Выход из личного кабинета"
	if (document.querySelector(".js_popup_leave")) {
		const popUp = document.querySelector(".js_popup_leave");
		const leaveBtn = document.querySelector(".js_popup_leave_go_away_btn");
		const stayHereBtn = document.querySelector(".js_popup_leave_stay_here_btn");

		// разлогиниваемся
		async function leaveCabinet() {
			console.log("запрос на разлогин >>>");

			try {
				// запускаем индикатор загрузки
				leaveBtn.classList.add("isLoading");

				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});

				if (!resp.ok) {
					throw new Error(resp.status);
				}

				// удаляем индикутор загрузки
				leaveBtn.classList.remove("isLoading");
				// закрываем попап
				closePopup(popUp);
			} catch (err) {
				console.log("Ошибка загрузки", err);
				// удаляем индикутор загрузки
				leaveBtn.classList.remove("isLoading");
				// закрываем попап
				closePopup(popUp);
			}
		}

		leaveBtn.addEventListener("click", () => {
			leaveCabinet();
		});

		stayHereBtn.addEventListener("click", () => {
			closePopup(popUp);
		});
	}

	// форма подписки на новости и попап с благодарностью
	if (document.querySelector(".js_subscr_section")) {
		const section = document.querySelector(".js_subscr_section");
		const popupThenks = document.querySelector(".js_thenks");

		const popupOrderForm = section.querySelector(".js_popup_form");
		const formInputs = section.querySelectorAll("input");

		// валидация
		popupOrderForm.addEventListener("submit", (e) => {
			e.preventDefault();

			formInputs.forEach((input) => {
				// перебираем все инпуты в форме;
				validateForm(input);
			});

			if (!popupOrderForm.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);
				// сюда пишем команды, которые должны сработать после успешной валидации;
				var form = $(section).find("form");

				BX.ajax({
					url: '/ajax/subscribe.php',
					method: 'POST',
					data: form.serialize(),
					dataType: 'html',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						popupThenks.classList.add("active");
					},
				});
			} else {
				console.log("no-validate");
			}
		});
	}

	// попап "Товар добавлен в корзину"
	if (document.querySelector(".js_added_to_card")) {
		const popUp = document.querySelector(".js_added_to_card");
		const stayHereBtn = document.querySelector(
			".js_added_to_card_continue_shoping"
		);
		const addCart = document.querySelector(".card__btn");

		addCart.addEventListener("click", () => {
			$(addCart).addClass('btn--tr');
		});

		stayHereBtn.addEventListener("click", () => {
			closePopup(popUp);
		});
	}

	// попап "Товар добавлен в корзину"
	if (document.querySelector(".js_popup_do_you_have_prescription")) {
		const popUp = document.querySelector(".js_popup_do_you_have_prescription");
		const yesBtn = document.querySelector(
			".js_popup_do_you_have_prescription_yes_btn"
		);
		const noBtn = document.querySelector(
			".js_popup_do_you_have_prescription_no_btn"
		);

		yesBtn.addEventListener("click", () => {
			closePopup(popUp);
		});
		noBtn.addEventListener("click", () => {
			closePopup(popUp);
		});
	}

	// попап "Оставить отзыв о товаре"
	if (document.querySelector(".js_write_a_review")) {
		const popUp = document.querySelector(".js_write_a_review");
		const popUpThanks = document.querySelector(".js_write_a_review_thanks");
		const form = popUp.querySelector(".js_popup_form");
		const addBtn = document.querySelector(".js_write_a_review_add");
		const reviewText = popUp.querySelector(".js_write_a_review_input");
		const starsWrapper = popUp.querySelector(".js_validation_rating");
		const stars = popUp.querySelectorAll(".js_rating_item");
		var user_id = popUp.querySelector(".js_rating_user_id");
		var user_name = popUp.querySelector(".js_rating_user_name");
		var product = popUp.querySelector(".js_rating_product");
		// var addReview = popUp.querySelector(".js_rating_add_review");

		clearInputError(reviewText);

		// запрос из попапа Оставить отзыв о товаре
		async function sendRequest(product, rating, reviewText, user_name, user_id) {
			console.log(
				"запрос из попапа Оставить отзыв о товаре >>",
				product,
				rating,
				reviewText,
				user_id,
				user_name
			);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				$.ajax({
					url: "/ajax/review.php",    //the page containing php script
					type: "post",    //request type,
					dataType: 'json',
					data: {
						product: product,
						stars: rating,
						text: reviewText,
						action: 'addReviw',
						user_name: user_name,
						user_id: user_id,
						addReview: 'Y'
					},
					success: function (result) {
						console.log(result);
					}
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log("Ошибка запроса из попапа Оставить отзыв о товаре", err);
			}
		}

		// самоочищение звезд по клику
		stars.forEach((star) => {
			star.addEventListener("click", () => {
				stars.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});

		form.addEventListener("submit", (e) => {
			e.preventDefault();

			// проверяем звезды
			console.log(starsWrapper.dataset.totalValue);
			if (starsWrapper.dataset.totalValue === "") {
				stars.forEach((star) => {
					star.classList.add("error");
				});

				starsWrapper.classList.add("error");
			} else {
				stars.forEach((star) => {
					star.classList.remove("error");
				});

				starsWrapper.classList.remove("error");
			}

			// проверяем отзыв
			if (reviewText.value === "") {
				reviewText.classList.add("error");
			} else {
				reviewText.classList.remove("error");
			}

			// проверяем всё ли океюшки
			if (
				!starsWrapper.classList.contains("error") &&
				!reviewText.classList.contains("error")
			) {
				console.log("validated");

				sendRequest(
					product.value,
					starsWrapper.dataset.totalValue,
					reviewText.value,
					user_name.value,
					user_id.value
				);

				closePopup(popUp);

				/// открываем попас благодарностями
				popUpThanks.classList.add("active");
			} else {
				console.log("NOT validated");
			}
		});
	}

	$('.edit-review__btn').click(function () {
		var reviewId = $(this).data('review-edit');
		var reviewText = $('#review-' + reviewId).data('text');
		var reviewName = $('#review-' + reviewId).data('name');
		var reviewRating = $('#review-' + reviewId).data('rating');
		var reviewProduct = $('#review-' + reviewId).data('productid');

		$('.js_write_a_review_done .js_write_a_review_input').val(reviewText);
		$('.js_write_a_review_done .js_input_name').val(reviewName);
		$('.js_write_a_review_done .js_active_rating').prop('data-total-value', reviewRating);
		$('.js_write_a_review_done .review-id').val(reviewId);
		$('.js_write_a_review_done .review-product-id').val(reviewProduct);

		console.log(reviewText);
	})

	// попап "Оставить отзыв о товаре Заполненный отзыв"
	if (document.querySelector(".js_write_a_review_done")) {
		const popUp = document.querySelector(".js_write_a_review_done");
		const popUpThanks = document.querySelector(".js_write_a_review_thanks");
		const form = popUp.querySelector(".js_popup_form");
		const nameInput = popUp.querySelector(".js_input_name");
		const idInput = popUp.querySelector(".review-id");
		const idProduct = popUp.querySelector(".review-product-id");
		const okBtn = popUp.querySelector(".js_yes_btn");
		const canselBtn = popUp.querySelector(".js_cansel_btn");
		const reviewText = popUp.querySelector(".js_write_a_review_input");
		const starsWrapper = popUp.querySelector(".js_validation_rating");
		const stars = popUp.querySelectorAll(".js_rating_item");


		// изменяем отзыв
		async function changeReview(textReview, idReview, nameReview, ratingReview, prodReview) {
			try {
				// запускаем индикатор загрузки
				okBtn.classList.add("isLoading");

				/*if (!resp.ok) {
					throw new Error(resp.status);
				}*/

				// отправляем запрос
				BX.ajax({
					url: '/ajax/review.php',
					method: 'POST',
					data: {
						action: 'update',
						id: idReview,
						name: nameReview,
						rating: ratingReview,
						text: textReview,
						product: prodReview
					},
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						$('#review-' + idReview + ' .my-review__text').text(textReview);
						console.log(data);
					},
				});


				// let respBody = await resp.json()

				// удаляем индикутор загрузки
				okBtn.classList.remove("isLoading");
				// закрываем попап
				closePopup(popUp);
				// показываем попап успех
				popUpThanks.classList.add("active");
			} catch (err) {
				console.log("Ошибка загрузки", err);
				// удаляем индикутор загрузки
				okBtn.classList.remove("isLoading");
				// закрываем попап
				closePopup(popUp);
				// показываем попап провал
				popupFail.classList.add("active");
			}
		}

		canselBtn.addEventListener("click", () => {
			closePopup(popUp);
		});

		clearInputError(reviewText);

		// самоочищение звезд по клику
		stars.forEach((star) => {
			star.addEventListener("click", () => {
				stars.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});

		form.addEventListener("submit", (e) => {
			e.preventDefault();

			// проверяем звезды
			if (starsWrapper.dataset.totalValue === "") {
				stars.forEach((star) => {
					star.classList.add("error");
				});

				starsWrapper.classList.add("error");
			} else {
				stars.forEach((star) => {
					star.classList.remove("error");
				});

				starsWrapper.classList.remove("error");
			}

			// проверяем имя
			if (nameInput.value === "") {
				nameInput.classList.add("error");
			} else {
				nameInput.classList.remove("error");
			}

			// проверяем отзыв
			if (reviewText.value === "") {
				reviewText.classList.add("error");
			} else {
				reviewText.classList.remove("error");
			}

			// проверяем всё ли океюшки
			if (
				!starsWrapper.classList.contains("error") &&
				!reviewText.classList.contains("error") &&
				!nameInput.classList.contains("error")
			) {
				changeReview(reviewText.value, idInput.value, nameInput.value, starsWrapper.dataset.totalValue, idProduct.value);
			} else {
				console.log("NOT validated");
			}
		});

	}

	// попап "Оцените качество заказа Доставка"
	if (document.querySelector(".js_popup_rate_an_order_delivery")) {
		const popUp = document.querySelector(".js_popup_rate_an_order_delivery");
		const popUpThanks = document.querySelector(
			".js_popup_rate_an_order_thanks"
		);
		const form = popUp.querySelector(".js_popup_form");
		const addBtn = document.querySelector(".js_write_a_review_add");
		const reviewText = popUp.querySelector(".js_write_a_review_input");
		const starsGroup1 = popUp.querySelector(".js_validation_rating_1");
		const stars1 = starsGroup1.querySelectorAll(".js_rating_item");
		const starsGroup2 = popUp.querySelector(".js_validation_rating_2");
		const stars2 = starsGroup2.querySelectorAll(".js_rating_item");
		const starsGroup3 = popUp.querySelector(".js_validation_rating_3");
		const stars3 = starsGroup3.querySelectorAll(".js_rating_item");

		clearInputError(reviewText);

		// запрос из попапа Оцените качество заказа Доставка
		async function sendRequest(order, star1, star2, star3, text) {
			console.log(
				"запрос из попапа Оцените качество заказа Доставка >>",
				order,
				star1,
				star2,
				star3,
				text
			);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log(
					"Ошибка запроса из попапа Оцените качество заказа Доставка",
					err
				);
			}
		}

		// самоочищение звезд по клику Блок 1
		stars1.forEach((star) => {
			star.addEventListener("click", () => {
				stars1.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});
		// самоочищение звезд по клику Блок 2
		stars2.forEach((star) => {
			star.addEventListener("click", () => {
				stars2.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});
		// самоочищение звезд по клику Блок 3
		stars3.forEach((star) => {
			star.addEventListener("click", () => {
				stars3.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});

		form.addEventListener("submit", (e) => {
			e.preventDefault();

			// проверяем звезды
			console.log(starsGroup1.dataset.totalValue);
			if (starsGroup1.dataset.totalValue === "") {
				stars1.forEach((star) => {
					star.classList.add("error");
				});

				starsGroup1.classList.add("error");
			} else {
				stars1.forEach((star) => {
					star.classList.remove("error");
				});

				starsGroup1.classList.remove("error");
			}
			// проверяем звезды
			console.log(starsGroup2.dataset.totalValue);
			if (starsGroup2.dataset.totalValue === "") {
				stars2.forEach((star) => {
					star.classList.add("error");
				});

				starsGroup2.classList.add("error");
			} else {
				stars2.forEach((star) => {
					star.classList.remove("error");
				});

				starsGroup2.classList.remove("error");
			}
			// проверяем звезды
			console.log(starsGroup3.dataset.totalValue);
			if (starsGroup3.dataset.totalValue === "") {
				stars3.forEach((star) => {
					star.classList.add("error");
				});

				starsGroup3.classList.add("error");
			} else {
				stars3.forEach((star) => {
					star.classList.remove("error");
				});

				starsGroup3.classList.remove("error");
			}

			// проверяем отзыв
			if (reviewText.value === "") {
				reviewText.classList.add("error");
			} else {
				reviewText.classList.remove("error");
			}

			// проверяем всё ли океюшки
			if (
				!starsGroup1.classList.contains("error") &&
				!starsGroup2.classList.contains("error") &&
				!starsGroup3.classList.contains("error") &&
				!reviewText.classList.contains("error")
			) {
				console.log("validated");
				closePopup(popUp);

				sendRequest(
					popUp.dataset.orderid,
					starsGroup1.dataset.totalValue,
					starsGroup2.dataset.totalValue,
					starsGroup3.dataset.totalValue,
					reviewText.value
				);

				/// открываем попас благодарностями
				popUpThanks.classList.add("active");
			} else {
				console.log("NOT validated");
			}
		});
	}

	// попап "Оцените качество заказа Самовывоз"
	if (document.querySelector(".js_popup_rate_an_order_pickup")) {
		const popUp = document.querySelector(".js_popup_rate_an_order_pickup");
		const popUpThanks = document.querySelector(
			".js_popup_rate_an_order_thanks"
		);
		const form = popUp.querySelector(".js_popup_form");
		const addBtn = document.querySelector(".js_write_a_review_add");
		const reviewText = popUp.querySelector(".js_write_a_review_input");
		const starsGroup1 = popUp.querySelector(".js_validation_rating_1");
		const stars1 = starsGroup1.querySelectorAll(".js_rating_item");
		const starsGroup2 = popUp.querySelector(".js_validation_rating_2");
		const stars2 = starsGroup2.querySelectorAll(".js_rating_item");
		const starsGroup3 = popUp.querySelector(".js_validation_rating_3");
		const stars3 = starsGroup3.querySelectorAll(".js_rating_item");

		clearInputError(reviewText);

		// запрос из попапа Оцените качество заказа Самовывоз
		async function sendRequest(order, star1, star2, star3, text) {
			console.log(
				"запрос из попапа Оцените качество заказа Самовывоз >>",
				order,
				star1,
				star2,
				star3,
				text
			);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log(
					"Ошибка запроса из попапа Оцените качество заказа Самовывоз",
					err
				);
			}
		}

		// самоочищение звезд по клику Блок 1
		stars1.forEach((star) => {
			star.addEventListener("click", () => {
				stars1.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});
		// самоочищение звезд по клику Блок 2
		stars2.forEach((star) => {
			star.addEventListener("click", () => {
				stars2.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});
		// самоочищение звезд по клику Блок 3
		stars3.forEach((star) => {
			star.addEventListener("click", () => {
				stars3.forEach((star) => {
					star.classList.remove("error");
				});
			});
		});

		form.addEventListener("submit", (e) => {
			e.preventDefault();

			// проверяем звезды
			console.log(starsGroup1.dataset.totalValue);
			if (starsGroup1.dataset.totalValue === "") {
				stars1.forEach((star) => {
					star.classList.add("error");
				});

				starsGroup1.classList.add("error");
			} else {
				stars1.forEach((star) => {
					star.classList.remove("error");
				});

				starsGroup1.classList.remove("error");
			}
			// проверяем звезды
			console.log(starsGroup2.dataset.totalValue);
			if (starsGroup2.dataset.totalValue === "") {
				stars2.forEach((star) => {
					star.classList.add("error");
				});

				starsGroup2.classList.add("error");
			} else {
				stars2.forEach((star) => {
					star.classList.remove("error");
				});

				starsGroup2.classList.remove("error");
			}
			// проверяем звезды
			console.log(starsGroup3.dataset.totalValue);
			if (starsGroup3.dataset.totalValue === "") {
				stars3.forEach((star) => {
					star.classList.add("error");
				});

				starsGroup3.classList.add("error");
			} else {
				stars3.forEach((star) => {
					star.classList.remove("error");
				});

				starsGroup3.classList.remove("error");
			}

			// проверяем отзыв
			if (reviewText.value === "") {
				reviewText.classList.add("error");
			} else {
				reviewText.classList.remove("error");
			}

			// проверяем всё ли океюшки
			if (
				!starsGroup1.classList.contains("error") &&
				!starsGroup2.classList.contains("error") &&
				!starsGroup3.classList.contains("error") &&
				!reviewText.classList.contains("error")
			) {
				console.log("validated");
				closePopup(popUp);

				sendRequest(
					popUp.dataset.orderid,
					starsGroup1.dataset.totalValue,
					starsGroup2.dataset.totalValue,
					starsGroup3.dataset.totalValue,
					reviewText.value
				);

				/// открываем попап с благодарностями
				popUpThanks.classList.add("active");
			} else {
				console.log("NOT validated");
			}
		});
	}

	// попап Отправить резюме
	if (document.querySelector(".js_popup_send_cv")) {
		const popup = document.querySelector(".js_popup_send_cv");
		const popupSuccess = document.querySelector(".js_popup_send_cv_success");
		const popupOrderForm = document.querySelector(".js_popup_send_cv");
		const nameInput = popupOrderForm.querySelector(".js_input_name");
		const lastnameInput = popupOrderForm.querySelector(".js_input_lastname");
		const emailInput = popupOrderForm.querySelector(".js_input_email");
		const phoneInput = popupOrderForm.querySelector(".js_input_masked_phone");
		const hiddenFilesInput = document.querySelector(
			".js_cv_hidden_files_input"
		);
		const visiblefilesInput = document.querySelector(
			".js_cv_visible_files_input"
		);

		// запрос из попапа Отправить резюме
		async function sendRequest(
			nameInput,
			lastnameInput,
			emailInput,
			phoneInput,
			hiddenFilesInput
		) {
			console.log(
				"запрос из попапа Отправить резюме >>",
				nameInput,
				lastnameInput,
				emailInput,
				phoneInput,
				hiddenFilesInput
			);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log("Ошибка запроса из попапа Отправить резюме", err);
			}
		}

		//переносим путь к файлу из реального скрытого инпута в фейковый
		hiddenFilesInput.addEventListener("change", () => {
			console.log("hiddenFilesInput.value", hiddenFilesInput.value);
			visiblefilesInput.innerHTML = hiddenFilesInput.value;
		});
		hiddenFilesInput.addEventListener("click", () => {
			visiblefilesInput.classList.remove("error");
		});

		// валидация
		popupOrderForm.addEventListener("submit", (e) => {
			e.preventDefault();
			// валидируем имя
			validateForm(nameInput);
			// валидируем фамилию
			validateForm(lastnameInput);
			// валидируем имя
			validateForm(emailInput);
			// валидируем имя
			validateForm(phoneInput);
			// валиидруем поле зегрузки файла
			if (hiddenFilesInput.value !== "") {
			} else {
				visiblefilesInput.classList.add("error");
			}
			if (!popupOrderForm.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);
				sendRequest(
					nameInput.value,
					lastnameInput.value,
					emailInput.value,
					phoneInput.value,
					hiddenFilesInput.value
				);
				console.log("validate");
				closePopup(popup);
				popupSuccess.classList.add("active");
			} else {
				console.log("no-validate");
			}
		});
	}

	// попап Сделать заказ
	if (document.querySelector(".js_popup_make_an_order")) {
		const popup = document.querySelector(".js_popup_make_an_order");
		const popupSuccess = document.querySelector(
			".js_popup_make_an_order_success"
		);
		const popupOrderForm = popup.querySelector(".js_popup_order_form");
		const nameInput = popup.querySelector(".js_input_name");
		const phoneInput = popup.querySelector(".js_input_masked_phone");
		const okBtn = popup.querySelector(".js_ok_btn");

		// запрос из попапа Сообщить о поступлении
		async function makeAnOrderRequest(product) {
			console.log("запрос из попапа Сделать заказ >>", product);
			okBtn.classList.add("isLoading");
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					okBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				okBtn.classList.remove("isLoading");
			} catch (err) {
				okBtn.classList.remove("isLoading");
				console.log("Ошибка запроса из попапа Сделать заказ", err);
			}
		}

		// валидация
		popupOrderForm.addEventListener("submit", (e) => {
			e.preventDefault();
			// валидируем имя
			validateForm(nameInput);
			// валидируем имя
			validateForm(phoneInput);
			if (!popupOrderForm.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);
				makeAnOrderRequest(popup.dataset.productid);
				console.log("validate");
				closePopup(popup);
				popupSuccess.classList.add("active");
			} else {
				console.log("no-validate");
			}
		});
	}

	// попап Сообщить о поступлении
	if (document.querySelector(".js_popup_report")) {
		const popup = document.querySelector(".js_popup_report");
		const popupSuccess = document.querySelector(".js_popup_report_success");
		const popupOrderForm = popup.querySelector(".js_popup_order_form");
		const nameInput = popup.querySelector(".js_input_name");
		const phoneInput = popup.querySelector(".js_input_masked_phone");
		const okBtn = popup.querySelector(".js_ok_btn");

		// запрос из попапа Сообщить о поступлении
		async function informAboutProductAvailibility(product) {
			console.log("запрос из попапа Сообщить о поступлении >>", product);
			okBtn.classList.add("isLoading");
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					okBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				okBtn.classList.remove("isLoading");
			} catch (err) {
				okBtn.classList.remove("isLoading");
				console.log("Ошибка запроса из попапа Сообщить о поступлении", err);
			}
		}

		// валидация
		popupOrderForm.addEventListener("submit", (e) => {
			e.preventDefault();
			// валидируем имя
			validateForm(nameInput);
			// валидируем имя
			validateForm(phoneInput);
			if (!popupOrderForm.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);
				informAboutProductAvailibility(popup.dataset.productid);
				console.log("validate");
				closePopup(popup);
				popupSuccess.classList.add("active");
			} else {
				console.log("no-validate");
			}
		});
	}

	// попап Вы хотите удалить отзыв?
	$('.delete-review__btn').click(function () {
		var reviewId = $(this).data('review-delete');
		$('.js_popup_delete_review .review-id').val(reviewId);
	})

	if (document.querySelector(".js_popup_delete_review")) {
		const popup = document.querySelector(".js_popup_delete_review");
		const popupOk = document.querySelector(".js_popup_delete_review_success");
		const popupFail = document.querySelector(".js_popup_delete_review_fail");
		const okBtn = popup.querySelector(".js_ok_btn");
		const offBtn = popup.querySelector(".js_off_btn");
		const idReview = popup.querySelector(".review-id");

		// удаляем отзыв
		async function deleteReview(idReview) {
			try {
				// запускаем индикатор загрузки
				okBtn.classList.add("isLoading");

				// отправляем запрос
				// let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
				// 	method: "DELETE",
				// });
				//
				// if (!resp.ok) {
				// 	throw new Error(resp.status);
				// }

				// отправляем запрос
				BX.ajax({
					url: '/ajax/review.php',
					method: 'POST',
					data: {
						action: 'delete',
						id: idReview,
					},
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						$('#review-' + idReview).hide();
						console.log(data);
					},
				});

				// удаляем индикутор загрузки
				okBtn.classList.remove("isLoading");
				// закрываем попап
				closePopup(popup);
				// показываем попап успех
				popupOk.classList.add("active");
			} catch (err) {
				console.log("Ошибка загрузки", err);
				// удаляем индикутор загрузки
				okBtn.classList.remove("isLoading");
				// закрываем попап
				closePopup(popup);
				// показываем попап провал
				popupFail.classList.add("active");
			}
		}

		offBtn.addEventListener("click", () => {
			closePopup(popup);
		});

		okBtn.addEventListener("click", () => {
			// удаляем отзыв
			deleteReview(idReview.value);
		});
	}

	$(".cabinet__address-delet-btn").click(function (event) {
		$('.js_popup_delete_address .js_address_id__delete').val($(this).data('address-id'));
	});

	/*$(".js_popup_delete_pharmacy").click(function (event) {
		const popup = document.querySelector(".js_popup_delete_pharmacy");
		const popupOk = document.querySelector(".js_popup_delete_pharmacy_success");
		const pharmacyInput = $(this).data("pharmacy-id");
		const okBtn = popup.querySelector(".js_ok_btn");
		const offBtn = popup.querySelector(".js_off_btn");
		const href = $(this).attr('href');
		console.log(pharmacyInput);

		// запрос из попапа Точно хотите удалить аптеку?
		function sendRequest(pharmacyInput) {
			console.log("запрос из попапа Точно хотите удалить аптеку? >>", pharmacyInput);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				BX.ajax({
					url: '/ajax/pharmacy.php',
					method: 'POST',
					data: {
						action: 'delete',
						id: pharmacyInput,
					},
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						$('#pharmacy-' + pharmacyInput).hide();
						//window.location.href = '/personal/?pharmacy=ok';
					},
				});
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log(
					"Ошибка запроса из попапа Точно хотите удалить аптеку?",
					err
				);
			}
		}

		/!*offBtn.addEventListener("click", () => {
			closePopup(popup);
		});*!/

		// okBtn.addEventListener("click", () => {
		$(okBtn).click(function () {
			console.log(pharmacyInput);
			/!*sendRequest(pharmacyInput);
			closePopup(popup);
			popupOk.classList.add("active");*!/
		});
	});*/

	// попап Вы хотите удалить аптеку?
	if (document.querySelector(".js_popup_delete_pharmacy")) {

	}

	// попап Изменить пароль в личном кабинете
	if (document.querySelector(".js_popup_update_password")) {
		const popup = document.querySelector(".js_popup_update_password");
		const popupOk = document.querySelector(".js_popup_update_password_success");
		const popupOrderForm = popup.querySelector(".js_popup_form");
		const formInputs = popup.querySelectorAll("input");
		const password1Input = popup.querySelector(".js_input_new_password_1");
		const password2Input = popup.querySelector(".js_input_new_password_2");
		const btnOff = popup.querySelector(".js_off_btn");

		// запрос из попапа Изменить пароль в личном кабинете?
		async function sendRequest(oldPassword, newPassword) {
			console.log(
				"запрос из попапа Изменить пароль в личном кабинете? >>",
				oldPassword,
				newPassword
			);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log(
					"Ошибка запроса из попапа Изменить пароль в личном кабинете",
					err
				);
			}
		}

		btnOff.addEventListener("click", () => {
			closePopup(popup);
		});

		// валидация
		popupOrderForm.addEventListener("submit", (e) => {
			e.preventDefault();

			formInputs.forEach((input) => {
				// перебираем все инпуты в форме;

				validateForm(input);
			});

			// проверяем, что пароли совпадают
			if (
				password1Input.value.length < 6 ||
				password1Input.value !== password2Input.value
			) {
				password1Input.classList.add("error");
				password2Input.classList.add("error");
			} else {
				password1Input.classList.remove("error");
				password2Input.classList.remove("error");
			}

			if (!popupOrderForm.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

				sendRequest(formInputs[0].value, password1Input.value);

				console.log("validate");
				closePopup(popup);
				popupOk.classList.add("active");
			} else {
				console.log("no-validate");
			}
		});
	}

	// попап Вы хотите удалить адрес?
	if (document.querySelector(".js_popup_delete_address")) {
		const popup = document.querySelector(".js_popup_delete_address");
		const popupOk = document.querySelector(".js_popup_delete_address_success");
		const okBtn = popup.querySelector(".js_ok_btn");
		const offBtn = popup.querySelector(".js_off_btn");
		const addressID = popup.querySelector('.js_address_id__delete');

		// запрос из попапа Вы хотите удалить адрес?
		async function sendRequest(address) {
			console.log("запрос из попапа Вы хотите удалить адрес? >>", address);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				BX.ajax({
					url: '/ajax/address.php',
					method: 'POST',
					data: {
						action: 'delete',
						id: addressID.value,
					},
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						window.location.href = '/personal/?address=ok';
						console.log("Удалено");
					},
				});
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log("Ошибка запроса из попапа Вы хотите удалить адрес", err);
			}
		}

		offBtn.addEventListener("click", () => {
			closePopup(popup);
		});

		okBtn.addEventListener("click", () => {
			sendRequest(popup.dataset.address);
			closePopup(popup);
			popupOk.classList.add("active");
		});
	}

	// попап Сохранение адреса в личном кабинете
	$('.delete-review__btn').click(function () {
		var reviewId = $(this).data('review-delete');
		$('.js_popup_delete_review .review-id').val(reviewId);
	});

	if (document.querySelector(".js_popup_save_address")) {
		const popup = document.querySelector(".js_popup_save_address");
		const popupOrderForm = popup.querySelector(".js_popup_form");
		// const okBtn = popup.querySelector(".js_ok_btn");
		const formInputs = popup.querySelectorAll("input");

		const cityInput = popup.querySelector(".js_city_input");
		const streetInput = popup.querySelector(".js_street_input");
		const homeInput = popup.querySelector(".js_home_input");
		const literaInput = popup.querySelector(".js_home_litera_input");
		const flatInput = popup.querySelector(".js_flat_input");
		const frontDoorInput = popup.querySelector(".js_front_door_input");
		const floorInput = popup.querySelector(".js_floor_input");
		const codeInput = popup.querySelector(".js_floor_code_input");
		const commentInput = popup.querySelector(".js_comment_input");
		const userInput = popup.querySelector(".user-id");

		// запрос из попапа Сохранение адреса в личном кабинете
		async function addAddress(
			cityInput,
			streetInput,
			homeInput,
			literaInput,
			flatInput,
			frontDoorInput,
			floorInput,
			codeInputput,
			commentInput,
			userInput
		) {
			console.log(
				"запрос из попапа Сохранение адреса в личном кабинете >>",
				cityInput,
				streetInput,
				homeInput,
				literaInput,
				flatInput,
				frontDoorInput,
				floorInput,
				codeInputput,
				commentInput
			);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				BX.ajax({
					url: '/ajax/address.php',
					method: 'POST',
					data: {
						action: 'add',
						city: cityInput,
						street: streetInput,
						house: homeInput,
						litera: literaInput,
						flat: flatInput,
						frontDoor: frontDoorInput,
						floor: flatInput,
						code: codeInputput,
						comment: commentInput,
						user: userInput
					},
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						// window.location.href = '/personal/?address=ok';
						console.log("Сохранен");
					},
				});
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log(
					"Ошибка запроса из попапа Сохранение адреса в личном кабинете",
					err
				);
			}
		}

		// валидация
		popupOrderForm.addEventListener("submit", (e) => {
			e.preventDefault();

			formInputs.forEach((input) => {
				// перебираем все инпуты в форме;

				validateForm(input);
			});

			if (!popupOrderForm.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);
				console.log("validate");

				addAddress(
					cityInput.value,
					streetInput.value,
					homeInput.value,
					literaInput.value,
					flatInput.value,
					frontDoorInput.value,
					floorInput.value,
					codeInput.value,
					commentInput.value,
					userInput.value
				);
				closePopup(popup);
				document.location.href = '/personal/?address=ok';
			} else {
				console.log("no-validate");
			}
		});
	}

	// попап Изменение адреса в личном кабинете
	if (document.querySelector(".js_popup_change_address.active")) {
		const popup = document.querySelector(".js_popup_change_address.active");
		const popupOrderForm = popup.querySelector(".js_popup_form");
		const formInputs = popup.querySelectorAll("input");
		const btnOff = popup.querySelector(".js_off_btn");
		const btnOk = popup.querySelector(".js_ok_btn");

		const cityInput = popup.querySelector(".js_city_input");
		const streetInput = popup.querySelector(".js_street_input");
		const homeInput = popup.querySelector(".js_home_input");
		const literaInput = popup.querySelector(".js_korpus_input");
		const flatInput = popup.querySelector(".js_flat_input");
		const frontDoorInput = popup.querySelector(".js_front_door_input");
		const floorInput = popup.querySelector(".js_floor_input");
		const codeInput = popup.querySelector(".js_floor_code_input");
		const commentInput = popup.querySelector(".js_comment_input");
		const userInput = popup.querySelector(".js_user-id");
		const id = popup.querySelector(".js_id_input");

		// запрос из попапа Изменение адреса
		async function updateAddress(
			id,
			cityInput,
			streetInput,
			homeInput,
			literaInput,
			flatInput,
			frontDoorInput,
			floorInput,
			codeInputput,
			commentInput,
			userInput
		) {
			console.log(
				"запрос из попапа Изменение адреса >>",
				id,
				cityInput,
				streetInput,
				homeInput,
				literaInput,
				flatInput,
				frontDoorInput,
				floorInput,
				codeInputput,
				commentInput,
				userInput
			);
			// cookieBtn.classList.add('isLoading')
			try {
				BX.ajax({
					url: '/ajax/address.php',
					method: 'POST',
					data: {
						action: 'update',
						id: id,
						city: cityInput,
						street: streetInput,
						house: homeInput,
						litera: literaInput,
						flat: flatInput,
						frontDoor: frontDoorInput,
						floor: flatInput,
						code: codeInputput,
						comment: commentInput,
						user: userInput
					},
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {
						// window.location.href = '/personal/?address=ok';
						console.log("Изменен");
					},
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log("Ошибка запроса из попапа Изменение адреса", err);
			}
		}

		btnOff.addEventListener("click", () => {
			closePopup(popup);
		});

		// валидация
		btnOk.addEventListener("click", (e) => {
			e.preventDefault();

			formInputs.forEach((input) => {
				// перебираем все инпуты в форме;

				validateForm(input);
			});

			if (!popupOrderForm.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

				updateAddress(
					id.value,
					cityInput.value,
					streetInput.value,
					homeInput.value,
					literaInput.value,
					flatInput.value,
					frontDoorInput.value,
					floorInput.value,
					codeInput.value,
					commentInput.value,
					userInput.value
				);

				console.log("validate");
				closePopup(popup);
			} else {
				console.log("no-validate");
			}
		});
	}

	// страница Обратная связь
	if (document.querySelector(".js_feedback")) {
		const popup = document.querySelector(".js_popup_feedback_success");
		const form = document.querySelector(".js_feedback_form");
		const formInputs = form.querySelectorAll("input");
		const phone = form.querySelector(".js_input_masked_phone");
		const message = form.querySelector(".js_input_messege");

		clearInputError(message);

		// валидация
		form.addEventListener("submit", (e) => {
			e.preventDefault();

			formInputs.forEach((input) => {
				// перебираем все инпуты в форме;

				validateForm(input);
				clearInputError(input);
			});

			// проверяем сообщение
			if (message.value == "") {
				message.classList.add("error");
			} else {
				message.classList.remove("error");
			}

			// если телефон не не начинали заполнять, то не валидировать его
			if (phone.value == "") {
				phone.classList.remove("error");
			}

			if (!form.querySelector(".error")) {
				//проверяем, чтоб все инпуты прошли валидацию (чтоб не было в форме ни одного элемента с класссом error);

				// сюда пишем команды, которые должны сработать после успешной валидации;

				console.log("validate");

				popup.classList.add("active");

				form.submit();

				formInputs.forEach((input) => {
					// перебираем все инпуты в форме;
					input.value = "";
				});

				message.value = "";
			} else {
				console.log("no-validate");
			}
		});
	}

	// попап Выбор сохраненной аптеки
	if (document.querySelector(".js_popup_select_saved_pharmacy")) {
		const popup = document.querySelector(".js_popup_select_saved_pharmacy");
		const cards = popup.querySelectorAll(".js_pharmacy_card");
		const input = document.querySelector(".js_checkout_address_input");

		// запрос из попапа Выбор сохраненной аптеки
		async function sendRequest(pharmacy) {
			console.log("запрос из попапа Выбор сохраненной аптеки >>", pharmacy);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log("Ошибка запроса из попапа Выбор сохраненной аптеки", err);
			}
		}

		cards.forEach((card) => {
			const btn = card.querySelector(".js_submit_btn");
			const address = card.querySelector(".js_target_address");

			btn.addEventListener("click", (e) => {
				e.preventDefault();

				input.value = address.innerHTML.trim();

				sendRequest(card.dataset.pharmacy);

				closePopup(popup);
			});
		});
	}

	// попап Сохранить аптеку
	if (document.querySelector(".js_popup_save_pharmacy")) {
		const popup = document.querySelector(".js_popup_save_pharmacy");
		const cards = popup.querySelectorAll(".js_pharmacy_card");
		const input = document.querySelector(".js_checkout_address_input");
		const openBtn = document.querySelector(".js_btn_popup_save_pharmacy");
		const submitBtn = popup.querySelector(".js_submit_btn");

		// запрос из попапа Сохранить аптеку
		async function sendRequest(address) {
			console.log("запрос из попапа Сохранить аптеку >>", address);
			// cookieBtn.classList.add('isLoading')
			try {
				// отправляем запрос
				let resp = await fetch("https://jsonplaceholder.typicode.com/posts/1", {
					method: "PUT",
					body: JSON.stringify({
						id: 1,
						title: "foo",
						body: "bar",
						userId: 1,
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
					},
				});
				if (!resp.ok) {
					cookieBtn.classList.remove("isLoading");
					throw new Error(resp.status);
				}
				// cookieBtn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log("Ошибка запроса из попапа Сохранить аптеку", err);
			}
		}

		// input.addEventListener('keyup', (e) => {
		//     e.preventDefault()

		//     // мб условие стоит и поменять
		//     if(input.value.length > 5) {
		//         openBtn.classList.add('active')
		//     } else {
		//         openBtn.classList.remove('active')
		//     }
		// })

		submitBtn.addEventListener("click", (e) => {
			e.preventDefault();

			openBtn.innerHTML = "Аптека сохранена";

			sendRequest(popup.dataset.pharmacy);

			closePopup(popup);
		});
	}

	// попап Выбор сохраненного адреса
	if (document.querySelector(".js_popup_saved_address")) {
		const popup = document.querySelector(".js_popup_saved_address");
		const streetInput = document.querySelector(".js_checkout_street_input");
		const profileInput = document.querySelector("#PROFILE_ID");
		const homeInput = document.querySelector(".js_checkout_home_input");
		const literaInput = document.querySelector(
			".js_checkout_home_litera_input"
		);
		const flatInput = document.querySelector(".js_checkout_flat_input");
		const frontDoorInput = document.querySelector(
			".js_checkout_front_door_input"
		);
		const floorInput = document.querySelector(".js_checkout_floor_input");
		const codeInput = document.querySelector(".js_checkout_floor_code_input");
		const commentInput = document.querySelector(".js_checkout_comment_input");
		const cards = popup.querySelectorAll(".js_address_card");

		// запрос из попапа Выбор сохраненного адреса
		async function sendRequest(address) {
			console.log("запрос из попапа Выбор сохраненного адреса >>", address);
			try {
			} catch (err) {
				console.log("Ошибка запроса из попапа Выбор сохраненного адреса", err);
			}
		}

		cards.forEach((card) => {
			const id = card.dataset.profileid;
			const btn = card.querySelector(".js_submit_btn");
			const popupStreetInput = card.querySelector(".js_input_street");
			const popupHomeInput = card.querySelector(".js_checkout_home_input");
			const popupLiteraInput = card.querySelector(
				".js_checkout_home_litera_input"
			);
			const popupFlatInput = card.querySelector(".js_checkout_flat_input");
			const popupFrontDoorInput = card.querySelector(
				".js_checkout_front_door_input"
			);
			const popupFloorInput = card.querySelector(".js_checkout_floor_input");
			const popupCodeInput = card.querySelector(
				".js_checkout_floor_code_input"
			);
			const popupCommentInput = card.querySelector(
				".js_checkout_comment_input"
			);

			// при выборе адреса перекинуть значения на страницу
			btn.addEventListener("click", (e) => {
				e.preventDefault();

				profileInput.value = id;
				streetInput.value = popupStreetInput.value.trim();
				homeInput.value = popupHomeInput.value.trim();
				literaInput.value = popupLiteraInput.value.trim();
				flatInput.value = popupFlatInput.value.trim();
				frontDoorInput.value = popupFrontDoorInput.value.trim();
				floorInput.value = popupFloorInput.value.trim();
				codeInput.value = popupCodeInput.value.trim();
				commentInput.value = popupCommentInput.value.trim();

				// console.log(literaInput);
				closePopup(popup);
			});
		});
	}

	// попап Сохранение адреса
	if (document.querySelector(".js_popup_save_address")) {
		const popup = document.querySelector(".js_popup_save_address");
		const popupOpenBtns = document.querySelectorAll(
			".js_save_address_popup_btn"
		);
		const streetInput = document.querySelector(".js_checkout_street_input");
		const homeInput = document.querySelector(".js_checkout_home_input");
		const literaInput = document.querySelector(
			".js_checkout_home_litera_input"
		);
		const flatInput = document.querySelector(".js_checkout_flat_input");
		const frontDoorInput = document.querySelector(
			".js_checkout_front_door_input"
		);
		const floorInput = document.querySelector(".js_checkout_floor_input");
		const codeInput = document.querySelector(".js_checkout_floor_code_input");
		const commentInput = document.querySelector(".js_checkout_comment_input");
		const card = popup.querySelector(".js_address_card");

		// показать кнопку при заполнеии улицы, дома, квартиры
		if (streetInput) {
			streetInput.addEventListener("keyup", () => {
				if (
					streetInput.value.length > 5 &&
					homeInput.value.length >= 1 &&
					flatInput.value.length >= 1
				) {
					popupOpenBtns.forEach((btn) => {
						btn.classList.add("active");
						console.log("збс");
					});
				} else {
					popupOpenBtns.forEach((btn) => {
						btn.classList.remove("active");
						console.log("не збс");
					});
				}
			});
			flatInput.addEventListener("keyup", () => {
				if (
					streetInput.value.length > 5 &&
					homeInput.value.length >= 1 &&
					flatInput.value.length >= 1
				) {
					popupOpenBtns.forEach((btn) => {
						btn.classList.add("active");
						console.log("збс");
					});
				} else {
					popupOpenBtns.forEach((btn) => {
						btn.classList.remove("active");
						console.log("не збс");
					});
				}
			});
			homeInput.addEventListener("keyup", () => {
				if (
					streetInput.value.length > 5 &&
					homeInput.value.length >= 1 &&
					flatInput.value.length >= 1
				) {
					popupOpenBtns.forEach((btn) => {
						btn.classList.add("active");
						console.log("збс");
					});
				} else {
					popupOpenBtns.forEach((btn) => {
						btn.classList.remove("active");
						console.log("не збс");
					});
				}
			});

			const submitBtn = card.querySelector(".js_submit_btn");
			const popupStreetInput = card.querySelector(".js_input_street");
			const popupHomeInput = card.querySelector(".js_checkout_home_input");
			const popupLiteraInput = card.querySelector(
				".js_checkout_home_litera_input"
			);
			const popupFlatInput = card.querySelector(".js_checkout_flat_input");
			const popupFrontDoorInput = card.querySelector(
				".js_checkout_front_door_input"
			);
			const popupFloorInput = card.querySelector(".js_checkout_floor_input");
			const popupCodeInput = card.querySelector(
				".js_checkout_floor_code_input"
			);
			const popupCommentInput = card.querySelector(
				".js_checkout_comment_input"
			);

			// при выборе адреса перекинуть значения на страницу
			popupOpenBtns.forEach((btn) => {
				btn.addEventListener("click", (e) => {
					popupStreetInput.value = streetInput.value.trim();
					popupHomeInput.value = homeInput.value.trim();
					popupLiteraInput.value = literaInput.value.trim();
					popupFlatInput.value = flatInput.value.trim();
					popupFrontDoorInput.value = frontDoorInput.value.trim();
					popupFloorInput.value = floorInput.value.trim();
					popupCodeInput.value = codeInput.value.trim();
					popupCommentInput.value = commentInput.value.trim();
				});
			});

			submitBtn.addEventListener("click", (e) => {
				sendRequest(
					popupStreetInput.value,
					popupHomeInput.value,
					popupLiteraInput.value,
					popupFlatInput.value,
					popupFrontDoorInput.value,
					popupFloorInput.value,
					popupCodeInput.value,
					popupCommentInput.value
				);

				e.preventDefault();

				closePopup(popup);
			});
		}
	}

	// табы на странице Помощь Оплата
	if (document.querySelector(".js_payment_inner")) {
		setTabs(".js_payment_inner", ".js_payment_tab_btn", ".js_payment_item");
	}
	// табы на странице Помощь Доставка
	if (document.querySelector(".js_delivery_inner")) {
		setTabs(".js_delivery_inner", ".js_delivery_tab_btn", ".js_delivery_item");
	}

	// табы на странице О компании Вакансии
	if (document.querySelector(".js_job_inner")) {
		setTabs(".js_job_inner", ".js_job_tab_btn", ".js_job_item");
	}

	// открывающийся пунк описания вакансии на странице О компании Вакансии
	if (document.querySelector(".js_job_spoiler_item")) {
		let items = document.querySelectorAll(".js_job_spoiler_item"); //спойлеры с поисанием

		// открываем спойлер при клике
		items.forEach((item) => {
			item.addEventListener("click", () => {
				// если кликаем на открытый спойлер, то просто закрываем его
				if (item.classList.contains("active")) {
					item.classList.remove("active");
				}
				// если на закрытый, то закрываем все и открываем кликнутый
				else {
					items.forEach((item) => {
						item.classList.remove("active");
					});

					item.classList.add("active");
				}
			});
		});
	}


	// let myMap; //тут храним карту
	// let initProfilePharm;
	/*
	// попап Добавить аптеку в личном кабинете
	if (document.querySelector(".js_popup_add_pharmacy") && !(document.querySelector(".js_checkout_map"))) {
		console.log('init');
		const popup = document.querySelector(".js_popup_add_pharmacy");
		const popupAddressInput = document.querySelector(".js_popup_address_input"); // поле поиска
		const popupSubmitBtn = document.querySelector(
			".js_popup_address_submit_btn"
		); // кнопка подтверждения
		let filterdPharmInfo = pharmInfo; // тут храним всю информацию об аптека, но уже отфильтрованную именно под этот попап

		initProfilePharm = function () {

			myMap = new ymaps.Map("map9", {
				// Создание экземпляра карты и его привязка к контейнеру с заданным id;
				center: [55.62728, 37.586311], // При инициализации карты обязательно нужно указать её центр и коэффициент масштабирования;
				zoom: 10,
				controls: [], // Скрываем элементы управления на карте;
			});

			let collection = new ymaps.GeoObjectCollection(null, {
				// Создаём коллекцию, в которую будем помещать метки (что-то типа массива);
			});

			// из всей информации об аптеках вытаскиваем только координаты
			let collectionCoords = filterdPharmInfo.map((item) => {
				return [item.x, item.y];
			});

			for (let i = 0, l = collectionCoords.length; i < l; i++) {
				// C помощью цикла добавляем все метки в коллекцию;
				// находим по коррдинатам полную информацию об аптеке для вставки в балун
				const ourPoint = filterdPharmInfo.filter((item) => {
					return item.x === collectionCoords[i][0];
				});

				collection.add(
					new ymaps.Placemark(collectionCoords[i], {
						balloonContent: `
                            <div class="big-balloon">
                            <h3 class="big-balloon__address-title">
                               ${ourPoint[0]["title"]}
                            </h3>
    
                            <span class="big-balloon__address-text js_baloon_address">
                                ${ourPoint[0]["address"]}
                            </span>
    
                            <div class="big-balloon__address-open-time-row">
                                <span class="big-balloon__address-open-days">
                                    ${ourPoint[0]["openDays"]}
                                </span>
    
                                <span class="big-balloon__address-open-time">
                                    ${ourPoint[0]["openTime"]}
                                </span>
                            </div>
    
                            <a class="big-balloon__address-phone" href="tel:${ourPoint[0]["phoneForLink"]}">
                                ${ourPoint[0]["phone"]}
                            </a>

                            <button class="big-balloon__choose-btn js_ballon_accept_btn" type="button">
                                Выбрать аптеку
                            </button>
                        </div>
                        `,
					})
				);
				collection.get(i).properties.set("iconContent", `${i + 1}`); // Чтоб в дальнейшем к ней можно было как-то обратиться к нужной метке, добавляем каждой порядковый номер, записываем его в свойство 'iconContent';
			}

			myMap.geoObjects.add(collection); // Добавляем коллекцию с метками на карту;

			collection.options.set("iconLayout", "default#image"); // Необходимо указать данный тип макета;
			collection.options.set("iconImageHref", "/local/templates/main/images/mark.svg"); // Своё изображение иконки метки;
			collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
			collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
			collection.options.set("hideIcon", false); // Размеры метки;

			// при нажатии Выбрать аптеку в балуне
			let map = document.querySelector(".js_checkout_map");

			if (map) {
				map.addEventListener("click", (e) => {
					// проверяем что кликнули в кнопку
					if (e.target.classList.contains("js_ballon_accept_btn")) {
						const targetBalloon = e.target.parentNode;

						const address = targetBalloon.querySelector(".js_baloon_address");

						popupAddressInput.value = address.innerHTML.trim();

						// закрываем балун
						myMap.balloon.close();

						// отображаем только одну метку на карте
						filterPoints(address.innerHTML.trim());
					}
				});
			}

			// при выборе аптеки отобраажем на карте только её
			function filterPoints(address) {
				filterdPharmInfo = pharmInfo.filter((item) => item.address == address);

				myMap.destroy();

				init();
			}

			//перкскакиваем к нужному адресу
			const storeList = document.querySelector(".store__list"); // Список адресов
		}

		ymaps.ready(initProfilePharm); // Дождёмся загрузки API и готовности DOM;

		// ищем подходящте под поиск аптеки
		function searchPoint() {
			filterdPharmInfo = pharmInfo.filter(
				(item) =>
					item.address.includes(popupAddressInput.value) ||
					item.title.includes(popupAddressInput.value)
			);

			myMap.destroy();

			init();
		}

		// оборачиваем поиск в дебаунс чтобы откинуть лишние запросы
		let debouncedSearchPoint = debounce(searchPoint, 1000);

		// запускаем поиск по вводу
		popupAddressInput.addEventListener("keyup", (e) => {
			debouncedSearchPoint();
		});

		// валидируем попап
		clearInputError(popupAddressInput);

		popupSubmitBtn.addEventListener("click", () => {
			if (popupAddressInput.value === "") {
				popupAddressInput.classList.add("error");
			} else {
				console.log('надо бы сохранить', filterdPharmInfo[0]);
				myPharmInfo.push(filterdPharmInfo[0]);
				// console.log('myPharmInfo >>', myPharmInfo);

				// перерисовываем карту на вкладке "сохраненные аптеки"
				myMapInCabinet.destroy();
				initCobinetCard();

				closePopup(popup);
			}
		});
	}*/

	// попап Добавить аптеку в личном кабинете
	/*if(document.querySelector('.js_popup_add_pharmacy')) {

		const popup = document.querySelector('.js_popup_add_pharmacy');
		const popupAddressInput = document.querySelector('.js_popup_address_input') // поле поиска
		const popupSubmitBtn = document.querySelector('.js_popup_address_submit_btn') // кнопка подтверждения
		let filterdPharmInfo = pharmInfo // тут храним всю информацию об аптека, но уже отфильтрованную именно под этот попап

		ymaps.ready(init); // Дождёмся загрузки API и готовности DOM;

		let myMap //тут храним карту

		function init() {

			myMap = new ymaps.Map('map9', { // Создание экземпляра карты и его привязка к контейнеру с заданным id;
				center: [55.627280, 37.586311], // При инициализации карты обязательно нужно указать её центр и коэффициент масштабирования;
				zoom: 10,
				controls: [] // Скрываем элементы управления на карте;
			});

			let collection = new ymaps.GeoObjectCollection(null, { // Создаём коллекцию, в которую будем помещать метки (что-то типа массива);
			});

			// из всей информации об аптеках вытаскиваем только координаты
			let collectionCoords = filterdPharmInfo.map(item => {
				return [item.x, item.y]
			})

			for (let i = 0, l = collectionCoords.length; i < l; i++) { // C помощью цикла добавляем все метки в коллекцию;
				// находим по коррдинатам полную информацию об аптеке для вставки в балун
				const ourPoint = filterdPharmInfo.filter(item => {
					return item.x === collectionCoords[i][0]
				})

				collection.add(new ymaps.Placemark(collectionCoords[i], {
						balloonContent: `
                            <div class="big-balloon">
                            <h3 class="big-balloon__address-title">
                               ${ourPoint[0]['title']}
                            </h3>

                            <span class="big-balloon__address-text js_baloon_address">
                                ${ourPoint[0]['address']}
                            </span>

                            <div class="big-balloon__address-open-time-row">
                                <span class="big-balloon__address-open-days">
                                    ${ourPoint[0]['openDays']}
                                </span>

                                <span class="big-balloon__address-open-time">
                                    ${ourPoint[0]['openTime']}
                                </span>
                            </div>

                            <a class="big-balloon__address-phone" href="tel:${ourPoint[0]['phoneForLink']}">
                                ${ourPoint[0]['phone']}
                            </a>

                            <button class="big-balloon__choose-btn js_ballon_accept_btn" type="button">
                                Выбрать аптеку
                            </button>
                        </div>
                        `
					}

				));
				collection.get(i).properties.set('iconContent', `${i + 1}`); // Чтоб в дальнейшем к ней можно было как-то обратиться к нужной метке, добавляем каждой порядковый номер, записываем его в свойство 'iconContent';
			}

			myMap.geoObjects.add(collection); // Добавляем коллекцию с метками на карту;

			collection.options.set('iconLayout', 'default#image'); // Необходимо указать данный тип макета;
			collection.options.set('iconImageHref', 'images/mark.svg'); // Своё изображение иконки метки;
			collection.options.set('iconImageSize', [28, 37]); // Размеры метки;
			collection.options.set('iconImageSize', [28, 37]); // Размеры метки;
			collection.options.set('hideIcon', false); // Размеры метки;

			// при нажатии Выбрать аптеку в балуне
			let map = document.querySelector('.js_checkout_map')

			if(map) {
				map.addEventListener('click', (e) => {
					// проверяем что кликнули в кнопку
					if(e.target.classList.contains('js_ballon_accept_btn')) {
						const targetBalloon = e.target.parentNode

						const address = targetBalloon.querySelector('.js_baloon_address')

						popupAddressInput.value = address.innerHTML.trim()

						// закрываем балун
						myMap.balloon.close()

						// отображаем только одну метку на карте
						filterPoints(address.innerHTML.trim())
					}
				})
			}

			// при выборе аптеки отобраажем на карте только её
			function filterPoints(address) {
				filterdPharmInfo = pharmInfo.filter(item => item.address == address)

				myMap.destroy()

				init()
			}

			//перкскакиваем к нужному адресу
			const storeList = document.querySelector('.store__list'); // Список адресов
		}

		// ищем подходящте под поиск аптеки
		function searchPoint () {
			filterdPharmInfo = pharmInfo.filter(item => item.address.includes(popupAddressInput.value) || item.title.includes(popupAddressInput.value))

			myMap.destroy()

			init()
		}

		// оборачиваем поиск в дебаунс чтобы откинуть лишние запросы
		let debouncedSearchPoint = debounce(searchPoint, 1000);

		// запускаем поиск по вводу
		popupAddressInput.addEventListener('keyup', (e) => {
			debouncedSearchPoint()
		})

		// валидируем попап
		clearInputError(popupAddressInput)

		popupSubmitBtn.addEventListener('click', () => {
			if(popupAddressInput.value === '') {
				popupAddressInput.classList.add('error')
			} else {

			}
			// console.log('asfsad');
			myPharmInfo.push(filterdPharmInfo[0])
			// console.log(myPharmInfo.id);
			try {
				// отправляем запрос
				BX.ajax({
					url: '/ajax/pharmacy.php',
					method: 'POST',
					data: {
						action: 'add',
						id: myPharmInfo.id,
					},
					dataType: 'json',
					timeout: 30,
					async: true,
					onsuccess: function (data) {

						window.location.href = '/personal/?pharmacy=ok';
						// перерисовываем карту на вкладке "сохраненные аптеки"
						myMapInCabinet.destroy();
						initCobinetCard();

						closePopup(popup);
					},
				});
				// cookieBconsoletn.classList.remove('isLoading')
			} catch (err) {
				// cookieBtn.classList.remove('isLoading')
				console.log(
					"Ошибка запроса из попапа Сохранение адреса в личном кабинете",
					err
				);
			}
		});
	}*/

	// карта на главной странице
	if (document.querySelector(".js_store_buttons")) {
		const storeBtnsWrap = document.querySelector(".js_store_buttons");
		const storeItems = document.querySelectorAll(".js_store_list li");
		//let myMap; //сама карта
		let filterdPharmInfo = pharmInfo; // тут храним всю информацию об аптека, но уже отфильтрованную именно под эту карту

		storeBtnsWrap.addEventListener("click", (e) => {
			const target = e.target;

			if (target.closest(".js_store_btn")) {
				storeBtnsWrap.querySelectorAll(".js_store_btn").forEach((btn) => {
					btn.classList.remove("active");
				});

				target.classList.add("active");

				// фильтруем на карте метки
				console.log("target.dataset.area >>", target.dataset.area);

				filterdPharmInfo = pharmInfo.filter(
					(item) => item.area === target.dataset.area
				);

				// обновляем карту
				//myMap.destroy();
				//init();

				if (target.dataset.category == "all") {
					storeItems.forEach((item) => {
						item.classList.remove("hidden");
					});
				} else {
					storeItems.forEach((item) => {
						item.classList.add("hidden");

						if (target.dataset.category == item.dataset.location) {
							item.classList.remove("hidden");
						}
					});
				}
			}
		});

		//ymaps.ready(init); // Дождёмся загрузки API и готовности DOM;

		function init() {
			myMap = new ymaps.Map("map", {
				// Создание экземпляра карты и его привязка к контейнеру с заданным id;
				center: [55.752933963675126, 37.52233749962665], // При инициализации карты обязательно нужно указать её центр и коэффициент масштабирования;
				zoom: 9,
				controls: [], // Скрываем элементы управления на карте;
			});

			let collection = new ymaps.GeoObjectCollection(null, {
				// Создаём коллекцию, в которую будемпомещать метки (что-то типа массива);
			});

			// из всей информации об аптеках вытаскиваем только координаты
			let collectionCoords = filterdPharmInfo.map((item) => {
				return [item.x, item.y];
			});

			for (let i = 0, l = collectionCoords.length; i < l; i++) {
				// C помощью цикла добавляем все метки в коллекцию;
				// находим по коррдинатам полную информацию об аптеке для вставки в балун
				const ourPoint = filterdPharmInfo.filter((item) => {
					return item.x === collectionCoords[i][0];
				});

				collection.add(
					new ymaps.Placemark(collectionCoords[i], {
						balloonContent: `
                            <div class="balloon">
                                <span class="balloon__time">
                                    ${ourPoint[0]["openTime"]} ${ourPoint[0].openDaysShort}
                                </span>

                                <a class="balloon__tel" href="${ourPoint[0]["phoneForLink"]}">
                                    ${ourPoint[0]["phone"]}
                                </a>

                                <a class="balloon__email" href="mailto:apteka@apeteka.ru">
                                    ${ourPoint[0]["mail"]}
                                </a>
                            </div>
                    `,
					})
				);
				collection.get(i).properties.set("iconContent", `${i + 1}`); // Чтоб в дальнейшем к ней можно было как-то обратиться к нужной метке, добавляем каждой порядковый номер, записываем его в свойство 'iconContent';
			}

			myMap.geoObjects.add(collection); // Добавляем коллекцию с метками на карту;

			collection.options.set("iconLayout", "default#image"); // Необходимо указать данный тип макета;
			collection.options.set("iconImageHref", "/local/templates/main/images/mark.svg"); // Своё изображение иконки метки;
			collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
			collection.options.set("iconImageSize", [28, 37]); // Размеры метки;
			collection.options.set("hideIcon", false); // Размеры метки;

			const storeList = document.querySelector(".store__list"); // Список адресов

			storeList.addEventListener("click", (e) => {
				if (e.target && e.target.tagName == "LI") {
					// Отслеживаем клик на блоках с адресами;
					for (let i = 0, l = collection.getLength(); i < l; i++) {
						// Проходимся циклом по коллекции меток
						if (
							e.target.dataset.number ==
							collection.get(i).properties.get("iconContent")
						) {
							// Проверяем совпадение  значение атриба 'data-number' и порядкового номера перебираемых меток;
							myMap.setZoom(16);
							myMap.setCenter(collection.get(i).geometry.getCoordinates());
						}
					}
				}
			});
		}
	}
});

function sendSMSCode() {
	startTimerLogin();
}

function startTimerLogin() {

	const popupSmsCode = document.querySelector(".js_popup_log_in_sms_code");
	let timeText = popupSmsCode.querySelector(".js_code_time_text");
	let timeSpan = popupSmsCode.querySelector(".js_code_time_span");
	let newCodeBtn = popupSmsCode.querySelector(".js_new_code");

	let codeInputs = popupSmsCode.querySelectorAll(".js_code_input");
// таймер на повторение кода
	let counterStart = 44;
	let timeCounter = counterStart;

	timeSpan.innerHTML = `00:${timeCounter}`;

	var timerInterval = setInterval(decreaseTimeCounter, 1000);

	function decreaseTimeCounter() {
		if (timeCounter.toString().length === 2) {
			timeSpan.innerHTML = `00:${timeCounter}`;
		} else {
			timeSpan.innerHTML = `00:0${timeCounter}`;
		}

		timeCounter--;

		if (timeCounter === 0) {
			clearInterval(timerInterval);

			timeText.classList.add("hide");
			newCodeBtn.classList.add("active");
		}
	}
}

function popUpAuth() {
	$('.js_popup_log_in').toggleClass('active');
}

function addToCart(ob, q, price) {
	if (!ob && !$(ob).data("action"))
		return;

	var href = $(ob).data("action");
	var button = $(ob);
	var count = button.siblings('.inputWrap');
	if (href) {
		$(ob).addClass('btn--tr');
		$(ob).find('span').text('В корзине');
		button.addClass('load');
		var tempHtml = button.html();
		$.get(href + "&quantity=" + q + "&ajax_basket=Y",
			function (data) {
				button.removeClass('load');
				if (button.hasClass('watchedBtn')) {
					button.addClass('issue');
				} else {
					count.show();
				}
				BX.onCustomEvent('OnBasketChange');
			}
		);
	}
	return false;
}

function disableAddToCart(id) {
	var button = $('#' + id);
	if (!button)
		return;
	var count = button.siblings('.inputWrap');
	count.show();
	button.hide();
}