window.addEventListener('DOMContentLoaded', () => {

    let swiper1 = new Swiper(".intro__swiper", {
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        slidesPerView: 1,
        spaceBetween: 10,
        observer: true,
        observeParents: true,
        observeSlideChildren: true,
    });


    // ф-ция для похожих слайдеров
    function carousel1(sliderWrap) {

        if (document.querySelector(sliderWrap)) {

            const slidersWraps = document.querySelectorAll(sliderWrap);

            slidersWraps.forEach(wrap => {

                const slider = wrap.querySelector('.swiper');
                const prev = wrap.querySelector('.swiper-button-prev');
                const next = wrap.querySelector('.swiper-button-next');
                const scrollbar = wrap.querySelector('.swiper-scrollbar');

                let swiper2 = new Swiper(slider, {
                    scrollbar: {
                        el: scrollbar,
                        draggable: true,
                    },
                    navigation: {
                        nextEl: next,
                        prevEl: prev,
                    },
                    slidesPerView: 1.5,
                    spaceBetween: 23,
                    observer: true,
                    observeParents: true,
                    observeSlideChildren: true,
                    breakpoints: {
                        480: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        640: {
                            slidesPerView: 3,
                            spaceBetween: 23
                        },
                        920: {
                            slidesPerView: 4,
                            spaceBetween: 23
                        },
                        1200: {
                            slidesPerView: 4,
                            spaceBetween: 50
                        }
                    }
                });

            });

        }

    }

    // слайдеры в табах (пример: секции 'поппулярные товары', 'новинки' на главной);

    carousel1('.popular__item');

    // типовые часто встречающиеся на сайте слайдеры (пример: секция 'недавно просмотренные' на главной);

    carousel1('.slider__inner');



    // слайдер 'полезные статьи' на главной странице

    let swiper3 = new Swiper(".articles__swiper", {
        slidesPerView: 3,
        spaceBetween: 19,
        observer: true,
        observeParents: true,
        observeSlideChildren: true,
        breakpoints: {
            480: {
                slidesPerView: 1.65,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 2.5,
                spaceBetween: 10,
            },
            780: {
                slidesPerView: 3,
                spaceBetween: 10,
            },
            1200: {
                slidesPerView: 3,
                spaceBetween: 25,
            }
        }
    });


    // слайдер 'категории' на странице каталог / категория

    let swiper4 = new Swiper(".js_category_slider_swiper", {
        slidesPerView: 3,
        spaceBetween: 19,
        observer: true,
        observeParents: true,
        observeSlideChildren: true,
        navigation: {
            nextEl: '.category_slider_button_next',
            prevEl: '.category_slider_button_prev',
        },

        breakpoints: {
            310: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            550: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            // 780: {
            //     slidesPerView: 3,
            //     spaceBetween: 10,
            // },
            900: {
                slidesPerView: 3,
                spaceBetween: 25,
            }
        }
    });

    // ф-ция для похожих слайдеров
    function carousel5(sliderWrap) {

        if (document.querySelector(sliderWrap)) {

            const slidersWraps = document.querySelectorAll(sliderWrap);

            slidersWraps.forEach(wrap => {

                const slider = wrap.querySelector('.swiper');
                const prev = wrap.querySelector('.swiper-button-prev');
                const next = wrap.querySelector('.swiper-button-next');
                const scrollbar = wrap.querySelector('.swiper-scrollbar');

                let swiper5 = new Swiper(slider, {
                    scrollbar: {
                        el: scrollbar,
                        draggable: true,
                    },
                    navigation: {
                        nextEl: next,
                        prevEl: prev,
                    },
                    slidesPerView: 2,
                    spaceBetween: 20,
                    observer: true,
                    observeParents: true,
                    observeSlideChildren: true,
                    breakpoints: {
                        480: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        640: {
                            slidesPerView: 3,
                            spaceBetween: 23
                        },
                        920: {
                            slidesPerView: 4,
                            spaceBetween: 23
                        },
                        1200: {
                            slidesPerView: 4,
                            spaceBetween: 50
                        }
                    }
                });

            });

        }

    }

    //слайдер Недавно просмотренные, но на 320 помежается 2 слайда
    carousel5('.js_slider_popular_small');

    //слайдер Популярные товары (с горизонтальными карточками) на странице Подкатегория каталога
    let swiper6 = new Swiper(".js_popular_row_product_slider_swiper", {
        slidesPerView: 2,
        spaceBetween: 20,
        loop: true,
        observer: true,
        observeParents: true,
        observeSlideChildren: true,
        navigation: {
            nextEl: '.js_popular_row_product_button_next',
            prevEl: '.js_popular_row_product_button_prev',
        },
    });


    // двойной слайдер на странице Карточка товара
    // нижний
    let swiper8 = new Swiper(".js_product_small_swiper", {
        slidesPerView: 2,
        spaceBetween: 7,
        loop: true,
        observer: true,
        observeParents: true,
        observeSlideChildren: true,
        navigation: {
            nextEl: '.js_product_small_swiper_button_next',
            prevEl: '.js_product_small_swiper_button_prev',
        },
    });

    // верхний
    let swiper7 = new Swiper(".js_product_main_swiper", {
        slidesPerView: 1,
        loop: true,
        observer: true,
        observeParents: true,
        observeSlideChildren: true,
        thumbs: {
            swiper: swiper8,
        }
    });


    // ф-ция для похожих слайдеров
    function carousel7(sliderWrap) {

        if (document.querySelector(sliderWrap)) {

            const slidersWraps = document.querySelectorAll(sliderWrap);

            slidersWraps.forEach(wrap => {

                const slider = wrap.querySelector('.swiper');
                const prev = wrap.querySelector('.swiper-button-prev');
                const next = wrap.querySelector('.swiper-button-next');
                const scrollbar = wrap.querySelector('.swiper-scrollbar');

                let swiper5 = new Swiper(slider, {
                    scrollbar: {
                        el: scrollbar,
                        draggable: true,
                    },
                    navigation: {
                        nextEl: next,
                        prevEl: prev,
                    },
                    slidesPerView: 2,
                    spaceBetween: 20,
                    observer: true,
                    observeParents: true,
                    observeSlideChildren: true,
                    breakpoints: {
                        480: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        640: {
                            slidesPerView: 3,
                            spaceBetween: 23
                        },
                        // 920: {
                        //     slidesPerView: 4,
                        //     spaceBetween: 23
                        // },
                        // 1200: {
                        //     slidesPerView: 4,
                        //     spaceBetween: 50
                        // }
                    }
                });

            });

        }

    }

    //слайдер Недавно просмотренные, но на 320 помежается 2 слайда
    carousel7('.js_slider_popular_small_popup');

    // ф-ция для похожих слайдеров
    const swiper18 = new Swiper('.js_popup_product_slider', {
        // Optional parameters
        slidesPerView: 1,
        loop: true,
      
        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
        },
      
        // Navigation arrows
        navigation: {
          nextEl: '.js_popup_product_slider_prev',
          prevEl: '.js_popup_product_slider_next',
        },
      

      });


    // слайдер для 
    const swiper9 = new Swiper('.js_delivery_swiper', {
        slidesPerView: 4,
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        spaceBetween: 20,
        observer: true,
        observeParents: true,
        observeSlideChildren: true,
        breakpoints: {
            300: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            480: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            760: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            920: {
                slidesPerView: 4,
                spaceBetween: 20
            },
        }
        });
});