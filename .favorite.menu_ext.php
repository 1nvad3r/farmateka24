<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$aMenuLinksExt = array();

if(CModule::IncludeModule('iblock'))
{
    $arFilter = array(
        "TYPE" => "catalog",
        "SITE_ID" => SITE_ID,
        "ID" => 5
    );

    $dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
    $dbIBlock = new CIBlockResult($dbIBlock);

    if ($arIBlock = $dbIBlock->GetNext())
    {
        if(defined("BX_COMP_MANAGED_CACHE"))
            $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_".$arIBlock["ID"]);

        if($arIBlock["ACTIVE"] == "Y")
        {
            $arSubItems = [];
            $itItems = CIBlockSection::GetList(
                ['SORT' => 'ASC'],
                ['IBLOCK_ID' => $arFilter["ID"], 'GLOBAL_ACTIVE' => 'Y'],
                true,
                ['UF_COLOR', 'NAME', 'SECTION_PAGE_URL', 'UF_LINK']
            );
            while($arSub = $itItems->GetNext()) {
                $color = "";
                if($arSub['UF_COLOR']){
                    $rsEnum = CUserFieldEnum::GetList(array(), array("ID" =>$arSub['UF_COLOR']));
                    if($arEnum = $rsEnum->GetNext())
                    {
                        $color = $arEnum["VALUE"];
                    }
                }
				if($arSub['UF_LINK']){
					$arSub['SECTION_PAGE_URL'] = $arSub['UF_LINK'];
				}

                $arSubItems[] = [
                    $arSub['NAME'],
                    $arSub['SECTION_PAGE_URL'],
                    [$arSub['SECTION_PAGE_URL']],
                    [
                        'FROM_IBLOCK' => $arFilter["ID"],
                        'IS_PARENT' => false,
                        'DEPTH_LEVEL' => 1,
                        'COLOR' => $color
                    ],
                ];
            }
        }
    }

    if(defined("BX_COMP_MANAGED_CACHE"))
        $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
}

$aMenuLinks = array_merge($aMenuLinks, $arSubItems);